package utilities;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.ri.common.PrintHelper;
import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.views.DocumentViewController;
import org.icepdf.ri.common.views.DocumentViewControllerImpl;

import javax.print.DocFlavor;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.ColorSupported;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrintQuality;
import javax.print.attribute.standard.PrinterName;
import java.io.IOException;

/**
 * Created by jasvedu on 3/17/14.
 */
public class PrintUtility {
    AttributeSet aset;
    PrintService[] getPrintServices(){
        return PrintServiceLookup.lookupPrintServices(null, null);
    }

    // find printer service by name
    PrintService[] getServicesByName(String name){
    aset = new HashAttributeSet();
    aset.add(new PrinterName("Microsoft XPS Document Writer", null));
    return  PrintServiceLookup.lookupPrintServices(null, aset);
    }

    // find services that support a set of print job capabilities (e.g. color)
    PrintService[] getServicesByCabability(String capability){
    aset = new HashAttributeSet();
    aset.add(ColorSupported.SUPPORTED);
    return PrintServiceLookup.lookupPrintServices(null, aset);
    }

    public PrintService getDefaultPrintService(){
       PrintService printService=PrintServiceLookup.lookupDefaultPrintService();
       System.out.println(printService.getName());
       return  printService;
    }


    public void printDocWithPrintService(String filePathName,String printerName){
        Document pdf = new Document();
        try {
            pdf.setFile(filePathName);
        } catch (PDFException e) {
            e.printStackTrace();
        } catch (PDFSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SwingController sc = new SwingController();
        DocumentViewController vc = new DocumentViewControllerImpl(sc);
        vc.setDocument(pdf);
// create a new print helper with a specified paper size and print
// quality
        PrintHelper printHelper = new PrintHelper(vc, pdf.getPageTree(),MediaSizeName.NA_LETTER, PrintQuality.DRAFT);
// try and print pages 1 - 10, 1 copy, scale to fit paper.

        printHelper.setupPrintService(findServiceByName(printerName), 0, 9, 1, true);
// print the document
        try {
            printHelper.print();
        } catch (PrintException e) {
            e.printStackTrace();
        }
    }

    
    public void printDocWithDefaultPrintService(String filePathName){
        Document pdf = new Document();
        try {
            pdf.setFile(filePathName);
        } catch (PDFException e) {
            e.printStackTrace();
        } catch (PDFSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SwingController sc = new SwingController();
        DocumentViewController vc = new DocumentViewControllerImpl(sc);
        vc.setDocument(pdf);
// create a new print helper with a specified paper size and print
// quality
        PrintHelper printHelper = new PrintHelper(vc, pdf.getPageTree(),MediaSizeName.NA_LETTER, PrintQuality.DRAFT);
// try and print pages 1 - 10, 1 copy, scale to fit paper.

        printHelper.setupPrintService(this.getDefaultPrintService(), 0, 9, 1, true);
// print the document
        try {
            printHelper.print();
        } catch (PrintException e) {
            e.printStackTrace();
        }
    }

    private  javax.print.PrintService findServiceByName(String printerName){
        // find printer service by name
        PrintService found=null;
        AttributeSet aset = new HashAttributeSet();
        aset.add(new PrinterName(printerName,null));
        PrintService[] services = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.JPEG, aset);
        found=services[0];
        return found;
    }
}
