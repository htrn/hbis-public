eventCreateWarStart = { warName, stagingDir ->
    def buildNumber = System.getProperty("buildNumber", "CUSTOM")
    println("Setting BUILD_NUMBER to " + buildNumber)
    ant.propertyfile(
            file:"${stagingDir}/WEB-INF/classes/application.properties") {
        entry(key:"build.number", value:buildNumber)
    }
}
