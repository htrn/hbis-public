<%@ page import="hbis.SurgicalHistory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'surgicalHistory.label', default: 'Surgical History')}" />
		<g:set var="parentEntityName" value="${message(code: 'chartReview.label', default: 'Chart Review')}" />
		<g:set var="parentOfParentEntityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<g:set var="parentOfParentOfParentEntityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#patient-button').click(function () {
    			$('#show-patient').toggle("slow");
			});
			
			$('#chartReview-button').click(function () {
    			$('#show-chartReview').toggle("slow");
			});
			
			$('#procedure-button').click(function () {
			  	$('#show-procedure').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#create-surgicalHistory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<div class="buttons">
			<button id="patient-button">Toggle Patient</button>
			<button id="procedure-button">Toggle Procedure</button>
			<button id="chartReview-button">Toggle Chart Review</button>
		</div>
		
		<div id="show-patient" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/patient" />
		</div>
		
		<div id="show-procedure" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/procedure" />
		</div>
		
		<div id="show-chartReview" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/chartReview" />
		</div>
		
		<div id="show-surgicalHistories" style="clear:both">
			<fieldset class="box"><legend>Surgical Histories for above Chart Review</legend>
				<g:render template="surgicalHistoryTable" />
			</fieldset>
		</div>
		
		<div id="create-surgicalHistory" class="content scaffold-create" role="main" style="clear:both">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${surgicalHistoryInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${surgicalHistoryInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons" style="clear:both">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
					<g:submitButton name="saveAndCreateAnotherSurgicalHistory" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Create Another Surgical History')}" />
					<g:link controller="chartReview" action="show" id="${surgicalHistoryInstance.chartReview.id}">Cancel Current Entry and Return to View Chart Review</g:link>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
