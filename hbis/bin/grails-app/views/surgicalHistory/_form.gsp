<%@ page import="hbis.SurgicalHistory" %>

<div class="fieldcontain ${hasErrors(bean: surgicalHistoryInstance, field: 'forBiosample', 'error')} required">
	<label for="forBiosample">
		<g:message code="surgicalHistory.forBiosample.label" default="For Biosample" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="forBiosample" name="forBiosample.id" from="${hbis.YesNo.list()}" optionKey="id" required="" value="${surgicalHistoryInstance?.forBiosample?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: surgicalHistoryInstance, field: 'procedure', 'error')} required">
	<label for="procedure">
		<g:message code="surgicalHistory.procedure.label" default="Procedure" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="procedure" name="procedure.id" from="${hbis.ProcedureName.list()}" optionKey="id" required="" value="${surgicalHistoryInstance?.procedure?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: surgicalHistoryInstance, field: 'anesthetics', 'error')} ">
	<label for="anesthetics">
		<g:message code="surgicalHistory.anesthetics.label" default="Anesthetics" />
	</label>
	<g:textField name="anesthetics" value="${surgicalHistoryInstance?.anesthetics}"/>
</div>

<g:hiddenField name="chartReview.id" value="${surgicalHistoryInstance?.chartReview.id}"/>

