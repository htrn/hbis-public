
<%@ page import="hbis.SurgicalHistory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'surgicalHistory.label', default: 'SurgicalHistory')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-surgicalHistory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-surgicalHistory" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="anesthetics" title="${message(code: 'surgicalHistory.anesthetics.label', default: 'Anesthetics')}" />
					
						<th><g:message code="surgicalHistory.chartReview.label" default="Chart Review" /></th>
					
						<th><g:message code="surgicalHistory.forBiosample.label" default="For Biosample" /></th>
					
						<th><g:message code="surgicalHistory.procedure.label" default="Procedure" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${surgicalHistoryInstanceList}" status="i" var="surgicalHistoryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${surgicalHistoryInstance.id}">${fieldValue(bean: surgicalHistoryInstance, field: "anesthetics")}</g:link></td>
					
						<td>${fieldValue(bean: surgicalHistoryInstance, field: "chartReview")}</td>
					
						<td>${fieldValue(bean: surgicalHistoryInstance, field: "forBiosample")}</td>
					
						<td>${fieldValue(bean: surgicalHistoryInstance, field: "procedure")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${surgicalHistoryInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
