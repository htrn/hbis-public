
<%@ page import="hbis.ChartReview" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'chartReview.label', default: 'ChartReview')}" />
		<g:set var="parentEntityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<g:set var="parentOfParentEntityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#patient-button').click(function () {
    			$('#show-patient').toggle("slow");
			});
			
			$('#procedure-button').click(function () {
			  	$('#show-procedure').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#show-chartReview" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<div class="buttons">
			<button id="patient-button">Toggle Patient</button>
			<button id="procedure-button">Toggle Procedure</button>
		</div>
		
		<div id="show-patient" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/patient" />
		</div>
		
		<div id="show-procedure" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/procedure" />
		</div>
		
		<div id="show-chartReview" class="content scaffold-show" role="main" style="clear:both">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:render template="/showDomainTemplates/chartReview" />
			<g:form>
				<fieldset class="buttons" style="clear:both">
					<g:hiddenField name="id" value="${chartReviewInstance?.id}" />
					<g:link class="edit" action="edit" id="${chartReviewInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					<g:link class="edit" controller="medicalHistory" action="create" params='["chartReview.id": "${chartReviewInstance.id}"]'><g:message code="medicalHistory.button.create.label" default="Add Medical History" /></g:link>
					<g:link class="edit" controller="surgicalHistory" action="create" params='["chartReview.id": "${chartReviewInstance.id}"]'><g:message code="surgicalHistory.button.create.label" default="Add Surgical History" /></g:link>
					<g:link class="edit" controller="medication" action="create" params='["chartReview.id": "${chartReviewInstance.id}"]'><g:message code="medication.button.create.label" default="Add Medication" /></g:link>
					<g:link class="edit" controller="familyHistory" action="create" params='["chartReview.id": "${chartReviewInstance.id}"]'><g:message code="familyHistory.button.create.label" default="Add Family History" /></g:link>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
