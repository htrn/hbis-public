<%@ page import="hbis.ChartReview" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'chartReview.label', default: 'ChartReview')}" />
		<g:set var="parentEntityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<g:set var="parentOfParentEntityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#patient-button').click(function () {
    			$('#show-patient').toggle("slow");
			});
			
			$('#procedure-button').click(function () {
			  	$('#show-procedure').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#create-chartReview" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<div class="buttons">
			<button id="patient-button">Toggle Patient</button>
			<button id="procedure-button">Toggle Procedure</button>
		</div>
		
		<div id="show-patient" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentEntityName]" /></h1>
				<g:render template="/showDomainTemplates/patient" />
		</div>
		
		<div id="show-procedure" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
				<g:render template="/showDomainTemplates/procedure" />
		</div>
		
		<div id="create-chartReview" class="content scaffold-create" role="main" style="clear:both">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${chartReviewInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${chartReviewInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
					<g:submitButton name="saveAndCreateMedicalHistory" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Create Medical History')}" />
					<g:submitButton name="saveAndCreateSurgicalHistory" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Create Surgical History')}" />
					<g:submitButton name="saveAndCreateMedication" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Create Medication')}" />
					<g:submitButton name="saveAndCreateFamilyHistory" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Create Family History')}" />
					<!--<g:submitButton name="saveAndCreateSpecimen" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Create Specimen')}" />-->
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
