<%@ page import="hbis.ChartReview" %>

<div class="leftFormColumn">

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'yearOfBirth', 'error')}">
	<label for="yearOfBirth">
		<g:message code="chartReview.yearOfBirth.label" default="Year Of Birth" />
	</label>
	<g:field name="yearOfBirth" type="text" value="${chartReviewInstance.yearOfBirth}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'yearOfDeath', 'error')} ">
	<label for="yearOfDeath">
		<g:message code="chartReview.yearOfDeath.label" default="Year Of Death" />
	</label>
	<g:field name="yearOfDeath" type="text" value="${chartReviewInstance.yearOfDeath}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'height', 'error')} ">
	<label for="height">
		<g:message code="chartReview.height.label" default="Height" />
	</label>
	<g:textField name="height" value="${chartReviewInstance?.height}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'weight', 'error')} ">
	<label for="weight">
		<g:message code="chartReview.weight.label" default="Weight" />
	</label>
	<g:field name="weight" type="text" value="${fieldValue(bean: chartReviewInstance, field: 'weight')}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'menstrualHistory', 'error')}
		
		required ">
	<label for="menstrualHistory">
		<span id="obstetrics-label" class="property-label">Obstetrics (if female)</span>
		<g:message code="chartReview.menstrualHistory.label" default="Menstrual History" />
	</label>
	<g:textField name="menstrualHistory" value="${chartReviewInstance?.menstrualHistory}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'lastMenstrualPeriod', 'error')} ">
	<label for="lastMenstrualPeriod">
		<g:message code="chartReview.lastMenstrualPeriod.label" default="Last Menstrual Period" />
	</label>
	<g:datePicker default="none" name="lastMenstrualPeriod" precision="day"  value="${chartReviewInstance?.lastMenstrualPeriod}" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'currentlyPregnant', 'error')} ">
	<label for="currentlyPregnant">
		<g:message code="chartReview.currentlyPregnant.label" default="Currently Pregnant" />
	</label>
	<g:textField name="currentlyPregnant" value="${chartReviewInstance?.currentlyPregnant}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'numberOfPregnancies', 'error')} ">
	<label for="numberOfPregnancies">
		<g:message code="chartReview.numberOfPregnancies.label" default="Number Of Pregnancies" />
	</label>
	<g:field name="numberOfPregnancies" type="text" value="${chartReviewInstance.numberOfPregnancies}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'numberOfLiveBirths', 'error')} ">
	<label for="numberOfLiveBirths">
		<g:message code="chartReview.numberOfLiveBirths.label" default="Number Of Live Births" />
	</label>
	<g:field name="numberOfLiveBirths" type="text" value="${chartReviewInstance.numberOfLiveBirths}" />
</div>



<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'additionalNotes', 'error')} ">
	<label for="additionalNotes">
		<g:message code="chartReview.additionalNotes.label" default="Additional Notes" />
		
	</label>
	<g:textField name="additionalNotes" value="${chartReviewInstance?.additionalNotes}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'requestVerifiedBy', 'error')}
		${hasErrors(bean: chartReviewInstance, field: 'dateVerified', 'error')} ">
	<label for="requestVerifiedBy">
		<g:message code="chartReview.requestVerifiedBy.label" default="Request Verified By" />
	</label>
	<g:textField name="requestVerifiedBy" value="${chartReviewInstance?.requestVerifiedBy}"/>
	
	<label for="dateVerified">
		<g:message code="chartReview.dateVerified.label" default="Date Verified" />
	</label>
	<g:datePicker default="none" name="dateVerified" precision="day"  value="${chartReviewInstance?.dateVerified}" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'requestCompletedBy', 'error')}
		${hasErrors(bean: chartReviewInstance, field: 'dateCompleted', 'error')} ">
	<label for="requestCompletedBy">
		<g:message code="chartReview.requestCompletedBy.label" default="Request Completed By" />
	</label>
	<g:textField name="requestCompletedBy" value="${chartReviewInstance?.requestCompletedBy}"/>
	
	<label for="dateCompleted">
		<g:message code="chartReview.dateCompleted.label" default="Date Completed" />
	</label>
	<g:datePicker default="none" name="dateCompleted" precision="day"  value="${chartReviewInstance?.dateCompleted}" noSelection="['null':'']" />
</div>

</div> <!-- End Left Form Column -->

<div class="rightFormColumn">

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'specialDietStatus', 'error')} ">
	<label for="specialDietStatus">
		<g:message code="chartReview.specialDietStatus.label" default="Special Diet Status" />
	</label>
	<g:select id="specialDietStatus" name="specialDietStatus.id" from="${hbis.SpecialDiet.list()}" optionKey="id" value="${chartReviewInstance?.specialDietStatus?.id}" class="many-to-one" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'specialDietOtherSpecify', 'error')} ">
	<label for="specialDietOtherSpecify">
		<g:message code="chartReview.specialDietOtherSpecify.label" default="Special Diet Other Specify" />
	</label>
	<g:textField name="specialDietOtherSpecify" value="${chartReviewInstance?.specialDietOtherSpecify}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'specialDietNotes', 'error')} ">
	<label for="specialDietNotes">
		<g:message code="chartReview.specialDietNotes.label" default="Special Diet Notes" />
		
	</label>
	<g:textField name="specialDietNotes" value="${chartReviewInstance?.specialDietNotes}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'smokingHistoryStatus', 'error')} ">
	<label for="smokingHistoryStatus">
		<g:message code="chartReview.smokingHistoryStatus.label" default="Smoking History Status" />
	</label>
	<g:select id="smokingHistoryStatus" name="smokingHistoryStatus.id" from="${hbis.SubstanceUseStatus.list()}" optionKey="id" value="${chartReviewInstance?.smokingHistoryStatus?.id}" class="many-to-one" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'smokingHistoryWhenQuit', 'error')} ">
	<label for="smokingHistoryWhenQuit">
		<g:message code="chartReview.smokingHistoryWhenQuit.label" default="Smoking History When Quit (Year)" />
	</label>
	<g:textField name="smokingHistoryWhenQuit" value="${chartReviewInstance?.smokingHistoryWhenQuit}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'smokingHistoryAmountPacksPerDay', 'error')} ">
	<label for="smokingHistoryAmountPacksPerDay">
		<g:message code="chartReview.smokingHistoryAmountPacksPerDay.label" default="Smoking History Amount Packs Per Day (fractions allowed)" />
	</label>
	<g:field name="smokingHistoryAmountPacksPerDay" type="text" value="${fieldValue(bean: chartReviewInstance, field: 'smokingHistoryAmountPacksPerDay')}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'smokingHistoryDuration', 'error')} ">
	<label for="smokingHistoryDuration">
		<g:message code="chartReview.smokingHistoryDuration.label" default="Smoking History Duration (Years)" />
	</label>
	<g:field name="smokingHistoryDuration" type="text" value="${chartReviewInstance.smokingHistoryDuration}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'alcoholConsumptionStatus', 'error')} ">
	<label for="alcoholConsumptionStatus">
		<g:message code="chartReview.alcoholConsumptionStatus.label" default="Alcohol Consumption Status" />
	</label>
	<g:select id="alcoholConsumptionStatus" name="alcoholConsumptionStatus.id" from="${hbis.SubstanceUseStatus.list()}" optionKey="id" value="${chartReviewInstance?.alcoholConsumptionStatus?.id}" class="many-to-one" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'alcoholConsumptionWhenQuit', 'error')} ">
	<label for="alcoholConsumptionWhenQuit">
		<g:message code="chartReview.alcoholConsumptionWhenQuit.label" default="Alcohol Consumption When Quit (Year)" />
	</label>
	<g:textField name="alcoholConsumptionWhenQuit" value="${chartReviewInstance?.alcoholConsumptionWhenQuit}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'alcoholConsumptionAmountPerWeek', 'error')} ">
	<label for="alcoholConsumptionAmountPerWeek">
		<g:message code="chartReview.alcoholConsumptionAmountPerWeek.label" default="Alcohol Consumption Amount Per Week" />
	</label>
	<g:field name="alcoholConsumptionAmountPerWeek" type="text" value="${chartReviewInstance.alcoholConsumptionAmountPerWeek}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'alcoholConsumptionDuration', 'error')} ">
	<label for="alcoholConsumptionDuration">
		<g:message code="chartReview.alcoholConsumptionDuration.label" default="Alcohol Consumption Duration (Years)" />
	</label>
	<g:field name="alcoholConsumptionDuration" type="text" value="${chartReviewInstance.alcoholConsumptionDuration}" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'recreationalDrugsStatus', 'error')} ">
	<label for="recreationalDrugsStatus">
		<g:message code="chartReview.recreationalDrugsStatus.label" default="Recreational Drugs Status" />
	</label>
	<g:select id="recreationalDrugsStatus" name="recreationalDrugsStatus.id" from="${hbis.SubstanceUseStatus.list()}" optionKey="id" value="${chartReviewInstance?.recreationalDrugsStatus?.id}" class="many-to-one" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: chartReviewInstance, field: 'recreationalDrugsNotes', 'error')} ">
	<label for="recreationalDrugsNotes">
		<g:message code="chartReview.recreationalDrugsNotes.label" default="Recreational Drugs Notes" />
	</label>
	<g:textField name="recreationalDrugsNotes" value="${chartReviewInstance?.recreationalDrugsNotes}" />
</div>

 <g:hiddenField name="procedure.id" value="${chartReviewInstance?.procedure?.id}"/>
 </div> <!-- End Right Form Column -->