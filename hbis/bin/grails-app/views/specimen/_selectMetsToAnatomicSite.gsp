<g:select 	id="metsToAnatomicSite" tabindex="1" 
			name="metsToAnatomicSite.id" 
			from="${metsToAnatomicSiteList?: hbis.MetsToAnatomicSite.list()}"
			optionKey="id" 
			optionValue="description" 
			value="${specimenInstance?.metsToAnatomicSite?.id}" 
			class="many-to-one" 
			noSelection="['null': '']"/>