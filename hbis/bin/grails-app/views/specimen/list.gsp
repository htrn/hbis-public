
<%@ page import="hbis.Specimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'specimen.label', default: 'Specimen')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-specimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-specimen" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="specimenChtnId" title="${message(code: 'specimen.specimenChtnId.label', default: 'Specimen Chtn ID')}" />
					
						<th><g:message code="specimen.procedure.label" default="Procedure" /></th>
					
						<g:sortableColumn property="alternateSpecimenID" title="${message(code: 'specimen.alternateSpecimenID.label', default: 'Alternate Specimen ID')}" />
					
						<g:sortableColumn property="procurementDate" title="${message(code: 'specimen.procurementDate.label', default: 'Procurement Date')}" />
					
						<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
					
						<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${specimenInstanceList}" status="i" var="specimenInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${specimenInstance.id}">${fieldValue(bean: specimenInstance, field: "specimenChtnId")}</g:link></td>
					
						<td>${fieldValue(bean: specimenInstance, field: "procedure")}</td>
					
						<td>${fieldValue(bean: specimenInstance, field: "alternateSpecimenID")}</td>
					
						<td><g:formatDate date="${specimenInstance.procurementDate}" /></td>
					
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryPrimaryAnatomicSite")}</td>
					
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryTissueType")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${specimenInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
