<g:select 	tabindex="1" 
			id="preliminaryPrimaryAnatomicSite" 
			name="preliminaryPrimaryAnatomicSite.id" 
			from="${hbis.AnatomicSite.list()}" 
			optionKey="id" 
			required="" 
			value="${preliminaryPrimaryAnatomicSiteId ?: specimenInstance?.preliminaryPrimaryAnatomicSite?.id}" 
			class="many-to-one" 
			noSelection="['null': '']"/>