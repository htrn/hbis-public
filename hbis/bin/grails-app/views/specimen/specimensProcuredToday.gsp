<%@ page import="hbis.Specimen" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'specimen.label', default: 'Specimen')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	
	<body>
		<div id="list-specimen" class="content scaffold-list" role="main">
			<h1>Specimens Procured Today</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
						<th><g:message code="patient.mrn.label" default="Pat. MRN" /></th>
						
						<th><g:message code="patient.lastName.label" default="Pat. Last Name" /></th>
					
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen Chtn ID" />
						
						<th><g:message code="specimen.procedure.procedureName.label" default="Surgery Name" /></th>
					
						<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
					
						<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
						
						<th></th>
						
						<th></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${specimenInstanceList}" status="i" var="specimenInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>${fieldValue(bean: specimenInstance, field: "procedure.patient.mrn")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "procedure.patient.lastName")}</td>
					
						<td><g:link action="show" id="${specimenInstance.id}">${fieldValue(bean: specimenInstance, field: "specimenChtnId")}</g:link></td>
						
						<td>${fieldValue(bean: specimenInstance, field: "procedure.procedureName")}</td>
					
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryPrimaryAnatomicSite")}</td>
					
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryTissueType")}</td>
						
						<td><g:link controller="specimen" action="createCopyDetails" id="${specimenInstance.id}">Copy Specimen Info Into New Specimen</g:link></td>
						
						<td><g:link controller="subspecimen" action="create" params='["specimen.id": "${specimenInstance.id}"]'>Create Subspecimen</g:link></td>
					
						<td><g:form method="post" action="delete" id="${specimenInstance?.id}">
								<g:submitButton name="delete" class="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'specimen.button.delete.confirm.message', default: 'Are you sure you want to delete this specimen and all of its subspecimens?')}');" />
							</g:form>
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>
		
		<g:form controller="patient" action="create">
		<div>
			<fieldset class="box"> <legend><strong>Search for Patient</strong></legend>
				MRN <g:remoteField name="mrn" controller="patient" action="performSearchMRN" paramName="mrn" update="divSearchMRN" value="" tabindex="1" />
			</fieldset>
		</div>
		
		<div id="divSearchMRN">
			<g:render template="/patient/searchMRNResult" var="patientInstance" collection="${patientList}" />
		</div>
		
		<div id="addNewPatient">
			<input type="submit" value="Add New Patient">
		</div>
		</g:form>
	</body>
</html>