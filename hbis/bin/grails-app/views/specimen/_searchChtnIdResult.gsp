
<ul class="property-list patient">
	<li class="fieldcontain">
		<g:if test="${specimenInstance?.specimenChtnId}">
			<fieldset class="box">
			<span id="specimenChtnId-label" class="property-label"><g:message code="specimen.specimenChtnId.label" default="Specimen Chtn ID " /></span>
			<span class="property-value" aria-labelledby="specimenChtnId-label"><g:fieldValue bean="${specimenInstance}" field="specimenChtnId"/>
			<g:link action="show" id="${specimenInstance.id}">
				View
			</g:link>
			&nbsp
			<g:link action="edit" id="${specimenInstance.id}">
				Edit
			</g:link>
			</span>
			<br/>
			<span id="subspecimens-label" class="property-label"><g:message code="specimen.subspecimen.label" default="Subspecimens: " /></span>
			<g:each in="${specimenInstance.subspecimens}" var="s">
				<span class="property-value" aria-labelledby="subspecimen-label">${s?.encodeAsHTML()}
				<g:link controller="subspecimen" action="show" id="${s.id}">
					View
				</g:link>
				&nbsp
				<g:link controller="subspecimen" action="edit" id="${s.id}">
					Edit
				</g:link>
				</span>
			</g:each>
			<br/>
			<span id="patient-label" class="property-label"><g:message code="specimen.patient.label" default="Patient: " />
				<g:link action="show" id="${specimenInstance.procedure.patient.id}"><g:fieldValue bean="${specimenInstance.procedure.patient}" field="mrn"/></g:link>:
					<g:fieldValue bean="${specimenInstance.procedure.patient}" field="firstName"/>&nbsp
					<g:fieldValue bean="${specimenInstance.procedure.patient}" field="lastName"/>,
					<g:fieldValue bean="${specimenInstance.procedure.patient}" field="gender"/>,
					<g:fieldValue bean="${specimenInstance.procedure.patient}" field="race"/>
			</span>
			<br/>
			<span id="procedure-label" class="property-label"><g:message code="specimen.procedure.label" default="Procedure: " />
				<g:link action="show" id="${specimenInstance.procedure.id}"><g:fieldValue bean="${specimenInstance.procedure}" field="procedureType"/></g:link>,
					<g:fieldValue bean="${specimenInstance.procedure}" field="procedureName"/>&nbsp,
					<g:formatDate format="MM/dd/yyyy" date="${specimenInstance.procedure.procedureDate}" />
			</span>
			<br/>
			</fieldset>
		</g:if>
		
	</li>
</ul>
