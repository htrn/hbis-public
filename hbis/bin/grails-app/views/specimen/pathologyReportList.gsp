
<%@ page import="hbis.Specimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'specimen.label', default: 'Specimen')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<g:javascript library="jquery" />
		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
          		$("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
	</head>
	<body>
		<div id="list-specimen" class="content scaffold-list" role="main">
			<h1>Pathology Report List</h1>
			<g:form action="pathologyReportList" >
				<fieldset class="form">
					<div>
						<label for="fromDate">
							<g:message code="default.procedureDate.fromDate" default="Procedure From Date" />
						</label>
						<g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
					</div>
					
					<div>
						<label for="toDate">
							<g:message code="default.procedureDate.toDate" default="Procedure To Date" />
						</label>
						<g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
					</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="apqi" class="save" value="${message(code: 'subspecimen.button.reassignQuery.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:if test="${specimenInstanceList}">
			<table>
				<thead>
					<tr>
						<th><g:message code="procedure.procedureDate.label" default="Procedure Date"/></th>
						
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID"/></th>
						
						<th><g:message code="patient.gender.label" default="Gender"/></th>
						
						<th><g:message code="procedure.patientAge.label" default="Age"/></th>
						
						<th><g:message code="procedure.procedureName.label" default="Procedure Name"/></th>
						
						<th><g:message code="patient.lastName.label" default="Last Name"/></th>
						
						<th><g:message code="patient.firstName.label" default="First Name"/></th>
						
						<th><g:message code="patient.mrn.label" default="MRN"/></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${specimenInstanceList}" status="i" var="specimenInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:formatDate date='${specimenInstance.procedure.procedureDate}' format="MM/dd/yyyy"/></td>
					
						<td>${fieldValue(bean: specimenInstance, field: "specimenChtnId")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "procedure.patient.gender")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "procedure.patientAge")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "procedure.procedureName")}</td>
					
						<td>${fieldValue(bean: specimenInstance, field: "procedure.patient.lastName")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "procedure.patient.firstName")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "procedure.patient.mrn")}</td>
					</tr>
				</g:each>
				</tbody>
			</table>
			</g:if>
		</div>
	</body>
</html>
