
<%@ page import="hbis.TissueRequest" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tissueRequest.label', default: 'TissueRequest')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-tissueRequest" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-tissueRequest" class="content scaffold-list" role="main">
			<h1>Search for Tissue Requests</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:form action="search" >
				<fieldset class="form">
				<div class="leftFormColumn">
				
					<div class="fieldcontain">
						<label for="name">
							<g:message code="tissueRequest.name.label" default="Name" />
						</label>
						<g:textField name="name" value="${name}"/>
					</div>
				
					<div class="fieldcontain">
						<label for="tissueRequestTissueQuestId">
							<g:message code="tissueRequest.tissueRequestTissueQuestId.label" default="Tissue Request Tissue Quest ID" />
						</label>
						<g:textField name="tissueRequestTissueQuestId" value="${tissueRequestTissueQuestId}"/>
					
					</div>
					<div class="fieldcontain">
						<label for="projectTissueQuestId">
							<g:message code="tissueRequest.projectTissueQuestId.label" default="Project Tissue Quest ID" />
						</label>
						<g:textField name="projectTissueQuestId" value="${projectTissueQuestId}"/>
					</div>
				
				</div> <!-- End left form column -->
				<div class="rightFormColumn">
				
					<div class="fieldcontain">
						<label for="contactMethod">
							<g:message code="tissueRequest.contactMethod.label" default="Contact Method" />
						</label>
						<g:select	id="contactMethod" 
							name="contactMethodId" 
							from="${hbis.TissueRequestContactMethod.list()}" 
							optionKey="id"
							value="${params.list('contactMethodId')*.toLong()}"
							class="many-to-one" 
							multiple="true"
						/>
					</div>
					
					<div class="fieldcontain">
						<label for="status">
							<g:message code="tissueRequest.status.label" default="Status" />
						</label>
						<g:select	id="status" 
							name="statusId" 
							from="${hbis.TissueRequestStatus.list()}" 
							optionKey="id"
							value="${params.list('statusId')*.toLong()}"
							class="many-to-one" 
							multiple="true"
						/>
					</div>
					
				</div> <!-- End right form column -->
				</fieldset>
				<fieldset class="buttons" style="clear:both">
						<g:hiddenField name="formSubmitted" value="true" />
						<g:submitButton name="search" class="save" value="${message(code: 'default.button.search.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="investigator" title="${message(code: 'tissueRequest.investigator.label', default: 'Investigator')}" params="${params}"/>
						
						<g:sortableColumn property="name" title="${message(code: 'tissueRequest.name.label', default: 'Name')}" params="${params}"/>
					
						<g:sortableColumn property="status" title="${message(code: 'tissueRequest.status.label', default: 'Status')}" params="${params}"/>
					
						<g:sortableColumn property="tissueRequestTissueQuestId" title="${message(code: 'tissueRequest.tissueRequestId.label', default: 'Tissue Request Tissue Quest ID')}" params="${params}"/>
						
						<g:sortableColumn property="projectTissueQuestId" title="${message(code: 'tissueRequest.tissueRequestId.label', default: 'Project Request Tissue Quest ID')}" params="${params}"/>
					
						<g:sortableColumn property="contactMethod" title="${message(code: 'tissueRequest.contactMethod.label', default: 'Contact Method')}" params="${params}"/>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${tissueRequestInstanceList}" status="i" var="tissueRequestInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link controller="investigator" action="show" id="${tissueRequestInstance.investigator.id}">${fieldValue(bean: tissueRequestInstance, field: "investigator")}</g:link></td>
					
						<td>${fieldValue(bean: tissueRequestInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: tissueRequestInstance, field: "status")}</td>
					
						<td><g:link action="show" id="${tissueRequestInstance.id}">${fieldValue(bean: tissueRequestInstance, field: "tissueRequestTissueQuestId")}</g:link></td>
						
						<td>${fieldValue(bean: tissueRequestInstance, field: "projectTissueQuestId")}</td>
					
						<td>${fieldValue(bean: tissueRequestInstance, field: "contactMethod")}</td>
						
						<td><g:link action="edit" id="${tissueRequestInstance.id}">Edit Tissue Request</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate action="search" total="${tissueRequestInstanceTotal}" params="${params}"/>
			</div>
		</div>
	</body>
</html>
