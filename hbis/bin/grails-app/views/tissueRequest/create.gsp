<%@ page import="hbis.TissueRequest" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tissueRequest.label', default: 'TissueRequest')}" />
		<g:set var="parentEntityName" value="${message(code: 'investigator.label', default: 'Investigator')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#investigator-button').click(function () {
    			$('#show-investigator').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#create-tissueRequest" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div class="buttons">
			<button id="investigator-button">Toggle Investigator</button>
		</div>
		
		<div id="show-investigator" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
				<g:render template="/showDomainTemplates/investigator" />
		</div>

		<div id="create-tissueRequest" class="content scaffold-create" role="main" style="clear:both">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${tissueRequestInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${tissueRequestInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<!--<g:hiddenField name="specimenDbId" value="${specimenDbId}"/>-->
					<g:hiddenField name="subspecimenParamMap" value="${subspecimenParamMap}"/>
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
