<%@ page import="hbis.TissueRequest" %>



<div class="fieldcontain ${hasErrors(bean: tissueRequestInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="tissueRequest.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${tissueRequestInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tissueRequestInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="tissueRequest.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="status" name="status.id" from="${hbis.TissueRequestStatus.list()}" optionKey="id" required="" value="${tissueRequestInstance?.status?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tissueRequestInstance, field: 'tissueRequestTissueQuestId', 'error')} ">
	<label for="tissueRequestTissueQuestId">
		<g:message code="tissueRequest.tissueRequestTissueQuestId.label" default="Tissue Request Tissue Quest ID" />
		
	</label>
	<g:textField name="tissueRequestTissueQuestId" value="${tissueRequestInstance?.tissueRequestTissueQuestId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tissueRequestInstance, field: 'projectTissueQuestId', 'error')} ">
	<label for="projectTissueQuestId">
		<g:message code="tissueRequest.tissueRequestTissueQuestId.label" default="Project Tissue Quest ID" />
		
	</label>
	<g:textField name="projectTissueQuestId" value="${tissueRequestInstance?.projectTissueQuestId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tissueRequestInstance, field: 'contactMethod', 'error')} required">
	<label for="contactMethod">
		<g:message code="tissueRequest.contactMethod.label" default="Contact Method" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="contactMethod" name="contactMethod.id" from="${hbis.TissueRequestContactMethod.list()}" optionKey="id" required="" value="${tissueRequestInstance?.contactMethod?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<g:hiddenField name="investigator.id" value="${tissueRequestInstance?.investigator?.id}"/>
