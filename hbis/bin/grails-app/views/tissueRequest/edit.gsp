<%@ page import="hbis.TissueRequest" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tissueRequest.label', default: 'TissueRequest')}" />
		<g:set var="parentEntityName" value="${message(code: 'investigator.label', default: 'Investigator')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#investigator-button').click(function () {
    			$('#show-investigator').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#edit-tissueRequest" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div class="buttons">
			<button id="investigator-button">Toggle Investigator</button>
		</div>
		
		<div id="show-investigator" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
				<g:render template="/showDomainTemplates/investigator" />
		</div>
		
		<div id="edit-tissueRequest" class="content scaffold-edit" role="main" style="clear:both">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${tissueRequestInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${tissueRequestInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form method="post" >
				<g:hiddenField name="id" value="${tissueRequestInstance?.id}" />
				<g:hiddenField name="version" value="${tissueRequestInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
