
<%@ page import="hbis.Medication" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'medication.label', default: 'Medication')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-medication" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-medication" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="medication.chartReview.label" default="Chart Review" /></th>
					
						<g:sortableColumn property="doseMg" title="${message(code: 'medication.doseMg.label', default: 'Dose Mg')}" />
					
						<g:sortableColumn property="doseOther" title="${message(code: 'medication.doseOther.label', default: 'Dose Other')}" />
					
						<g:sortableColumn property="frequency" title="${message(code: 'medication.frequency.label', default: 'Frequency')}" />
					
						<g:sortableColumn property="medicationName" title="${message(code: 'medication.medicationName.label', default: 'Medication Name')}" />
					
						<th><g:message code="medication.status.label" default="Status" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${medicationInstanceList}" status="i" var="medicationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${medicationInstance.id}">${fieldValue(bean: medicationInstance, field: "chartReview")}</g:link></td>
					
						<td>${fieldValue(bean: medicationInstance, field: "doseMg")}</td>
					
						<td>${fieldValue(bean: medicationInstance, field: "doseOther")}</td>
					
						<td>${fieldValue(bean: medicationInstance, field: "frequency")}</td>
					
						<td>${fieldValue(bean: medicationInstance, field: "medicationName")}</td>
					
						<td>${fieldValue(bean: medicationInstance, field: "status")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${medicationInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
