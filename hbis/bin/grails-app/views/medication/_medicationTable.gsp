<table>
				<thead>
					<tr>
					
						<th><g:message code="medication.medicationName.label" default="Medication Name" /></th>
					
						<th><g:message code="medication.doseMg.label" default="Dose (mg)" /></th>
					
						<th><g:message code="medication.doseOther.label" default="Dose (other)" /></th>
					
						<th><g:message code="medication.frequency.label" default="Frequency" /></th>
						
						<th><g:message code="medication.status.label" default="Status" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${chartReviewInstance.medications}" status="i" var="medicationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${medicationInstance.id}">${fieldValue(bean: medicationInstance, field: "medicationName")}</g:link></td>
					
						<td>${fieldValue(bean: medicationInstance, field: "doseMg")}</td>
					
						<td>${fieldValue(bean: medicationInstance, field: "doseOther")}</td>
					
						<td>${fieldValue(bean: medicationInstance, field: "frequency")}</td>
						
						<td>${fieldValue(bean: medicationInstance, field: "status")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>