<%@ page import="hbis.Medication" %>

<div class="fieldcontain ${hasErrors(bean: medicationInstance, field: 'medicationName', 'error')} ">
	<label for="medicationName">
		<g:message code="medication.medicationName.label" default="Medication Name" />
	</label>
	<g:textField name="medicationName" value="${medicationInstance?.medicationName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medicationInstance, field: 'doseMg', 'error')} required">
	<label for="doseMg">
		<g:message code="medication.doseMg.label" default="Dose Mg" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="doseMg" type="number" value="${medicationInstance.doseMg}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: medicationInstance, field: 'doseOther', 'error')} ">
	<label for="doseOther">
		<g:message code="medication.doseOther.label" default="Dose Other" />
	</label>
	<g:textField name="doseOther" value="${medicationInstance?.doseOther}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medicationInstance, field: 'frequency', 'error')} ">
	<label for="frequency">
		<g:message code="medication.frequency.label" default="Frequency" />
	</label>
	<g:textField name="frequency" value="${medicationInstance?.frequency}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medicationInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="medication.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="status" name="status.id" from="${hbis.MedicalStatus.list()}" optionKey="id" required="" value="${medicationInstance?.status?.id}" class="many-to-one"/>
</div>

<g:hiddenField name="chartReview.id" value="${medicationInstance?.chartReview.id}"/>

