
<%@ page import="hbis.QcResult" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcResult.label', default: 'QcResult')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-qcResult" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-qcResult" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list qcResult">
			
				<g:if test="${qcResultInstance?.qcCode}">
				<li class="fieldcontain">
					<span id="qcCode-label" class="property-label"><g:message code="qcResult.qcCode.label" default="Qc Code" /></span>
					
						<span class="property-value" aria-labelledby="qcCode-label"><g:fieldValue bean="${qcResultInstance}" field="qcCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.qcPathologist}">
				<li class="fieldcontain">
					<span id="qcPathologist-label" class="property-label"><g:message code="qcResult.qcPathologist.label" default="Qc Pathologist" /></span>
					
						<span class="property-value" aria-labelledby="qcPathologist-label"><g:fieldValue bean="${qcResultInstance}" field="qcPathologist"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.regionOfInterest}">
				<li class="fieldcontain">
					<span id="regionOfInterest-label" class="property-label"><g:message code="qcResult.regionOfInterest.label" default="Region Of Interest" /></span>
					
						<span class="property-value" aria-labelledby="regionOfInterest-label"><g:fieldValue bean="${qcResultInstance}" field="regionOfInterest"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.regionOfInterestArea}">
				<li class="fieldcontain">
					<span id="regionOfInterestArea-label" class="property-label"><g:message code="qcResult.regionOfInterestArea.label" default="Region Of Interest Area" /></span>
					
						<span class="property-value" aria-labelledby="regionOfInterestArea-label"><g:fieldValue bean="${qcResultInstance}" field="regionOfInterestArea"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.percentNecrosis}">
				<li class="fieldcontain">
					<span id="percentNecrosis-label" class="property-label"><g:message code="qcResult.percentNecrosis.label" default="Percent Necrosis" /></span>
					
						<span class="property-value" aria-labelledby="percentNecrosis-label"><g:fieldValue bean="${qcResultInstance}" field="percentNecrosis"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.linkPathReport}">
				<li class="fieldcontain">
					<span id="linkPathReport-label" class="property-label"><g:message code="qcResult.linkPathReport.label" default="Link Path Report" /></span>
					
						<span class="property-value" aria-labelledby="linkPathReport-label"><g:fieldValue bean="${qcResultInstance}" field="linkPathReport"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.linkImage}">
				<li class="fieldcontain">
					<span id="linkImage-label" class="property-label"><g:message code="qcResult.linkImage.label" default="Link Image" /></span>
					
						<span class="property-value" aria-labelledby="linkImage-label"><g:fieldValue bean="${qcResultInstance}" field="linkImage"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.actualAnatomicSite}">
				<li class="fieldcontain">
					<span id="actualAnatomicSite-label" class="property-label"><g:message code="qcResult.actualAnatomicSite.label" default="Actual Anatomic Site" /></span>
					
						<span class="property-value" aria-labelledby="actualAnatomicSite-label"><g:fieldValue bean="${qcResultInstance}" field="actualAnatomicSite"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.actualDiagnosis}">
				<li class="fieldcontain">
					<span id="actualDiagnosis-label" class="property-label"><g:message code="qcResult.actualDiagnosis.label" default="Actual Diagnosis" /></span>
					
						<span class="property-value" aria-labelledby="actualDiagnosis-label"><g:fieldValue bean="${qcResultInstance}" field="actualDiagnosis"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.actualSubDiagnosis}">
				<li class="fieldcontain">
					<span id="actualSubDiagnosis-label" class="property-label"><g:message code="qcResult.actualSubDiagnosis.label" default="Actual Sub Diagnosis" /></span>
					
						<span class="property-value" aria-labelledby="actualSubDiagnosis-label"><g:fieldValue bean="${qcResultInstance}" field="actualSubDiagnosis"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.actualModDiagnosis}">
				<li class="fieldcontain">
					<span id="actualModDiagnosis-label" class="property-label"><g:message code="qcResult.actualModDiagnosis.label" default="Actual Mod Diagnosis" /></span>
					
						<span class="property-value" aria-labelledby="actualModDiagnosis-label"><g:fieldValue bean="${qcResultInstance}" field="actualModDiagnosis"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.goodForTMA}">
				<li class="fieldcontain">
					<span id="goodForTMA-label" class="property-label"><g:message code="qcResult.goodForTMA.label" default="Good For TMA" /></span>
					
						<span class="property-value" aria-labelledby="goodForTMA-label"><g:formatBoolean boolean="${qcResultInstance?.goodForTMA}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.pathologistComments}">
				<li class="fieldcontain">
					<span id="pathologistComments-label" class="property-label"><g:message code="qcResult.pathologistComments.label" default="Pathologist Comments" /></span>
					
						<span class="property-value" aria-labelledby="pathologistComments-label"><g:fieldValue bean="${qcResultInstance}" field="pathologistComments"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="qcResult.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${qcResultInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcResultInstance?.specimen}">
				<li class="fieldcontain">
					<span id="specimen-label" class="property-label"><g:message code="qcResult.specimen.label" default="Specimen" /></span>
					
						<span class="property-value" aria-labelledby="specimen-label"><g:link controller="specimen" action="show" id="${qcResultInstance?.specimen?.id}">${qcResultInstance?.specimen?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${qcResultInstance?.id}" />
					<g:link class="edit" action="edit" id="${qcResultInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
