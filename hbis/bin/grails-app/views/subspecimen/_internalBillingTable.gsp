			<table>
				<thead>
					<tr>
						<th><g:message code="subspecimen.investigator.label" default="Investigator" /></th>
						
						<th><g:message code="shippingCart.shipDate.label" default="Ship Date" /></th>
						
						<th><g:message code="specimen.procurementDate.label" default="Procurement Date" /></th>
						
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
						
						<th><g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen ID" /></th>
						
						<th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>
						
						<th><g:message code="qcResult.finalPrimaryAnatomicSite.label" default="Final Primary Anatomic Site" /></th>
						
						<th><g:message code="qcResult.finalTissueType.label" default="Final Tissue Type" /></th>
						
						<th><g:message code="shippingCart.eRampNumber.label" default="eRamp Number" /></th>
						
						<th><g:message code="subspecimen.price.label" default="Price" /></th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${subspecimens}" status="i" var="subspecimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: subspecimenInstance, field: "investigator")}</td>
						
						<td><g:formatDate date='${subspecimenInstance.shippingCart.shipDate}' format="MM/dd/yyyy"/></td>
						
						<td><g:formatDate date='${subspecimenInstance.specimen.procurementDate}' format="MM/dd/yyyy"/></td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.specimenChtnId")}</td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.finalPrimaryAnatomicSite")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.finalTissueType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "shippingCart.eRampNumber")}</td>
						
						<td><g:formatNumber type="currency" number="${subspecimenInstance.price}" /></td>
						
						</tr>
					</g:each>
				</tbody>
			</table>