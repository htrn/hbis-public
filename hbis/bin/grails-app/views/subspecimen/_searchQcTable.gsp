			<table>
				<thead>
					<tr>
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
						
						<th><g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen ID" /></th>
						
						<th><g:message code="subspecimen.status.label" default="Status" /></th>
						
						<th><g:message code="qcResult.tissueQcMatch.label" default="QC Result" /></th>
						
						<th><g:message code="qcResult.finalPrimaryAnatomicSite.label" default="Final Primary Anatomic Site" /></th>
						
						<th><g:message code="qcResult.finalTissueType.label" default="Final Tissue Type" /></th>
						
						<th><g:message code="qcResult.finalDiagnosis.label" default="Final Diagnosis" /></th>
						
						<th><g:message code="specimen.primaryOrMets.label" default="Prim/Mets" /></th>
						
						<th><g:message code="specimen.metsToAnatomicSite.label" default="Mets To Anatomic Site" /></th>
						
						<th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>
						
						<th><g:message code="subspecimen.investigator.label" default="Investigator" /></th>
						
						<th><g:message code="procedure.patientAge.label" default="Pat. Age" /></th>
						
						<th><g:message code="procedure.patientAge.label" default="Age Unit" /></th>
						
						<th><g:message code="patient.race.label" default="Race" /></th>
						
						<th><g:message code="patient.gender.label" default="Gender" /></th>
						
						<th><g:message code="subspecimen.weight.label" default="Weight" /></th>
						
						<th><g:message code="subspecimen.weightUnit.label" default="Weight Unit" /></th>
						
						<th><g:message code="procedure.radiationHistory.label" default="Rad. Hist." /></th>
						
						<th><g:message code="procedure.chemoHistory.label" default="Chemo. Hist." /></th>
						
						<th><g:message code="procedure.procedureType.label" default="Procedure Type" /></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${subspecimens}" status="i" var="subspecimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.specimenChtnId")}</td>
					
						<td><g:link action="show" id="${subspecimenInstance.id}">${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</g:link></td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "status")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.tissueQcMatch")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.finalPrimaryAnatomicSite")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.finalTissueType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.finalDiagnosis")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.primaryOrMets")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.metsToAnatomicSite")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "investigator")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patientAge")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patientAgeUnit")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.race")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.gender")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "weight")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "weightUnit")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.radiationHistory")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.chemoHistory")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.procedureType")}</td>
					
						</tr>
					</g:each>
				</tbody>
			</table>