<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'weight', 'error')} ">
	<label for="weight">
		<g:message code="subspecimen.weight.label" default="Weight" />
	</label>
	<g:field tabindex="1" name="weight" type="text" value="${subspecimenInstance?.weight}" style="width:50px"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'weightUnit', 'error')} ">
	<label for="weightUnit">
		<g:message code="subspecimen.weightUnit.label" default="Weight Unit" />
	</label>
	<g:if test="${weightUnitId}">
		<g:select tabindex="1" id="weightUnit" name="weightUnit.id" from="${hbis.WeightMeasurement.list()}" optionKey="id" value="${weightUnitId}" class="many-to-one" noSelection="['null': '']"/>
	</g:if>
	<g:else>
		<g:select tabindex="1" id="weightUnit" name="weightUnit.id" from="${hbis.WeightMeasurement.list()}" optionKey="id" value="${subspecimenInstance?.weightUnit?.id}" class="many-to-one" noSelection="['null': '']"/>
	</g:else>
</div>