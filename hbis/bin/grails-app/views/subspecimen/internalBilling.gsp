
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:javascript library="jquery" />
		
		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
          		$("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
		
		<title>Internal Billing Report</title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>CCC & CHTN Internal Billing Report</h1>
			
			<g:form action="internalBilling" >
				<fieldset class="form">
					<div>
						<label for="organization">
							<g:message code="subcontract.organization.label" default="Organization" />
						</label>
						<g:select name="organization" from="${["CCC", "CHTN"]}" value="${organization}" />
					</div>
					
					<div>
						<label for="fromDate">
							<g:message code="default.shipDate.fromDate" default="Ship Date From" />
						</label>
						<g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
					</div>
					
					<div>
						<label for="toDate">
							<g:message code="default.shipDate.toDate" default="Ship Date To" />
						</label>
						<g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
					</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="reassign" class="save" value="${message(code: 'subspecimen.button.reassignQuery.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
				<g:if test="${subspecimens}">
					<div id="show-subspecimens" class="allowTableOverflow" style="clear:both">
						<fieldset class="box"><legend><strong>Subspecimens</strong>
												</legend>
							<g:render template="internalBillingTable" />
						</fieldset>
					</div>
					<div>
					Download PDF Report
							<g:jasperReport
   								jasper="internal-billing"
   								name="${organization}-internal-billing-${new Date().format('MMddyy')}"
   								controller="subspecimen"
   								action="internalBillingReport"
   								format="pdf"
   								description=" "
   								delimiter=" ">
   									<g:hiddenField name="fromDate" value="${fromDateTextField}" />
   									<g:hiddenField name="toDate" value="${toDateTextField}" />
   									<g:hiddenField name="organization" value="${organization}" />
   							</g:jasperReport>
   					</div>
				</g:if>
				<g:else>
					There are no results to display
				</g:else>
		</div>
	</body>
</html>