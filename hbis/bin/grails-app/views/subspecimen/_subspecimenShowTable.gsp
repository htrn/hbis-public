			<table>
				<thead>
					<tr>
						<th><g:message code="subspecimen.subspecimenChtnId.label" default="CHTN ID" /></th>
					
						<th><g:message code="subspecimen.investigator.label" default="Investigator" /></th>
					
						<th><g:message code="subspecimen.preparationType.label" default="Prep Type" /></th>
						
						<th><g:message code="subspecimen.preparationMedia.label" default="Prep Media" /></th>
						
						<th><g:message code="subspecimen.weight.label" default="Weight" /></th>
						
						<th><g:message code="subspecimen.weightUnit.label" default="W. Unit" /></th>
						
						<th><g:message code="subspecimen.volume.label" default="Volume" /></th>
							
						<th><g:message code="subspecimen.volumeUnit.label" default="V. Unit" /></th>
						
						<th><g:message code="subspecimen.sizeWidth.label" default="Width" /></th>
							
						<th><g:message code="subspecimen.sizeWidth.label" default="Length" /></th>
							
						<th><g:message code="subspecimen.sizeWidth.label" default="Height" /></th>
							
						<th><g:message code="subspecimen.volumeUnit.label" default="S. Unit" /></th>
						
						<th><g:message code="subspecimen.labelComment.label" default="Label Comment" /></th>
						
						<th></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${specimenInstance.subspecimens}" status="i" var="subspecimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${subspecimenInstance.id}">${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</g:link></td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "investigator")}</td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationMedia")}</td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "weight")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "weightUnit")}</td>

						<td>${fieldValue(bean: subspecimenInstance, field: "volume")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "volumeUnit")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "sizeWidth")}</td>
							
						<td>${fieldValue(bean: subspecimenInstance, field: "sizeLength")}</td>
							
						<td>${fieldValue(bean: subspecimenInstance, field: "sizeHeight")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "sizeUnit")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "labelComment")}</td>
								
						<td><g:link controller="subspecimen" 
								action="edit"
								id="${subspecimenInstance.id}"
								params="[view: 'create']">Edit</g:link>
								</td>
								
						<td><g:jasperReport
   						jasper="subspecimen-label"
   						name="subspecimen-label-${new Date().format('MMddyy')}"
   						controller="subspecimen"
   						action="subspecimenLabel"
   						format="pdf"
   						description="Label"
   						delimiter=" ">
   							<g:hiddenField name="subspecimenId" value="${subspecimenInstance?.id}" />
   						</g:jasperReport></td>
					
						</tr>
					</g:each>
				</tbody>
			</table>