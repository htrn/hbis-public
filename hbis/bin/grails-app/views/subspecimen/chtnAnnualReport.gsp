
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:javascript library="jquery" />
		
		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
          		$("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
		
		<title>Lookup Data for APQI Report</title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>Lookup Data for CHTN Annual Report</h1>
			
			<g:form action="chtnAnnualReport" >
				<fieldset class="form">
					<div>
						<label for="fromDate">
							<g:message code="default.procurementDate.fromDate" default="Procurement From Date" />
						</label>
						<g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
					</div>
					
					<div>
						<label for="toDate">
							<g:message code="default.procurementDate.toDate" default="Procurement To Date" />
						</label>
						<g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
					</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="reassign" class="save" value="${message(code: 'subspecimen.button.reassignQuery.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
				<g:if test="${displayCounts}">
					All Results are for OSU Procedures
				<div class="allowTableOverflow">
					<fieldset class="box"><legend>Subspecimens Procured for A, AC, B, and C investigators grouped by Procedure Type</legend>
					<table>
						<thead>
							<tr>
								<th><g:message code="procedure.procedureType.label" default="Procedure Type" /></th>
						
								<th><g:message code="query.Quantity.label" default="Quantity" /></th>
					
								<th></th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${subspecimenCountListProcuredProcedureType}" status="i" var="resultRow">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
									<td>${resultRow.procedureTypeDescription}</td>
					
									<td>${resultRow.quantity}</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					</fieldset>
					
					<fieldset class="box"><legend>Subspecimens Shipped to A, AC, B, and C investigators grouped by CHTN ID Prefix Letters</legend>
					<table>
						<thead>
							<tr>
								<th><g:message code="investigator.chtnIdPrefix.label" default="CHTN ID Prefix" /></th>
						
								<th><g:message code="query.Quantity.label" default="Quantity" /></th>
					
								<th></th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${subspecimenDistribtedListChtnIdPrefix}" status="i" var="resultRow">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
									<td>${resultRow.chtnIdPrefixLetters}</td>
					
									<td>${resultRow.quantity}</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					</fieldset>
					
					<fieldset class="box"><legend>Subspecimens Shipped to A, AC, B, and C investigators</legend>
					<table>
						<thead>
							<tr>
								<th><g:message code="specimen.specimenCategory.label" default="Specimen Category" /></th>
						
								<th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>
						
								<th><g:message code="subspecimen.preparationMedia.label" default="Preparation Media" /></th>
						
								<th><g:message code="query.Quantity.label" default="Quantity" /></th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${subspecimenDistribtedListCategoryPrepTypeMedia}" status="i" var="resultRow">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
									<td>${resultRow.specimenCategoryDescription}</td>
						
									<td>${resultRow.preparationTypeDescription}</td>
						
									<td>${resultRow.preparationMediaDescription}</td>
					
									<td>${resultRow.quantity}</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					</fieldset>
					<fieldset class="form">
					<div>
						Count of Chart Reviews shipped to A, AC, B, or C investigators: ${chartReviewShippedQuantity}
					</div>
					</br>
					</fieldset>
				</div>
			</g:if>
		</div>
	</body>
</html>