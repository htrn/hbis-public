
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:javascript library="jquery" />
		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#datepicker").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
		<g:javascript>
		</g:javascript>
		
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>Contact: External Fresh</h1>
			<h2>Have these investigators been called?</h2>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<div id="show-subspecimens" style="clear:both">
				<table>
				<thead>
					<tr>
						<th><g:message code="specimen.procurementDate.label" default="Procure Date" /></th>
						
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen" /></th>
						
						<th><g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen" /></th>
						
						<th><g:message code="subspecimen.investigator.label" default="Investigator" /></th>
						
						<th><g:message code="subspecimen.weight.label" default="Weight" /></th>
						
						<th><g:message code="subspecimen.weightUnit.label" default="Unit" /></th>
						
						<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tiss Type" /></th>
						
						<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Prim. Ana. Site" /></th>
						
						<th><g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diag." /></th>
						
						<th><g:message code="subspecimen.status.label" default="Status" /></th>
					
						<th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>
						
						<th><g:message code="specimen.tissueRequest.label" default="Tiss. Request" /></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${subspecimenInstanceList}" status="i" var="subspecimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td><g:formatDate format="MM/dd/yyyy" date="${subspecimenInstance.specimen.procurementDate}"/></td>
						
						<td><g:link controller="specimen" action="show" id="${subspecimenInstance.specimen.id}">${fieldValue(bean: subspecimenInstance, field: "specimen.specimenChtnId")}</g:link></td>
					
						<td><g:link action="show" id="${subspecimenInstance.id}">${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</g:link></td>
					
						<td><g:link controller="investigator" action="show" id="${subspecimenInstance.investigator.id}">${fieldValue(bean: subspecimenInstance, field: "investigator")}</g:link></td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "weight")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "weightUnit")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryTissueType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryPrimaryAnatomicSite")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryDiagnosis")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "status")}</td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "tissueRequest")}</td>
						
						<g:form action="update">
						<g:hiddenField name="id" value="${subspecimenInstance.id}" />
						<g:hiddenField name="version" value="${subspecimenInstance.version}" />
						<g:hiddenField name="view" value="contactExternalFresh" />
						<g:hiddenField name="called" value="${true}" />
						
						<td><g:submitButton name="update" value="Called" /></td>
						
						</g:form>
					
						</tr>
					</g:each>
				</tbody>
			</table>
			</div>
			
		</div>
	</body>
</html>
