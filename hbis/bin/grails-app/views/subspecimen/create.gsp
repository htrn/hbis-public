<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:set var="parentEntityName" value="${message(code: 'specimen.label', default: 'Specimen')}" />
		<g:set var="parentOfParentEntityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<g:set var="parentOfParentOfParentEntityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#patient-button').click(function () {
    			$('#show-patient').toggle("slow");
			});
			
			$('#procedure-button').click(function () {
			  	$('#show-procedure').toggle("slow");
			});
			
			$('#specimen-button').click(function () {
			  	$('#show-specimen').toggle("slow");
			});
		</g:javascript>
		
	</head>
	<body>
		<a href="#create-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div class="buttons">
			<button id="patient-button">Toggle Patient</button>
			<button id="procedure-button">Toggle Procedure</button>
			<button id="specimen-button">Toggle Specimen</button>
		</div>
		<div id="show-patient" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/patient" />
		</div>
		
		<div id="show-procedure" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/procedure" />
		</div>
		
		<div id="show-specimen" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/specimen" />
		</div>
		
		<div style="clear:both">
			<h1><g:message code="default.create.label" args="[entityName]" /> for ${specimenInstance.specimenChtnId}: ${specimenInstance.preliminaryPrimaryAnatomicSite}, ${specimenInstance.preliminaryTissueType}</h1>
		</div>
		
		<div style="clear:both">
			<g:link controller="specimen"
								action="createCopyDetails"
								id="${specimenInstance.id}"><button type="button">Copy Specimen Info Into New Specimen</button></g:link>
		</div>
		<div style="clear:both">
			<g:link controller="specimen"
								action="copySpecimenAndSubspecimens"
								id="${specimenInstance.id}"
								params="[view: 'createSubspecimen']"><button type="button">Copy Specimen and Subspecimens Into New Specimen</button></g:link>
		</div>
		
		<div id="show-subspecimens" class="allowTableOverflow" style="clear:both">
			<fieldset class="box"><legend>Subspecimens Procured for above Specimen</legend>
				<g:render template="subspecimenCreateTable" />
			</fieldset>
		</div>
		
		<div> <!-- Where feedback for QcMethod and looking at what is  -->
			<g:if test="${specimenInstance.qcMethod.description == 'Pathologist Review'}">
				<h3>QC Method for this Specimen is '${specimenInstance.qcMethod.description},' so make a Q subspecimen</h3>
			</g:if>
			<g:else>
				<h3>QC Method for this Specimen is '${specimenInstance.qcMethod.description},' so do not make a Q subspecimen</h3>
			</g:else>
		</div>
		
		<div id="create-subspecimen" class="content scaffold-create" role="main">
			<h1></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${subspecimenInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<!-- <g:actionSubmit action="save" name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /> -->
					<g:actionSubmit tabindex="1" name="saveAndCreateAnotherSubspecimen" action="saveAndCreateAnotherSubspecimen" class="save" value="${message(code: 'default.button.create.label', default: 'Save')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
