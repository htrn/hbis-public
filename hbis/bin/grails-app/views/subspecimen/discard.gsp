
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:javascript library="jquery" />
		
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>Discard Subspecimens</h1>
			
			<g:form action="discard" >
				<fieldset class="form">
				<div>
					<label for="investigator">
						<g:message code="subspecimen.investigator.label" default="Investigator" />
					</label>
					<g:select id="investigator" name="investigatorId" from="${hbis.Investigator.list()}" optionKey="id" value="${investigatorId}" class="many-to-one" noSelection="['null': '']"/>
				</div>
				<div>
					<label for="qcResultId">
						<g:message code="qcResult.tissueQcMatchId.label" default="QC Result" />
					</label>
					<g:select id="tissueQcMatch" name="tissueQcMatchId" from="${hbis.TissueQcMatch.list()}" optionKey="id" value="${tissueQcMatchId}" class="many-to-one" noSelection="['null': '']"/>
				</div>
				<div>
					<label for="specimenChtnId">
						<g:message code="specimen.specimenChtnId.label" default="Specimen ID" />
					</label>
					<g:textField name="specimenChtnId" value="${specimenChtnId}" />
				</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="reassign" class="save" value="${message(code: 'subspecimen.button.discardQuery.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<g:form action="performDiscard">
			<div id="show-subspecimens" style="clear:both">
				<fieldset class="box"><legend><strong>Subspecimens</strong>
												</legend>
					<g:render template="discardTable" />
				</fieldset>
			</div>
				<fieldset class="buttons">
					<g:hiddenField name="investigatorId" value="${investigatorId}" />
					<g:hiddenField name="tissueQcMatchId" value="${tissueQcMatchId}" />
					<g:hiddenField name="specimenChtnId" value="${specimenChtnId}" />
					<g:submitButton name="performDiscard" class="save" value="${message(code: 'subspecimen.button.reassignQuery.label', default: 'Discard Checked Subspecimens')}" />
				</fieldset>
			</g:form>
			
		</div>
	</body>
</html>