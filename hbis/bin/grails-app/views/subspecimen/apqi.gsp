
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:javascript library="jquery" />

		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
          		$("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>

		<title>Lookup Data for APQI Report</title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>Lookup Data for APQI Report</h1>

			<g:form action="apqi" >
				<fieldset class="form">
					<div>
						<label for="fromDate">
							<g:message code="default.procurementDate.fromDate" default="Procurement From Date" />
						</label>
						<g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
					</div>

					<div>
						<label for="toDate">
							<g:message code="default.procurementDate.toDate" default="Procurement To Date" />
						</label>
						<g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
					</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="apqi" class="save" value="${message(code: 'subspecimen.button.reassignQuery.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
				<g:if test="${displayCounts}">
					All Results are for OSU Procedures
					<fieldset class="form">
					<div>
						Distinct Patients: ${distinctPatients}
					</div>
					</br>
					<div>
						Total QC Performed: ${totalQcPerformed}
					</div>
					</br>
					<div>
						Number QC Passed: ${numberQcPassed}
					</div>
					</br>
					<div>
						Number QC Failed: ${numberQcFailed}
					</div>
					</br>
					<div>
						Parts Per Case: ${partsPerCase}
					</div>
					</br>
					<div>
						Total Subspecimens Procured: ${totalSubspecimensProcured}
					</div>
					</br>
					<div>
						Distinct Autopsy Patients: ${distinctAutopsyPatients}
					</div>
					</br>
					<div>
						Total Autopsy Subspecimens Procured: ${totalAutopsySubspecimens}
					</div>
					</br>
					</fieldset>
				<div class="allowTableOverflow">
				<table>
				<thead>
					<tr>
						<th><g:message code="procedure.procurementResult.label" default="Procurement Result" /></th>

						<th><g:message code="query.Quantity.label" default="Quantity" /></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${procurementResultQuantityList}" status="i" var="resultRow">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${resultRow.procurementResultDescription}</td>

						<td>${resultRow.quantity}</td>
					</tr>
				</g:each>
				</tbody>
			</table>

			<table>
				<thead>
					<tr>
						<th><g:message code="procedure.procedureName.label" default="Procedure Name" /></th>

						<th><g:message code="procedure.subProcedureName.label" default="Sub Procedure Name" /></th>

						<th><g:message code="procedure.procurementResultDescription.label" default="Procurement Result" /></th>

						<th><g:message code="query.Quantity.label" default="Quantity" /></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${procedureNameSubNameByResultQuantityList}" status="i" var="resultRow">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${resultRow.procedureNameDescription}</td>

						<td>${resultRow.subProcedureNameDescription}</td>

						<td>${resultRow.procurementResultDescription}</td>

						<td>${resultRow.quantity}</td>
					</tr>
				</g:each>
				</tbody>
			</table>

			<table>
				<thead>
					<tr>
						<th><g:message code="procedure.origin.label" default="Origin" /></th>

						<th><g:message code="query.Quantity.label" default="Quantity" /></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${originQuantityList}" status="i" var="resultRow">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${resultRow.originDescription}</td>

						<td>${resultRow.quantity}</td>
					</tr>
				</g:each>
				</tbody>
			</table>

            Export To CSV
            <g:jasperReport
                    jasper="apqi-search-results"
                    name="apqi-search-results-${new Date().format('MMddyy')}"
                    controller="subspecimen"
                    action="apqiSearchResultsToCSV"
                    format="csv"
                    description=" "
                    delimiter=" ">
            </g:jasperReport>

			</div>
			</g:if>
		</div>
	</body>
</html>