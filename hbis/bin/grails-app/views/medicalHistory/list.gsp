
<%@ page import="hbis.MedicalHistory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'medicalHistory.label', default: 'MedicalHistory')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-medicalHistory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-medicalHistory" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="medicalHistory.cancer.label" default="Cancer" /></th>
					
						<th><g:message code="medicalHistory.status.label" default="Status" /></th>
					
						<th><g:message code="medicalHistory.chartReview.label" default="Chart Review" /></th>
					
						<th><g:message code="medicalHistory.diagnosis.label" default="Diagnosis" /></th>
					
						<g:sortableColumn property="stage" title="${message(code: 'medicalHistory.stage.label', default: 'Stage')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${medicalHistoryInstanceList}" status="i" var="medicalHistoryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${medicalHistoryInstance.id}">${fieldValue(bean: medicalHistoryInstance, field: "cancer")}</g:link></td>
					
						<td>${fieldValue(bean: medicalHistoryInstance, field: "status")}</td>
					
						<td>${fieldValue(bean: medicalHistoryInstance, field: "chartReview")}</td>
					
						<td>${fieldValue(bean: medicalHistoryInstance, field: "diagnosis")}</td>
					
						<td>${fieldValue(bean: medicalHistoryInstance, field: "stage")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${medicalHistoryInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
