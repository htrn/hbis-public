<table>
				<thead>
					<tr>
					
						<th><g:message code="medicalHistory.cancer.label" default="Cancer" /></th>
					
						<th><g:message code="medicalHistory.status.label" default="Status" /></th>
					
						<th><g:message code="medicalHistory.diagnosis.label" default="Diagnosis" /></th>
					
						<th><g:message code="medicalHistory.stage.label" default="Stage" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${chartReviewInstance.medicalHistories}" status="i" var="medicalHistoryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${medicalHistoryInstance.id}">${fieldValue(bean: medicalHistoryInstance, field: "cancer")}</g:link></td>
					
						<td>${fieldValue(bean: medicalHistoryInstance, field: "status")}</td>
					
						<td>${fieldValue(bean: medicalHistoryInstance, field: "diagnosis")}</td>
					
						<td>${fieldValue(bean: medicalHistoryInstance, field: "stage")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>