<table>
				<thead>
					<tr>
					
						<th><g:message code="familyHistory.relative.label" default="Relative" /></th>
					
						<th><g:message code="familyHistory.diagnosis.label" default="Diagnosis" /></th>
	
					</tr>
				</thead>
				<tbody>
				<g:each in="${chartReviewInstance.familyHistories}" status="i" var="familyHistoryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${familyHistoryInstance.id}">${fieldValue(bean: familyHistoryInstance, field: "relative")}</g:link></td>
					
						<td>${fieldValue(bean: familyHistoryInstance, field: "diagnosis")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>