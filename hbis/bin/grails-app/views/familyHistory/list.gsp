
<%@ page import="hbis.FamilyHistory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'familyHistory.label', default: 'FamilyHistory')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-familyHistory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-familyHistory" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="familyHistory.chartReview.label" default="Chart Review" /></th>
					
						<th><g:message code="familyHistory.diagnosis.label" default="Diagnosis" /></th>
					
						<g:sortableColumn property="relative" title="${message(code: 'familyHistory.relative.label', default: 'Relative')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${familyHistoryInstanceList}" status="i" var="familyHistoryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${familyHistoryInstance.id}">${fieldValue(bean: familyHistoryInstance, field: "chartReview")}</g:link></td>
					
						<td>${fieldValue(bean: familyHistoryInstance, field: "diagnosis")}</td>
					
						<td>${fieldValue(bean: familyHistoryInstance, field: "relative")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${familyHistoryInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
