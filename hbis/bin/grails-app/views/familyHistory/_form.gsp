<%@ page import="hbis.FamilyHistory" %>

<div class="fieldcontain ${hasErrors(bean: familyHistoryInstance, field: 'relative', 'error')} ">
	<label for="relative">
		<g:message code="familyHistory.relative.label" default="Relative" />
	</label>
	<g:textField name="relative" value="${familyHistoryInstance?.relative}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: familyHistoryInstance, field: 'diagnosis', 'error')} required">
	<label for="diagnosis">
		<g:message code="familyHistory.diagnosis.label" default="Diagnosis" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="diagnosis" name="diagnosis.id" from="${hbis.Diagnosis.list()}" optionKey="id" required="" value="${familyHistoryInstance?.diagnosis?.id}" class="many-to-one"/>
</div>

<g:hiddenField name="chartReview.id" value="${familyHistoryInstance?.chartReview.id}"/>

