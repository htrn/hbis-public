<g:select	id="institutionType" 
			name="institutionType.id" 
			from="${institutionTypeList}" 
			optionKey="id" 
			value="${investigatorInstance?.institutionType?.id}" 
			class="many-to-one"
			/>