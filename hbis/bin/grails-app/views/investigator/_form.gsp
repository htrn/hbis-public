<%@ page import="hbis.Investigator" %>


<div class="leftFormColumn">

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'chtnId', 'error')} required">
	<label for="chtnId">
		<g:message code="investigator.chtnId.label" default="CHTN ID" />
		<span class="required-indicator">*</span>
	</label>
	<g:select	id="chtnIdPrefix" 
				name="chtnIdPrefix.id" 
				from="${hbis.InvestigatorChtnIdPrefix.list()}" 
				optionKey="id"
				value="${investigatorInstance?.chtnIdPrefix?.id}"
				class="many-to-one" 
				noSelection="['null': '']"
				onchange="${remoteFunction(
            		controller:'investigator', 
            		action:'ajaxGetExternalVsInternal',
					update:[success:'externalVsInternalContainer'],
            		params:'\'chtnIdPrefixId=\' + this.value', 
            		)};
            		${remoteFunction(
            		controller:'investigator', 
            		action:'ajaxGetInstitutionType',
					update:[success:'institutionTypeContainer'],
            		params:'\'chtnIdPrefixId=\' + this.value', 
            		)};
            		${remoteFunction(
            		controller:'investigator', 
            		action:'ajaxGetServiceProgramType',
					update:[success:'serviceProgramTypeContainer'],
            		params:'\'chtnIdPrefixId=\' + this.value', 
            		)}"
	/>
	<g:textField name="chtnId" required="" value="${investigatorInstance?.chtnId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'externalVsInternal', 'error')}">
	<label for="externalVsInternal">
		<g:message code="investigator.externalVsInternal.label" default="External or Internal" />
	</label>
	<div id="externalVsInternalContainer">
		<g:if test="${investigatorInstance.externalVsInternal?.id}">
			<g:hiddenField name="externalVsInternal.id" value="${investigatorInstance.externalVsInternal.id}" />
			${investigatorInstance.externalVsInternal}
		</g:if>
		<g:else>
			Choose a CHTN ID Prefix
		</g:else>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'institutionType', 'error')}">
	<label for="institutionType">
		<g:message code="investigator.institutionType.label" default="Institution Type" />
	</label>
	<div id="institutionTypeContainer">
		<g:if test="${investigatorInstance.institutionType?.id}">
			<g:hiddenField name="institutionType.id" value="${investigatorInstance.institutionType.id}" />
			${investigatorInstance.institutionType}
		</g:if>
		<g:else>
			Choose a CHTN ID Prefix
		</g:else>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'serviceProgramType', 'error')}">
	<label for="serviceProgramType">
		<g:message code="investigator.serviceProgramType.label" default="Service Program Type" />
	</label>
	<div id="serviceProgramTypeContainer">
		<g:if test="${investigatorInstance.serviceProgramType?.id}">
			<g:hiddenField name="serviceProgramType.id" value="${investigatorInstance.serviceProgramType.id}" />
			${investigatorInstance.serviceProgramType}
		</g:if>
		<g:else>
			Choose a CHTN ID Prefix
		</g:else>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'chtnDivision', 'error')} required">
	<label for="chtnDivision">
		<g:message code="investigator.chtnDivision.label" default="CHTN Division" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="chtnDivision" name="chtnDivision.id" from="${hbis.ChtnDivision.list()}" optionKey="id" value="${investigatorInstance?.chtnDivision?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'status', 'error')}">
	<label for="status">
		<g:message code="investigator.status.label" default="Status" />
	</label>
	<g:select id="status" name="status.id" from="${hbis.InvestigatorStatus.list()}" optionKey="id" value="${investigatorInstance?.chtnDivision?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'firstName', 'error')} required">
	<label for="firstName">
		<g:message code="investigator.firstName.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="firstName" required="" value="${investigatorInstance?.firstName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'middleName', 'error')} ">
	<label for="middleName">
		<g:message code="investigator.middleName.label" default="Middle Name" />
		
	</label>
	<g:textField name="middleName" value="${investigatorInstance?.middleName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="investigator.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lastName" required="" value="${investigatorInstance?.lastName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'primaryInstitution', 'error')} ">
	<label for="primaryInstitution">
		<g:message code="investigator.primaryInstitution.label" default="Primary Institution" />
		
	</label>
	<g:textField name="primaryInstitution" value="${investigatorInstance?.primaryInstitution}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'primaryDepartment', 'error')} ">
	<label for="primaryDepartment">
		<g:message code="investigator.primaryDepartment.label" default="Primary Department" />
		
	</label>
	<g:textField name="primaryDepartment" value="${investigatorInstance?.primaryDepartment}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'primaryStreetAddress1', 'error')} ">
	<label for="primaryStreetAddress1">
		<g:message code="investigator.primaryStreetAddress1.label" default="Primary Street Address1" />
		
	</label>
	<g:textField name="primaryStreetAddress1" value="${investigatorInstance?.primaryStreetAddress1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'primaryStreetAddress2', 'error')} ">
	<label for="primaryStreetAddress2">
		<g:message code="investigator.primaryStreetAddress2.label" default="Primary Street Address2" />
		
	</label>
	<g:textField name="primaryStreetAddress2" value="${investigatorInstance?.primaryStreetAddress2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'primaryCity', 'error')} ">
	<label for="primaryCity">
		<g:message code="investigator.primaryCity.label" default="Primary City" />
		
	</label>
	<g:textField name="primaryCity" value="${investigatorInstance?.primaryCity}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'primaryState', 'error')} ">
	<label for="primaryState">
		<g:message code="investigator.primaryState.label" default="Primary State" />
		
	</label>
	<g:textField name="primaryState" value="${investigatorInstance?.primaryState}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'primaryZipCode', 'error')} ">
	<label for="primaryZipCode">
		<g:message code="investigator.primaryZipCode.label" default="Primary Zip Code" />
		
	</label>
	<g:textField name="primaryZipCode" value="${investigatorInstance?.primaryZipCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'primaryCountry', 'error')} ">
	<label for="primaryCountry">
		<g:message code="investigator.primaryCountry.label" default="Primary Country" />
		
	</label>
	<g:textField name="primaryCountry" value="${investigatorInstance?.primaryCountry}"/>
</div>

</div> <!-- End Left Form Column -->

<div class="rightFormColumn">

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'billingInstitution', 'error')} ">
	<label for="billingInstitution">
		<g:message code="investigator.billingInstitution.label" default="Billing Institution" />
		
	</label>
	<g:textField name="billingInstitution" value="${investigatorInstance?.billingInstitution}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'billingDepartment', 'error')} ">
	<label for="billingDepartment">
		<g:message code="investigator.billingDepartment.label" default="Billing Department" />
		
	</label>
	<g:textField name="billingDepartment" value="${investigatorInstance?.billingDepartment}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'billingStreetAddress1', 'error')} ">
	<label for="billingStreetAddress1">
		<g:message code="investigator.billingStreetAddress1.label" default="Billing Street Address1" />
		
	</label>
	<g:textField name="billingStreetAddress1" value="${investigatorInstance?.billingStreetAddress1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'billingStreetAddress2', 'error')} ">
	<label for="billingStreetAddress2">
		<g:message code="investigator.billingStreetAddress2.label" default="Billing Street Address2" />
		
	</label>
	<g:textField name="billingStreetAddress2" value="${investigatorInstance?.billingStreetAddress2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'billingCity', 'error')} ">
	<label for="billingCity">
		<g:message code="investigator.billingCity.label" default="Billing City" />
		
	</label>
	<g:textField name="billingCity" value="${investigatorInstance?.billingCity}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'billingState', 'error')} ">
	<label for="billingState">
		<g:message code="investigator.billingState.label" default="Billing State" />
		
	</label>
	<g:textField name="billingState" value="${investigatorInstance?.billingState}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'billingZipCode', 'error')} ">
	<label for="billingZipCode">
		<g:message code="investigator.billingZipCode.label" default="Billing Zip Code" />
		
	</label>
	<g:textField name="billingZipCode" value="${investigatorInstance?.billingZipCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'billingCountry', 'error')} ">
	<label for="billingCountry">
		<g:message code="investigator.billingCountry.label" default="Billing Country" />
		
	</label>
	<g:textField name="billingCountry" value="${investigatorInstance?.billingCountry}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'shippingInstitution', 'error')} ">
	<label for="shippingInstitution">
		<g:message code="investigator.shippingInstitution.label" default="Shipping Institution" />
		
	</label>
	<g:textField name="shippingInstitution" value="${investigatorInstance?.shippingInstitution}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'shippingDepartment', 'error')} ">
	<label for="shippingDepartment">
		<g:message code="investigator.shippingDepartment.label" default="Shipping Department" />
		
	</label>
	<g:textField name="shippingDepartment" value="${investigatorInstance?.shippingDepartment}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'shippingStreetAddress1', 'error')} ">
	<label for="shippingStreetAddress1">
		<g:message code="investigator.shippingStreetAddress1.label" default="Shipping Street Address1" />
		
	</label>
	<g:textField name="shippingStreetAddress1" value="${investigatorInstance?.shippingStreetAddress1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'shippingStreetAddress2', 'error')} ">
	<label for="shippingStreetAddress2">
		<g:message code="investigator.shippingStreetAddress2.label" default="Shipping Street Address2" />
		
	</label>
	<g:textField name="shippingStreetAddress2" value="${investigatorInstance?.shippingStreetAddress2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'shippingCity', 'error')} ">
	<label for="shippingCity">
		<g:message code="investigator.shippingCity.label" default="Shipping City" />
		
	</label>
	<g:textField name="shippingCity" value="${investigatorInstance?.shippingCity}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'shippingState', 'error')} ">
	<label for="shippingState">
		<g:message code="investigator.shippingState.label" default="Shipping State" />
		
	</label>
	<g:textField name="shippingState" value="${investigatorInstance?.shippingState}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'shippingZipCode', 'error')} ">
	<label for="shippingZipCode">
		<g:message code="investigator.shippingZipCode.label" default="Shipping Zip Code" />
		
	</label>
	<g:textField name="shippingZipCode" value="${investigatorInstance?.shippingZipCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'shippingCountry', 'error')} ">
	<label for="shippingCountry">
		<g:message code="investigator.shippingCountry.label" default="Shipping Country" />
		
	</label>
	<g:textField name="shippingCountry" value="${investigatorInstance?.shippingCountry}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'courierName', 'error')} ">
	<label for="courierName">
		<g:message code="investigator.courierName.label" default="Courier Name" />
		
	</label>
	<g:select id="courierName" name="courierName.id" from="${hbis.ShipMode.list()}" optionKey="id" value="${investigatorInstance?.courierName?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: investigatorInstance, field: 'courierNumber', 'error')} ">
	<label for="courierNumber">
		<g:message code="investigator.courierNumber.label" default="Courier Number" />
		
	</label>
	<g:textField name="courierNumber" value="${investigatorInstance?.courierNumber}"/>
</div>

</div> <!-- End Right Form Column -->

