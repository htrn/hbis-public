<g:select	id="serviceProgramType" 
			name="serviceProgramType.id" 
			from="${serviceProgramTypeList}" 
			optionKey="id" 
			value="${investigatorInstance?.serviceProgramType?.id}" 
			class="many-to-one" 
			/>