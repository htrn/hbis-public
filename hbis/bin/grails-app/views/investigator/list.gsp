
<%@ page import="hbis.Investigator" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'investigator.label', default: 'Investigator')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-investigator" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-investigator" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="chtnId" title="${message(code: 'investigator.chtnId.label', default: 'Chtn Id')}" />
					
						<g:sortableColumn property="firstName" title="${message(code: 'investigator.firstName.label', default: 'First Name')}" />
					
						<g:sortableColumn property="middleName" title="${message(code: 'investigator.middleName.label', default: 'Middle Name')}" />
					
						<g:sortableColumn property="lastName" title="${message(code: 'investigator.lastName.label', default: 'Last Name')}" />
					
						<g:sortableColumn property="primaryStreetAddress1" title="${message(code: 'investigator.primaryStreetAddress1.label', default: 'Primary Street Address1')}" />
					
						<g:sortableColumn property="primaryStreetAddress2" title="${message(code: 'investigator.primaryStreetAddress2.label', default: 'Primary Street Address2')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${investigatorInstanceList}" status="i" var="investigatorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${investigatorInstance.id}">${fieldValue(bean: investigatorInstance, field: "chtnId")}</g:link></td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "firstName")}</td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "middleName")}</td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "lastName")}</td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "primaryStreetAddress1")}</td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "primaryStreetAddress2")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${investigatorInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
