
<%@ page import="hbis.Procedure" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-procedure" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-procedure" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="procedureDate" title="${message(code: 'procedure.procedureDate.label', default: 'Procedure Date')}" />
					
						<th><g:message code="procedure.procedureType.label" default="Procedure Type" /></th>
					
						<th><g:message code="procedure.procedureName.label" default="Procedure Name" /></th>
					
						<th><g:message code="procedure.procurementResult.label" default="Procurement Result" /></th>
					
						<th><g:message code="procedure.patient.label" default="Patient" /></th>
					
						<g:sortableColumn property="patientAge" title="${message(code: 'procedure.patientAge.label', default: 'Patient Age')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${procedureInstanceList}" status="i" var="procedureInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${procedureInstance.id}">${fieldValue(bean: procedureInstance, field: "procedureDate")}</g:link></td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procedureType")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procedureName")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procurementResult")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "patient")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "patientAge")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${procedureInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
