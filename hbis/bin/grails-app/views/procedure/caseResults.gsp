
<%@ page import="hbis.Procedure" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-procedure" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-procedure" class="content scaffold-list" role="main">
			<h1>Case Results</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${procedureInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${procedureInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<div class="allowTableOverflow">
			<g:form method="post" action="updateAllCaseResults">
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="procedureDate" title="${message(code: 'procedure.procedureDate.label', default: 'Procedure Date')}" />
						
						<th><g:message code="patient.mrn.label" default="MRN" /></th>
						
						<th><g:message code="patient.lastName.label" default="Last Name" /></th>
					
						<th><g:message code="procedure.procedureType.label" default="Procedure Type" /></th>
					
						<th><g:message code="procedure.procedureName.label" default="Procedure Name" /></th>
						
						<th><g:message code="procedure.subProcedureName.label" default="Sub Procedure Name" /></th>
						
						<th><g:message code="procedure.endTimeOfProcedure.label" default="End Time" /></th>
					
						<th><g:message code="procedure.procurementResult.label" default="Procurement Result" /></th>
						
						<th><g:message code="subspecimen.number.label" default="# Subspecimens" /></th>
						
						<th><g:message code="procedure.medComments.label" default="Med Comments" /></th>
						
						<th><g:message code="procedure.origin.label" default="Origin" /></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${procedureInstanceList}" status="i" var="procedureInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${procedureInstance.id}"><g:formatDate format="MM/dd/yyyy" date="${procedureInstance.procedureDate}"/></g:link></td>
						
						<td><g:link controller="patient" action="show" id="${procedureInstance.patient.id}">${fieldValue(bean: procedureInstance, field: "patient.mrn")}</g:link></td>
						
						<td>${fieldValue(bean: procedureInstance, field: "patient.lastName")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procedureType")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procedureName")}</td>
						
						<td>${fieldValue(bean: procedureInstance, field: "subProcedureName")}</td>
						
						<td><g:formatDate format="hh:mm aa" date="${procedureInstance.endTimeOfProcedure}"/></td>
					
						<td><g:select id="procurementResult" name="procedures.procurementResult.${procedureInstance.id}" optionKey="id" tabindex="1" value="${procedureInstance?.procurementResult?.id}" from="${hbis.ProcurementResult.list()}" class="many-to-one" default="none" noSelection="['null': '']"/></td>
						
						<td>${procedureInstance.getNumberOfSubspecimens()}</td>
						
						<td><g:textField name="procedures.medComments.${procedureInstance.id}" value="${procedureInstance?.medComments}" style="width:200px"/></td>
						
						<td><g:select id="origin" name="procedures.origin.${procedureInstance.id}" optionKey="id" tabindex="1" value="${procedureInstance?.origin?.id}" from="${hbis.Origin.list()}" class="many-to-one" default="none" noSelection="['null': '']"/></td>
						
						<td><g:link action="edit" id="${procedureInstance.id}" params="[view: 'caseResults']">Edit</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			</div>
			<fieldset class="buttons">
					<g:submitButton name="update" class="save" value="${message(code: 'procedure.button.updateAllCaseResults.label', default: 'Update All Case Results')}" />
			</fieldset>
		</g:form>
		</div>
	</body>
</html>
