
<%@ page import="hbis.Procedure" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		
		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#procedureDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
	</head>
	<body>
		<a href="#list-procedure" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-procedure" class="content scaffold-list" role="main">
			<h1>Search Case Results</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${procedureInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${procedureInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			
			
			<g:form action="searchCaseResults" >
				<fieldset class="form">
				<div>
					<label for="procedureDate">
						<g:message code="default.date.procedureDate" default="Procedure Date" />
					</label>
					<g:textField type="date" name="procedureDate" id="procedureDate" value="${procedureDate}" />
				</div>
				<div>
					<label for="mrn">
						<g:message code="patient.mrn.label" default="MRN" />
					</label>
					<g:textField name="mrn" value="${mrn}" />
				</div>
				<div>
					<label for="lastName">
						<g:message code="patient.lastName.label" default="Last Name" />
					</label>
					<g:textField name="lastName" value="${lastName}" />
				</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="searchCaseResults" class="save" value="${message(code: 'default.button.searchCaseResults.label', default: 'Search Case Results')}" />
				</fieldset>
			</g:form>
			
			<g:if test="${procedureInstanceList}">
			<div class="allowTableOverflow">
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="procedureDate" title="${message(code: 'procedure.procedureDate.label', default: 'Procedure Date')}" />
						
						<th><g:message code="patient.mrn.label" default="MRN" /></th>
						
						<th><g:message code="patient.lastName.label" default="Last Name" /></th>
					
						<th><g:message code="procedure.procedureType.label" default="Procedure Type" /></th>
					
						<th><g:message code="procedure.procedureName.label" default="Procedure Name" /></th>
						
						<th><g:message code="procedure.subProcedureName.label" default="Sub Procedure Name" /></th>
						
						<th><g:message code="procedure.procedureDate.label" default="Procedure Date" /></th>
						
						<th><g:message code="procedure.endTimeOfProcedure.label" default="End Time" /></th>
					
						<th><g:message code="procedure.procurementResult.label" default="Procurement Result" /></th>
						
						<th><g:message code="subspecimen.number.label" default="# Subspecimens" /></th>
						
						<th><g:message code="procedure.medComments.label" default="Med Comments" /></th>
						
						<th><g:message code="procedure.origin.label" default="Origin" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${procedureInstanceList}" status="i" var="procedureInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${procedureInstance.id}"><g:formatDate format="MM/dd/yyyy" date="${procedureInstance.procedureDate}"/></g:link></td>
						
						<td><g:link controller="patient" action="show" id="${procedureInstance.patient.id}">${fieldValue(bean: procedureInstance, field: "patient.mrn")}</g:link></td>
						
						<td>${fieldValue(bean: procedureInstance, field: "patient.lastName")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procedureType")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procedureName")}</td>
						
						<td>${fieldValue(bean: procedureInstance, field: "subProcedureName")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${procedureInstance.procedureDate}"/></td>
						
						<td><g:formatDate format="HH:mm aa" date="${procedureInstance.endTimeOfProcedure}"/></td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procurementResult")}</td>
						
						<td>${procedureInstance.getNumberOfSubspecimens()}</td>
						
						<td>${fieldValue(bean: procedureInstance, field: "medComments")}</td>
						
						<td>${fieldValue(bean: procedureInstance, field: "origin")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			</div>
			</g:if> <!-- End If checking procedureInstanceList has anything in it-->
		</div>
	</body>
</html>
