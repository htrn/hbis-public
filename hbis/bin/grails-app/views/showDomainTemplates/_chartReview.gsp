<div class="leftShowColumn">
<ul class="property-list chartReview">
	<li class="fieldcontain">
		<span id="yearOfBirth-label" class="property-label"><g:message code="chartReview.yearOfBirth.label" default="Year Of Birth: " /></span>
		<span class="property-value" aria-labelledby="yearOfBirth-label"><g:fieldValue bean="${chartReviewInstance}" field="yearOfBirth"/></span>
		
		<span id="yearOfDeath-label" class="property-label"><g:message code="chartReview.yearOfDeath.label" default="Year Of Death: " /></span>
		<span class="property-value" aria-labelledby="yearOfDeath-label"><g:fieldValue bean="${chartReviewInstance}" field="yearOfDeath"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="height-label" class="property-label"><g:message code="chartReview.height.label" default="Height: " /></span>
		<span class="property-value" aria-labelledby="height-label"><g:fieldValue bean="${chartReviewInstance}" field="height"/></span>
		
		<span id="weight-label" class="property-label"><g:message code="chartReview.weight.label" default="Weight: " /></span>
		<span class="property-value" aria-labelledby="weight-label"><g:fieldValue bean="${chartReviewInstance}" field="weight"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="obstetrics-label" class="property-label">Obstetrics (if female)</span>
		<span id="menstrualHistory-label" class="property-label"><g:message code="chartReview.menstrualHistory.label" default="Menstrual History: " /></span>
		<span class="property-value" aria-labelledby="menstrualHistory-label"><g:fieldValue bean="${chartReviewInstance}" field="menstrualHistory"/></span>
		
		<span id="lastMenstrualPeriod-label" class="property-label"><g:message code="chartReview.lastMenstrualPeriod.label" default="Last Menstrual Period: " /></span>
		<span class="property-value" aria-labelledby="lastMenstrualPeriod-label"><g:formatDate date="${chartReviewInstance?.lastMenstrualPeriod}" /></span>
	</li>
	
	<li class="fieldcontain">
		<span id="currentlyPregnant-label" class="property-label"><g:message code="chartReview.currentlyPregnant.label" default="Currently Pregnant: " /></span>
		<span class="property-value" aria-labelledby="currentlyPregnant-label"><g:fieldValue bean="${chartReviewInstance}" field="currentlyPregnant"/></span>
		
		<span id="numberOfPregnancies-label" class="property-label"><g:message code="chartReview.numberOfPregnancies.label" default="Number Of Pregnancies: " /></span>
		<span class="property-value" aria-labelledby="numberOfPregnancies-label"><g:fieldValue bean="${chartReviewInstance}" field="numberOfPregnancies"/></span>
		
		<span id="numberOfLiveBirths-label" class="property-label"><g:message code="chartReview.numberOfLiveBirths.label" default="Number Of Live Births: " /></span>
		<span class="property-value" aria-labelledby="numberOfLiveBirths-label"><g:fieldValue bean="${chartReviewInstance}" field="numberOfLiveBirths"/></span>
	</li>
</ul>
</div> <!-- End leftShowColumn -->
	
<div class="middleShowColumn">
<ul class="property-list chartReview">
	<li class="fieldcontain">
		<span id="specialDietStatus-label" class="property-label"><g:message code="chartReview.specialDietStatus.label" default="Special Diet Status: " /></span>
		<span class="property-value" aria-labelledby="specialDietStatus-label">${chartReviewInstance?.specialDietStatus?.encodeAsHTML()}</span>
		
		<span id="specialDietOtherSpecify-label" class="property-label"><g:message code="chartReview.specialDietOtherSpecify.label" default="Special Diet Other Specify: " /></span>
		<span class="property-value" aria-labelledby="specialDietOtherSpecify-label"><g:fieldValue bean="${chartReviewInstance}" field="specialDietOtherSpecify"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="specialDietNotes-label" class="property-label"><g:message code="chartReview.specialDietNotes.label" default="Special Diet Notes: " /></span>
		<span class="property-value" aria-labelledby="specialDietNotes-label"><g:fieldValue bean="${chartReviewInstance}" field="specialDietNotes"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="smokingHistoryStatus-label" class="property-label"><g:message code="chartReview.smokingHistoryStatus.label" default="Smoking History Status: " /></span>
		<span class="property-value" aria-labelledby="smokingHistoryStatus-label">${chartReviewInstance?.smokingHistoryStatus?.encodeAsHTML()}</span>
		
		<span id="smokingHistoryWhenQuit-label" class="property-label"><g:message code="chartReview.smokingHistoryWhenQuit.label" default="Smoking History When Quit (Year): " /></span>
		<span class="property-value" aria-labelledby="smokingHistoryWhenQuit-label"><g:fieldValue bean="${chartReviewInstance}" field="smokingHistoryWhenQuit"/></span>
	</li>

	<li class="fieldcontain">
		<span id="smokingHistoryAmountPacksPerDay-label" class="property-label"><g:message code="chartReview.smokingHistoryAmountPacksPerDay.label" default="Smoking History Amount Packs Per Day: " /></span>
		<span class="property-value" aria-labelledby="smokingHistoryAmountPacksPerDay-label"><g:fieldValue bean="${chartReviewInstance}" field="smokingHistoryAmountPacksPerDay"/></span>
		
		<span id="smokingHistoryDuration-label" class="property-label"><g:message code="chartReview.smokingHistoryDuration.label" default="Smoking History Duration (Years): " /></span>
		<span class="property-value" aria-labelledby="smokingHistoryDuration-label"><g:fieldValue bean="${chartReviewInstance}" field="smokingHistoryDuration"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="alcoholConsumptionStatus-label" class="property-label"><g:message code="chartReview.alcoholConsumptionStatus.label" default="Alcohol Consumption Status: " /></span>
		<span class="property-value" aria-labelledby="alcoholConsumptionStatus-label">${chartReviewInstance?.alcoholConsumptionStatus?.encodeAsHTML()}</span>
		
		<span id="alcoholConsumptionWhenQuit-label" class="property-label"><g:message code="chartReview.alcoholConsumptionWhenQuit.label" default="Alcohol Consumption When Quit (Year):" /></span>
		<span class="property-value" aria-labelledby="alcoholConsumptionWhenQuit-label"><g:fieldValue bean="${chartReviewInstance}" field="alcoholConsumptionWhenQuit"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="alcoholConsumptionAmountPerWeek-label" class="property-label"><g:message code="chartReview.alcoholConsumptionAmountPerWeek.label" default="Alcohol Consumption Amount Per Week: " /></span>
		<span class="property-value" aria-labelledby="alcoholConsumptionAmountPerWeek-label"><g:fieldValue bean="${chartReviewInstance}" field="alcoholConsumptionAmountPerWeek"/></span>
		
		<span id="alcoholConsumptionDuration-label" class="property-label"><g:message code="chartReview.alcoholConsumptionDuration.label" default="Alcohol Consumption Duration (Years): " /></span>
		<span class="property-value" aria-labelledby="alcoholConsumptionDuration-label"><g:fieldValue bean="${chartReviewInstance}" field="alcoholConsumptionDuration"/></span>
	</li>
</ul>
</div> <!-- End middleShowColumn -->
	
<div class="rightShowColumn">
<ul class="property-list chartReview">
	<li class="fieldcontain">
		<span id="recreationalDrugsStatus-label" class="property-label"><g:message code="chartReview.recreationalDrugsStatus.label" default="Recreational Drugs Status: " /></span>
		<span class="property-value" aria-labelledby="recreationalDrugsStatus-label">${chartReviewInstance?.recreationalDrugsStatus?.encodeAsHTML()}</span>
		
		<span id="recreationalDrugsNotes-label" class="property-label"><g:message code="chartReview.recreationalDrugsNotes.label" default="Recreational Drugs Notes: " /></span>
		<span class="property-value" aria-labelledby="recreationalDrugsNotes-label"><g:fieldValue bean="${chartReviewInstance}" field="recreationalDrugsNotes"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="medicalHistories-label" class="property-label"><g:message code="chartReview.medicalHistories.label" default="Medical Histories: " /></span>
		<g:each in="${chartReviewInstance.medicalHistories}" var="m">
			<span class="property-value" aria-labelledby="medicalHistories-label"><g:link controller="medicalHistory" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
	
	<li class="fieldcontain">
		<span id="surgicalHistories-label" class="property-label"><g:message code="chartReview.surgicalHistories.label" default="Surgical Histories: " /></span>
		<g:each in="${chartReviewInstance.surgicalHistories}" var="s">
			<span class="property-value" aria-labelledby="surgicalHistories-label"><g:link controller="surgicalHistory" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
	
	<li class="fieldcontain">
		<span id="medications-label" class="property-label"><g:message code="chartReview.medications.label" default="Medications: " /></span>
		<g:each in="${chartReviewInstance.medications}" var="m">
			<span class="property-value" aria-labelledby="medications-label"><g:link controller="medication" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
	
	<li class="fieldcontain">
		<span id="familyHistories-label" class="property-label"><g:message code="chartReview.familyHistories.label" default="Family Histories: " /></span>
		<g:each in="${chartReviewInstance.familyHistories}" var="f">
			<span class="property-value" aria-labelledby="familyHistories-label"><g:link controller="familyHistory" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
	
	<li class="fieldcontain">
		<span id="additionalNotes-label" class="property-label"><g:message code="chartReview.additionalNotes.label" default="Additional Notes: " /></span>
		<span class="property-value" aria-labelledby="additionalNotes-label"><g:fieldValue bean="${chartReviewInstance}" field="additionalNotes"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="requestVerifiedBy-label" class="property-label"><g:message code="chartReview.requestVerifiedBy.label" default="Request Verified By: " /></span>
		<span class="property-value" aria-labelledby="requestVerifiedBy-label"><g:fieldValue bean="${chartReviewInstance}" field="requestVerifiedBy"/></span>
		
		<span id="dateVerified-label" class="property-label"><g:message code="chartReview.dateVerified.label" default="Date Verified: " /></span>
		<span class="property-value" aria-labelledby="dateVerified-label"><g:formatDate date="${chartReviewInstance?.dateVerified}" /></span>
	</li>
			
	<li class="fieldcontain">
		<span id="requestCompletedBy-label" class="property-label"><g:message code="chartReview.requestCompletedBy.label" default="Request Completed By: " /></span>
		<span class="property-value" aria-labelledby="requestCompletedBy-label"><g:fieldValue bean="${chartReviewInstance}" field="requestCompletedBy"/></span>
		
		<span id="dateCompleted-label" class="property-label"><g:message code="chartReview.dateCompleted.label" default="Date Completed: " /></span>
		<span class="property-value" aria-labelledby="dateCompleted-label"><g:formatDate date="${chartReviewInstance?.dateCompleted}" /></span>
	</li>
	<li class="fieldcontain">
		<span id="shippingCarts-label" class="property-label"><g:message code="chartReview.shippingCarts.label" default="Shipping Carts" /></span>
		<g:each in="${chartReviewInstance.shippingCarts}" var="sc">
			<span class="property-value" aria-labelledby="shippingCarts-label"><g:link controller="shippingCart" action="show" id="${sc.id}">${sc?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
</ul>
</div> <!-- End rightShowColumn -->