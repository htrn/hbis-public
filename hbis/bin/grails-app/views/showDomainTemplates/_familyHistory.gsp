<div class="leftShowColumn">
<ul class="property-list familyHistory">
	<li class="fieldcontain">
		<span id="relative-label" class="property-label"><g:message code="familyHistory.relative.label" default="Relative" /></span>
		<span class="property-value" aria-labelledby="relative-label"><g:fieldValue bean="${familyHistoryInstance}" field="relative"/></span>
	</li>
	<li class="fieldcontain">
		<span id="diagnosis-label" class="property-label"><g:message code="familyHistory.diagnosis.label" default="Diagnosis" /></span>
		<span class="property-value" aria-labelledby="diagnosis-label"><g:link controller="diagnosis" action="show" id="${familyHistoryInstance?.diagnosis?.id}">${familyHistoryInstance?.diagnosis?.encodeAsHTML()}</g:link></span>
	</li>
	<li class="fieldcontain">
		<span id="chartReview-label" class="property-label"><g:message code="familyHistory.chartReview.label" default="Chart Review" /></span>
		<span class="property-value" aria-labelledby="chartReview-label"><g:link controller="chartReview" action="show" id="${familyHistoryInstance?.chartReview?.id}">${familyHistoryInstance?.chartReview?.encodeAsHTML()}</g:link></span>
	</li>
</ul>
</div>