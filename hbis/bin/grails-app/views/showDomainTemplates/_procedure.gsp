<div class="leftShowColumn">
<ul class="property-list procedure">
	<li class="fieldcontain">
		<span id="procedureDate-label" class="property-label"><g:message code="procedure.procedureDate.label" default="Procedure Date" /></span>
		<span class="property-value" aria-labelledby="procedureDate-label"><g:formatDate format="MM/dd/yyyy" date="${procedureInstance?.procedureDate}" /></span>
	</li>
	<li class="fieldcontain">
		<span id="endTimeOfProcedure-label" class="property-label"><g:message code="procedure.endTimeOfProcedure.label" default="End Time Of Procedure" /></span>
		<span class="property-value" aria-labelledby="endTimeOfProcedure-label"><g:formatDate format="hh:mm aa" date="${procedureInstance?.endTimeOfProcedure}" /></span>
	</li>	
			
	<li class="fieldcontain">
		<span id="procedureType-label" class="property-label"><g:message code="procedure.procedureType.label" default="Procedure Type" /></span>
		<span class="property-value" aria-labelledby="procedureType-label">${procedureInstance?.procedureType?.encodeAsHTML()}</span>
	</li>
	<li class="fieldcontain">				
		<span id="procedureName-label" class="property-label"><g:message code="procedure.procedureName.label" default="Procedure Name" /></span>
		<span class="property-value" aria-labelledby="procedureName-label">${procedureInstance?.procedureName?.encodeAsHTML()}</span>
	</li>
	<li class="fieldcontain">				
		<span id="subProcedureName-label" class="property-label"><g:message code="procedure.subProcedureName.label" default="Sub Procedure Name" /></span>
		<span class="property-value" aria-labelledby="subProcedureName-label">${procedureInstance?.subProcedureName?.encodeAsHTML()}</span>
	</li>
	<li class="fieldcontain">				
		<span id="procurementResult-label" class="property-label"><g:message code="procedure.procurementResult.label" default="Procurement Result" /></span>
		<span class="property-value" aria-labelledby="procurementResult-label">${procedureInstance?.procurementResult?.encodeAsHTML()}</span>
	</li>
			
	<li class="fieldcontain">
		<span id="patientAge-label" class="property-label"><g:message code="procedure.patientAge.label" default="Patient Age" /></span>
		<span class="property-value" aria-labelledby="patientAge-label"><g:fieldValue bean="${procedureInstance}" field="patientAge"/></span>
					
		<span id="patientAgeUnit-label" class="property-label"><g:message code="procedure.patientAgeUnit.label" default="Patient Age Unit" /></span>
		<span class="property-value" aria-labelledby="patientAgeUnit-label">${procedureInstance?.patientAgeUnit?.encodeAsHTML()}</span>
	</li>
			
	<li class="fieldcontain">
		<span id="dateOfDeath-label" class="property-label"><g:message code="procedure.dateOfDeath.label" default="Date Of Death" /></span>
		<span class="property-value" aria-labelledby="dateOfDeath-label"><g:formatDate format="MM/dd/yyyy" date="${procedureInstance?.dateOfDeath}" /></span>
	</li>
</ul>
</div> <!-- End leftShowColumn -->

<div class="middleShowColumn">
<ul class="property-list procedure">
	<li class="fieldcontain">
		<span id="chemoHistory-label" class="property-label"><g:message code="procedure.chemoHistory.label" default="Chemo History" /></span>
		<span class="property-value" aria-labelledby="chemoHistory-label">${procedureInstance?.chemoHistory?.encodeAsHTML()}</span>
					
		<span id="chemoHistoryComments-label" class="property-label"><g:message code="procedure.chemoHistoryComments.label" default="Chemo History Comments" /></span>
		<span class="property-value" aria-labelledby="chemoHistoryComments-label"><g:fieldValue bean="${procedureInstance}" field="chemoHistoryComments"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="radiationHistory-label" class="property-label"><g:message code="procedure.radiationHistory.label" default="Radiation History" /></span>
		<span class="property-value" aria-labelledby="radiationHistory-label">${procedureInstance?.radiationHistory?.encodeAsHTML()}</span>
					
		<span id="radiationHistoryComments-label" class="property-label"><g:message code="procedure.radiationHistoryComments.label" default="Radiation History Comments" /></span>
		<span class="property-value" aria-labelledby="radiationHistoryComments-label"><g:fieldValue bean="${procedureInstance}" field="radiationHistoryComments"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="procedureEventInstitution-label" class="property-label"><g:message code="procedure.procedureEventInstitution.label" default="Procedure Event Institution" /></span>
		<span class="property-value" aria-labelledby="procedureEventInstitution-label"><g:fieldValue bean="${procedureInstance}" field="procedureEventInstitution"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="building-label" class="property-label"><g:message code="procedure.building.label" default="Building" /></span>
		<span class="property-value" aria-labelledby="building-label"><g:fieldValue bean="${procedureInstance}" field="building"/></span>
					
		<span id="roomNumber-label" class="property-label"><g:message code="procedure.roomNumber.label" default="Room Number" /></span>
		<span class="property-value" aria-labelledby="roomNumber-label"><g:fieldValue bean="${procedureInstance}" field="roomNumber"/></span>
	</li>
</ul>
</div> <!-- End middleShowColumn -->

<div class="rightShowColumn">
<ul class="property-list procedure">	
	<li class="fieldcontain">
		<span id="origin-label" class="property-label"><g:message code="procedure.origin.label" default="Origin" /></span>
		<span class="property-value" aria-labelledby="origin-label"><g:fieldValue bean="${procedureInstance}" field="origin"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="medComments-label" class="property-label"><g:message code="procedure.medComments.label" default="Med Comments" /></span>
		<span class="property-value" aria-labelledby="medComments-label"><g:fieldValue bean="${procedureInstance}" field="medComments"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="specimens-label" class="property-label"><g:message code="procedure.specimens.label" default="Specimens" /></span>
		<g:each in="${procedureInstance?.specimens?}" var="s">
			<span class="property-value" aria-labelledby="specimens-label"><g:link controller="specimen" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
			
	<li class="fieldcontain">
		<span id="chartReview-label" class="property-label"><g:message code="procedure.chartReview.label" default="Chart Review" /></span>
		<span class="property-value" aria-labelledby="chartReview-label"><g:link controller="chartReview" action="show" id="${procedureInstance?.chartReview?.id}">${procedureInstance?.chartReview?.encodeAsHTML()}</g:link></span>
	</li>
</ul>
</div> <!-- End rightShowColumn -->