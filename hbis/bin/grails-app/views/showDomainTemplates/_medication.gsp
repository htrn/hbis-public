<div class="leftShowColumn">
<ul class="property-list medication">
	<li class="fieldcontain">
		<span id="medicationName-label" class="property-label"><g:message code="medication.medicationName.label" default="Medication Name" /></span>
		<span class="property-value" aria-labelledby="medicationName-label"><g:fieldValue bean="${medicationInstance}" field="medicationName"/></span>
	</li>
	<li class="fieldcontain">
		<span id="doseMg-label" class="property-label"><g:message code="medication.doseMg.label" default="Dose Mg" /></span>
		<span class="property-value" aria-labelledby="doseMg-label"><g:fieldValue bean="${medicationInstance}" field="doseMg"/></span>
	</li>
	<li class="fieldcontain">
		<span id="doseOther-label" class="property-label"><g:message code="medication.doseOther.label" default="Dose Other" /></span>
		<span class="property-value" aria-labelledby="doseOther-label"><g:fieldValue bean="${medicationInstance}" field="doseOther"/></span>
	</li>
	<li class="fieldcontain">
		<span id="frequency-label" class="property-label"><g:message code="medication.frequency.label" default="Frequency" /></span>
		<span class="property-value" aria-labelledby="frequency-label"><g:fieldValue bean="${medicationInstance}" field="frequency"/></span>
	</li>
	<li class="fieldcontain">
		<span id="status-label" class="property-label"><g:message code="medication.status.label" default="Status" /></span>
		<span class="property-value" aria-labelledby="status-label"><g:link controller="medicalStatus" action="show" id="${medicationInstance?.status?.id}">${medicationInstance?.status?.encodeAsHTML()}</g:link></span>
	</li>
	<li class="fieldcontain">
		<span id="chartReview-label" class="property-label"><g:message code="medication.chartReview.label" default="Chart Review" /></span>
		<span class="property-value" aria-labelledby="chartReview-label"><g:link controller="chartReview" action="show" id="${medicationInstance?.chartReview?.id}">${medicationInstance?.chartReview?.encodeAsHTML()}</g:link></span>
	</li>
</ul>
</div>