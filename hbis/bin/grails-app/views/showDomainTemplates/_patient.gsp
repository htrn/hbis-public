<div class="leftShowColumn">
<ul class="property-list patient">
	<li class="fieldcontain">
		<span id="mrn-label" class="property-label"><g:message code="patient.mrn.label" default="MRN" /></span>
		<span class="property-value" aria-labelledby="mrn-label"><g:fieldValue bean="${patientInstance}" field="mrn"/></span>
	</li>
	<li class="fieldcontain">
		<span id="firstName-label" class="property-label"><g:message code="patient.firstName.label" default="First Name" /></span>
		<span class="property-value" aria-labelledby="firstName-label"><g:fieldValue bean="${patientInstance}" field="firstName"/></span>
	</li>
	<li class="fieldcontain">		
		<span id="middleInitial-label" class="property-label"><g:message code="patient.middleInitial.label" default="Middle Initial" /></span>
		<span class="property-value" aria-labelledby="middleInitial-label"><g:fieldValue bean="${patientInstance}" field="middleInitial"/></span>
	</li>
	<li class="fieldcontain">		
		<span id="lastName-label" class="property-label"><g:message code="patient.lastName.label" default="Last Name" /></span>
		<span class="property-value" aria-labelledby="lastName-label"><g:fieldValue bean="${patientInstance}" field="lastName"/></span>
	</li>
</ul>
</div>
<div class="middleShowColumn">
<ul class="property-list patient">
	<li class="fieldcontain">
		<span id="gender-label" class="property-label"><g:message code="patient.gender.label" default="Gender" /></span>
		<span class="property-value" aria-labelledby="gender-label"><g:link controller="gender" action="show" id="${patientInstance?.gender?.id}">${patientInstance?.gender?.encodeAsHTML()}</g:link></span>
	</li>
	<li class="fieldcontain">
		<span id="race-label" class="property-label"><g:message code="patient.race.label" default="Race" /></span>
		<span class="property-value" aria-labelledby="race-label"><g:link controller="race" action="show" id="${patientInstance?.race?.id}">${patientInstance?.race?.encodeAsHTML()}</g:link></span>
	</li>
	<li class="fieldcontain">
		<span id="dateOfBirth-label" class="property-label"><g:message code="patient.dateOfBirth.label" default="Date Of Birth" /></span>
		<span class="property-value" aria-labelledby="dateOfBirth-label"><g:formatDate format="MM/dd/yyyy" date="${patientInstance?.dateOfBirth}"/></span>
	</li>
	<g:if test="${patientInstance?.procedures}">
		<li class="fieldcontain">
			<span id="procedures-label" class="property-label"><g:message code="patient.procedures.label" default="Procedures" /></span>
			<g:each in="${patientInstance?.procedures?}" var="p">
				<span class="property-value" aria-labelledby="procedures-label"><g:link controller="procedure" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
			</g:each>
		</li>
	</g:if>
	<g:if test="${patientInstance?.consents}">
		<li class="fieldcontain">
			<span id="consents-label" class="property-label"><g:message code="patient.consents.label" default="Consents" /></span>
			<g:each in="${patientInstance?.consents?}" var="c">
				<span class="property-value" aria-labelledby="consents-label"><g:link controller="consent" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
			</g:each>
		</li>
	</g:if>
</ul>
</div>
