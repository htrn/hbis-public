<div class="leftShowColumn">
<ul class="property-list shippingCart">
	<li class="fieldcontain">
		<span id="id-label" class="property-label"><g:message code="shippingCart.id.label" default="Shipping Cart ID" /></span>
		<span class="property-value" aria-labelledby="id-label"><g:fieldValue bean="${shippingCartInstance}" field="id"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="status-label" class="property-label"><g:message code="shippingCart.status.label" default="Status" /></span>
		<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${shippingCartInstance}" field="status"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="description-label" class="property-label"><g:message code="shippingCart.description.label" default="Description" /></span>
		<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${shippingCartInstance}" field="description"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="attentionTo-label" class="property-label"><g:message code="shippingCart.attentionTo.label" default="Attention To" /></span>
		<span class="property-value" aria-labelledby="attentionTo-label"><g:fieldValue bean="${shippingCartInstance}" field="attentionTo"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="institution-label" class="property-label"><g:message code="shippingCart.institution.label" default="Institution" /></span>
		<span class="property-value" aria-labelledby="institution-label"><g:fieldValue bean="${shippingCartInstance}" field="institution"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="streetAddress1-label" class="property-label"><g:message code="shippingCart.streetAddress1.label" default="Street Address 1" /></span>
		<span class="property-value" aria-labelledby="streetAddress1-label"><g:fieldValue bean="${shippingCartInstance}" field="streetAddress1"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="streetAddress2-label" class="property-label"><g:message code="shippingCart.streetAddress2.label" default="Street Address 2" /></span>
		<span class="property-value" aria-labelledby="streetAddress2-label"><g:fieldValue bean="${shippingCartInstance}" field="streetAddress2"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="city-label" class="property-label"><g:message code="shippingCart.city.label" default="City" /></span>
		<span class="property-value" aria-labelledby="city-label"><g:fieldValue bean="${shippingCartInstance}" field="city"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="state-label" class="property-label"><g:message code="shippingCart.state.label" default="State" /></span>
		<span class="property-value" aria-labelledby="state-label"><g:fieldValue bean="${shippingCartInstance}" field="state"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="zipCode-label" class="property-label"><g:message code="shippingCart.zipCode.label" default="Zip Code" /></span>
		<span class="property-value" aria-labelledby="zipCode-label"><g:fieldValue bean="${shippingCartInstance}" field="zipCode"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="country-label" class="property-label"><g:message code="shippingCart.country.label" default="Country" /></span>
		<span class="property-value" aria-labelledby="country-label"><g:fieldValue bean="${shippingCartInstance}" field="country"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="phone-label" class="property-label"><g:message code="shippingCart.phone.label" default="Phone" /></span>
		<span class="property-value" aria-labelledby="phone-label"><g:fieldValue bean="${shippingCartInstance}" field="phone"/></span>
	</li>
</ul>
</div> <!-- End leftShowColumn -->

<div class="middleShowColumn">
<ul class="property-list shippingCart">
	<li class="fieldcontain">
		<span id="poOrRefNumber-label" class="property-label"><g:message code="shippingCart.poOrRefNumber.label" default="PO Or Reference Number" /></span>
		<span class="property-value" aria-labelledby="poOrRefNumber-label"><g:fieldValue bean="${shippingCartInstance}" field="poOrRefNumber"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="eRampNumber-label" class="property-label"><g:message code="shippingCart.eRampNumber.label" default="eRamp Number" /></span>
		<span class="property-value" aria-labelledby="eRampNumber-label"><g:fieldValue bean="${shippingCartInstance}" field="eRampNumber"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shipDate-label" class="property-label"><g:message code="shippingCart.shipDate.label" default="Ship Date" /></span>
		<span class="property-value" aria-labelledby="shipDate-label"><g:formatDate date="${shippingCartInstance?.shipDate}" /></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shipMode-label" class="property-label"><g:message code="shippingCart.shipMode.label" default="Ship Mode" /></span>
		<span class="property-value" aria-labelledby="shipMode-label"><g:link controller="shipMode" action="show" id="${shippingCartInstance?.shipMode?.id}">${shippingCartInstance?.shipMode?.encodeAsHTML()}</g:link></span>
	</li>
	
	<li class="fieldcontain">
		<span id="deliveryMethodAccount-label" class="property-label"><g:message code="shippingCart.deliveryMethodAccount.label" default="Delivery Method Account" /></span>
		<span class="property-value" aria-labelledby="deliveryMethodAccount-label"><g:fieldValue bean="${shippingCartInstance}" field="deliveryMethodAccount"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="trackingNumber-label" class="property-label"><g:message code="shippingCart.trackingNumber.label" default="Tracking Number" /></span>
		<span class="property-value" aria-labelledby="trackingNumber-label"><g:fieldValue bean="${shippingCartInstance}" field="trackingNumber"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="shippingCost-label" class="property-label"><g:message code="shippingCart.shippingCost.label" default="Shipping Cost" /></span>
		<span class="property-value" aria-labelledby="shippingCost-label"><g:fieldValue bean="${shippingCartInstance}" field="shippingCost"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="commentsOther-label" class="property-label"><g:message code="shippingCart.commentsOther.label" default="Other Comments" /></span>
		<span class="property-value" aria-labelledby="commentsOther-label"><g:fieldValue bean="${shippingCartInstance}" field="commentsOther"/></span>
	</li>
</ul>
</div> <!-- End middleShowColumn -->

<div class="rightShowColumn">
<ul class="property-list shippingCart">		
	<li class="fieldcontain">
		<span id="investigator-label" class="property-label"><g:message code="shippingCart.investigator.label" default="Investigator" /></span>
		<span class="property-value" aria-labelledby="investigator-label"><g:link controller="investigator" action="show" id="${shippingCartInstance?.investigator?.id}">${shippingCartInstance?.investigator?.encodeAsHTML()}</g:link></span>
	</li>
			
	<li class="fieldcontain">
		<span id="subspecimens-label" class="property-label"><g:message code="shippingCart.subspecimens.label" default="Subspecimens" /></span>
		<g:each in="${shippingCartInstance.subspecimens}" var="s">
			<span class="property-value" aria-labelledby="subspecimens-label"><g:link controller="subspecimen" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
	
	<li class="fieldcontain">
		<span id="invoice-label" class="property-label"><g:message code="shippingCart.invoice.label" default="Invoice" /></span>
		<span class="property-value" aria-labelledby="invoice-label"><g:link controller="invoice" action="show" id="${shippingCartInstance?.invoice?.id}">${shippingCartInstance?.invoice?.encodeAsHTML()}</g:link></span>
	</li>
</ul>
</div> <!-- End rightShowColumn -->