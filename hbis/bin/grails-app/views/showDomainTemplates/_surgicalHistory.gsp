<div class="leftShowColumn">
<ul class="property-list surgicalHistory">
	<li class="fieldcontain">
		<span id="anesthetics-label" class="property-label"><g:message code="surgicalHistory.anesthetics.label" default="Anesthetics" /></span>
		<span class="property-value" aria-labelledby="anesthetics-label"><g:fieldValue bean="${surgicalHistoryInstance}" field="anesthetics"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="forBiosample-label" class="property-label"><g:message code="surgicalHistory.forBiosample.label" default="For Biosample" /></span>
		<span class="property-value" aria-labelledby="forBiosample-label"><g:link controller="yesNo" action="show" id="${surgicalHistoryInstance?.forBiosample?.id}">${surgicalHistoryInstance?.forBiosample?.encodeAsHTML()}</g:link></span>
	</li>
	
	<li class="fieldcontain">
		<span id="procedure-label" class="property-label"><g:message code="surgicalHistory.procedure.label" default="Procedure" /></span>
		<span class="property-value" aria-labelledby="procedure-label"><g:link controller="procedureName" action="show" id="${surgicalHistoryInstance?.procedure?.id}">${surgicalHistoryInstance?.procedure?.encodeAsHTML()}</g:link></span>
	</li>

	<li class="fieldcontain">
		<span id="chartReview-label" class="property-label"><g:message code="surgicalHistory.chartReview.label" default="Chart Review" /></span>
		<span class="property-value" aria-labelledby="chartReview-label"><g:link controller="chartReview" action="show" id="${surgicalHistoryInstance?.chartReview?.id}">${surgicalHistoryInstance?.chartReview?.encodeAsHTML()}</g:link></span>
	</li>
</ul>
</div>