<div class="leftShowColumn">
<ul class="property-list subspecimen">
	<li class="fieldcontain">
		<span id="subspecimenChtnId-label" class="property-label"><g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen Chtn ID: " /></span>
		<span class="property-value" aria-labelledby="subspecimenChtnId-label"><g:fieldValue bean="${subspecimenInstance}" field="subspecimenChtnId"/></span>
		
		<span id="status-label" class="property-label"><g:message code="subspecimen.status.label" default="Status: " /></span>
		<span class="property-value" aria-labelledby="status-label"><g:link controller="subspecimenStatus" action="show" id="${subspecimenInstance?.status?.id}">${subspecimenInstance?.status?.encodeAsHTML()}</g:link></span>
		
		<span id="preparationType-label" class="property-label"><g:message code="subspecimen.preparationType.label" default="Preparation Type: " /></span>
		<span class="property-value" aria-labelledby="preparationType-label"><g:link controller="preparationType" action="show" id="${subspecimenInstance?.preparationType?.id}">${subspecimenInstance?.preparationType?.encodeAsHTML()}</g:link></span>
	</li>
			
	<li class="fieldcontain">
		<span id="preparationMedia-label" class="property-label"><g:message code="subspecimen.preparationMedia.label" default="Preparation Media: " /></span>
		<span class="property-value" aria-labelledby="preparationMedia-label"><g:fieldValue bean="${subspecimenInstance}" field="preparationMedia"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="weight-label" class="property-label"><g:message code="subspecimen.weight.label" default="Weight: " /></span>
		<span class="property-value" aria-labelledby="weight-label"><g:fieldValue bean="${subspecimenInstance}" field="weight"/></span>
		
		<span id="weightUnit-label" class="property-label"><g:message code="subspecimen.weightUnit.label" default="Weight Unit: " /></span>
		<span class="property-value" aria-labelledby="weightUnit-label"><g:fieldValue bean="${subspecimenInstance}" field="weightUnit"/></span></span>
		
		<span id="volume-label" class="property-label"><g:message code="subspecimen.volume.label" default="Volume: " /></span>
		<span class="property-value" aria-labelledby="volume-label"><g:fieldValue bean="${subspecimenInstance}" field="volume"/></span>
		
		<span id="volumeUnit-label" class="property-label"><g:message code="subspecimen.volumeUnit.label" default="Volume Unit: " /></span>
		<span class="property-value" aria-labelledby="volumeUnit-label"><g:fieldValue bean="${subspecimenInstance}" field="volumeUnit"/></span>
	</li>
</ul>
</div> <!-- End leftShowColumn -->

<div class="middleShowColumn">
<ul class="property-list subspecimen">
			
	<li class="fieldcontain">
		<span id="sizeWidth-label" class="property-label"><g:message code="subspecimen.sizeWidth.label" default="Size Width: " /></span>
		<span class="property-value" aria-labelledby="sizeWidth-label"><g:fieldValue bean="${subspecimenInstance}" field="sizeWidth"/></span>
		
		<span id="sizeLength-label" class="property-label"><g:message code="subspecimen.sizeLength.label" default="Size Length: " /></span>
		<span class="property-value" aria-labelledby="sizeLength-label"><g:fieldValue bean="${subspecimenInstance}" field="sizeLength"/></span>
		
		<span id="sizeHeight-label" class="property-label"><g:message code="subspecimen.sizeHeight.label" default="Size Height: " /></span>
		<span class="property-value" aria-labelledby="sizeHeight-label"><g:fieldValue bean="${subspecimenInstance}" field="sizeHeight"/></span>
		
		<span id="sizeUnit-label" class="property-label"><g:message code="subspecimen.sizeUnit.label" default="Size Unit: " /></span>
		<span class="property-value" aria-labelledby="sizeUnit-label"><g:fieldValue bean="${subspecimenInstance}" field="sizeUnit"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="comments-label" class="property-label"><g:message code="subspecimen.comments.label" default="Comments" /></span>
		<span class="property-value" aria-labelledby="comments-label"><g:fieldValue bean="${subspecimenInstance}" field="comments"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="labelComment-label" class="property-label"><g:message code="subspecimen.labelComment.label" default="Label Comment: " /></span>
		<span class="property-value" aria-labelledby="labelComment-label"><g:fieldValue bean="${subspecimenInstance}" field="labelComment"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="price-label" class="property-label"><g:message code="subspecimen.price.label" default="Price: " /></span>
		<span class="property-value" aria-labelledby="price-label"><g:fieldValue bean="${subspecimenInstance}" field="price"/></span>
	</li>
</ul>
</div> <!-- End middleShowColumn -->

<div class="rightShowColumn">
<ul class="property-list subspecimen">
			
	<li class="fieldcontain">
		<span id="dateCreated-label" class="property-label"><g:message code="subspecimen.dateCreated.label" default="Date Created" /></span>
		<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${subspecimenInstance?.dateCreated}" /></span>
	</li>
	
	<li class="fieldcontain">
		<span id="investigator-label" class="property-label"><g:message code="subspecimen.investigator.label" default="Investigator: " /></span>
		<span class="property-value" aria-labelledby="investigator-label"><g:link controller="investigator" action="show" id="${subspecimenInstance?.investigator?.id}">${subspecimenInstance?.investigator?.encodeAsHTML()}</g:link></span>

		<span id="tissueRequest-label" class="property-label"><g:message code="subspecimen.tissueRequest.label" default="Tissue Request" /></span>
		<span class="property-value" aria-labelledby="tissueRequest-label"><g:link controller="tissueRequest" action="show" id="${subspecimenInstance?.tissueRequest?.id}">${subspecimenInstance?.tissueRequest?.encodeAsHTML()}</g:link></span>
	</li>
	
	<li class="fieldcontain">
		<span id="needsChartReview-label" class="property-label"><g:message code="subspecimen.needsChartReview.label" default="Needs Chart Review " /></span>
		<span class="property-value" aria-labelledby="needsChartReview-label"><g:formatBoolean boolean="${subspecimenInstance.needsChartReview}" true="Yes" false="No"/></span>
	
		<span id="shippingCart-label" class="property-label"><g:message code="subspecimen.shippingCart.label" default="Shipping Cart: " /></span>
		<span class="property-value" aria-labelledby="shippingCart-label"><g:link controller="shippingCart" action="show" id="${subspecimenInstance?.shippingCart?.id}">${subspecimenInstance?.shippingCart?.encodeAsHTML()}</g:link></span>
	</li>
</ul>
</div> <!-- End rightShowColumn -->