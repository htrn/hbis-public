<div class="leftShowColumn">
<ul class="property-list specimen">
	<li class="fieldcontain">
	<span id="techInitials-label" class="property-label"><g:message code="specimen.techInitials.label" default="Tech Initials" /></span>
		<span class="property-value" aria-labelledby="techInitials-label"><g:fieldValue bean="${specimenInstance}" field="techInitials"/></span>
		
		<span id="specimenChtnId-label" class="property-label"><g:message code="specimen.specimenChtnId.label" default="Specimen Chtn ID" /></span>
		<span class="property-value" aria-labelledby="specimenChtnId-label"><g:fieldValue bean="${specimenInstance}" field="specimenChtnId"/></span>
		
		<span id="alternateSpecimenID-label" class="property-label"><g:message code="specimen.alternateSpecimenID.label" default="Alternate Specimen ID" /></span>
		<span class="property-value" aria-labelledby="alternateSpecimenID-label"><g:fieldValue bean="${specimenInstance}" field="alternateSpecimenID"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="procurementDate-label" class="property-label"><g:message code="specimen.procurementDate.label" default="Procurement Date" /></span>
		<span class="property-value" aria-labelledby="procurementDate-label"><g:formatDate date="${specimenInstance?.procurementDate}" /></span>
	</li>
			
	<li class="fieldcontain">
		<span id="specimenCategory-label" class="property-label"><g:message code="specimen.specimenCategory.label" default="Specimen Category" /></span>
		<span class="property-value" aria-labelledby="specimenCategory-label">${specimenInstance?.specimenCategory?.encodeAsHTML()}</span>
		
		<span id="preliminaryPrimaryAnatomicSite-label" class="property-label"><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></span>
		<span class="property-value" aria-labelledby="preliminaryPrimaryAnatomicSite-label">${specimenInstance?.preliminaryPrimaryAnatomicSite?.encodeAsHTML()}</span>
		
		<span id="side-label" class="property-label"><g:message code="specimen.side.label" default="Side" /></span>
		<span class="property-value" aria-labelledby="side-label"><g:link controller="side" action="show" id="${specimenInstance?.side?.id}">${specimenInstance?.side?.encodeAsHTML()}</g:link></span>
	</li>
</ul>
</div> <!-- End leftShowColumn -->

<div class="middleShowColumn">
<ul class="property-list procedure">
	<li class="fieldcontain">
		<span id="preliminaryTissueType-label" class="property-label"><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></span>
		<span class="property-value" aria-labelledby="preliminaryTissueType-label"><g:link controller="preliminaryTissueType" action="show" id="${specimenInstance?.preliminaryTissueType?.id}">${specimenInstance?.preliminaryTissueType?.encodeAsHTML()}</g:link></span>
		
		<span id="primaryOrMets-label" class="property-label"><g:message code="specimen.primaryOrMets.label" default="Primary Or Mets" /></span>
		<span class="property-value" aria-labelledby="primaryOrMets-label"><g:link controller="primaryMets" action="show" id="${specimenInstance?.primaryOrMets?.id}">${specimenInstance?.primaryOrMets?.encodeAsHTML()}</g:link></span>

		<span id="metsToAnatomicSite-label" class="property-label"><g:message code="specimen.metsToAnatomicSite.label" default="Mets To Anatomic Site" /></span>
		<span class="property-value" aria-labelledby="metsToAnatomicSite-label"><g:link controller="anatomicSite" action="show" id="${specimenInstance?.metsToAnatomicSite?.id}">${specimenInstance?.metsToAnatomicSite?.encodeAsHTML()}</g:link></span>
	</li>
			
	<li class="fieldcontain">
		<span id="preliminaryDiagnosis-label" class="property-label"><g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diagnosis" /></span>
		<span class="property-value" aria-labelledby="preliminaryDiagnosis-label"><g:link controller="diagnosis" action="show" id="${specimenInstance?.preliminaryDiagnosis?.id}">${specimenInstance?.preliminaryDiagnosis?.encodeAsHTML()}</g:link></span>
		
		<span id="preliminarySubDiagnosis-label" class="property-label"><g:message code="specimen.preliminarySubDiagnosis.label" default="Prelim. Sub Diagnosis" /></span>
		<span class="property-value" aria-labelledby="preliminarySubDiagnosis-label"><g:link controller="diagnosis" action="show" id="${specimenInstance?.preliminarySubDiagnosis?.id}">${specimenInstance?.preliminarySubDiagnosis?.encodeAsHTML()}</g:link></span>
		
		<span id="preliminaryModDiagnosis-label" class="property-label"><g:message code="specimen.preliminaryModDiagnosis.label" default="Prelim. Mod Diagnosis" /></span>
		<span class="property-value" aria-labelledby="preliminaryModDiagnosis-label"><g:link controller="diagnosis" action="show" id="${specimenInstance?.preliminaryModDiagnosis?.id}">${specimenInstance?.preliminaryModDiagnosis?.encodeAsHTML()}</g:link></span>
	</li>
			
	<li class="fieldcontain">
		<span id="timeReceived-label" class="property-label"><g:message code="specimen.timeReceived.label" default="Time Received" /></span>
		<span class="property-value" aria-labelledby="timeReceived-label"><g:formatDate format="hh:mm aa" date="${specimenInstance?.timeReceived}" /></span>
		
		<span id="timePrepared-label" class="property-label"><g:message code="specimen.timePrepared.label" default="Time Prepared" /></span>
		<span class="property-value" aria-labelledby="timePrepared-label"><g:formatDate format="hh:mm aa" date="${specimenInstance?.timePrepared}" /></span>
	</li>
</ul>
</div> <!-- End middleShowColumn -->

<div class="rightShowColumn">
<ul class="property-list procedure">
			
	<li class="fieldcontain">
		<span id="hoursPostAutopsy-label" class="property-label"><g:message code="specimen.hoursPostAutopsy.label" default="Hours Post Autopsy" /></span>
		<span class="property-value" aria-labelledby="hoursPostAutopsy-label"><g:fieldValue bean="${specimenInstance}" field="hoursPostAutopsy"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="comments-label" class="property-label"><g:message code="specimen.comments.label" default="Comments: " /></span>
		<span class="property-value" aria-labelledby="comments-label"><g:fieldValue bean="${specimenInstance}" field="comments"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="qcMethod-label" class="property-label"><g:message code="specimen.qcMethod.label" default="QC Method" /></span>
		<span class="property-value" aria-labelledby="qcMethod-label"><g:fieldValue bean="${specimenInstance}" field="qcMethod"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="qcBatch-label" class="property-label"><g:message code="specimen.qcBatch.label" default="QC Batch" /></span>
		<span class="property-value" aria-labelledby="qcBatch-label"><g:fieldValue bean="${specimenInstance}" field="qcBatch"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="qcResult-label" class="property-label"><g:message code="specimen.qcResult.label" default="QC Result" /></span>
		<span class="property-value" aria-labelledby="qcResult-label">${specimenInstance?.qcResult?.encodeAsHTML()}</span>
	</li>
			
	<li class="fieldcontain">
		<span id="subspecimens-label" class="property-label"><g:message code="specimen.subspecimen.label" default="Subspecimens: " /></span>
		<g:each in="${specimenInstance.subspecimens}" var="s">
			<span class="property-value" aria-labelledby="subspecimen-label"><g:link controller="subspecimen" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
</ul>
</div> <!-- End rightShowColumn -->