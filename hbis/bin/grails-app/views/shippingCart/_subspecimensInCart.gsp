<form>
    <table>
        <thead>
        <tr>
            <th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>

            <th><g:message code="subspecimen.subspecimenChtnId.label" default="Subsp. ID" /></th>

            <%--<th><g:message code="specimen.procurementDate.label" default="Procurement Date" /></th>--%>

            <th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>

            <th><g:message code="specimen.primaryOrMets.label" default="Primary/Mets" /></th>

            <th><g:message code="subspecimen.tissueRequest.label" default="Tissue REQ#" /></th>


            <g:if test="${shippingCartInstance.qcStatusFilter.description == 'Skip QC Check'}">

                <th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>

                <th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>

                <th><g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diagnosis" /></th>

            </g:if>
            <g:elseif test="${shippingCartInstance.qcStatusFilter.description == 'QC Complete'}">

                <th><g:message code="qcResult.finalPrimaryAnatomicSite.label" default="Final Primary Anatomic Site" /></th>

                <th><g:message code="qcResult.finalTissueType.label" default="Final Tissue Type" /></th>

                <th><g:message code="qcResult.finalDiagnosis.label" default="Final Diagnosis" /></th>

                <th><g:message code="qcBatch.status.label" default="QC Batch Status" /></th>

            </g:elseif>

            <th><g:message code="qcResult.tissueQcMethod.label" default="QC Method" /></th>

            <th><g:message code="qcResult.tissueQcMatch.label" default="Tissue QC Match" /></th>

            <th><g:message code="subspecimen.price.label" default="Price" /></th>

            <th><g:message code="subspecimen.comments.label" default="Comments" /></th>

            <th><g:message code="shippingCart.RemoveFromCart.label" default="Remove From Cart" /></th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${subspecimensInCart.sort{a, b -> a.specimen.specimenChtnId <=> b.specimen.specimenChtnId ?: a.subspecimenChtnId <=> b.subspecimenChtnId}}" status="i" var="subspecimenInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td>${fieldValue(bean: subspecimenInstance, field: "specimen.specimenChtnId")}</td>

                <td>${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</td>

                <%--<td><g:formatDate format="MM/dd/yyyy" date="${subspecimenInstance.specimen.procurementDate}"/></td>--%>

                <td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>

                <td>${fieldValue(bean: subspecimenInstance, field: "specimen.primaryOrMets")}</td>

                <td><g:link action="show" id="${subspecimenInstance.tissueRequest?.id}">${fieldValue(bean: subspecimenInstance, field: "tissueRequest.tissueRequestTissueQuestId")}</g:link></td>


                <g:if test="${shippingCartInstance.qcStatusFilter.description == 'Skip QC Check'}">

                    <td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryPrimaryAnatomicSite")}</td>

                    <td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryTissueType")}</td>

                    <td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryDiagnosis")}</td>

                </g:if>
                <g:elseif test="${shippingCartInstance.qcStatusFilter.description == 'QC Complete'}">

                    <td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.finalPrimaryAnatomicSite")}</td>

                    <td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.finalTissueType")}</td>

                    <td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.finalDiagnosis")}</td>

                    <td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcBatch.status")}</td>

                </g:elseif>

                <td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcMethod")}</td>

                <td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.tissueQcMatch")}</td>

                <td>${fieldValue(bean: subspecimenInstance, field: "price")}</td>

                <td>${fieldValue(bean: subspecimenInstance, field: "comments")}</td>

                <td>
                    <g:checkBox name="subspecimensToRemove" value="${subspecimenInstance.id}" checked="false"/>
                </td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <g:hiddenField name="addRemove" value="Remove" />
    <g:hiddenField name="shippingCartVersion" value="${shippingCartInstance.version}" />
    <g:hiddenField name="shippingCartId" value="${shippingCartInstance.id}" />
    <g:hiddenField name="fromDate" value="${fromDate}" />
    <g:hiddenField name="toDate" value="${toDate}" />
    <g:hiddenField name="preliminaryPrimaryAnatomicSiteId" value="${preliminaryPrimaryAnatomicSiteId}" />
    <g:hiddenField name="preliminaryTissueTypeId" value="${preliminaryTissueTypeId}" />

    <g:actionSubmit value="Remove From Cart" action="addRemoveSubspecimen"/>

</form>