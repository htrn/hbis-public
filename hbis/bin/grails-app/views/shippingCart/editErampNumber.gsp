
<%@ page import="hbis.ShippingCart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shippingCart.label', default: 'ShippingCart')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-shippingCart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-shippingCart" class="content scaffold-list" role="main">
			<h1>Search for Shipped Carts to Edit the eRamp Number</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${shippingCartInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${shippingCartInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			
			<g:form action="editErampNumber" >
					<div>
						<label for="investigator">
							<g:message code="subspecimen.investigator.label" default="Investigator" />
						</label>
						<g:select id="investigator" name="investigatorId" from="${hbis.Investigator.list()}" optionKey="id" required="" value="${investigatorId}" class="many-to-one" noSelection="['null': '']"/>
					</div>
					<div>
						<label for="invoiceId">
							<g:message code="shippingCart.id.label" default="Shipping Cart ID" />
						</label>
						<g:textField name="shippingCartId" value="${shippingCartId}" />
					</div>
				<fieldset class="buttons">
					<g:submitButton name="editErampNumber" class="save" value="${message(code: 'shippingCart.button.editErampNumberQuery.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${shippingCartInstanceList}">
				<div class="allowTableOverflow">
				<table>
					<thead>
						<tr>
							<th><g:message code="shippingCart.id.label" default="ID" />
							
							<th><g:message code="shippingCart.status.label" default="Status" /></th>
							
							<th><g:message code="shippingCart.shipDate.label" default="Ship Date" />
							
							<th><g:message code="shippingCart.investigator.label" default="Investigator" />
							
							<th><g:message code="shippingCart.description.label" default="Description" />
							
							<th><g:message code="shippingCart.institutionsAllowed.label" default="Inst. Allowed" /></th>
							
							<th><g:message code="subspecimen.number.label" default="# Subspecimens" /></th>
							
							<th><g:message code="subspecimen.number.label" default="# Chart Reviews" /></th>
						
							<th><g:message code="shippingCart.eRampNumber.label" default="eRamp Number" /></th>
							
							<th><g:message code="shippingCart.updateButton.label" default="" /></th>
						
						</tr>
					</thead>
					<tbody>
					<g:each in="${shippingCartInstanceList}" status="i" var="shippingCartInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
							<td><g:link action="show" id="${shippingCartInstance.id}">${fieldValue(bean: shippingCartInstance, field: "id")}</g:link></td>
							
							<td>${fieldValue(bean: shippingCartInstance, field: "status")}</td>
							
							<td><g:formatDate format="MM/dd/yyyy" date="${shippingCartInstance.shipDate}"/></td>
							
							<td>${fieldValue(bean: shippingCartInstance, field: "investigator")}</td>
							
							<td>${fieldValue(bean: shippingCartInstance, field: "description")}</td>
							
							<td>${fieldValue(bean: shippingCartInstance, field: "institutionsAllowed")}</td>
							
							<td>${shippingCartInstance.getNumberOfSubspecimens()}</td>
							
							<td>${shippingCartInstance.getNumberOfChartReviews()}</td>
							<g:form action="update">
								<g:hiddenField name="id" value="${shippingCartInstance.id}" />
								<g:hiddenField name="version" value="${shippingCartInstance.version}" />
								<g:hiddenField name="view" value="editErampNumber" />
								<g:hiddenField name="investigatorId" value="${investigatorId}" />
								<g:hiddenField name="shippingCartId" value="${shippingCartId}" />
								
							<td><g:textField name="eRampNumber" value="${shippingCartInstance.eRampNumber}" /></td></td>
							
							<td><g:submitButton name="update" value="Update" /></td>
							</g:form>
						</tr>
					</g:each>
					</tbody>
				</table>
				</div>
			</g:if>
			<g:else>
				<h3>No results to display.  Enter new criteria and try again.</h3>
			</g:else>
		</div>
	</body>
</html>