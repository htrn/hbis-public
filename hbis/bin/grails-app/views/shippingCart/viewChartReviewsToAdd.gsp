
<%@ page import="hbis.ShippingCart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shippingCart.label', default: 'ShippingCart')}" />
		<title><g:message code="shippingCart.viewChartReviewsToAdd.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-shippingCart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<div id="show-shippingCart" class="content scaffold-show" role="main">
			<h1><g:message code="shippingCart.viewChartReviewsToAdd.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
		</div>
		
		<div class="allowTableOverflow">
			<div id="show-subspecimens" style="clear:both">
				<fieldset class="box"><legend>Chart Reviews assigned to ${shippingCartInstance.investigator}
									  </legend>
					<g:render template="assignedChartReviews" />
				</fieldset>
			</div>
		
			<div id="show-subspecimensInCart" style="clear:both">
				<fieldset class="box"><legend>Subspecimens in this cart</legend>
					<g:render template="chartReviewsInCart" />
				</fieldset>
			</div>
		</div>
		Download Pre-Packing Slip:
					<g:jasperReport
   						jasper="pre-packing-slip"
   						name="pre-packing-slip-${new Date().format('MMddyy')}"
   						controller="shippingCart"
   						action="prePackingSlip"
   						format="pdf, xls"
   						description=" "
   						delimiter=" ">
   							<g:hiddenField name="shippingCartId" value="${shippingCartInstance?.id}" />
   						</g:jasperReport>
		
		<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${shippingCartInstance?.id}" />
					<g:actionSubmit class="save" action="viewSubspecimensToAdd" value="${message(code: 'shippingCart.button.viewSubspecimensToAdd.label', default: 'View Subspecimens to Add')}" />
					<g:actionSubmit class="save" action="confirm" value="${message(code: 'default.button.proceed.label', default: 'Proceed to Shipping Cart Confirmation')}" />
				</fieldset>
		</g:form>

		<script>
			jQuery.fn.extend({
				disable: function(state) {
					return this.each(function() {
						this.disabled = state;
					});
				}
			});

			// register a handler for the change event of each chart review price input
			$('#show-subspecimens tr td').find('input[name^="price"]').each(function(index, input){
				$(input).on('input', function(){
					// if the input does not pass validation, disable the "Add" button
					var regex = /^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|0)?\.[0-9]{1,2}$/;
					var addButton = $(this).parent().siblings('td').find('button');

					if(!regex.test($(this).val())){
						addButton.disable(true);
					}else{
						addButton.disable(false);
					}
				});
			});

			$('.add-to-cart').click(function(){ // The .add-to-cart button is found in the _assignedChartReviews.gsp partial
				// get the td parent element of the "Add" button that was clicked
				var td = $(this).parent();

				var chartReviewId = td.siblings("input[name^='chart-review-id']").val(),
						chartReviewVersion = td.siblings("input[name^='chart-review-version']").val(),
						shippingCartId = td.siblings("input[name^='shipping-cart-id']").val(),
						shippingCartVersion = null,
						fromDate = $('#from-date').val(),
						toDate = $('#to-date').val(),
						primaryAnatomicSiteId = $('#primary-anatomic-site-id').val(),
						tissueTypeId = $('#tissue-type-id').val(),
						price = td.siblings('td').find('input[name^="price"]').val();

				// hide the table row of the element that was clicked
				td.closest('tr').fadeOut();

				var requestData = {
					chartReviewId: chartReviewId,
					chartReviewVersion: chartReviewVersion,
					shippingCartId: shippingCartId,
					shippingCartVersion: shippingCartVersion,
					fromDate: fromDate,
					toDate: toDate,
					primaryAnatomicSiteId: primaryAnatomicSiteId,
					tissueTypeId: tissueTypeId,
					price: price,
					addRemove: 'Add'
				};

				$.ajax({
					type: 'GET',
					url: '${request.contextPath}/shippingCart/addRemoveChartReview',
					data: requestData,
					contentType: "application/json",
					success: function(data){
						var chartReview = data.chartReview;
						var d = new Date(chartReview.procedureDate);
						var month = d.getMonth() + 1,
								day = d.getDate(),
								year = d.getFullYear();

						// adds padding to single digit months or days
						month = ('0' + month).slice(-2);
						day = ('0' + day).slice(-2);

						var tableRow = $(document.createElement('tr'));

						var tableDataId = $(document.createElement('td')).text(chartReview.id),
								tableDataDate = $(document.createElement('td')).text(month + '/' + day + '/' + year),
								tableDataSpecimenId = $(document.createElement('td')).text(chartReview.specimens[0].specimenChtnId),
								tableDataPrice = $(document.createElement('td')).text(chartReview.price),
								tableDataRemove = $(document.createElement('td')),
								removeLink = $(document.createElement('a'));

						tableDataId.data('chartReviewVersion', chartReview.chartReviewVersion);

						removeLink.html('Remove');
						tableDataRemove.append(removeLink);

						tableRow.append([tableDataId, tableDataDate, tableDataSpecimenId, tableDataPrice, tableDataRemove]);

						tableRow.hide().appendTo('#show-subspecimensInCart tbody').fadeIn();

						// we need to loop through each tr element, in order to add the correct chartReviewId to the href
						// attribute of each Remove link for charts in the shipping cart
						$('#show-subspecimensInCart tbody tr').each(function(index, tr){
							var td = $(tr).find('td:first');
							var id = td.text();
							var version = td.data().chartReviewVersion;

							// update the href attribute on the anchor tag
							$(tr).find('td a').attr('href', '${request.contextPath}/shippingCart/addRemoveChartReview?chartReviewId='+ id +'&chartReviewVersion='+ version +'&shippingCartId='+ shippingCartId +'&shippingCartVersion='+ chartReview.shippingCartVersion +'&fromDate='+ fromDate +'&toDate='+ toDate +'&primaryAnatomicSiteId='+ primaryAnatomicSiteId +'&tissueTypeId='+ tissueTypeId +'&addRemove=Remove');
						});
					}
				});
			});
		</script>
	</body>
</html>
