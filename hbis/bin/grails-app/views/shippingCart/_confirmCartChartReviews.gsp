<table>
				<thead>
					<tr>
						<th><g:message code="chartReview.id.label" default="Chart Review ID" /></th>
						
						<th><g:message code="procedure.procedureDate.label" default="Procedure Date" /></th>
						
						<th><g:message code="shippingCart.specimenChtnIds.label" default="Specimen IDs" /></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${shippingCartInstance.chartReviews}" status="i" var="chartReviewInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: chartReviewInstance, field: "id")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${chartReviewInstance.procedure.procedureDate}"/></td>
					
						<td><g:set var="firstSpecimenChtnId" value="${true}" />
							<g:each in="${chartReviewInstance.procedure.specimens}" status="j" var="specimenInstance">
								<g:set var="displaySpecimenChtnId" value="${false}" />
								<g:each in="${specimenInstance.subspecimens}" status="k" var="subspecimenInstance">
									<g:if test="${subspecimenInstance.needsChartReview && (subspecimenInstance.investigator == shippingCartInstance.investigator)}">
										<g:set var="displaySpecimenChtnId" value="${true}" />
									</g:if>
								</g:each>
								<g:if test="${displaySpecimenChtnId}">
									<g:if test="${firstSpecimenChtnId}">
										${specimenInstance.specimenChtnId}
										<g:set var="firstSpecimenChtnId" value="${false}" />
									</g:if>
									<g:else>
										, ${specimenInstance.specimenChtnId}
									</g:else>
								</g:if>
							</g:each>
						</td>
						
						</tr>
					</g:each>
				</tbody>
			</table>