<%@ page import="hbis.ShippingCart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shippingCart.label', default: 'ShippingCart')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-shippingCart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="confirm-shippingCart" class="content scaffold-create" role="main">
			<h1><g:message code="default.confirm.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<div id="show-subspecimensInCart" class="allowTableOverflow" style="clear:both">
				<fieldset class="box"><legend><strong>Subspecimens in this cart</strong></legend>
					<g:render template="confirmCartSubspecimens" />
				</fieldset>
				
				<fieldset class="box"><legend><strong>Chart Reviews in this cart</strong></legend>
					<g:render template="confirmCartChartReviews" />
				</fieldset>
			</div>
			<g:form method="post" >
				<g:hiddenField name="id" value="${shippingCartInstance?.id}" />
				<g:hiddenField name="version" value="${shippingCartInstance?.version}" />
				<g:hiddenField name="shippingCartStatus" value="Shipped" />
				<g:hiddenField name="view" value="confirm" />
				<fieldset class="form">
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'investigator', 'error')}">
						<span id="investigator-label" class="property-label"><g:message code="shippingCart.investigator.label" default="Investigator" /></span>
						<span class="property-value" aria-labelledby="investigator-label">${shippingCartInstance?.investigator?.encodeAsHTML()}</span>
					</div>
					<div class="leftFormColumn">
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'attentionTo', 'error')} ">
						<label for="attentionTo">
							<g:message code="shippingCart.attentionTo.label" default="Attention To" />
						</label>
						<g:if test="${shippingCartInstance.attentionTo}">
							<g:textField name="attentionTo" value="${shippingCartInstance?.attentionTo}"/>
						</g:if>
						<g:else>
							<g:textField name="attentionTo" value="${shippingCartInstance.investigator.firstName} ${shippingCartInstance.investigator.lastName}"/>
						</g:else>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'institution', 'error')} ">
						<label for="institution">
							<g:message code="shippingCart.institution.label" default="Institution" />
						</label>
						<g:if test="${shippingCartInstance.institution}">
							<g:textField name="institution" value="${shippingCartInstance.institution}"/>
						</g:if>
						<g:else>
							<g:textField name="institution" value="${shippingCartInstance.investigator.shippingInstitution}"/>
						</g:else>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'department', 'error')} ">
						<label for="department">
							<g:message code="shippingCart.department.label" default="Department" />
						</label>
						<g:if test="${shippingCartInstance.department}">
							<g:textField name="department" value="${shippingCartInstance.department}"/>
						</g:if>
						<g:else>
							<g:textField name="department" value="${shippingCartInstance.investigator.shippingDepartment}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'streetAddress1', 'error')} required">
						<label for="streetAddress1">
							<g:message code="shippingCart.streetAddress1.label" default="Street Address 1" />
						</label>
						<g:if test="${shippingCartInstance.streetAddress1}">
							<g:textField name="streetAddress1" required="" value="${shippingCartInstance.streetAddress1}"/>
						</g:if>
						<g:else>
							<g:textField name="streetAddress1" required="" value="${shippingCartInstance.investigator.shippingStreetAddress1}"/>
						</g:else>
						
					</div>

					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'streetAddress1', 'error')} ">
						<label for="streetAddress2">
							<g:message code="shippingCart.streetAddress2.label" default="Street Address 2" />
						</label>
						<g:if test="${shippingCartInstance.streetAddress2}">
							<g:textField name="streetAddress2" value="${shippingCartInstance.streetAddress2}"/>
						</g:if>
						<g:else>
							<g:textField name="streetAddress2" value="${shippingCartInstance.investigator.shippingStreetAddress2}"/>
						</g:else>
						
					</div>

					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'city', 'error')} ">
						<label for="city">
							<g:message code="shippingCart.city.label" default="City" />
						</label>
						<g:if test="${shippingCartInstance.city}">
							<g:textField name="city" value="${shippingCartInstance.city}"/>
						</g:if>
						<g:else>
							<g:textField name="city" value="${shippingCartInstance.investigator.shippingCity}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'state', 'error')} ">
						<label for="state">
							<g:message code="shippingCart.state.label" default="State" />
						</label>
						<g:if test="${shippingCartInstance.state}">
							<g:textField name="state" value="${shippingCartInstance.state}"/>
						</g:if>
						<g:else>
							<g:textField name="state" value="${shippingCartInstance.investigator.shippingState}"/>
						</g:else>
						
					</div>

					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'zipCode', 'error')} ">
						<label for="zipCode">
							<g:message code="shippingCart.zipCode.label" default="Zip Code" />
						</label>
						<g:if test="${shippingCartInstance.state}">
							<g:textField name="zipCode" value="${shippingCartInstance.zipCode}"/>
						</g:if>
						<g:else>
							<g:textField name="zipCode" value="${shippingCartInstance.investigator.shippingZipCode}"/>
						</g:else>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'country', 'error')} ">
						<label for="country">
							<g:message code="shippingCart.country.label" default="Country" />
						</label>
						<g:if test="${shippingCartInstance.country}">
							<g:textField name="country" value="${shippingCartInstance.country}"/>
						</g:if>
						<g:else>
							<g:textField name="country" value="${shippingCartInstance.investigator.shippingCountry}"/>
						</g:else>
						
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'phone', 'error')} ">
						<label for="phone">
							<g:message code="shippingCart.phone.label" default="Phone" />
						</label>
						<g:textField name="phone" value="${shippingCartInstance?.phone}"/>
					</div>
					
					</div> <!-- End Left Form Column -->

					<div class="rightFormColumn">
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'poOrRefNumber', 'error')} ">
						<label for="poOrRefNumber">
							<g:message code="shippingCart.poOrRefNumber.label" default="PO Or Reference Number" />
						</label>
						
						<g:textField name="poOrRefNumber" value="${shippingCartInstance.poOrRefNumber}"/>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'eRampNumber', 'error')} ">
						<label for="eRampNumber">
							<g:message code="shippingCart.eRampNumber.label" default="eRamp Number" />
						</label>
						<g:textField name="eRampNumber" value="${shippingCartInstance?.eRampNumber}"/>
					</div>

					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'shipDate', 'error')} ">
						<label for="shipDate">
							<g:message code="shippingCart.shipDate.label" default="Ship Date" />
						</label>
						<g:datePicker name="shipDate" precision="day"  value="${shippingCartInstance?.shipDate ?: new Date()}" default="none" noSelection="['': '']" />
					</div>

					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'shipMode', 'error')} ">
						<label for="shipMode">
							<g:message code="shippingCart.shipMode.label" default="Ship Mode/Delivery Method" />
						</label>
						<g:if test="${shippingCartInstance.shipMode}">
							<g:select id="shipMode" name="shipMode.id" from="${hbis.ShipMode.list()}" optionKey="id" value="${shippingCartInstance.shipMode?.id}" class="many-to-one" noSelection="['null': '']"/>
						</g:if>
						<g:else>
							<g:select id="shipMode" name="shipMode.id" from="${hbis.ShipMode.list()}" optionKey="id" value="${shippingCartInstance.investigator.courierName?.id}" class="many-to-one" noSelection="['null': '']"/>
						</g:else>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'trackingNumber', 'error')} ">
						<label for="deliveryMethodAccount">
							<g:message code="shippingCart.deliveryMethodAccount.label" default="Delivery Method Account" />
						</label>
						<g:if test="${shippingCartInstance.deliveryMethodAccount}">
							<g:textField name="deliveryMethodAccount" value="${shippingCartInstance.deliveryMethodAccount}"/>
						</g:if>
						<g:else>
							<g:textField name="deliveryMethodAccount" value="${shippingCartInstance.investigator.courierNumber}"/>
						</g:else>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'shippingCost', 'error')} ">
						<label for="shippingCost">
							<g:message code="shippingCart.shippingCost.label" default="Shipping Cost" />
						</label>
						<g:textField name="shippingCost" value="${shippingCartInstance?.shippingCost}"/>
					</div>

					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'trackingNumber', 'error')} ">
						<label for="trackingNumber">
							<g:message code="shippingCart.trackingNumber.label" default="Tracking Number" />
						</label>
						<g:textField name="trackingNumber" value="${shippingCartInstance?.trackingNumber}"/>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'commentsOther', 'error')} ">
						<label for="commentsOther">
							<g:message code="shippingCart.commentsOther.label" default="Other Comments" />
						</label>
						<g:textField name="commentsOther" value="${shippingCartInstance?.commentsOther}"/>
					</div>
					</div><!-- End Form Right Column -->
				</fieldset>
				<fieldset class="buttons" style="clear:both">
					<g:actionSubmit class="save" action="update" onclick="return confirm('${message(code: 'shippingCart.button.ship.confirm.message', default: 'Are you sure you want to mark this cart as Shipped?')}');" value="${message(code: 'default.button.confirm.label', default: 'Send Shipment')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
