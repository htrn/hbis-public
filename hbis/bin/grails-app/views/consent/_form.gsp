<%@ page import="hbis.Consent" %>


<div class="leftFormColumn">

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'consentType', 'error')} ">
	<label for="consentType">
		<g:message code="consent.consentType.label" default="Consent Type" />
	</label>
	<g:select id="consentType" name="consentType.id" from="${hbis.ConsentType.list()}" optionKey="id" value="${consentInstance?.consentType?.id}" class="many-to-one" default="none" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="consent.status.label" default="Status" />
	</label>
	<g:select id="status" name="status.id" from="${hbis.ConsentStatus.list()}" optionKey="id" value="${consentInstance?.status?.id}" class="many-to-one" default="none" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'status', 'error')} ">
	<label for="protocols">
		<g:message code="consent.protocols.label" default="Protocols" />
	</label>
	<g:select id="protocols" name="protocols" from="${hbis.Protocol.list()}" optionKey="id" value="${consentInstance?.protocols*.id}" multiple="true" class="many-to-one" default="none" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'method', 'error')} ">
	<label for="method">
		<g:message code="consent.method.label" default="Method" />
	</label>
	<g:select id="method" name="method.id" from="${hbis.ConsentMethod.list()}" optionKey="id" value="${consentInstance?.method?.id}" class="many-to-one" default="none" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'consentDate', 'error')} required">
	<label for="consentDate">
		<g:message code="consent.consentDate.label" default="Consent Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="consentDate" precision="day"  value="${consentInstance?.consentDate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'statusChangeDate', 'error')} required">
	<label for="statusChangeDate">
		<g:message code="consent.statusChangeDate.label" default="Status Change Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="statusChangeDate" precision="day"  value="${consentInstance?.statusChangeDate}"  />
</div>

</div> <!-- End Left Form Column -->

<div class="rightFormColumn">
<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'consenterFirstName', 'error')} ">
	<label for="consenterFirstName">
		<g:message code="consent.consenterFirstName.label" default="Consenter First Name" />
	</label>
	<g:textField name="consenterFirstName" value="${consentInstance?.consenterFirstName}" />
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'consenterLastName', 'error')} ">
	<label for="consenterLastName">
		<g:message code="consent.consenterLastName.label" default="Consenter Last Name" />
	</label>
	<g:textField name="consenterLastName" value="${consentInstance?.consenterLastName}" />
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'consenterPhoneNumber', 'error')} ">
	<label for="consenterPhoneNumber">
		<g:message code="consent.consenterPhoneNumber.label" default="Consenter Phone Number" />
	</label>
	<g:textField name="consenterPhoneNumber" value="${consentInstance?.consenterPhoneNumber}" />
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'consenterEmail', 'error')} ">
	<label for="consenterEmail">
		<g:message code="consent.consenterEmail.label" default="Consenter Email" />
	</label>
	<g:textField name="consenterEmail" value="${consentInstance?.consenterEmail}" />
</div>

<div class="fieldcontain ${hasErrors(bean: consentInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="consent.location.label" default="Location" />
		
	</label>
	<g:textField name="location" value="${consentInstance?.location}"/>
</div>
<g:hiddenField name="patient.id" value="${consentInstance?.patient?.id}"/>
</div> <!-- End Right Form Column -->