
<%@ page import="hbis.Consent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'consent.label', default: 'Consent')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-consent" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-consent" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="consentType" title="${message(code: 'consent.consentType.label', default: 'Consent Type')}" />
					
						<g:sortableColumn property="status" title="${message(code: 'consent.status.label', default: 'Status')}" />
					
						<g:sortableColumn property="method" title="${message(code: 'consent.method.label', default: 'Method')}" />
					
						<g:sortableColumn property="consentDate" title="${message(code: 'consent.consentDate.label', default: 'Consent Date')}" />
					
						<g:sortableColumn property="consenterEmail" title="${message(code: 'consent.consenterEmail.label', default: 'Consenter Email')}" />
					
						<g:sortableColumn property="consenterFirstName" title="${message(code: 'consent.consenterFirstName.label', default: 'Consenter First Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${consentInstanceList}" status="i" var="consentInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${consentInstance.id}">${fieldValue(bean: consentInstance, field: "consentType")}</g:link></td>
					
						<td>${fieldValue(bean: consentInstance, field: "status")}</td>
					
						<td>${fieldValue(bean: consentInstance, field: "method")}</td>
					
						<td><g:formatDate date="${consentInstance.consentDate}" /></td>
					
						<td>${fieldValue(bean: consentInstance, field: "consenterEmail")}</td>
					
						<td>${fieldValue(bean: consentInstance, field: "consenterFirstName")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${consentInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
