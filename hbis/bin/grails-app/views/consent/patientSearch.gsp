
<%@ page import="hbis.Consent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'consent.label', default: 'Consent')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-consent" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-consent" class="content scaffold-list" role="main">
			<h1>Search for Patient Consent</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:form controller="patient" action="create">
		<div>
			<fieldset class="box"> <legend><strong>Search for Patient</strong></legend>
				MRN <g:remoteField name="mrn" controller="patient" action="performSearchMRN" paramName="mrn" update="divSearchMRN" value="" />
			</fieldset>
		</div>
		
		<div id="divSearchMRN">
			<g:render template="/patient/searchMRNResult" var="patientInstance" collection="${patientList}" />
		</div>
		
		<div id="addNewPatient">
			<input type="submit" value="Add New Patient">
		</div>
		</g:form>
		</div>
	</body>
</html>
