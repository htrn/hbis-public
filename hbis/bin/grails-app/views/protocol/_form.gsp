<%@ page import="hbis.Protocol" %>



<div class="fieldcontain ${hasErrors(bean: protocolInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="protocol.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${protocolInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: protocolInstance, field: 'principalInvestigatorName', 'error')} ">
	<label for="principalInvestigatorName">
		<g:message code="protocol.principalInvestigatorName.label" default="Principal Investigator Name" />
		
	</label>
	<g:textField name="principalInvestigatorName" value="${protocolInstance?.principalInvestigatorName}"/>
</div>

