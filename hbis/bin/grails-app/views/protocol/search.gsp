
<%@ page import="hbis.Protocol" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'protocol.label', default: 'Protocol')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-protocol" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="list-protocol" class="content scaffold-list" role="main">
			<h1><g:message code="default.search.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:form action="search" >
				<fieldset class="form">
				
				<div class="fieldcontain">
					<label for="firstName">
						<g:message code="protocol.description.label" default="Protocol Description" />
					</label>
					<g:textField name="description" value="${description}"/>
					
				</div>
				<div class="fieldcontain">
					<label for="lastName">
						<g:message code="protocol.principalInvestigatorName.label" default="Principal Investigator Name" />
					</label>
					<g:textField name="principalInvestigatorName" value="${principalInvestigatorName}"/>
				</div>
			</fieldset>
			<fieldset class="buttons" style="clear:both">
					<g:hiddenField name="formSubmitted" value="true" />
					<g:submitButton name="search" class="save" value="${message(code: 'default.button.search.label', default: 'Update Results')}" />
			</fieldset>
			</g:form>	
					
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="description" title="${message(code: 'protocol.description.label', default: 'Protocol Description')}" params="${params}"/>
					
						<g:sortableColumn property="principalInvestigatorName" title="${message(code: 'protocol.principalInvestigatorName.label', default: 'Principal Investigator Name')}" params="${params}"/>
					
						<th></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${protocolInstanceList}" status="i" var="protocolInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${protocolInstance.id}">${fieldValue(bean: protocolInstance, field: "description")}</g:link></td>
					
						<td>${fieldValue(bean: protocolInstance, field: "principalInvestigatorName")}</td>
						
						<td><g:link action="edit" id="${protocolInstance.id}">Edit Protocol</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${protocolInstanceTotal}" params="${params}"/>
			</div>
		</div>
	</body>
</html>
