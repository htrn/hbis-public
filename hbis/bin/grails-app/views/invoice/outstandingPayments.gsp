
<%@ page import="hbis.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<g:javascript library="jquery" />
		
		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
          		$("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
    	
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-invoice" class="content scaffold-list" role="main">
			<h1><g:message code="invoice.outstandingPayments.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoiceInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${invoiceInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			
			
			<g:form action="outstandingPayments" >
				<fieldset class="form">
				<div>
						<label for="fromDate">
							<g:message code="default.invoiceDate.fromDate" default="Invoice Date From" />
						</label>
						<g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
					</div>
					
					<div>
						<label for="toDate">
							<g:message code="default.invoiceDate.toDate" default="Invoice Date To" />
						</label>
						<g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
					</div>
				
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="outstandingPayments" class="save" value="${message(code: 'invoice.button.displayResults.label', default: 'Display Results')}" />
				</fieldset>
			</g:form>
			
			<g:if test="${invoiceInstanceList}">
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'invoice.id.label', default: 'Invoice ID')}" />
						
						<th><g:message code="invoice.investigator.label" default="Investigator" /></th>
						
						<g:sortableColumn property="invoiceDate" title="${message(code: 'invoice.invoiceDate.label', default: 'Invoice Date')}" />
						
						<g:sortableColumn property="lastInvoiceSentDate" title="${message(code: 'invoice.lastInvoiceSentDate.label', default: 'Last Date Invoice Sent')}" />
					
						<g:sortableColumn property="billingStatus" title="${message(code: 'invoice.billingStatus.label', default: 'Billing Status')}" />
					
						<g:sortableColumn property="paymentStatus" title="${message(code: 'invoice.paymentStatus.label', default: 'Payment Status')}" />
					
						<th><g:message code="invoice.getTotalPrice.label" default="Total Price" /></th>
						
						<th><g:message code="invoice.amountPaid.label" default="Amount Paid" /></th>
						
						<g:sortableColumn property="paymentReceivedDate" title="${message(code: 'invoice.paymentReceivedDate.label', default: 'Payment Received Date')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${invoiceInstanceList}" status="i" var="invoiceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${invoiceInstance.id}">${fieldValue(bean: invoiceInstance, field: "id")}</g:link></td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "investigator")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${invoiceInstance.invoiceDate}" /></td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${invoiceInstance.lastInvoiceSentDate}" /></td>
						
						<td>${fieldValue(bean: invoiceInstance, field: "billingStatus")}</td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "paymentStatus")}</td>
						
						<td><g:formatNumber type="currency" number="${invoiceInstance.totalPrice}" /></td>
						
						<td><g:formatNumber type="currency" number="${invoiceInstance.amountPaid}" /></td>
					
						<td><g:formatDate format="MM/dd/yyyy" date="${invoiceInstance.paymentReceivedDate}" /></td>
						
					</tr>
				</g:each>
				</tbody>
			</table>
			Download Excel Report
			<g:jasperReport
   								jasper="outstanding-payments"
   								name="outstanding-payments-${new Date().format('MMddyy')}"
   								controller="invoice"
   								action="outstandingPaymentsReport"
   								format="xls"
   								description=" "
   								delimiter=" ">
   									<g:hiddenField name="fromDate" value="${fromDateTextField}" />
   									<g:hiddenField name="toDate" value="${toDateTextField}" />
   							</g:jasperReport>
			</g:if>
		</div>
	</body>
</html>
