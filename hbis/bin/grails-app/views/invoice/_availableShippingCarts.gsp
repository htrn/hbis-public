<table>
				<thead>
					<tr>
						<th><g:message code="shippingCart.id.label" default="Shipping Cart ID" /></th>
					
						<th><g:message code="shippingCart.dateCreated.label" default="Date Created" /></th>
						
						<th><g:message code="shippingCart.investigator.label" default="Investigator" /></th>
						
						<th><g:message code="shippingCart.poOrRefNumber.label" default="PO/Ref Number" /></th>
						
						<th><g:message code="shippingCart.commentsOther.label" default="Comments" /></th>
						
						<th><g:message code="shippingCart.subspecimen.count.label" default="# Subspecimens" /></th>
						
						<th><g:message code="shippingCart.chartReview.count.label" default="# Chart Reviews" /></th>
						
						<th><g:message code="shippingCart.totalPrice.label" default="Total Price (includes Shipping Cost)" /></th>
					
						<th><g:message code="invoice.action.label" default="Action" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${availableShippingCarts}" status="i" var="shippingCartInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${shippingCartInstance.id}">${fieldValue(bean: shippingCartInstance, field: "id")}</g:link></td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${shippingCartInstance.dateCreated}"/></td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "investigator")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "poOrRefNumber")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "commentsOther")}</td>
						
						<td>${shippingCartInstance.getNumberOfSubspecimens()}</td>
						
						<td>${shippingCartInstance.getNumberOfChartReviews()}</td>
						
						<td>${shippingCartInstance.getTotalPrice()}</td>
						
						<td><g:link controller="invoice" 
								action="addRemoveShippingCart"
								params='[shippingCartId: "${shippingCartInstance.id}", shippingCartVersion: "${shippingCartInstance.version}", 
										invoiceId: "${invoiceInstance.id}", invoiceVersion: "${invoiceInstance.version}",
										addRemove: "Add"]'>Add</g:link>
								</td>
					
					</tr>
				</g:each>
				</tbody>
</table>