
<%@ page import="hbis.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="invoice.accountsReceivable.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-invoice" class="content scaffold-list" role="main">
			<h1><g:message code="invoice.accountsReceivable.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoiceInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${invoiceInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			
			
			<g:form action="accountsReceivable" >
				<fieldset class="form">
				<div>
					<label for="investigator">
						<g:message code="invoice.investigator.label" default="Investigator" />
					</label>
					<g:select id="investigator" name="investigatorId" from="${hbis.Investigator.list()}" optionKey="id" value="${investigatorId}" class="many-to-one" noSelection="['null': '']"/>
				</div>
				<div>
					<label for="invoiceId">
						<g:message code="invoice.id.label" default="Invoice Number" />
					</label>
					<g:textField name="invoiceId" value="${invoiceId}" />
				</div>
				<div>
					<label for="institution">
						<g:message code="invoice.institution.label" default="Institution" />
					</label>
					<g:textField name="institution" value="${institution}" />
				</div>
				
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="accountsReceivable" class="save" value="${message(code: 'invoice.button.accountsReceivableQuery.label', default: 'Display Results')}" />
				</fieldset>
			</g:form>
			
			<g:if test="${invoiceInstanceList}">
			<div class="allowTableOverflow">
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'invoice.id.label', default: 'ID')}" />
						
						<th><g:message code="invoice.investigator.label" default="Investigator" /></th>
						
						<th><g:message code="invoice.institution.label" default="Institution" /></th>
						
						<g:sortableColumn property="invoiceDate" title="${message(code: 'invoice.invoiceDate.label', default: 'Invoice Date')}" />
					
						<g:sortableColumn property="paymentStatus" title="${message(code: 'invoice.paymentStatus.label', default: 'Payment Status')}" />
					
						<th><g:message code="invoice.getTotalPrice.label" default="Total Price" /></th>
						
						<g:sortableColumn property="paymentReceivedDate" title="${message(code: 'invoice.paymentReceivedDate.label', default: 'Payment Received Date')}" />
						
						<th><g:message code="invoice.amountPaid.label" default="Amount Paid" /></th>
						
						<th><g:message code="invoice.paymentReceivedBy.label" default="Payment Received By" /></th>
						
						<th><g:message code="invoice.paymentMethod.label" default="Payment Method" /></th>
						
						<th><g:message code="invoice.action.label" default="" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${invoiceInstanceList}" status="i" var="invoiceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${invoiceInstance.id}">${fieldValue(bean: invoiceInstance, field: "id")}</g:link></td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "investigator")}</td>
						
						<td>${fieldValue(bean: invoiceInstance, field: "institution")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${invoiceInstance.invoiceDate}" /></td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "paymentStatus")}</td>
						
						<td><g:formatNumber type="currency" number="${invoiceInstance.getTotalPrice()}" /></td>
					
						<g:form action="update">
						<g:hiddenField name="id" value="${invoiceInstance.id}" />
						<g:hiddenField name="version" value="${invoiceInstance.version}" />
						<g:hiddenField name="view" value="accountsReceivable" />
						<g:hiddenField name="invoiceId" value="${invoiceId}" />
						<g:hiddenField name="investigatorId" value="${investigatorId}" />
						<td><g:datePicker name="paymentReceivedDate" precision="day"  value="${invoiceInstance?.paymentReceivedDate}"  /></td>
						
						<td><g:field name="amountPaid" type="text"  value="${invoiceInstance.amountPaid}" style="width:80px;" /></td>
						
						<td><g:field name="paymentReceivedBy" type="text"  value="${invoiceInstance.paymentReceivedBy}" style="width:150px;" /></td>
						
						<td><g:select id="paymentMethod" name="paymentMethod.id" from="${hbis.PaymentMethod.list()}" optionKey="id" value="${invoiceInstance?.paymentMethod?.id}" class="many-to-one" noSelection="['null': '']" /></td>
						
						<td><g:submitButton name="update" value="Update" /></td>
						</g:form>
					</tr>
				</g:each>
				</tbody>
			</table>
			</div>
			</g:if>
		</div>
	</body>
</html>
