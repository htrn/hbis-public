
<%@ page import="hbis.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-invoice" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<ul class="property-list invoice">
			<h3>Download Invoice</h3>
			<g:jasperReport
   						jasper="invoice"
   						name="invoice-${new Date().format('MMddyy')}"
   						controller="invoice"
   						action="invoiceReport"
   						format="pdf"
   						description=" "
   						delimiter=" ">
   							<g:hiddenField name="invoiceId" value="${invoiceInstance?.id}" />
   						</g:jasperReport>
   			</ul>
			<g:render template="/showDomainTemplates/invoice" />
			<!-- 
			<g:form>
				
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${invoiceInstance?.id}" />
					
					<g:link class="edit" action="edit" id="${invoiceInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					 
				</fieldset>
			</g:form>
			-->
		</div>
	</body>
</html>
