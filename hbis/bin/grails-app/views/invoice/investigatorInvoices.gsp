<%--
  Created by IntelliJ IDEA.
  User: jhines
  Date: 10/21/15
  Time: 1:28 PM
--%>
<%@ page import="hbis.Investigator; hbis.Invoice" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:javascript library="jquery" />

    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
            $("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
        });

    </script>

    <title>Investigator Invoices</title>
</head>

<body>
    <div id="list-invoice" class="content scaffold-list" role="main">
        <h1>Investigator Invoices</h1>

        <g:form action="investigatorInvoices" class="investigator-invoice-search-form">
            <fieldset class="form">
                <div>
                    <label for="fromDate">
                        <g:message code="default.invoiceDate.fromDate" default="Invoice Date From" />
                    </label>
                    <g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
                </div>

                <div>
                    <label for="toDate">
                        <g:message code="default.invoiceDate.toDate" default="Invoice Date To" />
                    </label>
                    <g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
                </div>

                <div>
                    <label for="selected-investigator-id">
                        <g:message code="invoice.investigator" default="Investigator" />
                    </label>
                    <g:select
                            name="investigatorId"
                            id="selected-investigator-id"
                            noSelection="${['null':'Select an investigator']}"
                            value="${investigatorId}"
                            from="${hbis.Investigator.list()}"
                            optionKey="id" />
                </div>

            </fieldset>
            <fieldset class="buttons">
                <g:submitButton id="apqiSearchBtn" name="investigator-invoices" class="save" value="${message(code: 'invoice.button.displayResults.label', default: 'Display Results')}" />
            </fieldset>
        </g:form>

        <g:if test="${flash.pleaseSelectInvestigator}">
            <div class="message" role="status">${flash.pleaseSelectInvestigator}</div>
        </g:if>

        <g:if test="${invoiceList}">
            <table>
                <thead>
                <tr>

                    <th><g:message code="invoice.id.label" default="Invoice ID"/></th>
                    <!--<g:sortableColumn property="id" title="${message(code: 'invoice.id.label', default: 'Invoice ID')}"/>-->

                    <th><g:message code="invoice.investigator.label" default="Investigator" /></th>

                    <th><g:message code="invoice.invoiceDate.label" default="Invoice Date"/></th>

                    <th><g:message code="invoice.lastInvoiceSentDate.label" default="Last Date Invoice Sent" /></th>

                    <th><g:message code="invoice.billingStatus.label" default="Billing Status" /></th>

                    <th><g:message code="invoice.paymentStatus.label" default="Payment Status" /></th>

                    <th><g:message code="invoice.getTotalPrice.label" default="Total Price" /></th>

                    <th><g:message code="invoice.amountPaid.label" default="Amount Paid" /></th>

                    <th><g:message code="invoice.paymentReceivedDate.label" default="Last Payment Received Date" /></th>

                    <th><g:message code="invoice.paymentReceivedBy.label" default="Comments"/></th>

                </tr>
                </thead>
                <tbody>
                <g:each in="${invoiceList}" status="i" var="invoice">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                        <td><g:link action="show" id="${invoice.id}">${fieldValue(bean: invoice, field: "id")}</g:link></td>

                        <td>${fieldValue(bean: invoice, field: "investigator")}</td>

                        <td><g:formatDate format="MM/dd/yyyy" date="${invoice.invoiceDate}" /></td>

                        <td><g:formatDate format="MM/dd/yyyy" date="${invoice.lastInvoiceSentDate}" /></td>

                        <td>${fieldValue(bean: invoice, field: "billingStatus")}</td>

                        <td>${fieldValue(bean: invoice, field: "paymentStatus")}</td>

                        <td><g:formatNumber type="currency" number="${invoice.totalPrice}" /></td>

                        <td><g:formatNumber type="currency" number="${invoice.amountPaid}" /></td>

                        <td><g:formatDate format="MM/dd/yyyy" date="${invoice.paymentReceivedDate}" /></td>

                        <td>${fieldValue(bean: invoice, field: "paymentReceivedBy")}</td>

                    </tr>
                </g:each>
                <tr>
                    <td><b>Total</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b><g:formatNumber type="currency" number="${sumTotalPrice}" /></b></td>
                    <td><b><g:formatNumber type="currency" number="${sumAmountPaid}" /></b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            Download Excel Report
            <g:jasperReport
                    jasper="payments-received"
                    name="payments-received-${new Date().format('MMddyy')}"
                    controller="invoice"
                    action="paymentsReceivedReport"
                    format="xls"
                    description=" "
                    delimiter=" ">
                <g:hiddenField name="fromDate" value="${fromDateTextField}" />
                <g:hiddenField name="toDate" value="${toDateTextField}" />
                <g:hiddenField name="investigatorId" value="${investigatorId}" />
            </g:jasperReport>
        </g:if>
    </div>
</body>
</html>