
<%@ page import="hbis.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-invoice" class="content scaffold-list" role="main">
			<h1><g:message code="invoice.latePayments.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoiceInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${invoiceInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			
			
			<g:form action="latePayment" >
				<fieldset class="form">
				<div>
					<label for="investigator">
						<g:message code="invoice.investigator.label" default="Investigator" />
					</label>
					<g:select id="investigator" name="investigatorId" from="${hbis.Investigator.list()}" optionKey="id" value="${investigatorId}" class="many-to-one" noSelection="['null': '']"/>
				</div>
				<div>
					<label for="daysSinceLastInvoiceSent">
						<g:message code="invoice.daysSinceLastInvoiceSent.label" default="Days Since Last Invoice Sent" />
					</label>
					<g:field type="number" name="daysSinceLastInvoiceSent" value="${daysSinceLastInvoiceSent}" />
				</div>
				
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="latePayment" class="save" value="${message(code: 'invoice.button.latePaymentQuery.label', default: 'Display Results')}" />
				</fieldset>
			</g:form>
			
			<g:if test="${invoiceInstanceList}">
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'invoice.id.label', default: 'Invoice ID')}" />
						
						<th><g:message code="invoice.investigator.label" default="Investigator" /></th>
						
						<g:sortableColumn property="invoiceDate" title="${message(code: 'invoice.invoiceDate.label', default: 'Invoice Date')}" />
						
						<g:sortableColumn property="lastInvoiceSentDate" title="${message(code: 'invoice.lastInvoiceSentDate.label', default: 'Last Date Invoice Sent')}" />
					
						<g:sortableColumn property="billingStatus" title="${message(code: 'invoice.billingStatus.label', default: 'Billing Status')}" />
					
						<g:sortableColumn property="paymentStatus" title="${message(code: 'invoice.paymentStatus.label', default: 'Payment Status')}" />
					
						<th><g:message code="invoice.getTotalPrice.label" default="Total Price" /></th>
						
						<th><g:message code="invoice.amountPaid.label" default="Amount Paid" /></th>
						
						<g:sortableColumn property="paymentReceivedDate" title="${message(code: 'invoice.paymentReceivedDate.label', default: 'Payment Received Date')}" />
						
						<th><g:message code="invoice.report.label" default="Action" /></th>
						
						<th><g:message code="invoice.action.label" default="" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${invoiceInstanceList}" status="i" var="invoiceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${invoiceInstance.id}">${fieldValue(bean: invoiceInstance, field: "id")}</g:link></td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "investigator")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${invoiceInstance.invoiceDate}" /></td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${invoiceInstance.lastInvoiceSentDate}" /></td>
						
						<td>${fieldValue(bean: invoiceInstance, field: "billingStatus")}</td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "paymentStatus")}</td>
						
						<td><g:formatNumber type="currency" number="${invoiceInstance.getTotalPrice()}" /></td>
						
						<td><g:formatNumber type="currency" number="${invoiceInstance.amountPaid}" /></td>
					
						<td><g:formatDate format="MM/dd/yyyy" date="${invoiceInstance.paymentReceivedDate}" /></td>
						
						<td>
							<!--<g:jasperReport
   								jasper="late-invoice"
   								name="late-invoice-${new Date().format('MMddyy')}"
   								controller="invoice"
   								action="lateInvoiceReport"
   								format="pdf"
   								description=" "
   								delimiter=" ">
   									<g:hiddenField name="invoiceId" value="${invoiceInstance?.id}" />
   							</g:jasperReport></td>
   							-->
   							<g:jasperForm 
   								jasper="late-invoice"
   								controller="invoice"
   								action="lateInvoiceReport">
   								<g:hiddenField name="invoiceId" value="${invoiceInstance?.id}" />
   								<g:hiddenField name="investigatorId" value="${investigatorId}" />
   								<g:hiddenField name="daysSinceLastInvoiceSent" value="${daysSinceLastInvoiceSent}" />
   								<g:jasperButton format="pdf" text="Resend Invoice"/>
   							</g:jasperForm>
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
			</g:if>
		</div>
	</body>
</html>
