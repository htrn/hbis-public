<%@ page import="hbis.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="create-invoice" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoiceInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${invoiceInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'investigator', 'error')} required">
						<label for="investigator">
							<g:message code="invoice.investigator.label" default="Investigator" />
						<span class="required-indicator">*</span>
						</label>
						<g:select id="investigator" name="investigator.id" from="${investigators}" optionKey="id" required="" value="${investigatorId}" class="many-to-one" noSelection="['null': '']" />
					</div>
				</fieldset>
			
				<fieldset class="buttons">
					<!--<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />-->
					<g:actionSubmit name="saveAndAddShippingCarts" action="saveAndAddShippingCarts" class="save" value="${message(code: 'default.button.custom.label', default: 'Proceed to Add Shipping Carts')}" />
				</fieldset>
			</g:form>
			
			<g:if test="${availableShippingCarts}">
			<h1>Available Shipping Carts to be Invoiced</h1>
			<div class="allowTableOverflow">
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'shippingCart.id.label', default: 'ID')}" />
						
						<th><g:message code="shippingCart.status.label" default="Status" /></th>
						
						<g:sortableColumn property="shipDate" title="${message(code: 'shippingCart.shipDate.label', default: 'Ship Date')}" />
						
						<g:sortableColumn property="investigator" title="${message(code: 'shippingCart.investigator.label', default: 'Investigator')}" />
						
						<g:sortableColumn property="description" title="${message(code: 'shippingCart.description.label', default: 'Description')}" />
						
						<th><g:message code="shippingCart.institutionsAllowed.label" default="Institutions Allowed" /></th>
						
						<th><g:message code="subspecimen.number.label" default="# Subspecimens" /></th>
						
						<th><g:message code="subspecimen.number.label" default="# Chart Reviews" /></th>
					
						<th><g:message code="invoice.createLink.label" default="" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${availableShippingCarts}" status="i" var="shippingCartInstance">
					<g:if test="${!(shippingCartInstance.investigator.chtnIdPrefix.prefix == 'C' && (shippingCartInstance.institutionsAllowed.description == 'OSU only' || shippingCartInstance.institutionsAllowed.description == 'All'))}">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link controller="shippingCart" action="show" id="${shippingCartInstance.id}">${fieldValue(bean: shippingCartInstance, field: "id")}</g:link></td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "status")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${shippingCartInstance.shipDate}"/></td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "investigator")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "description")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "institutionsAllowed")}</td>
						
						<td>${shippingCartInstance.getNumberOfSubspecimens()}</td>
						
						<td>${shippingCartInstance.getNumberOfChartReviews()}</td>
						
						<td><g:link action="saveAndAddShippingCarts" 
									params="['investigator.id': shippingCartInstance.investigator.id]">Create Invoice for this Investigator</g:link>
						</td>
					
					</tr>
					</g:if>
				</g:each>
				</tbody>
			</table>
			</div>
			</g:if>
		</div>
	</body>
</html>
