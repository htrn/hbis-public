<%@ page import="hbis.Patient" %>



<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'mrn', 'error')} required">
	<label for="mrn">
		<g:message code="patient.mrn.label" default="MRN" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField tabindex="1" name="mrn" pattern="${patientInstance.constraints.mrn.matches}" required="" value="${patientInstance?.mrn}" />
</div>

<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'firstName', 'error')} ">
	<label for="firstName">
		<g:message code="patient.firstName.label" default="First Name" />
		
	</label>
	<g:textField tabindex="1" name="firstName" value="${patientInstance?.firstName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'middleInitial', 'error')} ">
	<label for="middleInitial">
		<g:message code="patient.middleInitial.label" default="Middle Initial" />
		
	</label>
	<g:textField tabindex="1" name="middleInitial" value="${patientInstance?.middleInitial}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="patient.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField tabindex="1" name="lastName" required="" value="${patientInstance?.lastName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'gender', 'error')} ">
	<label for="gender">
		<g:message code="patient.gender.label" default="Gender" />
		
	</label>
	<g:select tabindex="1" id="gender" name="gender.id" from="${hbis.Gender.list()}" optionKey="id" value="${patientInstance?.gender?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'race', 'error')} ">
	<label for="race">
		<g:message code="patient.race.label" default="Race" />
		
	</label>
	<g:select tabindex="1" id="race" name="race.id" from="${hbis.Race.list()}" optionKey="id" value="${patientInstance?.race?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<script type="text/javascript">
	$(document).ready(function() {
			$("#dateOfBirth").datepicker({dateFormat: 'mm/dd/yy'});
		})
</script>

<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'dateOfBirth', 'error')} ">
	<label for="dateOfBirth">
		<g:message code="patient.dateOfBirth.label" default="Date Of Birth" />
		
	</label>

	<g:textField tabindex="1" type="date" name="dateOfBirth" id="dateOfBirth" value="${formatDate(format:'MM/dd/yyyy',date:patientInstance?.dateOfBirth)}"/>
	
</div>

<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'originInstitution', 'error')} ">
	<label for="originInstitution">
		<g:message code="patient.originInstitution.label" default="Origin Institution" />
		
	</label>
	<g:select tabindex="1" id="originInstitution" name="originInstitution.id" from="${hbis.Institution.list()}" optionKey="id" value="${patientInstance?.originInstitution?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<g:if test="${patientInstance?.procedures}">
<div class="fieldcontain ${hasErrors(bean: patientInstance, field: 'procedures', 'error')} ">
	<label for="procedures">
		<g:message code="patient.procedures.label" default="Procedures" />
		
	</label>
	
	<ul class="one-to-many">
	<g:each in="${patientInstance?.procedures?}" var="p">
    	<li><g:link controller="procedure" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
	</g:each>
</ul>
</g:if>

</div>

