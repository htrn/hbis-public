<%@ page import="hbis.Patient" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		MRN: <g:remoteField name="mrn" controller="patient" action="performSearchMRN" paramName="mrn" update="divSearchMRN" value="" />
		
		<div id="divSearchMRN">
			<g:render template="searchMRNResult" var="patientInstance" collection="${patientList}" />
		</div>
	</body>
</html>