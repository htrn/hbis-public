<table>
	<thead>
		<tr>
			<th><g:message code="specimen.procurementDate.label" default="Procurement Date" /></th>

			<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>

			<th><g:message code="patient.mrn.label" default="MRN" /></th>

			<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>

			<th><g:message code="specimen.primaryOrMets.label" default="Primary/Mets" /></th>

			<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>

			<th><g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diagnosis" /></th>

			<th><g:message code="qcBatch.Action.label" default="Remove From QC Batch" /></th>
		</tr>
	</thead>
	<tbody>
		<g:each in="${qcBatchInstance.specimens}" status="i" var="specimenInstance">
			<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

			<td><g:formatDate format="MM/dd/yyyy" date="${specimenInstance.procurementDate}"/></td>

			<td>${fieldValue(bean: specimenInstance, field: "specimenChtnId")}</td>

			<td>${fieldValue(bean: specimenInstance, field: "procedure.patient.mrn")}</td>

			<td>${fieldValue(bean: specimenInstance, field: "preliminaryPrimaryAnatomicSite")}</td>

			<td>${fieldValue(bean: specimenInstance, field: "primaryOrMets")}</td>

			<td>${fieldValue(bean: specimenInstance, field: "preliminaryTissueType")}</td>

			<td>${fieldValue(bean: specimenInstance, field: "preliminaryDiagnosis")}</td>

			%{--<td><g:link controller="qcBatch"--}%
					%{--action="addRemoveSpecimen"--}%
					%{--params='[specimenId: "${specimenInstance.id}", specimenVersion: "${specimenInstance.version}",--}%
							%{--qcBatchId: "${qcBatchInstance.id}", qcBatchVersion: "${qcBatchInstance.version}",--}%
							%{--fromDate: "${fromDate}", toDate: "${toDate}", addRemove: "Remove"]'>Remove from QC Batch</g:link>--}%
					%{--</td>--}%

			<td>
				<g:checkBox name="specimensToRemove" value="${specimenInstance.id}" checked="false"/>
			</td>

			</tr>
		</g:each>
	</tbody>
</table>