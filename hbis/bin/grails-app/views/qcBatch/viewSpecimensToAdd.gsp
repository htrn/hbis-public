<%@ page import="hbis.ChartReview" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QC Batch')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-chartReview" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		
		<div id="list-chartReview" class="content scaffold-list" role="main">
			<h1>View Specimens to Add to the QC Batch</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
		
		<!-- The below form allows for the procurement date range to be updated 
		<g:form action="viewSpecimensToAdd">
			<g:hiddenField name="id" value="${qcBatchInstance?.id}" />
			<div>
				<label for="fromDate">
					<g:message code="default.date.fromDate" default="From Date" />
				</label>
				<g:datePicker name="fromDate" precision="day"  value="${fromDate}" default="none" noSelection="['null': '']" />
			</div>
			<div>
				<label for="toDate">
					<g:message code="default.date.toDate" default="To Date" />
				</label>
				<g:datePicker name="toDate" precision="day"  value="${toDate}" default="none" noSelection="['null': '']" />
			</div>
			<fieldset class="buttons">
					<g:submitButton name="viewSpecimensToAdd" class="save" value="${message(code: 'default.button.viewSpecimensToAdd.label', default: 'Update Procurement Date Criteria')}" />
			</fieldset>
		</g:form>
		-->
		
		<div id="show-specimens" class="group" style="clear:both">
			<form>
				<fieldset class="box"><legend>Specimens with Q subspecimen having QC or Assigned status</legend>
					<g:render template="resultQcSpecimens" />
				</fieldset>

				<g:hiddenField name="qcBatchId" value="${qcBatchInstance.id}" />
				<g:hiddenField name="fromDate" value="${fromDate}" />
				<g:hiddenField name="toDate" value="${toDate}" />

				<g:hiddenField name="addRemove" value="Add" />

				<g:actionSubmit value="Add To QC Batch" action="addRemoveSpecimen"/>
			</form>
		</div>
		
		<div id="show-specimensAddedToQcBatch" class="group" style="clear:both">
			<form>
				<fieldset class="box"><legend>Specimens in this QC batch</legend>
					<g:render template="specimensAddedToQcBatch" />
				</fieldset>

				<g:hiddenField name="qcBatchId" value="${qcBatchInstance.id}" />
				<g:hiddenField name="fromDate" value="${fromDate}" />
				<g:hiddenField name="toDate" value="${toDate}" />

				<g:hiddenField name="addRemove" value="Remove" />

				<g:actionSubmit value="Remove From QC Batch" action="addRemoveSpecimen"/>
			</form>
		</div>
		
		<g:form action="update">
				<fieldset class="form">
				<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'description', 'error')} ">
					<label for="description">
						<g:message code="qcBatch.description.label" default="Description" />
					</label>
					<g:textField name="description" value="${qcBatchInstance?.description}"/>
				</div>
				</fieldset>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${qcBatchInstance?.id}" />
					<g:hiddenField name="version" value="${qcBatchInstance?.version}" />
					<g:hiddenField name="qcBatchStatus" value="Sent to Scan" />
					<g:hiddenField name="view" value="preparation" />
					
					<g:submitButton name="update" class="save" value="${message(code: 'qcBatch.button.sentToScan.label', default: 'Confirm & Send to Scan')}" />
				</fieldset>
		</g:form>
		
	</body>
</html>