
<%@ page import="hbis.QcBatch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QcBatch')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-qcBatch" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-qcBatch" class="content scaffold-list" role="main">
			<h1>Prepare QC Batches to Send to Scanning</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${qcBatchInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${qcBatchInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'qcBatch.id.label', default: 'Batch ID')}" />
						
						<g:sortableColumn property="description" title="${message(code: 'qcBatch.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="status" title="${message(code: 'qcBatch.status.label', default: 'Status')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'qcBatch.dateCreated.label', default: 'Date Created')}" />
						
						<th><g:message code="qcBatch.numberOfSpecimens.label" default="# Specimens" /></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${qcBatchInstanceList}" status="i" var="qcBatchInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>${fieldValue(bean: qcBatchInstance, field: "id")}</td>
						
						<td>${fieldValue(bean: qcBatchInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: qcBatchInstance, field: "status")}</td>
					
						<td><g:formatDate date="${qcBatchInstance.dateCreated}" /></td>
						
						<td>${qcBatchInstance.numberOfSpecimens}</td>
						
						<td><g:link action="viewSpecimensToAdd" id="${qcBatchInstance.id}">Add Specimens to this Batch</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>
		<g:link action="saveAndAddSpecimens"><button type="button">Create New QC Batch</button></g:link>
	</body>
</html>
