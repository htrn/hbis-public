
<%@ page import="hbis.QcBatch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QcBatch')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-qcBatch" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-qcBatch" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list qcBatch">
			
				<g:if test="${qcBatchInstance?.batchNumber}">
				<li class="fieldcontain">
					<span id="batchNumber-label" class="property-label"><g:message code="qcBatch.batchNumber.label" default="Batch Number" /></span>
					
						<span class="property-value" aria-labelledby="batchNumber-label"><g:fieldValue bean="${qcBatchInstance}" field="batchNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcBatchInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="qcBatch.status.label" default="Status" /></span>
					
						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${qcBatchInstance}" field="status"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcBatchInstance?.dateComplete}">
				<li class="fieldcontain">
					<span id="dateComplete-label" class="property-label"><g:message code="qcBatch.dateComplete.label" default="Date Complete" /></span>
					
						<span class="property-value" aria-labelledby="dateComplete-label"><g:formatDate date="${qcBatchInstance?.dateComplete}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcBatchInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="qcBatch.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${qcBatchInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcBatchInstance?.dateImageAnalysisComplete}">
				<li class="fieldcontain">
					<span id="dateImageAnalysisComplete-label" class="property-label"><g:message code="qcBatch.dateImageAnalysisComplete.label" default="Date Image Analysis Complete" /></span>
					
						<span class="property-value" aria-labelledby="dateImageAnalysisComplete-label"><g:formatDate date="${qcBatchInstance?.dateImageAnalysisComplete}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcBatchInstance?.datePathologistConfirmationComplete}">
				<li class="fieldcontain">
					<span id="datePathologistConfirmationComplete-label" class="property-label"><g:message code="qcBatch.datePathologistConfirmationComplete.label" default="Date Pathologist Confirmation Complete" /></span>
					
						<span class="property-value" aria-labelledby="datePathologistConfirmationComplete-label"><g:formatDate date="${qcBatchInstance?.datePathologistConfirmationComplete}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcBatchInstance?.dateScanningComplete}">
				<li class="fieldcontain">
					<span id="dateScanningComplete-label" class="property-label"><g:message code="qcBatch.dateScanningComplete.label" default="Date Scanning Complete" /></span>
					
						<span class="property-value" aria-labelledby="dateScanningComplete-label"><g:formatDate date="${qcBatchInstance?.dateScanningComplete}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcBatchInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="qcBatch.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${qcBatchInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${qcBatchInstance?.specimens}">
				<li class="fieldcontain">
					<span id="specimens-label" class="property-label"><g:message code="qcBatch.specimens.label" default="Specimens" /></span>
					
						<g:each in="${qcBatchInstance.specimens}" var="s">
						<span class="property-value" aria-labelledby="specimens-label"><g:link controller="specimen" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${qcBatchInstance?.id}" />
					<g:link class="edit" action="edit" id="${qcBatchInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
