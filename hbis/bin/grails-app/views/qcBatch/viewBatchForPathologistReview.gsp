
<%@ page import="hbis.QcBatch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QC Batch')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		
	</head>
	<body>
		<a href="#list-qcBatch" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-qcBatch" class="content scaffold-list" role="main">
			<h1>Pathology Confirmation for QC Batch ${qcBatchInstance.id}</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${qcBatchInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${qcBatchInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<!-- Trying update of entire table... -->
			<g:form method="post" controller="specimen" action="updateAllPathologistReview">
			<div class="allowTableOverflow">
			<table>
				<thead>
					<tr>
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
						
						<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
						
						<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
						
						<th><g:message code="specimen.primaryOrMets.label" default="Primary Or Mets" /></th>
						
						<th><g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diagnosis" /></th>
						
						<th><g:message code="qcResult.linkSvsImage.label" default="SVS" /></th>
						
						<th><g:message code="qcResult.linkOverlayImage.label" default="Overlay" /></th>
						
						<th><g:message code="qcResult.linkPathReport.label" default="Report" /></th>
						
						<th><g:message code="qcResult.regionOfInterest.label" default="ROI" /></th>
						
						<th><g:message code="qcResult.regionOfInterestArea.label" default="ROI Area" /></th>
						
						<th><g:message code="qcResult.percentNecrosis.label" default="% Necrosis" /></th>
						
						<th><g:message code="qcResult.tissueQcMatch.label" default="Tissue QC Match" /></th>
						
						<th><g:message code="qcResult.pathologistComments.label" default="Pathologist Comments" /></th>
						
						<th><g:message code="qcResult.finalAnatomicSite.label" default="Final Anatomic Site" /></th>
						
						<th><g:message code="qcResult.finalTissueType.label" default="Final Tissue Type" /></th>
									
						<th><g:message code="qcResult.finalDiagnosis.label" default="Final Diagnosis" /></th>
						
						<th><g:message code="qcResult.finalSubDiagnosis.label" default="Final Sub Diagnosis" /></th>
						
						<th><g:message code="qcResult.finalModDiagnosis.label" default="Final Mod Diagnosis" /></th>
								
						<th><g:message code="qcResult.goodForTma.label" default="Good for TMA" /></th>
						
						<th><g:message code="qcResult.finalGrade.label" default="Grade" /></th>
						
						<th><g:message code="qcResult.finalStage.label" default="Stage" /></th>
						
						<th><g:message code="qcResult.finalMolecularStatusEr.label" default="ER Molecular Status" /></th>
						
						<th><g:message code="qcResult.finalMolecularStatusPr.label" default="PR Molecular Status" /></th>
						
						<th><g:message code="qcResult.finalMolecularStatusHer2.label" default="Her2 Molecular Status" /></th>
						
						<th><g:message code="qcResult.qcFollowUpAction.label" default="Follow Up Action" /></th>
						
						
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${qcBatchInstance.specimens.sort{a,b -> a.specimenChtnId.compareTo(b.specimenChtnId)}}" status="i" var="specimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: specimenInstance, field: "specimenChtnId")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryPrimaryAnatomicSite")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryTissueType")}</td>
						
						<td><g:select id="primaryOrMets" name="specimens.primaryOrMets.${specimenInstance.id}" from="${hbis.PrimaryMets.list()}" optionKey="id" value="${specimenInstance?.primaryOrMets?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryDiagnosis")}</td>
						
						<td><a href="${specimenInstance.qcResult.linkSvsImage}">SVS Image</a></td>
						
						<td><a href="${specimenInstance.qcResult.linkOverlayImage}" target="_blank">Overlay Image</a></td>
						
						<td><a href="${specimenInstance.qcResult.linkPathReport}" target="_blank">Path Report</a></td>
						
						<td><g:select name="specimens.regionOfInterest.${specimenInstance.id}" value="${specimenInstance?.qcResult?.regionOfInterest}" from="${0..100}" noSelection="['null': '']"/></td>
						<td><g:field name="specimens.regionOfInterestArea.${specimenInstance.id}" type="number" value="${specimenInstance?.qcResult?.regionOfInterestArea}"/></td>
						<!-- <td>${fieldValue(bean: specimenInstance, field: "qcResult.regionOfInterestArea")}</td>-->
						
						<td><g:select name="specimens.percentNecrosis.${specimenInstance.id}" value="${specimenInstance?.qcResult?.percentNecrosis}" from="${0..100}" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.tissueQcMatch.${specimenInstance.id}" from="${hbis.TissueQcMatch.list()}" optionKey="id" value="${specimenInstance?.qcResult?.tissueQcMatch?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:textField name="specimens.pathologistComments.${specimenInstance.id}" value="${specimenInstance?.qcResult?.pathologistComments}" style="width:200px"/></td>
						
						<td><g:select name="specimens.finalPrimaryAnatomicSite.${specimenInstance.id}" from="${hbis.AnatomicSite.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalPrimaryAnatomicSite?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.finalTissueType.${specimenInstance.id}" from="${hbis.TissueType.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalTissueType?.id}" class="many-to-one" noSelection="['null': '']"/></td>
										
						<td><g:select name="specimens.finalDiagnosis.${specimenInstance.id}" from="${hbis.Diagnosis.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalDiagnosis?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.finalSubDiagnosis.${specimenInstance.id}" from="${hbis.SubDiagnosis.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalSubDiagnosis?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.finalModDiagnosis.${specimenInstance.id}" from="${hbis.ModDiagnosis.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalModDiagnosis?.id}" class="many-to-one" noSelection="['null': '']"/></td>
									
						<td><g:hiddenField name="goodForTma.${specimenInstance.id}" value="on" />
							<g:checkBox name="goodForTma.${specimenInstance.id}" value="on" checked="${specimenInstance?.qcResult?.goodForTma}" /></td>
							
						<td><g:select name="specimens.finalGrade.${specimenInstance.id}" from="${hbis.Grade.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalGrade?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.finalStage.${specimenInstance.id}" from="${hbis.Stage.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalStage?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.finalMolecularStatusEr.${specimenInstance.id}" from="${hbis.MolecularStatusEr.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalMolecularStatusEr?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.finalMolecularStatusPr.${specimenInstance.id}" from="${hbis.MolecularStatusPr.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalMolecularStatusPr?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.finalMolecularStatusHer2.${specimenInstance.id}" from="${hbis.MolecularStatusHer2.list()}" optionKey="id" value="${specimenInstance?.qcResult?.finalMolecularStatusHer2?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						<td><g:select name="specimens.qcFollowUpAction.${specimenInstance.id}" from="${hbis.QcFollowUpAction.list()}" optionKey="id" value="${specimenInstance?.qcResult?.qcFollowUpAction?.id}" class="many-to-one" noSelection="['null': '']"/></td>
						
						</tr>
					</g:each>
				</tbody>
			</table>
			</div>
			<g:hiddenField name="qcBatchId" value="${qcBatchInstance.id}" />
			<g:submitButton name="saveProgress" class="save" value="${message(code: 'qcBatch.button.saveProgress.label', default: 'Save Current Progress')}" />
				<fieldset class="form">
					<g:hiddenField name="qcBatchVersion" value="${qcBatchInstance.version}" />
					<g:hiddenField name="view" value="pathologistReview" />
					<g:hiddenField name="qcBatchStatus" value="Complete" />
					<!-- Hidden field for date completed goes here -->
				</fieldset>
				
				<fieldset class="buttons">
					<g:submitButton name="update" class="save" value="${message(code: 'qcBatch.button.pathologistReviewComplete.label', default: 'Submit Your Pathology Confirmation')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
