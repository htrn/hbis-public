
<%@ page import="hbis.QcBatch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QC Batch')}" />
		
		<g:javascript library="jquery" />
		
		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
          		$("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
    	
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-qcBatch" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-qcBatch" class="content scaffold-list" role="main">
			<h1>Search QC Batches</h1>
			<g:form action="search" >
				<fieldset class="form">
					<div>
						<label for="qcBatchStatus">
							<g:message code="qcBatch.status.label" default="QC Batch Status" />
						</label>
						<g:select id="qcBatchStatus" name="qcBatchStatusId" from="${hbis.QcBatchStatus.list()}" optionKey="id" value="${qcBatchStatusId}" class="many-to-one" noSelection="['null': '']"/>
					</div>
					
					<div>
						<label for="fromDate">
							<g:message code="qcBatch.dateCreated.fromDate" default="Date Created From" />
						</label>
						<g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
					</div>
					
					<div>
						<label for="toDate">
							<g:message code="qcBatch.dateCreated.toDate" default="Date Created To" />
						</label>
						<g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
					</div>
					
					<div>
						<label for="description">
							<g:message code="qcBatch.description.toDate" default="Description" />
						</label>
						<g:textField name="description" value="${description}" />
					</div>
					
					<div>
						<label for="specimenChtnId">
							<g:message code="specimen.specimenChtnId.toDate" default="Specimen Number" />
						</label>
						<g:textField name="specimenChtnId" value="${specimenChtnId}" />
					</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="search" class="save" value="${message(code: 'default.button.search.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${qcBatchInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${qcBatchInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:if test="${qcBatchInstanceList}">
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'qcBatch.id.label', default: 'Batch ID')}" />
						
						<g:sortableColumn property="description" title="${message(code: 'qcBatch.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="status" title="${message(code: 'qcBatch.status.label', default: 'Status')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'qcBatch.dateCreated.label', default: 'Date Created')}" />
						
						<th><g:message code="qcBatch.numberOfSpecimens.label" default="# Specimens" /></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${qcBatchInstanceList}" status="i" var="qcBatchInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>${fieldValue(bean: qcBatchInstance, field: "id")}</td>
						
						<td>${fieldValue(bean: qcBatchInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: qcBatchInstance, field: "status")}</td>
					
						<td><g:formatDate date="${qcBatchInstance.dateCreated}" /></td>
						
						<td>${qcBatchInstance.numberOfSpecimens}</td>
						
						<td><g:link action="viewBatch" id="${qcBatchInstance.id}">View Batch</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			</g:if>
		</div>
	</body>
</html>
