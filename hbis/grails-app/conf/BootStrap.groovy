import org.apache.shiro.crypto.hash.Sha256Hash
import hbis.*

class BootStrap {


    def init = { servletContext ->
		def user = new ShiroUser(username: "user123", passwordHash: new Sha256Hash("password").toHex())
		user.addToPermissions("*:*")
		if (user.save()) {
			println "User: ${user.username}, ADDED!"
    	} else {
			println "Error creating User: ${user.username}"
    	}

        if(!Price.count()) {
            def prices = [0.00, 4.00, 4.50, 5.00, 6.00, 7.00, 8.00, 8.50, 12.00, 14.00, 16.00, 18.00,
                          20.00, 25.00, 27.10, 33.14, 40.00, 50.00, 75.00, 98.52, 100.00, 150.00]
            prices.each { it ->
                try {
                    def price = new Price(
                            name: "CHTN",
                            price: it as BigDecimal,
                            display: true)

                    price.save(failOnError: true)

                    if (price.hasErrors()) {
                        log.error("Could not create Price Object ${asm.errors}")
                    }
                } catch (Exception e) {
                    log.error("Could not create Price Object ${e.message}")
                }
            }
        }
    }
    def destroy = {
    }
}

