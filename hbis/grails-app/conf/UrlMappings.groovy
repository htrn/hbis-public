class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/" {
			controller = "specimen"
			action = "specimensProcuredToday"
		}
		"500"(view:'/error')
	}
}
