grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

def environmentName = grails.util.Environment.current.name.toUpperCase().take(4)
def date = Date.parse('EEE MMM dd HH:mm:ss zzz yyy', new Date().toString())
def buildNumber = System.properties['buildNumber'] ?: date.format('MMddyyHHmmss')

grails.project.war.file = "target/${appName}-${appVersion}-${environmentName}-build-${buildNumber}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()
        mavenRepo "https://repository.jboss.org/nexus/"
        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"

        grailsRepo "https://grails.org/plugins"

        // repositories for the clover code coverage plugin
        mavenRepo "https://maven.atlassian.com/repository/public/"
        mavenRepo "https://maven.atlassian.com/content/repositories/atlassian-public-snapshot/"
        mavenRepo "https://maven.atlassian.com/content/repositories/atlassian-central-snapshot/"
    }

    def spockVersion = '0.7'

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
        runtime 'mysql:mysql-connector-java:5.1.24'
        runtime 'org.mariadb.jdbc:mariadb-java-client:1.1.7'

        test "org.spockframework:spock-grails-support:0.7-groovy-2.0"

        compile "commons-codec:commons-codec:1.6"

        compile "org.grails.plugins:codenarc:0.24.1"

        compile 'com.atlassian.clover:clover:4.1.1'
    }

    plugins {
        runtime ":hibernate:$grailsVersion"
        runtime ":jquery:1.8.3"
        runtime ":resources:1.1.6"
        runtime ':jasper:1.6.1'

        // Uncomment these (or add new ones) to enable additional resources capabilities
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.5"

        build ":tomcat:$grailsVersion"
        build ":codenarc:0.24.1"

        runtime ":database-migration:1.3.2"

        compile ':cache:1.0.1'

        compile ":platform-core:1.0.0"

        compile ":shiro:1.1.4"

        compile ":jasper:1.6.1"

        compile ":jquery-ui:1.8.24"

        compile 'org.grails.plugins:clover:4.1.1'

        test(":spock:$spockVersion") {
            exclude "spock-grails-support"
        }
    }
}
