package hbis

import java.text.SimpleDateFormat

class SubspecimenService {
    def sdf = new SimpleDateFormat("MM/dd/yyyy")

    /**
     * This lookup table will give us the ability to generate a comma separated list of strings
     * and maintain the same order as we see in the general subspecimen search results
     */
    def csvHeaderLookup = [
            showMrn: [ header: 'MRN', weight: 1, accessor: 'specimen.procedure.patient.mrn' ],
            showGender: [ header: 'Gender', weight: 2, accessor: 'specimen.procedure.patient.gender' ],
            showRace: [ header: 'Race', weight: 3, accessor: 'specimen.procedure.patient.race' ],
            showPatientAge: [ header: 'Patient Age', weight: 5, accessor: 'specimen.procedure.patientAge' ],
            showOriginInstitution: [ header: 'Institution', weight: 4, accessor: 'specimen.procedure.patient.originInstitution' ],
            showProcedureType: [ header: 'Procedure Type', weight: 6, accessor: 'specimen.procedure.procedureType.description' ],
            showInvestigatorChtnId: [ header: 'Invest. ID Prefix', weight: 7, accessor: 'investigator.chtnIdPrefix' ],
            showInvestigatorFirstName: [ header: 'Invest. First Name', weight: 8, accessor: 'investigator.firstName' ],
            showInvestigatorLastName: [ header: 'Invest. Last Name', weight: 9, accessor: 'investigator.lastName' ],
            showInvestigatorInstitutionType: [ header: 'Invest. Inst. Type', weight: 9.1, accessor: 'investigator.institutionType' ],
            showSpecimenChtnId: [ header: 'Specimen ID', weight: 0, accessor: 'specimen.specimenChtnId' ],
            showProcurementDate: [ header: 'Procurement Date', weight: 10, accessor: 'specimen.procurementDate' ],
            showTimePreparedDate: [ header: 'Prepared Time', weight: 12, accessor: 'specimen.timePrepared' ],
            showTimeReceivedDate: [ header: 'Received Time', weight: 11, accessor: 'specimen.timeReceived' ],
            showPreliminaryPrimaryAnatomicSite: [ header: 'Prelim. Primary Anatomic Site', weight: 20, accessor: 'specimen.preliminaryPrimaryAnatomicSite' ],
            showFinalPrimaryAnatomicSite: [ header: 'Final Primary Anatomic Site', weight: 21, accessor: 'specimen.qcResult.finalPrimaryAnatomicSite' ],
            showPreliminaryTissueType: [ header: 'Prelim. Tissue Type', weight: 22, accessor: 'specimen.preliminaryTissueType' ],
            showFinalTissueType: [ header: 'Final Tissue Type', weight: 24, accessor: 'specimen.qcResult.finalTissueType' ],
            showPrimaryOrMets: [ header: 'Specimen PrimaryMets', weight: 23, accessor: 'specimen.primaryOrMets' ],
            showPreliminaryDiagnosis: [ header: 'Prelim. Diagnosis', weight: 25, accessor: 'specimen.preliminaryDiagnosis' ],
            showFinalDiagnosis: [ header: 'Final Diagnosis', weight: 26, accessor: 'specimen.qcResult.finalDiagnosis' ],
            showPreliminarySubDiagnosis: [ header: 'Prelim. Sub Diagnosis', weight: 27, accessor: 'specimen.preliminarySubDiagnosis' ],
            showFinalSubDiagnosis: [ header: 'Final Sub Diagnosis', weight: 28, accessor: 'specimen.qcResult.finalSubDiagnosis' ],
            showPreliminaryModDiagnosis: [ header: 'Prelim. Mod Diagnosis', weight: 27.1, accessor: 'specimen.preliminaryModDiagnosis' ],
            showFinalModDiagnosis: [ header: 'Final Mod Diagnosis', weight: 29, accessor: 'specimen.qcResult.finalModDiagnosis' ],
            showTissueQcMatch: [ header: 'QC Result', weight: 30, accessor: 'specimen.qcResult.tissueQcMatch' ],
            showPreparationType: [ header: 'Preparation Type', weight: 31, accessor: 'preparationType' ],
            showPreparationMedia: [ header: 'Preparation Media', weight: 32, accessor: 'preparationMedia' ],
            showWeight: [ header: 'Weight', weight: 33, accessor: 'weight'],
            showVolume: [ header: 'Volume', weight: 34, accessor: 'volume' ],
            showSizeWidth: [ header: 'Width', weight: 36, accessor: 'sizeWidth' ],
            showSizeLength: [ header: 'Length', weight: 35, accessor: 'sizeLength' ],
            showSizeHeigth: [ header: 'Size', weight: 36.1, accessor: 'sizeHeight' ],
            showSubspecimenStatus: [ header: 'Status', weight: 16, accessor: 'status' ],
            showInvoiceDate: [ header: 'Invoice Date', weight: 19, accessor: 'shippingCart.invoice.invoiceDate' ],
            showSizeHeight: [ header: 'Height', weight: 37, accessor: 'sizeHeight' ],
            showShipDate: [ header: 'Ship Date', weight: 18, accessor: 'shippingCart.shipDate' ],
            showFinalMolecularStatusEr: [ header: 'Final Molecular Status ER', weight: 38, accessor: 'specimen.qcResult.finalMolecularStatusEr' ],
            showFinalMolecularStatusPr: [ header: 'Final Molecular Status PR', weight: 39, accessor: 'specimen.qcResult.finalMolecularStatusPr' ],
            showFinalMolecularStatusHer2: [ header: 'Final Molecular Status HER2', weight: 40, accessor: 'specimen.qcResult.finalMolecularStatusHer2' ],
            showTissueRequestTissueQuestId: [ header: 'Tissue Quest ID', weight: 41, accessor: 'tissueRequest.tissueRequestTissueQuestId' ],
            showSpecimenCategory: [ header: 'Specimen Category', weight: 13, accessor: 'specimen.specimenCategory' ],
            showQCFollowUpAction: [ header: 'QC Follow Up Action', weight: 30.1, accessor: 'specimen.qcResult.qcFollowUpAction' ],
            showMetsToAnatomicSite: [ header: 'Mets to Anatomic Site', weight: 14, accessor: 'specimen.metsToAnatomicSite'],
            showProcurementTechInitials: [ header: 'Procurement Tech Initials', weight: 15, accessor: 'specimen.techInitials' ],
            showSubspecimenLabelComment: [ header: 'Label Comment', weight: 17, accessor: 'labelComment' ]
    ]

    def generalSearch(params) {
        //This will be a very general search, and all parameters may or may not appear, so must check
        //  for each when placing into local variables or when including them in the SubspecimenCriteria
        def sdfForHourAndMin = new SimpleDateFormat("HHmm"), procurementDateFrom, procurementDateTo,
                shipDateFrom, shipDateTo, invoiceDateFrom, invoiceDateTo, timePreparedFrom, timePreparedTo, timeReceivedFrom, timeReceivedTo,
                cal = Calendar.getInstance()

        if(params.shipDateFromTextField || params.shipDateToTextField) {
            shipDateFrom = params.shipDateFromTextField ? sdf.parse(params.shipDateFromTextField) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')
            shipDateTo = params.shipDateToTextField ? sdf.parse(params.shipDateToTextField) : new Date()
            cal.setTime(shipDateTo)
            shipDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")
        }

        if(params.invoiceDateFromTextField || params.invoiceDateToTextField) {
            invoiceDateFrom = params.invoiceDateFromTextField ? sdf.parse(params.invoiceDateFromTextField) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')
            invoiceDateTo = params.invoiceDateToTextField ? sdf.parse(params.invoiceDateToTextField) : new Date()
            cal.setTime(invoiceDateTo)
            invoiceDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")
        }

        if(params.procurementDateFromTextField || params.procurementDateToTextField) {
            procurementDateFrom = params.procurementDateFromTextField ? sdf.parse(params.procurementDateFromTextField) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')
            procurementDateTo = params.procurementDateToTextField ? sdf.parse(params.procurementDateToTextField) : new Date()
            cal.setTime(procurementDateTo)
            procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")
        }

        if(params.timePreparedFrom && params.timePreparedTo) {
            timePreparedFrom = sdfForHourAndMin.parse(params.timePreparedFrom)
            timePreparedTo = sdfForHourAndMin.parse(params.timePreparedTo)
        }

        if(params.timeReceivedFrom && params.timeReceivedTo) {
            timeReceivedFrom = sdfForHourAndMin.parse(params.timeReceivedFrom)
            timeReceivedTo = sdfForHourAndMin.parse(params.timeReceivedTo)
        }

        def investigatorChtnIdPrefixList
        if(params.investigatorChtnIdPrefixId) {
            investigatorChtnIdPrefixList = InvestigatorChtnIdPrefix.getAll(params.list('investigatorChtnIdPrefixId'))
        }

        def subspecimenCriteria = Subspecimen.createCriteria(), subspecimens

        subspecimens = subspecimenCriteria.list {
            if(params.preparationTypeId) {
                eq("preparationType", PreparationType.read(params.preparationTypeId.toLong()))
            }
            if(params.preparationMediaId) {
                eq("preparationMedia", PreparationMedia.read(params.preparationMediaId.toLong()))
            }
            if(params.subspecimenStatusId) {
                eq("status", SubspecimenStatus.read(params.subspecimenStatusId.toLong()))
            }
            if(params.weight) {
                eq("weight", new java.math.BigDecimal(params.weight)) //Since weight is incoming as a String, need to cast it as a BigDecimal
            }
            if(params.volume) {
                eq("volume", new java.math.BigDecimal(params.volume))
            }
            if(params.sizeWidth) {
                eq("sizeWidth", new java.math.BigDecimal(params.sizeWidth))
            }
            if(params.sizeLength) {
                eq("sizeLength", new java.math.BigDecimal(params.sizeLength))
            }
            if(params.sizeHeight) {
                eq("sizeHeight", new java.math.BigDecimal(params.sizeHeight))
            }
            if(params.investigatorChtnId || params.investigatorFirstName || params.investigatorLastName || investigatorChtnIdPrefixList || params.institutionTypeId) {
                investigator {
                    if(params.investigatorChtnId) {
                        ilike("chtnId", "%" + params.investigatorChtnId + "%")
                    }
                    if(params.investigatorFirstName) {
                        ilike("firstName", "%" + params.investigatorFirstName + "%")
                    }
                    if(params.investigatorLastName) {
                        ilike("lastName", "%" + params.investigatorLastName + "%")
                    }
                    if(investigatorChtnIdPrefixList) {
                        'in'("chtnIdPrefix", investigatorChtnIdPrefixList)
                    }
                    if(params.institutionTypeId) {
                        eq("institutionType", InstitutionType.read(params.institutionTypeId.toLong()))
                    }
                }
            }

            if(params.subspecimenLabelComment) {
                ilike("labelComment", "%" + params.subspecimenLabelComment + "%")
            }

            if(shipDateFrom || shipDateTo || invoiceDateFrom || invoiceDateTo) {
                shippingCart {
                    if(shipDateFrom || shipDateTo) {
                        between("shipDate", shipDateFrom, shipDateTo)
                    }
                    if(invoiceDateFrom || invoiceDateTo) {
                        invoice {
                            between("invoiceDate", invoiceDateFrom, invoiceDateTo)
                        }
                    }
                }
            }
            specimen {
                if(params.specimenChtnId) {
                    ilike("specimenChtnId", "%" + params.specimenChtnId + "%")
                }

                if(procurementDateFrom && procurementDateTo) {
                    between("procurementDate", procurementDateFrom, procurementDateTo)
                }

                if(timePreparedFrom && timePreparedTo) {
                    between("timePrepared", timePreparedFrom, timePreparedTo)
                }

                if(timeReceivedFrom && timeReceivedTo) {
                    between("timeReceived", timeReceivedFrom, timeReceivedTo)
                }

                //get the correct SpecimenCategory object
                if(params.specimenCategoryId) {
                    eq("specimenCategory", SpecimenCategory.read(params.specimenCategoryId.toLong()))
                }

                if(params.metsToAnatomicSite) {
                    eq("metsToAnatomicSite", MetsToAnatomicSite.read(params.metsToAnatomicSite.toLong()))
                }

                if(params.techInitialsId) {
                    eq("techInitials", ProcurementTechInitials.read(params.techInitialsId.toLong()))
                }

                if(params.preliminaryPrimaryAnatomicSite) {
                    preliminaryPrimaryAnatomicSite {
                        ilike("description", "%" + params.preliminaryPrimaryAnatomicSite + "%")
                    }
                }

                if(params.preliminaryDiagnosis) {
                    preliminaryDiagnosis {
                        ilike("description", "%" + params.preliminaryDiagnosis + "%")
                    }
                }

                if(params.preliminarySubDiagnosis) {
                    preliminarySubDiagnosis {
                        ilike("description", "%" + params.preliminarySubDiagnosis + "%")
                    }
                }

                if(params.preliminaryModDiagnosis) {
                    preliminaryModDiagnosis {
                        ilike("description", "%" + params.preliminaryModDiagnosis + "%")
                    }
                }

                if(params.preliminaryTissueTypeId) {
                    eq("preliminaryTissueType", TissueType.read(params.preliminaryTissueTypeId.toLong()))
                }
                if(params.primaryOrMetsId) {
                    eq("primaryOrMets", PrimaryMets.read(params.primaryOrMetsId.toLong()))
                }
                procedure {
                    patient {
                        if(params.mrn) {
                            ilike("mrn", "%" + params.mrn + "%")
                        }
                        if(params.genderId) {
                            eq("gender", Gender.read(params.genderId.toLong()))
                        }
                        if(params.raceId) {
                            eq("race", Race.read(params.raceId.toLong()))
                        }
                        if(params.originInstitutionId) {
                            eq("originInstitution", Institution.read(params.originInstitutionId.toLong()))
                        }
                    }
                    if(params.procedureTypeId) {
                        eq("procedureType", ProcedureType.read(params.procedureTypeId.toLong()))
                    }
                    if(params.patientAge) {
                        eq("patientAge", params.int('patientAge'))
                    }
                    if(params.patientAgeUnitId) {
                        eq("patientAgeUnit", AgeUnit.read(params.patientAgeUnitId.toLong()))
                    }
                }
                if(params.tissueQcMatchPassFail || params.finalPrimaryAnatomicSite || params.finalTissueTypeId
                        || params.finalDiagnosis || params.finalSubDiagnosis || params.finalModDiagnosis
                        || params.finalMolecularStatusErId || params.finalMolecularStatusPrId || params.finalMolecularStatusHer2Id
                        || params.qcFollowUpAction
                ) {
                    qcResult {
                        if (params.tissueQcMatchPassFail) {
                            tissueQcMatch {
                                ilike("description", params.tissueQcMatchPassFail + "%")
                            }
                        }
                        if(params.finalPrimaryAnatomicSite) {
                            finalPrimaryAnatomicSite {
                                ilike("description", "%" + params.finalPrimaryAnatomicSite + "%")
                            }
                        }
                        if(params.finalTissueTypeId) {
                            eq("finalTissueType", TissueType.read(params.finalTissueTypeId.toLong()))
                        }

                        if(params.finalDiagnosis) {
                            finalDiagnosis {
                                ilike("description", "%" + params.finalDiagnosis + "%")
                            }
                        }

                        if(params.finalSubDiagnosis) {
                            finalSubDiagnosis {
                                ilike("description", "%" + params.finalSubDiagnosis + "%")
                            }
                        }

                        if(params.finalModDiagnosis) {
                            finalModDiagnosis {
                                ilike("description", "%" + params.finalModDiagnosis + "%")
                            }
                        }
                        if(params.finalMolecularStatusErId) {
                            eq("finalMolecularStatusEr", MolecularStatusEr.read(params.finalMolecularStatusErId.toLong()))
                        }
                        if(params.finalMolecularStatusPrId) {
                            eq("finalMolecularStatusPr", MolecularStatusPr.read(params.finalMolecularStatusPrId.toLong()))
                        }
                        if(params.finalMolecularStatusHer2Id) {
                            eq("finalMolecularStatusHer2", MolecularStatusHer2.read(params.finalMolecularStatusHer2Id.toLong()))
                        }

                        if(params.qcFollowUpAction) {
                            eq("qcFollowUpAction", QcFollowUpAction.read(params.qcFollowUpAction.toLong()))
                        }
                    }
                }
            }
            if(params.tissueRequestTissueQuestId) {
                tissueRequest {
                    ilike("tissueRequestTissueQuestId", "%" + params.tissueRequestTissueQuestId + "%")
                }
            }
        }

        subspecimens.sort { a, b ->
            a.specimen.specimenChtnId <=> b.specimen.specimenChtnId ?: a.subspecimenChtnId <=> b.subspecimenChtnId
        }

        return subspecimens
    }

    def apqiSearch(params) {
        def apqiSearchResults = [:], distinctPatientsCriteria, totalQcPerformedSpecimenCriteria, numberQcPassedSpecimenCriteria,
            numberQcFailedSpecimenCriteria, partsPerCaseSpecimenCriteria, totalSubspecimensProcuredCriteria,
            distinctAutopsyPatientsCriteria, totalAutopsySubspecimensCriteria

        distinctPatientsCriteria = Patient.createCriteria()
        totalQcPerformedSpecimenCriteria = Specimen.createCriteria()
        numberQcPassedSpecimenCriteria = Specimen.createCriteria()
        numberQcFailedSpecimenCriteria = Specimen.createCriteria()


        partsPerCaseSpecimenCriteria = Specimen.createCriteria()
        totalSubspecimensProcuredCriteria = Subspecimen.createCriteria()
        distinctAutopsyPatientsCriteria = Patient.createCriteria()
        totalAutopsySubspecimensCriteria = Subspecimen.createCriteria()

        def sdf = new SimpleDateFormat("MM/dd/yyyy"), fromDate, toDate

        def cal = Calendar.getInstance()

        fromDate = sdf.parse(params.fromDate)

        toDate = sdf.parse(params.toDate)
        cal.setTime(toDate)
        toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")
        apqiSearchResults.displayCounts = false

        // createCriteria between is exclusive, so have to subtract or add to the given dates to make it inclusive of the dates given
        apqiSearchResults.distinctPatients = distinctPatientsCriteria.get {
            projections {
                countDistinct('id')
            }
            procedures {
                procedureEventInstitution {
                    eq("description", "OSU")
                }
                specimens {
                    between("procurementDate", fromDate, toDate)
                }
            }
        }
        apqiSearchResults.totalQcPerformed = totalQcPerformedSpecimenCriteria.get {
            projections {
                countDistinct('id')
            }
            qcBatch {
                eq("status", QcBatchStatus.findByDescription("Complete"))
            }
            between("procurementDate", fromDate, toDate)
            procedure {
                procedureEventInstitution {
                    eq("description", "OSU")
                }
            }
        }
        apqiSearchResults.numberQcPassed = numberQcPassedSpecimenCriteria.get {
            projections {
                countDistinct('id')
            }
            qcResult {
                tissueQcMatch {
                    ilike("description", "Pass%")
                }
            }
            between("procurementDate", fromDate, toDate)
            procedure {
                procedureEventInstitution {
                    eq("description", "OSU")
                }
            }
        }
        apqiSearchResults.numberQcFailed = numberQcFailedSpecimenCriteria.get {
            projections {
                countDistinct('id')
            }
            qcResult {
                tissueQcMatch {
                    ilike("description", "Fail%")
                }
            }
            between("procurementDate", fromDate, toDate)
            procedure {
                procedureEventInstitution {
                    eq("description", "OSU")
                }
            }
        }
        apqiSearchResults.partsPerCase = partsPerCaseSpecimenCriteria.get {
            projections {
                countDistinct('specimenChtnId')
            }
            between("procurementDate", fromDate, toDate)
            procedure {
                procedureEventInstitution {
                    eq("description", "OSU")
                }
            }
        }
        apqiSearchResults.totalSubspecimensProcured = totalSubspecimensProcuredCriteria.get {
            projections {
                countDistinct('id')
            }
            specimen {
                between("procurementDate", fromDate, toDate)
                procedure {
                    procedureEventInstitution {
                        eq("description", "OSU")
                    }
                }
            }
        }
        apqiSearchResults.distinctAutopsyPatients = distinctAutopsyPatientsCriteria.get {
            projections {
                countDistinct('id')
            }
            procedures {
                procedureType {
                    eq("description", "Autopsy")
                }
                procedureEventInstitution {
                    eq("description", "OSU")
                }
                specimens {
                    between("procurementDate", fromDate, toDate)
                }
            }
        }
        apqiSearchResults.totalAutopsySubspecimens = totalAutopsySubspecimensCriteria.get {
            projections {
                countDistinct('id')
            }
            specimen {
                between("procurementDate", fromDate, toDate)
                procedure {
                    procedureType {
                        eq("description", "Autopsy")
                    }
                    procedureEventInstitution {
                        eq("description", "OSU")
                    }
                }
            }
        }

        // In HQL, between is inclusive, so there is no altering of dates given
        apqiSearchResults.procurementResultQuantityList = Procedure.executeQuery(
                "select new map(procurementResult.description as procurementResultDescription, Count(*) as quantity) " +
                        "from Procedure as procedure " +
                        "left outer join procedure.procurementResult as procurementResult " +
                        "where procedure.procedureDate between :fromDate and :toDate " +
                        "and procedure.procedureEventInstitution.description = 'OSU' " +
                        "group by procurementResult.description " +
                        "order by procurementResult.description",
                [fromDate: fromDate, toDate: toDate])

        apqiSearchResults.procedureNameSubNameByResultQuantityList = Procedure.executeQuery(
                "select new map(procedure.procedureName.description as procedureNameDescription, subProcedureName.description as subProcedureNameDescription, procurementResult.description as procurementResultDescription, Count(*) as quantity) " +
                        "from Procedure as procedure " +
                        "left outer join procedure.subProcedureName as subProcedureName " +
                        "left outer join procedure.procurementResult as procurementResult " +
                        "where procedure.procedureDate between :fromDate and :toDate " +
                        "and procedure.procedureEventInstitution.description = 'OSU' " +
                        "group by procedure.procedureName.description, subProcedureName.description, procurementResult.description " +
                        "order by procedure.procedureName.description, subProcedureName.description, procurementResult.description ",
                [fromDate: fromDate, toDate: toDate])

        apqiSearchResults.originQuantityList = Procedure.executeQuery (
                "select new map(origin.description as originDescription, Count(*) as quantity) " +
                        "from Procedure as procedure " +
                        "left outer join procedure.origin as origin " +
                        "where procedure.procedureDate between :fromDate and :toDate " +
                        "and procedure.procedureEventInstitution.description = 'OSU' " +
                        "group by origin.description " +
                        "order by origin.description",
                [fromDate: fromDate, toDate: toDate])

        apqiSearchResults.displayCounts = true

        return apqiSearchResults
    }

    def findSelectedFields(params) {
        def fields = []

        // grab only the lookup table elements we are interested in
        params.each { k, v  ->
            if(v == 'on'){
                fields.add(csvHeaderLookup[k])
            }
        }

        // subspecimen id will always appear, but it should appear after the specimen id
        // if specimen id is visible among search results
        fields.add([header: 'Subspecimen ID', weight: 0.1, accessor: 'subspecimenChtnId'])

        //sort headers by weight
        fields.sort { a, b ->
            a.weight <=> b.weight
        }

        return fields
    }

    def generateCSVHeader(fields) {
        def headerString = ''

        //build the header string
        fields.each { it ->
            headerString += it.header + ','

            if(it.header == 'Patient Age'){
                headerString += 'Unit,'
            }

            if(it.header == 'Invest. ID Prefix'){
                headerString += 'Invest. ID,'
            }

            if(it.header == 'Weight'){
                headerString += 'Weight Unit,'
            }

            if(it.header == 'Volume'){
                headerString += 'Volume Unit,'
            }

            if(it.header == 'Length' || it.header == 'Width' || it.header == 'Height'){
                headerString += 'Units,'
            }
        }

        // remove the last comma
        headerString = headerString.replaceAll(/,\s*$/, '')

        headerString += '\n'

        return headerString
    }

    def searchQcReport(params) {
        def sdf = new SimpleDateFormat("MM/dd/yyyy"), fromDate, toDate,
                tissueQcMatchString = params.tissueQcMatch // named tissueQcMatchString here to avoid errors with referencing tissueQcMatch in the criteria below

        fromDate = params.fromDate ? sdf.parse(params.fromDate) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')
        toDate = params.toDate ? sdf.parse(params.toDate) : new Date()

        def subspecimenCriteria = Subspecimen.createCriteria(), subspecimens = []

        if(tissueQcMatchString) {
            subspecimens = subspecimenCriteria.list {
                eq("status", SubspecimenStatus.findByDescription("QC"))
                specimen {
                    between("procurementDate", fromDate, toDate)

                    if(tissueQcMatchString) {
                        qcResult {
                            tissueQcMatch {
                                ilike("description", tissueQcMatchString + "%")
                            }
                        }
                    }
                    order("specimenChtnId", "asc")
                }
                order("subspecimenChtnId", "asc")
            }
        }

        return subspecimens
    }

    def subcontractReport(params) {
        def sdf = new SimpleDateFormat("MM/dd/yyyy"), fromDate, toDate

        fromDate = params.fromDate ? sdf.parse(params.fromDate) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')
        toDate = params.toDate ? sdf.parse(params.toDate) : new Date()

        def organization = params.organization, subspecimenCriteria = Subspecimen.createCriteria(), subspecimens = []

        if( organization && fromDate && toDate ) {
            subspecimens = subspecimenCriteria.list {
                //Add check for shipped status
                status {
                    eq("description", "Shipped")
                }
                shippingCart {
                    between("shipDate", fromDate, toDate)
                }
                specimen {
                    procedure {
                        procedureEventInstitution {
                            if(organization == "CCF") {
                                eq("description", "CCF")
                            }
                            else if(organization == "CWR") {
                                eq("description", "CWR")
                            }

                        }
                    }
                    order("specimenChtnId", "asc")
                }
                order("subspecimenChtnId", "asc")
            }
        }

        return subspecimens
    }

    def searchProcurementReport(investigatorId, fromDate, toDate) {
        def subspecimenCriteria = Subspecimen.createCriteria(), subspecimens = []

        if( fromDate || toDate ) {
            subspecimens = subspecimenCriteria.list {
                if(investigatorId != 'null') {
                    eq("investigator", Investigator.read(investigatorId.toLong()))
                }
                specimen {
                    between("procurementDate", fromDate, toDate)

                    order("specimenChtnId", "asc")
                }
                order("subspecimenChtnId", "asc")
            }
        }

        return subspecimens
    }

    def internalBillingReport(params) {
        def sdf = new SimpleDateFormat("MM/dd/yyyy")
        def fromDate
        def toDate
        if(params.fromDate) {
            fromDate = sdf.parse(params.fromDate)
        }

        if(params.toDate) {
            toDate = sdf.parse(params.toDate)
        }
        def organization = params.organization
        def subspecimenCriteria = Subspecimen.createCriteria()
        def subspecimens = []
        if( organization && fromDate && toDate ) {
            subspecimens = subspecimenCriteria.list {
                status {
                    eq("description", "Shipped")
                }
                shippingCart {
                    between("shipDate", fromDate, toDate)
                }
                if(organization == "CCC") {
                    or {
                        investigator {
                            eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("E"))
                        }
                        and {
                            investigator {
                                eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("C"))
                            }
                            specimen {
                                procedure {
                                    procedureEventInstitution {
                                        eq("description", "OSU")
                                    }
                                }
                            }
                        }
                    }
                }
                else if(organization == "CHTN") {
                    or {
                        investigator {
                            eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("B"))
                        }
                        and {
                            investigator {
                                eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("C"))
                            }
                            specimen {
                                procedure {
                                    procedureEventInstitution {
                                        or {
                                            eq("description", "CCF")
                                            eq("description", "CWR")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                order("investigator", "asc")
                specimen {
                    order("specimenChtnId", "asc")
                }
                order("subspecimenChtnId", "asc")
            }
        }

        return subspecimens
    }

    def searchDistributionReport(params) {
        def fromDate
        def toDate
        if(params.fromDate) {
            fromDate = sdf.parse(params.fromDate)
        }

        if(params.toDate) {
            toDate = sdf.parse(params.toDate)
        }
        def investigatorId = params.investigatorId.toLong()
        def subspecimenCriteria = Subspecimen.createCriteria()
        def subspecimens = []
        if( investigatorId && (fromDate || toDate) ) {
            subspecimens = subspecimenCriteria.list {
                eq("investigator", Investigator.read(investigatorId))
                status {
                    eq("description", "Shipped")
                }
                shippingCart {
                    if(fromDate && !toDate) {
                        ge("shipDate", fromDate) //Adding and removing 1 from dates so those dates are inclusive
                    }
                    else if (!fromDate && toDate) {
                        le("shipDate", toDate)
                    }
                    else { //Both fromDate and toDate are provided
                        between("shipDate", fromDate, toDate)
                    }
                }
                specimen {
                    order("specimenChtnId", "asc")
                }
                order("subspecimenChtnId", "asc")
            }
        }

        return subspecimens
    }

    def searchQc(fromDate, toDate, tissueQcMatchString) {
        def subspecimens, subspecimenCriteria = Subspecimen.createCriteria()

        subspecimens = subspecimenCriteria.list {
            eq("status", SubspecimenStatus.findByDescription("QC"))
            specimen {
                between("procurementDate", fromDate, toDate)

                if(tissueQcMatchString) {
                    qcResult {
                        tissueQcMatch {
                            ilike("description", tissueQcMatchString + "%")
                        }
                    }
                }
                order("specimenChtnId", "asc")
            }
            order("subspecimenChtnId", "asc")
        }

        return subspecimens
    }

    def generateCSVContent(subspecimen, fields) {
        def csvContentString = ''

        //build the content string
        fields.each { it ->
            csvContentString += it.accessor.split( /\./ )
                    .inject( subspecimen ) { obj, prop -> obj?."$prop" }
                    .toString() + ','

            if(it.header == 'Patient Age'){
                csvContentString += 'specimen.procedure.patientAge'.split( /\./ )
                                        .inject( subspecimen ) { obj, prop -> obj?."$prop" }
                                        .toString() + ','
            }

            if(it.header == 'Invest. ID Prefix'){
                csvContentString += 'investigator.chtnId'.split( /\./ )
                        .inject( subspecimen ) { obj, prop -> obj?."$prop" }
                        .toString() + ','
            }

            if(it.header == 'Weight'){
                csvContentString += 'weightUnit'.split( /\./ )
                        .inject( subspecimen ) { obj, prop -> obj?."$prop" }
                        .toString() + ','
            }

            if(it.header == 'Volume'){
                csvContentString += 'volumeUnit'.split( /\./ )
                        .inject( subspecimen ) { obj, prop -> obj?."$prop" }
                        .toString() + ','
            }

            if(it.header == 'Length' || it.header == 'Width' || it.header == 'Height'){
                csvContentString += 'sizeUnit'.split( /\./ )
                        .inject( subspecimen ) { obj, prop -> obj?."$prop" }
                        .toString() + ','
            }
        }

        csvContentString = csvContentString.replaceAll(/,\s*$/, '')

        csvContentString += '\n'

        return csvContentString
    }

    def chtnAnnualReport(params){
        def fromDate, toDate, chtnAnnualReportResults = [:]

        if(params.fromDate) {
            fromDate = sdf.parse(params.fromDate)
        }

        if(params.toDate) {
            toDate = sdf.parse(params.toDate)
        }

        chtnAnnualReportResults.fromDate = fromDate
        chtnAnnualReportResults.toDate = toDate
        chtnAnnualReportResults.displayCounts = false

        if(fromDate && toDate) {
            chtnAnnualReportResults.subspecimenCountListProcuredProcedureType = Subspecimen.executeQuery(
                    "select new map(procedureType.description as procedureTypeDescription, Count(*) as quantity) " +
                            "from Subspecimen as subspecimen " +
                            "inner join subspecimen.specimen.procedure.procedureType as procedureType " +
                            "where (subspecimen.investigator is not null) " +
                            " and ( subspecimen.investigator.chtnIdPrefix.prefix = 'A' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'AC' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'B' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'C' ) " +
                            " and (subspecimen.specimen.procurementDate between :fromDate and :toDate) " +
                            "and ( subspecimen.specimen.procedure.procedureEventInstitution.description = 'OSU' ) " +
                            "group by procedureType.description " +
                            "order by procedureType.description ",
                    [fromDate: fromDate, toDate: toDate])

            chtnAnnualReportResults.subspecimenDistribtedListChtnIdPrefix = Subspecimen.executeQuery(
                    "select new map(chtnIdPrefix.prefix as chtnIdPrefixLetters, Count(*) as quantity) " +
                            "from Subspecimen as subspecimen " +
                            "left outer join subspecimen.investigator.chtnIdPrefix as chtnIdPrefix " +
                            "where ( subspecimen.status.description = 'Shipped' ) " +
                            " and (subspecimen.investigator is not null) " +
                            " and ( subspecimen.investigator.chtnIdPrefix.prefix = 'A' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'AC' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'B' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'C' ) " +
                            " and (subspecimen.shippingCart is not null) " +
                            " and (subspecimen.shippingCart.shipDate between :fromDate and :toDate) " +
                            "and ( subspecimen.specimen.procedure.procedureEventInstitution.description = 'OSU' ) " +
                            "group by chtnIdPrefix.prefix " +
                            "order by chtnIdPrefix.prefix ",
                    [fromDate: fromDate, toDate: toDate])

            chtnAnnualReportResults.subspecimenDistribtedListCategoryPrepTypeMedia = Subspecimen.executeQuery(
                    "select new map(specimenCategory.description as specimenCategoryDescription, preparationType.description as preparationTypeDescription, preparationMedia.description as preparationMediaDescription, Count(*) as quantity) " +
                            "from Subspecimen as subspecimen " +
                            "left outer join subspecimen.specimen.specimenCategory as specimenCategory " +
                            "left outer join subspecimen.preparationType as preparationType " +
                            "left outer join subspecimen.preparationMedia as preparationMedia " +
                            "where ( subspecimen.status.description = 'Shipped' ) " +
                            " and (subspecimen.investigator is not null) " +
                            " and ( subspecimen.investigator.chtnIdPrefix.prefix = 'A' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'AC' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'B' or " +
                            " subspecimen.investigator.chtnIdPrefix.prefix = 'C' ) " +
                            " and (subspecimen.shippingCart is not null) " +
                            " and (subspecimen.shippingCart.shipDate between :fromDate and :toDate) " +
                            "and ( subspecimen.specimen.procedure.procedureEventInstitution.description = 'OSU' ) " +
                            "group by specimenCategory.description, preparationType.description, preparationMedia.description " +
                            "order by specimenCategory.description, preparationType.description, preparationMedia.description ",
                    [fromDate: fromDate, toDate: toDate])

            def chartReviewShippedCriteria = ChartReview.createCriteria()
            chtnAnnualReportResults.chartReviewShippedQuantity = chartReviewShippedCriteria.get {
                projections {
                    count('shippingCarts')
                }
                isNotNull("shippingCarts")
                shippingCarts {
                    status {
                        eq("description", "Shipped")
                    }
                    investigator {
                        chtnIdPrefix {
                            or {
                                eq("prefix", "A")
                                eq("prefix", "AC")
                                eq("prefix", "B")
                                eq("prefix", "C")
                            }
                        }
                    }
                    between("shipDate", fromDate, toDate)
                }
                procedure {
                    procedureEventInstitution {
                        eq("description", "OSU")
                    }
                }
            }

            chtnAnnualReportResults.displayCounts = true
        }

        chtnAnnualReportResults
    }

    def subcontractQuery(organization, fromDate, toDate) {
        def subspecimens, subspecimenCriteria = Subspecimen.createCriteria()

        subspecimens = subspecimenCriteria.list {
            //Add check for shipped status
            status {
                eq("description", "Shipped")
            }
            shippingCart {
                between("shipDate", fromDate, toDate)
            }
            specimen {
                procedure {
                    procedureEventInstitution {
                        if(organization == "CCF") {
                            eq("description", "CCF")
                        }
                        else if(organization == "CWR") {
                            eq("description", "CWR")
                        }

                    }
                }
                order("specimenChtnId", "asc")
            }
            order("subspecimenChtnId", "asc")
        }

        subspecimens
    }

    def internalBilling(organization, fromDate, toDate) {
        def subspecimens, subspecimenCriteria = Subspecimen.createCriteria()

        subspecimens = subspecimenCriteria.list {
            status {
                eq("description", "Shipped")
            }
            shippingCart {
                between("shipDate", fromDate, toDate)
            }
            if(organization == "CCC") {
                or {
                    investigator {
                        eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("E"))
                    }
                    and {
                        investigator {
                            eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("C"))
                        }
                        specimen {
                            procedure {
                                procedureEventInstitution {
                                    eq("description", "OSU")
                                }
                            }
                        }
                    }
                }
            }
            else if(organization == "CHTN") {
                or {
                    investigator {
                        eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("B"))
                    }
                    and {
                        investigator {
                            eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("C"))
                        }
                        specimen {
                            procedure {
                                procedureEventInstitution {
                                    or {
                                        eq("description", "CCF")
                                        eq("description", "CWR")
                                    }
                                }
                            }
                        }
                    }
                }
            }
            order("investigator", "asc")
            specimen {
                order("specimenChtnId", "asc")
            }
            order("subspecimenChtnId", "asc")
        }

        subspecimens
    }

    def searchProcurement(investigatorId, fromDate, toDate) {
        def subspecimens, subspecimenCriteria = Subspecimen.createCriteria()

        subspecimens = subspecimenCriteria.list {
            if(investigatorId != "null") {
                eq("investigator", Investigator.read(investigatorId))
            }
            specimen {
                between("procurementDate", fromDate, toDate)

                order("specimenChtnId", "asc")
            }
            order("subspecimenChtnId", "asc")
        }

        subspecimens
    }

    def searchDistribution(investigatorId, fromDate, toDate) {
        def subspecimens, subspecimenCriteria = Subspecimen.createCriteria()

        subspecimens = subspecimenCriteria.list {
            eq("investigator", Investigator.read(investigatorId))
            status {
                eq("description", "Shipped")
            }
            shippingCart {
                if(fromDate && !toDate) {
                    ge("shipDate", fromDate) //Adding and removing 1 from dates so those dates are inclusive
                }
                else if (!fromDate && toDate) {
                    le("shipDate", toDate)
                }
                else { //Both fromDate and toDate are provided
                    between("shipDate", fromDate, toDate)
                }
            }
            specimen {
                order("specimenChtnId", "asc")
            }
            order("subspecimenChtnId", "asc")
        }

        subspecimens
    }



    //TODO:Request #33: It's the good way for DBA to change logic from database. However, 'like' query don't have a good performance. Need test the performance against the dataset. This code might be refactored again if performance is really bad.
    def getPreparationMediaList(def preparationTypeDescription){
        def preparationMediaList = []
        if(!(preparationTypeDescription == "null" || preparationTypeDescription == null)) {
            def preparationMediaCriteria = PreparationMedia.createCriteria()
            switch(preparationTypeDescription) {
                case 'Fixed':
                    preparationMediaList = preparationMediaCriteria.list {
                        isNotNull("fixed")
                        order("description", "asc")
                    }
                    break
                case 'Fresh':
                    preparationMediaList = preparationMediaCriteria.list {
                        isNotNull("fresh")
                        order("description", "asc")
                    }
                    break

                case 'Frozen':
                    preparationMediaList = preparationMediaCriteria.list {
                        isNotNull("frozen")
                        order("description", "asc")
                    }
                    break
                default:
                    break
            }
        }
        return preparationMediaList
    }
}
