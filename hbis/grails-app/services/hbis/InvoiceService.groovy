package hbis

import java.text.SimpleDateFormat

class InvoiceService {

    def investigatorInvoiceQueryHelper(from, to, id) {
		def sdf = new SimpleDateFormat("MM/dd/yyyy")
		def fromDate
		def toDate
		def investigatorId = id

		fromDate = from ? sdf.parse(from) : sdf.parse('01/01/1970')
		toDate = to ? sdf.parse(to) : new Date()

		def dateQuery = {
			between "invoiceDate", fromDate, toDate
		}

		def queryConditionals = {
			ne "billingStatus.id", (long)1
			and{
				order('investigator', 'asc')
				order('invoiceDate','asc')
			}
		}

		def invoiceCriteria = Invoice.createCriteria()
		def invoiceList

		if(investigatorId){
			def investigatorQuery = {
				eq "investigator.id", investigatorId
			}

			invoiceList = invoiceCriteria.listDistinct(join(investigatorQuery, dateQuery, queryConditionals))
		}else{
			invoiceList = invoiceCriteria.listDistinct(join(dateQuery, queryConditionals))
		}

		return invoiceList		
	}

	Closure join(Object[] queries) {
		return {
			for(q in queries) {
				q.delegate = delegate
				q()
			}
		}
	}
}
