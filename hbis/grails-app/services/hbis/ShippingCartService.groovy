package hbis

class ShippingCartService {

    def getShippingCartForPackingSlip(Long id) {
        def shippingCartCriteria = ShippingCart.createCriteria()

        def shippingCart = shippingCartCriteria.listDistinct {
            eq('id', id)
            fetchMode("subspecimens", org.hibernate.FetchMode.JOIN)
            fetchMode("chartReviews", org.hibernate.FetchMode.JOIN)
        }

        List subspecimenList = []

        subspecimenList.addAll(shippingCart[0].subspecimens)

        subspecimenList.sort { a, b ->
            a.specimen.specimenChtnId <=> b.specimen.specimenChtnId ?: a.subspecimenChtnId <=> b.subspecimenChtnId
        }

        shippingCart[0].subspecimenList.addAll(subspecimenList)

        return shippingCart
    }
}
