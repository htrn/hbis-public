<%@ page import="org.apache.shiro.SecurityUtils" %>
<!DOCTYPE html>
<html dir="ltr" lang="en-US"><head><!-- Created by Artisteer v4.0.0.58475 -->
    <meta charset="utf-8">
    <title>Home</title>
    <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">

    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <g:external dir="css/art" file="style.css" />
    <!--[if lte IE 7]><link rel="/hbis/css/art/stylesheet" href="style.ie7.css" media="screen" /><![endif]-->    
    <g:external dir="css/art" file="style.responsive.css" />
    <g:external dir="css/art" file="nav.css" />
    <g:external dir="css" file="from_main.css" />
    <g:external dir="css" file="osu_navbar-resp.css" />
        
    <g:javascript src="jquery.js" />

    <g:javascript src="script.js" />

    <g:javascript src="script.responsive.js" />

    
<meta name="description" content="Description">
<meta name="keywords" content="Keywords">
<g:layoutHead/>
<r:require module="jquery-ui"/>
<r:layoutResources />


</head>
<body>
<div id="osu_navbar">
        <div class="navbar-container">
            <div class="univ_info">
                <p class="univ_name"><a href="http://osu.edu/" title="The Ohio State University">The Ohio State University</a></p>
            </div>
            <div class="univ_links">
                <div class="links">
                    <ul>
                        <li><a href="http://www.osu.edu/help.php" class="help" title="Help">Help</a></li>
                        <li><a href="http://buckeyelink.osu.edu/" class="buckeyelink" title="BuckeyeLink">BuckeyeLink</a></li>
                        <li><a href="http://www.osu.edu/map/" class="map" title="Map">Map</a></li>
                        <li><a href="http://www.osu.edu/findpeople.php" class="findpeople" title="Find People">Find People</a></li>
                        <li><a href="https://email.osu.edu/" class="webmail" title="Webmail">Webmail</a></li> 
                        <li><a href="http://www.osu.edu/search/" class="search" title="Search">Search Ohio State</a></li>
                    </ul>
                </div><!-- end links -->
            </div><!-- end univ_links-->
        </div><!-- end navbar-container --> <!-- remove for full-width -->
    </div><!-- end osu_navbar -->
<div id="art-main">
<div class="title">
Research Tissue Procurement Information System<br>
</div>
<div id="login-user">
<g:if test="${ SecurityUtils.subject.isAuthenticated()}">            
 Welcome  <%="${ SecurityUtils.subject.getPrincipal()}" %> &nbsp;&nbsp; <g:link controller="auth" action="signOut">Log me out</g:link>
</g:if>
</div>
<nav>
    <g:if test="${ SecurityUtils.subject.isAuthenticated()}">   
    <ul>
        <li><a href="">Procurement</a>
             <ul>
                <li><g:link controller="procedure" action="amProcedureList">AM Procedures List</g:link></li>

             		<li><g:link controller="specimen" action="specimensProcuredToday">Specimens Collection</g:link></li>
                <li><g:link controller="procedure" action="caseResults">Case Results</g:link></li>
                <li><g:link controller="subspecimen" action="editPrices">Subspecimen Pricing</g:link></li>
             </ul>
        </li>
        <li><g:link controller="consent" action="patientSearch">Consent</g:link>
        	<ul>
        			<li><g:link controller="protocol" action="create">Add Protocol</g:link></li>
        			<li><g:link controller="protocol" action="search">Search/Edit Protocols</g:link></li>
        	</ul>
        
        </li>
        <li><g:link controller="chartReview" action="listSubspecimensNeedChartReview">Chart Review</g:link>
        	<!-- Since consolidation of Chart Review screens, does it need a menu item?
        	<ul>
        			<li><g:link url="chartReview/listSubspecimensNeedChartReview">Enter Chart Review Data</g:link>
        	</ul>
        	 -->
        </li>
        <li><a href="">Quality Control</a>
        	<ul>
        		<li><g:link controller="qcBatch" action="listBatchesInPreparation">QC Preparation</g:link></li>
        		<li><g:link controller="qcBatch" action="listBatchesToScan">Scanning</g:link></li>
        		<li><g:link controller="qcBatch" action="listBatchesForImageAnalysis">Image Analysis</g:link></li>
        		<li><g:link controller="qcBatch" action="listBatchesForPathologistReview">Pathologist Review</g:link></li>
        		<li><g:link controller="qcBatch" action="listCompleteBatches">QC Complete</g:link></li>
        		<li><g:link controller="qcBatch" action="search">QC Batch Search</g:link></li>
        	</ul>
        </li>
        <li><a href="">De-identification</a></li>
        <li><a href="">Shipping</a>
        	<ul>
        			<li><g:link controller="shippingCart" action="create">Create Shipping Cart</g:link></li>
        			<li><g:link controller="shippingCart" action="viewPending">Pending Shipping Carts</g:link></li>
        			<li><g:link controller="shippingCart" action="editErampNumber">Edit eRamp Number</g:link></li>
        			<li><g:link controller="subspecimen" action="reassign">Reassign Subspecimens</g:link></li>
        			<li><g:link controller="subspecimen" action="discard">Discard Subspecimens</g:link></li>
        			<li><g:link controller="subspecimen" action="contactExternalFresh">Contact External Fresh</g:link></li>
        			<li><g:link controller="subspecimen" action="contactInternalFresh">Contact Internal Fresh</g:link></li>
        			<li><g:link controller="subspecimen" action="contactEmail">Contact Email</g:link></li>
        	</ul>
        </li>
        <li><a href="">Billing & Accounting</a>
                <ul>
                    <li><g:link controller="invoice" action="create">Create New Invoice</g:link></li>
                    <li><g:link controller="invoice" action="viewPending">Pending Invoices</g:link></li>
                    <li><g:link controller="invoice" action="accountsReceivable">Accounts Receivable</g:link></li>
                    <li><g:link controller="invoice" action="latePayment">Re-invoicing for Late Payments</g:link></li>
                    <li><g:link controller="price">Update Pricing List</g:link></li>
                </ul>
        </li>
        <li><a href="">Reporting</a>
          <ul>
          		<li><g:link controller="invoice" action="outstandingPayments">Outstanding Payments</g:link></li>
          		<li><g:link controller="subspecimen" action="histologyWorksheet">Histology Worksheet</g:link></li>
          		<li><g:link controller="procedure" action="searchCaseResults">Search Case Results</g:link></li>
          		<li><g:link controller="subspecimen" action="searchQc">Search QC</g:link></li>
          		<li><g:link controller="subspecimen" action="apqi">APQI</g:link></li>
          		<li><g:link controller="subspecimen" action="subcontractQuery">Subcontract</g:link></li>
          		<li><g:link controller="subspecimen" action="internalBilling">CCC & CHTN Billing</g:link></li>
          		<li><g:link controller="subspecimen" action="searchProcurement">Search Procurement</g:link></li>
          		<li><g:link controller="subspecimen" action="searchDistribution">Search Distribution</g:link></li>
          		<li><g:link controller="specimen" action="pathologyReportList">Pathology Report List</g:link></li>
          		<li><g:link controller="subspecimen" action="search">Subspecimen Search</g:link></li>
          		<li><g:link controller="subspecimen" action="chtnAnnualReport">CHTN Annual Report</g:link></li>
              <li><g:link controller="invoice" action="investigatorInvoices">Investigator Invoices</g:link></li>
          </ul>
       </li>
        <li><a href="">Investigator</a>
          <ul>
          		<li><g:link controller="investigator" action="create">Add Investigator</g:link></li>
          		<li><g:link controller="investigator" action="search">Search/Edit Investigators</g:link></li>
          		<li><g:link controller="tissueRequest" action="search">Search/Edit Requests</g:link></li>
          </ul>
       </li>
       <g:if test="${ SecurityUtils.subject.hasRole('ROLE_ADMIN')}">  
         <li><a href="">Users and Roles</a>
          <ul>
               <li><g:link controller="shiroUser">Users Management</g:link></li>
               <li><g:link controller="shiroRole">Roles Management</g:link></li>
          </ul>
        </li>
       </g:if>
</ul>
</g:if>
</nav>

<div class="art-sheet clearfix">
            <div class="art-layout-wrapper clearfix">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content clearfix">
							<g:layoutBody/>
					    </div>
                    </div>
                </div>
            </div>
    </div>
<footer class="art-footer clearfix">
  <div class="art-footer-inner">
<div class="art-content-layout">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-0" style="width: 25%">
        <p>Human Tissue Resource Network © <%= Calendar.getInstance().get(Calendar.YEAR) %></p>
        <p>Department of Pathology</p>
        <p>The Ohio State University Wexner Medical Center</p>
        <p><a href="#">Privacy Policy</a></p>
        <p>Version:<g:meta name="app.version" />, Build:<g:meta name="build.number"/></p>
    </div><div class="art-layout-cell layout-item-0" style="width: 25%">
        <p>2001 Polaris Parkway</p>
        <p>Columbus, Ohio, U.S.A. 43240</p>
    </div><div class="art-layout-cell layout-item-0" style="width: 25%">
        <p>T: (614) 293 - 5493</p>
        <p>F: (614) 293 - 7013</p>
        <p>E: <a href="#">email@domain.com</a></p>
    </div>
    </div>
</div>

    
  </div>
</footer>

</div>

<r:layoutResources />
</body>
</html>