
<%@ page import="hbis.QcBatch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QC Batch')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-qcBatch" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-qcBatch" class="content scaffold-list" role="main">
			<h1>Viewing QC Batch to Scan</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${qcBatchInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${qcBatchInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<table>
				<thead>
					<tr>
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
						
						<th><g:message code="specimen.procurementDate.label" default="Procurement Date" /></th>
						
						<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
						
						<th><g:message code="specimen.primaryOrMets.label" default="Primary/Mets" /></th>
						
						<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
						
						<th><g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diagnosis" /></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${qcBatchInstance.specimens}" status="i" var="specimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: specimenInstance, field: "specimenChtnId")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${specimenInstance.procurementDate}"/></td>
					
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryPrimaryAnatomicSite")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "primaryOrMets")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryTissueType")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryDiagnosis")}</td>
					
						</tr>
					</g:each>
				</tbody>
			</table>
			
			<g:form action="update">
				<fieldset class="form">
					<g:hiddenField name="id" value="${qcBatchInstance?.id}" />
					<g:hiddenField name="version" value="${qcBatchInstance?.version}" />
					<g:hiddenField name="view" value="scanning" />
					<g:hiddenField name="qcBatchStatus" value="Scanning Complete" />
				
					<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'dateScanningComplete', 'error')} required">
						<label for="dateScanningComplete">
							<g:message code="qcBatch.dateScanningComplete.label" default="Date Scanning Complete" />
							<span class="required-indicator">*</span>
						</label>
						<g:datePicker name="dateScanningComplete" required="" default="${new Date().clearTime()}" precision="day"  value="${qcBatchInstance?.dateScanningComplete}" noSelection="['null':'']" />
					</div>
				</fieldset>
				<button>
					<g:submitButton name="update"  value="${message(code: 'qcBatch.button.scanningComplete.label', default: 'Scanning Complete for This Batch')}" />
				</button>
			</g:form>
		</div>
	</body>
</html>
