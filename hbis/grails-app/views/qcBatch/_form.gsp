<%@ page import="hbis.QcBatch" %>



<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'batchNumber', 'error')} required">
	<label for="batchNumber">
		<g:message code="qcBatch.batchNumber.label" default="Batch Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="batchNumber" required="" value="${qcBatchInstance?.batchNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="qcBatch.status.label" default="Status" />
		
	</label>
	<g:select id="status" name="status.id" from="${hbis.QcBatchStatus.list()}" optionKey="id" value="${qcBatchInstance?.status?.id}" class="many-to-one" noSelection="['null': '']"/>	
</div>

<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'dateComplete', 'error')} ">
	<label for="dateComplete">
		<g:message code="qcBatch.dateComplete.label" default="Date Complete" />
	</label>
	<g:datePicker  name="dateComplete" default="none" precision="day"  value="${qcBatchInstance?.dateComplete}" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'dateImageAnalysisCompleted', 'error')} ">
	<label for="dateImageAnalysisComplete">
		<g:message code="qcBatch.dateImageAnalysisComplete.label" default="Date Image Analysis Complete" />
	</label>
	<g:datePicker name="dateImageAnalysisComplete" default="none" precision="day"  value="${qcBatchInstance?.dateImageAnalysisComplete}" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'datePathologistConfirmationComplete', 'error')} ">
	<label for="datePathologistConfirmationComplete">
		<g:message code="qcBatch.datePathologistConfirmationComplete.label" default="Date Pathologist Confirmation Complete" />
	</label>
	<g:datePicker name="datePathologistConfirmationComplete" default="none" precision="day"  value="${qcBatchInstance?.datePathologistConfirmationComplete}" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'dateScanningCompleted', 'error')} ">
	<label for="dateScanningComplete">
		<g:message code="qcBatch.dateScanningComplete.label" default="Date Scanning Complete" />
	</label>
	<g:datePicker name="dateScanningComplete" default="none" precision="day"  value="${qcBatchInstance?.dateScanningComplete}" noSelection="['null':'']" />
</div>

<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'specimens', 'error')} ">
	<label for="specimens">
		<g:message code="qcBatch.specimens.label" default="Specimens" />
	</label>
	
<ul class="one-to-many">
<g:each in="${qcBatchInstance?.specimens?}" var="s">
    <li><g:link controller="specimen" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>

</div>

