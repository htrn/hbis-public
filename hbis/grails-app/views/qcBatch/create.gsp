<%@ page import="hbis.QcBatch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QC Batch')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-qcBatch" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="create-qcBatch" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${qcBatchInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${qcBatchInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="saveAndAddSpecimens" >
				<fieldset class="form">
				</fieldset>
				<fieldset class="buttons">
					<!--<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />-->
					<g:submitButton name="saveAndAddSpecimens" class="save" value="${message(code: 'qcBatch.button.create.label', default: 'Continue To Add Specimens')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
