
<%@ page import="hbis.QcBatch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QC Batch')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-qcBatch" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-qcBatch" class="content scaffold-list" role="main">
			<h1>Viewing QC Batch to Scan</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${qcBatchInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${qcBatchInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<table>
				<thead>
					<tr>
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
						
						<th><g:message code="patient.mrn.label" default="MRN" /></th>
						
						<th><g:message code="specimen.procurementDate.label" default="Procurement Date" /></th>
						
						<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
						
						<th><g:message code="specimen.primaryOrMets.label" default="Primary/Mets" /></th>
						
						<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
						
						<th><g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diagnosis" /></th>
						
						<th><g:message code="specimen.preliminaryDiagnosis.label" default="QC Result" /></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${qcBatchInstance.specimens}" status="i" var="specimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: specimenInstance, field: "specimenChtnId")}</td>
					
						<td>${fieldValue(bean: specimenInstance, field: "procedure.patient.mrn")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${specimenInstance.procurementDate}"/></td>
					
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryPrimaryAnatomicSite")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "primaryOrMets")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryTissueType")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryDiagnosis")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "qcResult.tissueQcMatch")}</td>
					
						</tr>
					</g:each>
				</tbody>
			</table>
			<!-- 
			<g:form action="update">
				<fieldset class="form">
					<g:hiddenField name="id" value="${qcBatchInstance?.id}" />
					<g:hiddenField name="version" value="${qcBatchInstance?.version}" />
					<g:hiddenField name="view" value="completeReview" />
					<g:hiddenField name="qcBatchStatus" value="" />
				
					This is the form area where the batch report link should go
				</fieldset>
				<g:submitButton name="update"  value="${message(code: 'qcBatch.button.qcComplete.label', default: 'Update in some ways?')}" />
			</g:form>
			 -->
			Download all QC Reports from this batch
			<g:jasperReport
   						jasper="qc-report-all"
   						name="qc-report-all-${new Date().format('MMddyy')}"
   						controller="qcBatch"
   						action="qcReportAll"
   						format="pdf"
   						description=" "
   						delimiter=" ">
   							<g:hiddenField name="qcBatchId" value="${qcBatchInstance?.id}" />
   			</g:jasperReport>
		</div>
	</body>
</html>
