
<%@ page import="hbis.QcBatch" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'qcBatch.label', default: 'QC Batch')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-qcBatch" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-qcBatch" class="content scaffold-list" role="main">
			<h1>Viewing QC Batch ${qcBatchInstance.id} for Image Analysis</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${qcBatchInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${qcBatchInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<!-- Trying update of entire table... -->
			
			<g:form method="post" controller="specimen" action="updateAllImageAnalysis">
			<div class="allowTableOverflow">
			<table>
				<thead>
					<tr>
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
						
						<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
						
						<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
						
						<th><g:message code="qcResult.regionOfInterest.label" default="ROI %" /></th>
						
						<th><g:message code="qcResult.regionOfInterestArea.label" default="ROI Area" /></th>
						
						<th><g:message code="qcResult.percentNecrosis.label" default="% Necrosis" /></th>
						
						<th><g:message code="qcResult.linkPathReport.label" default="Report" /></th>
						
						<th><g:message code="qcResult.linkOverlayImage.label" default="Overlay" /></th>
						
						<th><g:message code="qcResult.linkSvsImage.label" default="SVS" /></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${qcBatchInstance.specimens.sort{a,b -> a.specimenChtnId.compareTo(b.specimenChtnId)}}" status="i" var="specimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: specimenInstance, field: "specimenChtnId")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryPrimaryAnatomicSite")}</td>
						
						<td>${fieldValue(bean: specimenInstance, field: "preliminaryTissueType")}</td>
						
						<td><g:select name="specimens.regionOfInterest.${specimenInstance.id}" value="${specimenInstance?.qcResult?.regionOfInterest}" from="${0..100}" noSelection="[0: '']"/></td>
						
						<td><g:field name="specimens.regionOfInterestArea.${specimenInstance.id}" type="number" value="${specimenInstance?.qcResult?.regionOfInterestArea}"/></td>
						
						<td><g:select name="specimens.percentNecrosis.${specimenInstance.id}" value="${specimenInstance?.qcResult?.percentNecrosis}" from="${0..100}" noSelection="[0: '']"/></td>
						
						<td><g:textField name="specimens.linkPathReport.${specimenInstance.id}" value="${specimenInstance?.qcResult?.linkPathReport}" style="width:150px" /></td>
						
						<td><g:textField name="specimens.linkOverlayImage.${specimenInstance.id}" value="${specimenInstance?.qcResult?.linkOverlayImage}" style="width:150px" /></td>
						
						<td><g:textField name="specimens.linkSvsImage.${specimenInstance.id}" value="${specimenInstance?.qcResult?.linkSvsImage}" style="width:150px" /></td>
							
						<td><g:hiddenField name="specimens.id.${specimenInstance.id}" value="${specimenInstance.id}" />
							<g:hiddenField name="specimens.version.${specimenInstance.id}" value="${specimenInstance.version}" />
						</td>
						
					
						</tr>
					</g:each>
				</tbody>
			</table>
			</div>
			<g:hiddenField name="qcBatchId" value="${qcBatchInstance?.id}" />
			<g:submitButton name="saveProgress" class="save" value="${message(code: 'qcBatch.button.saveProgress.label', default: 'Save Current Progress')}" />
			
				<fieldset class="form">
					<g:hiddenField name="qcBatchVersion" value="${qcBatchInstance?.version}" />
					<g:hiddenField name="view" value="imageAnalysis" />
					<g:hiddenField name="qcBatchStatus" value="Image Analysis Complete" />
					
					<div class="fieldcontain ${hasErrors(bean: qcBatchInstance, field: 'dateImageAnalysisCompleted', 'error')} required">
						<label for="dateImageAnalysisComplete">
							<g:message code="qcBatch.dateImageAnalysisComplete.label" default="Date Image Analysis Complete" />
							<span class="required-indicator">*</span>
						</label>
						<g:datePicker name="dateImageAnalysisComplete" required="" default="${new Date().clearTime()}" precision="day"  value="${qcBatchInstance?.dateImageAnalysisComplete}" noSelection="['null':'']" />
					</div>
				</fieldset>
				
				<fieldset class="buttons">
					<g:submitButton name="update" class="save" value="${message(code: 'qcBatch.button.imageAnalysisComplete.label', default: 'Image Analysis Complete for This Batch')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
