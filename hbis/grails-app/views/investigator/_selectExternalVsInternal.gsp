<g:select 	id="externalVsInternal" 
			name="externalVsInternal.id" 
			from="${externalVsInternalList}" 
			optionKey="id" 
			value="${investigatorInstance?.externalVsInternal?.id}" 
			class="many-to-one"
			/>