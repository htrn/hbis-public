
<%@ page import="hbis.Investigator" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'investigator.label', default: 'Investigator')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-investigator" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-investigator" class="content scaffold-list" role="main">
			<h1>Search for Investigators</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:form action="search" >
				<fieldset class="form">
				<div class="leftFormColumn">
				
					<div class="fieldcontain">
						<label for="chtnId">
							<g:message code="investigator.chtnId.label" default="CHTN ID" />
						</label>
						<g:select	id="chtnIdPrefix" 
							name="chtnIdPrefixId" 
							from="${hbis.InvestigatorChtnIdPrefix.list()}" 
							optionKey="id"
							value="${params.list('chtnIdPrefixId')*.toLong()}"
							class="many-to-one" 
							multiple="true"
						/>
						<g:textField name="chtnId" value="${chtnId}"/>
					</div>
				
					<div class="fieldcontain">
						<label for="firstName">
							<g:message code="investigator.firstName.label" default="First Name" />
						</label>
						<g:textField name="firstName" value="${firstName}"/>
					
					</div>
					<div class="fieldcontain">
						<label for="lastName">
							<g:message code="investigator.lastName.label" default="Last Name" />
						</label>
						<g:textField name="lastName" value="${lastName}"/>
					</div>
				
				</div> <!-- End left form column -->
				<div class="rightFormColumn">
				
					<div class="fieldcontain">
						<label for="chtnDivision">
							<g:message code="investigator.chtnDivision.label" default="CHTN Division" />
						</label>
						<g:select	id="chtnDivision" 
							name="chtnDivisionId" 
							from="${hbis.ChtnDivision.list()}" 
							optionKey="id"
							value="${params.list('chtnDivisionId')*.toLong()}"
							class="many-to-one" 
							multiple="true"
						/>
					</div>
					
					<div class="fieldcontain">
						<label for="status">
							<g:message code="investigator.status.label" default="Status" />
						</label>
						<g:select	id="status" 
							name="statusId" 
							from="${hbis.InvestigatorStatus.list()}" 
							optionKey="id"
							value="${params.list('statusId')*.toLong()}"
							class="many-to-one" 
							multiple="true"
						/>
					</div>
					
				</div> <!-- End right form column -->
				</fieldset>
				<fieldset class="buttons" style="clear:both">
						<g:hiddenField name="formSubmitted" value="true" />
						<g:submitButton name="search" class="save" value="${message(code: 'default.button.search.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<table  style="clear:both">
				<thead>
					<tr>
						<g:sortableColumn property="chtnIdPrefix" title="${message(code: 'investigator.chtnIdPrefix.label', default: 'Chtn Id Prefix')}" params="${params}" />
						
						<g:sortableColumn property="chtnId" title="${message(code: 'investigator.chtnId.label', default: 'Chtn Id')}" params="${params}" />
					
						<g:sortableColumn property="firstName" title="${message(code: 'investigator.firstName.label', default: 'First Name')}" params="${params}" />
					
						<g:sortableColumn property="lastName" title="${message(code: 'investigator.lastName.label', default: 'Last Name')}" params="${params}" />
					
						<g:sortableColumn property="chtnDivision" title="${message(code: 'investigator.chtnDivision.label', default: 'Division')}" params="${params}" />
					
						<g:sortableColumn property="status" title="${message(code: 'investigator.status.label', default: 'Status')}" params="${params}" />
						
						<th>Tissue Requests</th>
						
						<th></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${investigatorInstanceList}" status="i" var="investigatorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>${fieldValue(bean: investigatorInstance, field: "chtnIdPrefix")}</td>
						
						<td><g:link action="show" id="${investigatorInstance.id}">${fieldValue(bean: investigatorInstance, field: "chtnId")}</g:link></td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "firstName")}</td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "lastName")}</td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "chtnDivision")}</td>
					
						<td>${fieldValue(bean: investigatorInstance, field: "status")}</td>
						
						<td><g:if test="${investigatorInstance?.tissueRequests}">
								<g:each in="${investigatorInstance.tissueRequests}" var="tr">
									<g:link controller="tissueRequest" action="show" id="${tr.id}">${tr?.encodeAsHTML()}</g:link><br>
								</g:each>
							</g:if></td>
						
						<td><g:link action="edit" id="${investigatorInstance.id}">Edit Investigator</g:link></td>
						
						<td><g:link controller="tissueRequest" action="create" params="['investigator.id':investigatorInstance.id]">Add Tissue Request</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate action="search" total="${investigatorInstanceTotal}" params="${params}"/>
			</div>
		</div>
	</body>
</html>
