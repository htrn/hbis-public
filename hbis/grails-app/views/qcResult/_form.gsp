<%@ page import="hbis.QcResult" %>



<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'qcCode', 'error')} ">
	<label for="qcCode">
		<g:message code="qcResult.qcCode.label" default="Qc Code" />
		
	</label>
	<g:textField name="qcCode" value="${qcResultInstance?.qcCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'qcPathologist', 'error')} ">
	<label for="qcPathologist">
		<g:message code="qcResult.qcPathologist.label" default="Qc Pathologist" />
		
	</label>
	<g:textField name="qcPathologist" value="${qcResultInstance?.qcPathologist}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'regionOfInterest', 'error')} ">
	<label for="regionOfInterest">
		<g:message code="qcResult.regionOfInterest.label" default="Region Of Interest" />
		
	</label>
	<g:field name="regionOfInterest" type="number" value="${qcResultInstance.regionOfInterest}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'regionOfInterestArea', 'error')} ">
	<label for="regionOfInterestArea">
		<g:message code="qcResult.regionOfInterestArea.label" default="Region Of Interest Area" />
		
	</label>
	<g:field name="regionOfInterestArea" type="number" value="${qcResultInstance.regionOfInterestArea}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'percentNecrosis', 'error')} ">
	<label for="percentNecrosis">
		<g:message code="qcResult.percentNecrosis.label" default="Percent Necrosis" />
		
	</label>
	<g:field name="percentNecrosis" type="number" value="${qcResultInstance.percentNecrosis}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'linkPathReport', 'error')} ">
	<label for="linkPathReport">
		<g:message code="qcResult.linkPathReport.label" default="Link Path Report" />
		
	</label>
	<g:textField name="linkPathReport" value="${qcResultInstance?.linkPathReport}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'linkImage', 'error')} ">
	<label for="linkImage">
		<g:message code="qcResult.linkImage.label" default="Link Image" />
		
	</label>
	<g:textField name="linkImage" value="${qcResultInstance?.linkImage}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'actualAnatomicSite', 'error')} ">
	<label for="actualAnatomicSite">
		<g:message code="qcResult.actualAnatomicSite.label" default="Actual Anatomic Site" />
		
	</label>
	<g:textField name="actualAnatomicSite" value="${qcResultInstance?.actualAnatomicSite}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'actualDiagnosis', 'error')} ">
	<label for="actualDiagnosis">
		<g:message code="qcResult.actualDiagnosis.label" default="Actual Diagnosis" />
		
	</label>
	<g:textField name="actualDiagnosis" value="${qcResultInstance?.actualDiagnosis}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'actualSubDiagnosis', 'error')} ">
	<label for="actualSubDiagnosis">
		<g:message code="qcResult.actualSubDiagnosis.label" default="Actual Sub Diagnosis" />
		
	</label>
	<g:textField name="actualSubDiagnosis" value="${qcResultInstance?.actualSubDiagnosis}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'actualModDiagnosis', 'error')} ">
	<label for="actualModDiagnosis">
		<g:message code="qcResult.actualModDiagnosis.label" default="Actual Mod Diagnosis" />
		
	</label>
	<g:textField name="actualModDiagnosis" value="${qcResultInstance?.actualModDiagnosis}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'goodForTMA', 'error')} ">
	<label for="goodForTMA">
		<g:message code="qcResult.goodForTMA.label" default="Good For TMA" />
		
	</label>
	<g:checkBox name="goodForTMA" value="${qcResultInstance?.goodForTMA}" />
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'pathologistComments', 'error')} ">
	<label for="pathologistComments">
		<g:message code="qcResult.pathologistComments.label" default="Pathologist Comments" />
		
	</label>
	<g:textField name="pathologistComments" value="${qcResultInstance?.pathologistComments}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: qcResultInstance, field: 'specimen', 'error')} required">
	<label for="specimen">
		<g:message code="qcResult.specimen.label" default="Specimen" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="specimen" name="specimen.id" from="${hbis.Specimen.list()}" optionKey="id" required="" value="${qcResultInstance?.specimen?.id}" class="many-to-one"/>
</div>

