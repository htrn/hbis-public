			<table>
				<thead>
					<tr>
						<th><g:message code="patient.mrn.label" default="MRN" /></th>
						
						<th><g:message code="procedure.procedureDate.label" default="Procedure Date" /></th>
						
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen" /></th>
						
						<th><g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen" /></th>
						
						<th></th>
						
						<th></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${subspecimens}" status="i" var="subspecimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<g:if test="${subspecimenInstance.specimen.procedure.chartReview}">
							</g:if>
							<g:else>
							<td><g:link controller="patient" action="show" id="${subspecimenInstance.specimen.procedure.patient.id}">${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.mrn")}</g:link></td>
						
							<td><g:link controller="procedure" action="show" id="${subspecimenInstance.specimen.procedure.id}">${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.procedureDate")}</g:link></td>
						
							<td><g:link controller="specimen" action="show" id="${subspecimenInstance.specimen.id}">${fieldValue(bean: subspecimenInstance, field: "specimen.specimenChtnId")}</g:link></td>
					
							<td><g:link controller="subspecimen" action="show" id="${subspecimenInstance.id}">${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</g:link></td>
						
							<td>
								<g:link controller="chartReview" 
									action="create"
									params='["procedure.id": "${subspecimenInstance.specimen.procedure.id}"]'>Create Chart Review</g:link>
							</td>
							</g:else>
						</tr>
					</g:each>
				</tbody>
			</table>