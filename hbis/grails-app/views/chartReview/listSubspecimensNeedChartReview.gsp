
<%@ page import="hbis.ChartReview" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'chartReview.label', default: 'ChartReview')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-chartReview" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<g:form action="listSubspecimensNeedChartReview" >
				<fieldset class="form">
				<div>
					<label for="fromDate">
						<g:message code="default.date.fromDate" default="From Date" />
					</label>
					<g:datePicker name="fromDate" precision="day"  value="${fromDate}" default="none" noSelection="['': '']" />
				</div>
				<div>
					<label for="toDate">
						<g:message code="default.date.toDate" default="To Date" />
					</label>
					<g:datePicker name="toDate" precision="day"  value="${toDate}" default="none" noSelection="['': '']" />
				</div>
				<div>
					<label for="mrn">
						<g:message code="patient.mrn.label" default="MRN" />
					</label>
					<g:textField name="mrn" value="${mrn}"/>
				</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="listSubspecimensNeedChartReview" class="save" value="${message(code: 'default.button.setProcurementDateRange.label', default: 'Refresh Results With Above Criteria')}" />
				</fieldset>
			</g:form>
		
		<div id="list-chartReview" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<div id="show-subspecimens" style="clear:both">
			<fieldset class="box"><legend>Subspecimens in need of a chart review 
												<g:if test="${!fromDate.equals(null) && !toDate.equals(null)}">
													that were procured from <g:formatDate format="MM/dd/yyyy" date="${fromDate}"/> to <g:formatDate format="MM/dd/yyyy" date="${toDate}"/>
												</g:if>
												<g:elseif test="${!fromDate.equals(null) && toDate.equals(null)}">
													that were procured from <g:formatDate format="MM/dd/yyyy" date="${fromDate}"/> until now
												</g:elseif>
												<g:elseif test="${fromDate.equals(null) && !toDate.equals(null)}">
													that were procured from the beginning of time until <g:formatDate format="MM/dd/yyyy" date="${toDate}"/>
												</g:elseif>
												</legend>
				<g:render template="subspecimenNeedChartReviewTable" />
			</fieldset>
		</div>
		</div>
	</body>
</html>
