
<%@ page import="hbis.ChartReview" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'chartReview.label', default: 'ChartReview')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-chartReview" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-chartReview" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="chartReview.specialDietStatus.label" default="Special Diet Status" /></th>
					
						<th><g:message code="chartReview.smokingHistoryStatus.label" default="Smoking History Status" /></th>
					
						<th><g:message code="chartReview.alcoholConsumptionStatus.label" default="Alcohol Consumption Status" /></th>
					
						<th><g:message code="chartReview.recreationalDrugsStatus.label" default="Recreational Drugs Status" /></th>
					
						<g:sortableColumn property="additionalNotes" title="${message(code: 'chartReview.additionalNotes.label', default: 'Additional Notes')}" />
					
						<g:sortableColumn property="alcoholConsumptionAmountPerWeek" title="${message(code: 'chartReview.alcoholConsumptionAmountPerWeek.label', default: 'Alcohol Consumption Amount Per Week')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${chartReviewInstanceList}" status="i" var="chartReviewInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${chartReviewInstance.id}">${fieldValue(bean: chartReviewInstance, field: "specialDietStatus")}</g:link></td>
					
						<td>${fieldValue(bean: chartReviewInstance, field: "smokingHistoryStatus")}</td>
					
						<td>${fieldValue(bean: chartReviewInstance, field: "alcoholConsumptionStatus")}</td>
					
						<td>${fieldValue(bean: chartReviewInstance, field: "recreationalDrugsStatus")}</td>
					
						<td>${fieldValue(bean: chartReviewInstance, field: "additionalNotes")}</td>
					
						<td>${fieldValue(bean: chartReviewInstance, field: "alcoholConsumptionAmountPerWeek")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${chartReviewInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
