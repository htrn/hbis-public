<%@ page import="hbis.Medication" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'medication.label', default: 'Medication')}" />
		<g:set var="parentEntityName" value="${message(code: 'chartReview.label', default: 'Chart Review')}" />
		<g:set var="parentOfParentEntityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<g:set var="parentOfParentOfParentEntityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#patient-button').click(function () {
    			$('#show-patient').toggle("slow");
			});
			
			$('#procedure-button').click(function () {
			  	$('#show-procedure').toggle("slow");
			});
			
			$('#chartReview-button').click(function () {
			  	$('#show-chartReview').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#edit-medication" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<div class="buttons">
			<button id="patient-button">Toggle Patient</button>
			<button id="procedure-button">Toggle Procedure</button>
			<button id="chartReview-button">Toggle Chart Review</button>
		</div>
		
		<div id="show-patient" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/patient" />
		</div>
		
		<div id="show-procedure" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/procedure" />
		</div>
		
		<div id="show-chartReview" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/chartReview" />
		</div>
		
		<div id="edit-medication" class="content scaffold-edit" role="main" style="clear:both">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${medicationInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${medicationInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form method="post" >
				<g:hiddenField name="id" value="${medicationInstance?.id}" />
				<g:hiddenField name="version" value="${medicationInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons" style="clear:both">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
