<table>
				<thead>
					<tr>
						<th><g:message code="shippingCart.id.label" default="Shipping Cart ID" /></th>
					
						<th><g:message code="shippingCart.dateCreated.label" default="Date Created" /></th>
						
						<th><g:message code="shippingCart.investigator.label" default="Investigator" /></th>
						
						<th><g:message code="shippingCart.poOrRefNumber.label" default="PO/Ref Number" /></th>
						
						<th><g:message code="shippingCart.commentsOther.label" default="Comments" /></th>
						
						<th><g:message code="shippingCart.subspecimen.count.label" default="# Subspecimens" /></th>
						
						<th><g:message code="shippingCart.subspecimen.count.label" default="# Chart Reviews" /></th>
						
						<th><g:message code="shippingCart.totalPrice.label" default="Total Price" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${invoiceInstance.shippingCarts}" status="i" var="shippingCartInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link controller="shippingCart" action="show" id="${shippingCartInstance.id}">${fieldValue(bean: shippingCartInstance, field: "id")}</g:link></td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${shippingCartInstance.dateCreated}"/></td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "investigator")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "poOrRefNumber")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "commentsOther")}</td>
						
						<td>${shippingCartInstance.getNumberOfSubspecimens()}</td>
						
						<td>${shippingCartInstance.getNumberOfChartReviews()}</td>
						
						<td>${shippingCartInstance.getTotalPrice()}</td>
					
					</tr>
				</g:each>
				</tbody>
</table>