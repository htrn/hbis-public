<%@ page import="hbis.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#edit-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="edit-invoice" class="content scaffold-edit" role="main">
			<h1><g:message code="invoice.addShippingCarts.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoiceInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${invoiceInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			
			<div class="allowTableOverflow">
				<div id="show-shippingCarts" style="clear:both">
					<fieldset class="box"><legend>Shipping Carts shipped to ${invoiceInstance.investigator} that have not been added to an Invoice</legend>
						<g:render template="availableShippingCarts" />
					</fieldset>
				</div>
			
				<div id="show-shippingCartsInInvoice" style="clear:both">
					<fieldset class="box"><legend>Shipping Carts in this Invoice</legend>
						<g:render template="shippingCartsInInvoice" />
					</fieldset>
				</div>
			</div>
			<g:form method="post" >
				<g:hiddenField name="id" value="${invoiceInstance?.id}" />
				<fieldset class="form">
					<!-- Form is empty -->
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="confirm" value="${message(code: 'default.button.confirm.label', default: 'Continue to Confirmation')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
