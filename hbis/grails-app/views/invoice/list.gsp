
<%@ page import="hbis.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-invoice" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoiceInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${invoiceInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'invoice.id.label', default: 'ID')}" />
					
						<g:sortableColumn property="billingStatus" title="${message(code: 'invoice.billingStatus.label', default: 'Billing Status')}" />
					
						<g:sortableColumn property="paymentStatus" title="${message(code: 'invoice.paymentStatus.label', default: 'Payment Status')}" />
					
						<g:sortableColumn property="city" title="${message(code: 'invoice.city.label', default: 'City')}" />
					
						<th><g:message code="invoice.investigator.label" default="Investigator" /></th>
					
						<g:sortableColumn property="invoiceDate" title="${message(code: 'invoice.invoiceDate.label', default: 'Invoice Date')}" />
					
						<g:sortableColumn property="paymentReceivedDate" title="${message(code: 'invoice.paymentReceivedDate.label', default: 'Payment Received Date')}" />
						
						<th><g:message code="invoice.action.label" default="Action" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${invoiceInstanceList}" status="i" var="invoiceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${invoiceInstance.id}">${fieldValue(bean: invoiceInstance, field: "id")}</g:link></td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "billingStatus")}</td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "paymentStatus")}</td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "city")}</td>
					
						<td>${fieldValue(bean: invoiceInstance, field: "investigator")}</td>
					
						<td><g:formatDate date="${invoiceInstance.invoiceDate}" /></td>
					
						<td><g:formatDate date="${invoiceInstance.paymentReceivedDate}" /></td>
						
						<td><g:link
								action="viewShippingCartsToAdd"
								id="${invoiceInstance.id}">Add/Remove Shipping Carts</g:link>
								</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${invoiceInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
