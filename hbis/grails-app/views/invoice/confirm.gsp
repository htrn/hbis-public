<%@ page import="hbis.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$("#invoiceDate").datepicker({dateFormat: 'mm/dd/yy'});
			})
		</script>
	</head>
	<body>
		<a href="#edit-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="edit-invoice" class="content scaffold-edit" role="main">
			<h1><g:message code="default.confirm.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoiceInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${invoiceInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			
			<div id="show-shippingCartsInInvoice" style="clear:both">
				<fieldset class="box"><legend>Shipping Carts in this Invoice</legend>
					<g:render template="confirmInvoiceContents" />
				</fieldset>
			</div class="content">
			<div>
				<h2>Total Price of all Shipping Carts in Invoice: ${invoiceInstance.getTotalPrice()}</h2>
			</div>
			
			
			<g:form method="post" >
				<g:hiddenField name="id" value="${invoiceInstance?.id}" />
				<g:hiddenField name="version" value="${invoiceInstance?.version}" />
				<g:hiddenField name="invoiceBillingStatus" value="Invoice Sent" />
				<g:hiddenField name="invoicePaymentStatus" value="Not Received" />
				<g:hiddenField name="view" value="confirm" />
				<fieldset class="form">
					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'investigator', 'error')}">
						<span id="investigator-label" class="property-label"><g:message code="invoice.investigator.label" default="Investigator" /></span>
						<span class="property-value" aria-labelledby="investigator-label"><g:link controller="investigator" action="show" id="${invoiceInstance?.investigator?.id}">${invoiceInstance?.investigator?.encodeAsHTML()}</g:link></span>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'attentionTo', 'error')} ">
						<label for="attentionTo">
							<g:message code="invoice.attentionTo.label" default="Attention To" />
						</label>
						<g:if test="${invoiceInstance.attentionTo}">
							<g:textField name="attentionTo" value="${invoiceInstance?.attentionTo}"/>
						</g:if>
						<g:else>
							<g:textField name="attentionTo" value="${invoiceInstance.investigator.firstName} ${invoiceInstance.investigator.lastName}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'institution', 'error')} ">
						<label for="institution">
							<g:message code="invoice.institution.label" default="Institution" />
						</label>
						<g:if test="${invoiceInstance.institution}">
							<g:textField name="institution" value="${invoiceInstance.institution}"/>
						</g:if>
						<g:else>
							<g:textField name="institution" value="${invoiceInstance.investigator.billingInstitution}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'department', 'error')} ">
						<label for="department">
							<g:message code="invoice.department.label" default="Department" />
						</label>
						<g:if test="${invoiceInstance.department}">
							<g:textField name="department" value="${invoiceInstance.department}"/>
						</g:if>
						<g:else>
							<g:textField name="department" value="${invoiceInstance.investigator.billingDepartment}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'streetAddress1', 'error')} ">
						<label for="streetAddress1">
							<g:message code="invoice.streetAddress1.label" default="Street Address 1" />
						</label>
						<g:if test="${invoiceInstance.streetAddress1}">
							<g:textField name="streetAddress1" value="${invoiceInstance.streetAddress1}"/>
						</g:if>
						<g:else>
							<g:textField name="streetAddress1" value="${invoiceInstance.investigator.billingStreetAddress1}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'streetAddress2', 'error')} ">
						<label for="streetAddress2">
							<g:message code="invoice.streetAddress2.label" default="Street Address 2" />
						</label>
						<g:if test="${invoiceInstance.streetAddress2}">
							<g:textField name="streetAddress2" value="${invoiceInstance.streetAddress2}"/>
						</g:if>
						<g:else>
							<g:textField name="streetAddress2" value="${invoiceInstance.investigator.billingStreetAddress2}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'city', 'error')} ">
						<label for="city">
							<g:message code="invoice.city.label" default="City" />
						</label>
						<g:if test="${invoiceInstance.city}">
							<g:textField name="city" value="${invoiceInstance.city}"/>
						</g:if>
						<g:else>
							<g:textField name="city" value="${invoiceInstance.investigator.billingCity}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'state', 'error')} ">
						<label for="state">
							<g:message code="invoice.state.label" default="State" />
						</label>
						<g:if test="${invoiceInstance.state}">
							<g:textField name="state" value="${invoiceInstance.state}"/>
						</g:if>
						<g:else>
							<g:textField name="state" value="${invoiceInstance.investigator.billingState}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'zipCode', 'error')} ">
						<label for="zipCode">
							<g:message code="invoice.zipCode.label" default="Zip Code" />
						</label>
						<g:if test="${invoiceInstance.zipCode}">
							<g:textField name="zipCode" value="${invoiceInstance.zipCode}"/>
						</g:if>
						<g:else>
							<g:textField name="zipCode" value="${invoiceInstance.investigator.billingZipCode}"/>
						</g:else>
					</div>

					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'country', 'error')} ">
						<label for="country">
							<g:message code="invoice.zipCode.label" default="Country" />
						</label>
						<g:if test="${invoiceInstance.country}">
							<g:textField name="country" value="${invoiceInstance.country}"/>
						</g:if>
						<g:else>
							<g:textField name="country" value="${invoiceInstance.investigator.billingCountry}"/>
						</g:else>
					</div>
					
					<div class="fieldcontain ">
						<label for="poOrRefNumber">
							<g:message code="invoice.poOrRefNumber.label" default="PO Or Reference Number (Work in progress!)" />
						</label>
						<!-- Value from first ShippingCart (may be blank) -->
						<g:textField name="poOrRefNumber" value="${poOrRefNumber}"/>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceDate', 'error')} required">
						<label for="invoiceDate">
							<g:message code="invoice.invoiceDate.label" default="Invoice Date" />
							<span class="required-indicator">*</span>
						</label>
						<!--<g:datePicker name="invoiceDate" precision="day"  value="${invoiceInstance?.invoiceDate}"  />-->
						<g:textField type="date" name="invoiceDate" id="invoiceDate" value="${formatDate(format:'MM/dd/yyyy',date:invoiceInstance?.invoiceDate?: new Date())}"/>
					</div>

				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'invoice.button.confirm.label', default: 'Send Invoice')}" />
					<!--<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />-->
				</fieldset>
			</g:form>
			Preview Invoice
			<g:jasperReport
   						jasper="invoice"
   						name="invoice-${new Date().format('MMddyy')}"
   						controller="invoice"
   						action="invoiceReport"
   						format="pdf"
   						description=" "
   						delimiter=" ">
   							<g:hiddenField name="invoiceId" value="${invoiceInstance?.id}" />
   						</g:jasperReport>
		</div>
	</body>
</html>
