<%@ page import="hbis.Invoice" %>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'investigator', 'error')}">
	<span id="investigator-label" class="property-label"><g:message code="invoice.investigator.label" default="Investigator" /></span>
	<span class="property-value" aria-labelledby="investigator-label"><g:link controller="investigator" action="show" id="${invoiceInstance?.investigator?.id}">${invoiceInstance?.investigator?.encodeAsHTML()}</g:link></span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'attentionTo', 'error')} ">
	<label for="attentionTo">
		<g:message code="invoice.attentionTo.label" default="Attention To" />
	</label>
	<g:textField name="attentionTo" value="${invoiceInstance?.attentionTo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'institution', 'error')} ">
	<label for="institution">
		<g:message code="invoice.institution.label" default="Institution" />
	</label>
	<g:textField name="institution" value="${invoiceInstance?.institution}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'department', 'error')} ">
	<label for="department">
		<g:message code="invoice.department.label" default="Department" />
	</label>
	<g:textField name="department" value="${invoiceInstance?.department}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'streetAddress1', 'error')} ">
	<label for="streetAddress1">
		<g:message code="invoice.streetAddress1.label" default="Street Address 1" />
		
	</label>
	<g:textField name="streetAddress1" value="${invoiceInstance?.streetAddress1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'streetAddress2', 'error')} ">
	<label for="streetAddress2">
		<g:message code="invoice.streetAddress2.label" default="Street Address 2" />
		
	</label>
	<g:textField name="streetAddress2" value="${invoiceInstance?.streetAddress2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'city', 'error')} ">
	<label for="city">
		<g:message code="invoice.city.label" default="City" />
	</label>
	<g:textField name="city" value="${invoiceInstance?.city}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'state', 'error')} ">
	<label for="state">
		<g:message code="invoice.state.label" default="State" />
		
	</label>
	<g:textField name="state" value="${invoiceInstance?.state}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'zipCode', 'error')} ">
	<label for="zipCode">
		<g:message code="invoice.zipCode.label" default="Zip Code" />
		
	</label>
	<g:textField name="zipCode" value="${invoiceInstance?.zipCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'country', 'error')} ">
	<label for="country">
		<g:message code="invoice.zipCode.label" default="Country" />
		
	</label>
	<g:textField name="country" value="${invoiceInstance?.country}"/>
</div>



<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceDate', 'error')} required">
	<label for="invoiceDate">
		<g:message code="invoice.invoiceDate.label" default="Invoice Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="invoiceDate" precision="day"  value="${invoiceInstance?.invoiceDate}"  />
</div>


<!-- 
<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'shipMode', 'error')} required">
	<label for="shipMode">
		<g:message code="invoice.shipMode.label" default="Ship Mode" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="shipMode" name="shipMode.id" from="${hbis.ShipMode.list()}" optionKey="id" required="" value="${invoiceInstance?.shipMode?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'trackingNumber', 'error')} ">
	<label for="trackingNumber">
		<g:message code="invoice.trackingNumber.label" default="Tracking Number" />
		
	</label>
	<g:textField name="trackingNumber" value="${invoiceInstance?.trackingNumber}"/>
</div>


<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'billingStatus', 'error')} ">
	<label for="billingStatus">
		<g:message code="invoice.billingStatus.label" default="Billing Status" />
	</label>
	<g:select id="billingStatus" name="billingStatus.id" from="${hbis.InvoiceBillingStatus.list()}" optionKey="id" value="${invoiceInstance?.billingStatus?.id}" class="many-to-one" default="none" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'paymentStatus', 'error')} ">
	<label for="paymentStatus">
		<g:message code="invoice.paymentStatus.label" default="Payment Status" />
	</label>
	<g:select id="paymentStatus" name="paymentStatus.id" from="${hbis.InvoicePaymentStatus.list()}" optionKey="id" value="${invoiceInstance?.paymentStatus?.id}" class="many-to-one" default="none" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'paymentReceivedDate', 'error')} required">
	<label for="paymentReceivedDate">
		<g:message code="invoice.paymentReceivedDate.label" default="Payment Received Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="paymentReceivedDate" precision="day"  value="${invoiceInstance?.paymentReceivedDate}"  />
</div>
 -->

