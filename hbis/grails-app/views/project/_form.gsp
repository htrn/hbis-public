<%@ page import="hbis.Project" %>



<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="project.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${projectInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'projectId', 'error')} ">
	<label for="projectId">
		<g:message code="project.projectId.label" default="Project Id" />
		
	</label>
	<g:textField name="projectId" value="${projectInstance?.projectId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="project.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="status" name="status.id" from="${hbis.ProjectStatus.list()}" optionKey="id" required="" value="${projectInstance?.status?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<g:hiddenField name="investigator.id" value="${projectInstance?.investigator?.id}"/>

<!-- 
<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'investigator', 'error')} required">
	<label for="investigator">
		<g:message code="project.investigator.label" default="Investigator" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="investigator" name="investigator.id" from="${hbis.Investigator.list()}" optionKey="id" required="" value="${projectInstance?.investigator?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'tissueRequests', 'error')} ">
	<label for="tissueRequests">
		<g:message code="project.tissueRequests.label" default="Tissue Requests" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${projectInstance?.tissueRequests?}" var="t">
    <li><g:link controller="tissueRequest" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="tissueRequest" action="create" params="['project.id': projectInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'tissueRequest.label', default: 'TissueRequest')])}</g:link>
</li>
</ul>

</div>
 -->

