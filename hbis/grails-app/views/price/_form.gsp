<%@ page import="hbis.Price" %>



<div class="fieldcontain ${hasErrors(bean: priceInstance, field: 'display', 'error')} ">
	<label for="display">
		<g:message code="price.display.label" default="Display" />
		
	</label>
	<g:checkBox name="display" value="${priceInstance?.display}" />
</div>

<div class="fieldcontain ${hasErrors(bean: priceInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="price.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${priceInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceInstance, field: 'price', 'error')} required">
	<label for="price">
		<g:message code="price.price.label" default="Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="price" value="${fieldValue(bean: priceInstance, field: 'price')}" required=""/>
</div>

