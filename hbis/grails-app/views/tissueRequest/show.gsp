
<%@ page import="hbis.TissueRequest" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tissueRequest.label', default: 'TissueRequest')}" />
		<g:set var="parentEntityName" value="${message(code: 'investigator.label', default: 'Investigator')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#investigator-button').click(function () {
    			$('#show-investigator').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#show-tissueRequest" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div class="buttons">
			<button id="investigator-button">Toggle Investigator</button>
		</div>
		
		<div id="show-investigator" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
				<g:render template="/showDomainTemplates/investigator" />
		</div>

		<div id="show-tissueRequest" class="content scaffold-show" role="main" style="clear:both">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:render template="/showDomainTemplates/tissueRequest" />
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${tissueRequestInstance?.id}" />
					<g:link class="edit" action="edit" id="${tissueRequestInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
