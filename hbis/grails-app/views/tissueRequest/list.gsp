
<%@ page import="hbis.TissueRequest" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tissueRequest.label', default: 'TissueRequest')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-tissueRequest" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-tissueRequest" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'tissueRequest.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="status" title="${message(code: 'tissueRequest.status.label', default: 'Status')}" />
					
						<g:sortableColumn property="tissueRequestId" title="${message(code: 'tissueRequest.tissueRequestId.label', default: 'Tissue Request Id')}" />
					
						<g:sortableColumn property="oldTissueRequestId" title="${message(code: 'tissueRequest.oldTissueRequestId.label', default: 'Old Tissue Request Id')}" />
					
						<g:sortableColumn property="contactMethod" title="${message(code: 'tissueRequest.contactMethod.label', default: 'Contact Method')}" />
					
						<th><g:message code="tissueRequest.project.label" default="Project" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${tissueRequestInstanceList}" status="i" var="tissueRequestInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${tissueRequestInstance.id}">${fieldValue(bean: tissueRequestInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: tissueRequestInstance, field: "status")}</td>
					
						<td>${fieldValue(bean: tissueRequestInstance, field: "tissueRequestId")}</td>
					
						<td>${fieldValue(bean: tissueRequestInstance, field: "oldTissueRequestId")}</td>
					
						<td>${fieldValue(bean: tissueRequestInstance, field: "contactMethod")}</td>
					
						<td>${fieldValue(bean: tissueRequestInstance, field: "project")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${tissueRequestInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
