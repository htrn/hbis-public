<%@ page import="hbis.MedicalHistory" %>

<div class="fieldcontain ${hasErrors(bean: medicalHistoryInstance, field: 'cancer', 'error')} required">
	<label for="cancer">
		<g:message code="medicalHistory.cancer.label" default="Cancer" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="cancer" name="cancer.id" from="${hbis.PrimaryMets.list()}" optionKey="id" required="" value="${medicalHistoryInstance?.cancer?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medicalHistoryInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="medicalHistory.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="status" name="status.id" from="${hbis.MedicalStatus.list()}" optionKey="id" required="" value="${medicalHistoryInstance?.status?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medicalHistoryInstance, field: 'diagnosis', 'error')} required">
	<label for="diagnosis">
		<g:message code="medicalHistory.diagnosis.label" default="Diagnosis" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="diagnosis" name="diagnosis.id" from="${hbis.Diagnosis.list()}" optionKey="id" required="" value="${medicalHistoryInstance?.diagnosis?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: medicalHistoryInstance, field: 'stage', 'error')} ">
	<label for="stage">
		<g:message code="medicalHistory.stage.label" default="Stage" />
		
	</label>
	<g:textField name="stage" value="${medicalHistoryInstance?.stage}"/>
</div>

<g:hiddenField name="chartReview.id" value="${medicalHistoryInstance?.chartReview.id}"/>

