
<ul class="property-list patient">
	<li class="fieldcontain">
	<fieldset class="box">
					<span id="mrn-label" class="property-label"><g:message code="patient.mrn.label" default="MRN" /></span>
					<span class="property-value" aria-labelledby="mrn-label"><g:link action="show" id="${patientInstance.id}"><g:fieldValue bean="${patientInstance}" field="mrn"/></g:link></span>
					
					<span id="firstName-label" class="property-label"><g:message code="patient.firstName.label" default="First Name" /></span>
					<span class="property-value" aria-labelledby="firstName-label"><g:fieldValue bean="${patientInstance}" field="firstName"/></span>
		
					<span id="lastName-label" class="property-label"><g:message code="patient.lastName.label" default="Last Name" /></span>
					<span class="property-value" aria-labelledby="lastName-label"><g:fieldValue bean="${patientInstance}" field="lastName"/></span>
					
			<li class="fieldcontain">
				<g:if test="${patientInstance?.consents}">
					<g:each in="${patientInstance?.consents}" var="c">
						<fieldset class="box">
						<span id="id-label" class="property-label"><g:message code="consent.id.label" default="Consent ID" /></span>
						<span class="property-value" aria-labelledby="id-label"><g:link controller="consent" action="show" id="${c.id}"><g:fieldValue bean="${c}" field="id"/></g:link></span>
						
						<span id="status-label" class="property-label"><g:message code="consent.status.label" default="Consent Status" /></span>
						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${c}" field="status"/></span>
						
						<g:if test="${c.protocols}">
							<span id="protocols-label" class="property-label"><g:message code="consent.protocols.label" default="Protocols" /></span>
							<g:each in="${c.protocols}" var="p">
								<g:link controller="protocol" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link>
							</g:each>
						</g:if>
						</fieldset>
					</g:each>
				</g:if>
				<g:else>
					No consents added for this patient
				</g:else>
				<g:link controller="consent" action="create" params="['patient.id': patientInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'consent.label', default: 'Consent')])}</g:link>
			</li>
		
			<li class="fieldcontain">
				<g:if test="${patientInstance?.procedures}">
					<g:each in="${patientInstance.procedures}" var="p">
						<fieldset class="box">
						<span id="procedure-label" class="property-label"><g:message code="procedure.label" default="Procedure" /></span>
						<span class="property-value" aria-labelledby="procedures-label"><g:link controller="procedure" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
								<span id="specimens-label" class="property-label"><g:message code="procedure.specimens.label" default="Specimens" /></span>
								<g:if test="${p.specimens}">
									<g:each in="${p.specimens}" var="sp">
										<span class="property-value" aria-labelledby="specimens-label"><g:link controller="specimen" action="show" id="${sp.id}">${sp?.encodeAsHTML()}</g:link>
										<g:if test="${sp.subspecimens}">
											with subspecimens 
											<g:each in="${sp.subspecimens}" var="subsp">
												<g:link controller="subspecimen" action="show" id="${subsp.id}"> ${subsp?.subspecimenChtnId}</g:link>,
											</g:each>
										</g:if>
										<g:else>
											has no subspecimens 
										</g:else>
										<g:link controller="subspecimen" action="create" tabindex="1" params="['specimen.id': sp?.id]">${message(code: 'default.add.label', args: [message(code: 'subspecimen.label', default: 'Subspecimen')])}</g:link>
									</span>
									</g:each>
								</g:if>
								<g:else>
									No specimens added to this procedure
								</g:else>
								<g:link controller="specimen" action="create" tabindex="1" params="['procedure.id': p.id]"><button type="button">Add Specimen</button></g:link>
								<!--<br>
								<g:link controller="chartReview" action="create" params="['procedure.id': p?.id]">${message(code: 'default.add.label', args: [message(code: 'chartReview.label', default: 'Chart Review')])}</g:link>
								-->
								
						</fieldset>
					</g:each>
				</g:if>
				<g:else>
					No procedures added for this patient
				</g:else>
			</li>
		<g:link controller="procedure" action="create" params="['patient.id': patientInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'procedure.label', default: 'Procedure')])}</g:link>
	</fieldset>
	</li>
</ul>