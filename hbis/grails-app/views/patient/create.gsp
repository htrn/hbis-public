<%@ page import="hbis.Patient" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-patient" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="create-patient" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${patientInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${patientInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton tabindex="1" name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Save')}" />
					<g:submitButton tabindex="1" name="saveAndCreateProcedure" class="save"  value="${message(code: 'default.button.custom.label', default: 'Save and Create Procedure')}" />
					<g:submitButton tabindex="1" name="saveAndCreateConsent" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Create Consent')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
