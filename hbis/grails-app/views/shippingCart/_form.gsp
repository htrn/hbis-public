<%@ page import="hbis.ShippingCart" %>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'investigator', 'error')}">
	<span id="investigator-label" class="property-label"><g:message code="shippingCart.investigator.label" default="Investigator" /></span>
	<span class="property-value" aria-labelledby="investigator-label"><g:link controller="investigator" action="show" id="${shippingCartInstance?.investigator?.id}">${shippingCartInstance?.investigator?.encodeAsHTML()}</g:link></span>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'attentionTo', 'error')} ">
	<label for="attentionTo">
		<g:message code="shippingCart.attentionTo.label" default="Attention To" />
	</label>
	<g:textField name="attentionTo" value="${shippingCartInstance?.attentionTo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'institution', 'error')} ">
	<label for="institution">
		<g:message code="shippingCart.institution.label" default="Institution" />
	</label>
	<g:textField name="institution" value="${shippingCartInstance?.institution}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'streetAddress1', 'error')} required">
	<label for="streetAddress1">
		<g:message code="shippingCart.streetAddress1.label" default="Street Address 1" />
	</label>
	<g:textField name="streetAddress1" required="" value="${shippingCartInstance?.streetAddress1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'streetAddress1', 'error')} ">
	<label for="streetAddress2">
		<g:message code="shippingCart.streetAddress2.label" default="Street Address 2" />
	</label>
	<g:textField name="streetAddress2" value="${shippingCartInstance?.streetAddress2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'city', 'error')} ">
	<label for="city">
		<g:message code="shippingCart.city.label" default="City" />
		
	</label>
	<g:textField name="city" value="${shippingCartInstance?.city}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'state', 'error')} ">
	<label for="state">
		<g:message code="shippingCart.state.label" default="State" />
		
	</label>
	<g:textField name="state" value="${shippingCartInstance?.state}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'zipCode', 'error')} ">
	<label for="zipCode">
		<g:message code="shippingCart.zipCode.label" default="Zip Code" />
	</label>
	<g:textField name="zipCode" value="${shippingCartInstance?.zipCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'country', 'error')} ">
	<label for="country">
		<g:message code="shippingCart.country.label" default="Country" />
	</label>
	<g:textField name="country" value="${shippingCartInstance?.country}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'phone', 'error')} ">
	<label for="phone">
		<g:message code="shippingCart.phone.label" default="Phone" />
	</label>
	<g:textField name="phone" value="${shippingCartInstance?.phone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'eRampNumber', 'error')} ">
	<label for="eRampNumber">
		<g:message code="shippingCart.eRampNumber.label" default="eRamp Number" />
	</label>
	<g:textField name="eRampNumber" value="${shippingCartInstance?.eRampNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'shipDate', 'error')} ">
	<label for="shipDate">
		<g:message code="shippingCart.shipDate.label" default="Ship Date" />
		
	</label>
	<g:datePicker name="shipDate" precision="day"  value="${shippingCartInstance?.shipDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'shipMode', 'error')} ">
	<label for="shipMode">
		<g:message code="shippingCart.shipMode.label" default="Ship Mode" />
		
	</label>
	<g:select id="shipMode" name="shipMode.id" from="${hbis.ShipMode.list()}" optionKey="id" value="${shippingCartInstance?.shipMode?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'trackingNumber', 'error')} ">
	<label for="trackingNumber">
		<g:message code="shippingCart.trackingNumber.label" default="Tracking Number" />
		
	</label>
	<g:textField name="trackingNumber" value="${shippingCartInstance?.trackingNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="shippingCart.status.label" default="Status" />
		
	</label>
	<g:select id="status" name="status.id" from="${hbis.ShippingCartStatus.list()}" optionKey="id" value="${shippingCartInstance?.status?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

