<%@ page import="hbis.ShippingCart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shippingCart.label', default: 'ShippingCart')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-shippingCart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="create-shippingCart" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${shippingCartInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${shippingCartInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'investigator', 'error')} required">
						<label for="investigator">
							<g:message code="shippingCart.investigator.label" default="Investigator" />
							<span class="required-indicator">*</span>
						</label>
						<g:select id="investigator" name="investigator.id" from="${hbis.Investigator.list()}" optionKey="id" required="" value="${shippingCartInstance?.investigator?.id}" class="many-to-one"/>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'institutionsAllowed', 'error')} required">
						<label for="institutionsAllowed">
							<g:message code="shippingCart.institutionsAllowed.label" default="Subspecimen Institutions Allowed in this Shipping Cart" />
							<span class="required-indicator">*</span>
						</label>
						<g:select id="institutionsAllowed" name="institutionsAllowed.id" from="${hbis.ShippingCartInstitutionsAllowed.list()}" optionKey="id" required="" value="${shippingCartInstance?.institutionsAllowed?.id}" class="many-to-one"/>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'qcStatusFilter', 'error')} required">
						<label for="qcStatusFilter">
							<g:message code="shippingCart.qcStatusFilter.label" default="Select QC State" />
							<span class="required-indicator">*</span>
						</label>
						<g:select id="qcStatusFilter" name="qcStatusFilter.id" from="${hbis.ShippingCartQcStatus.list()}" optionKey="id" required="" value="${shippingCartInstance?.qcStatusFilter?.id}" class="many-to-one"/>
					</div>
					
					<div class="fieldcontain ${hasErrors(bean: shippingCartInstance, field: 'description', 'error')} ">
						<label for="description">
							<g:message code="shippingCart.description.label" default="Description" />
						</label>
						<g:textField name="description" value="${shippingCartInstance?.description}"/>
					</div>
					
					<div>
						<label for="preliminaryPrimaryAnatomicSite">
							<g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" />
						</label>
						<g:select id="preliminaryPrimaryAnatomicSite" name="preliminaryPrimaryAnatomicSiteId" from="${hbis.AnatomicSite.list()}" optionKey="id" required="" value="${preliminaryPrimaryAnatomicSiteId}" class="many-to-one" noSelection="['null': '']"/>
					</div>
					
					<div>
						<label for="preliminaryTissueType">
							<g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" />
						</label>
						<g:select id="preliminaryTissueType" name="preliminaryTissueTypeId" from="${hbis.TissueType.list()}" optionKey="id" required="" value="${preliminaryTissueTypeId}" class="many-to-one" noSelection="['null': '']"/>
					</div>
					
					<div>
						<label for="fromDate">
							<g:message code="shippingCart.date.fromDate" default="Procurement Date From" />
						</label>
						<g:datePicker name="fromDate" precision="day"  value="${fromDate}" default="none" noSelection="['': '']" />
					</div>
					
					<div>
						<label for="toDate">
							<g:message code="shippingCart.date.toDate" default="Procurement Date To" />
						</label>
						<g:datePicker name="toDate" precision="day"  value="${toDate}" default="none" noSelection="['': '']" />
					</div>
				</fieldset>
				<fieldset class="buttons">
					<!--<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />-->
					<g:submitButton name="saveAndAddSubspecimens" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Add Subspecimens')}" />
					<g:submitButton name="saveAndAddChartReviews" class="save" value="${message(code: 'default.button.custom.label', default: 'Save and Add Chart Reviews')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
