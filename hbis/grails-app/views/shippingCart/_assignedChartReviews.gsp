			<table>
				<thead>
					<tr>
						<th><g:message code="chartReview.id.label" default="Chart Review ID" /></th>
						
						<th><g:message code="procedure.procedureDate.label" default="Procedure Date" /></th>
						
						<th><g:message code="shippingCart.specimenChtnIds.label" default="Specimen IDs" /></th>

						<th><g:message code="chartReview.price.label" default="Price" /></th>

						<th><g:message code="shippingCart.AddToCart.label" default="Add To Cart" /></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${chartReviewList}" status="i" var="chartReviewInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: chartReviewInstance, field: "id")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${chartReviewInstance.procedure.procedureDate}"/></td>
						
						<td><g:set var="firstSpecimenChtnId" value="${true}" />
							<g:each in="${chartReviewInstance.procedure.specimens}" status="j" var="specimenInstance">
								<g:set var="displaySpecimenChtnId" value="${false}" />
								<g:each in="${specimenInstance.subspecimens}" status="k" var="subspecimenInstance">
									<g:if test="${subspecimenInstance.needsChartReview && (subspecimenInstance.investigator == shippingCartInstance.investigator)}">
										<g:set var="displaySpecimenChtnId" value="${true}" />
									</g:if>
								</g:each>
								<g:if test="${displaySpecimenChtnId}">
									<g:if test="${firstSpecimenChtnId}">
										${specimenInstance.specimenChtnId}
										<g:set var="firstSpecimenChtnId" value="${false}" />
									</g:if>
									<g:else>
										, ${specimenInstance.specimenChtnId}
									</g:else>
								</g:if>
							</g:each>
						</td>

						<td>
							<g:if test="${chartReviewInstance.price == 0.00}">
								$<g:textField name="price-${chartReviewInstance.id}" id="price-${chartReviewInstance.id}" type="text" value="60.00" />
							</g:if>
							<g:else>
								$<g:textField name="price-${chartReviewInstance.id}" id="price-${chartReviewInstance.id}" type="text" value="${chartReviewInstance.price}" />
							</g:else>
						</td>

						<input type="hidden" name="chart-review-id-${chartReviewInstance.id}" id="chart-review-id-${chartReviewInstance.id}" value="${chartReviewInstance.id}">
						<input type="hidden" name="chart-review-version-${chartReviewInstance.version}" id="chart-review-version-${chartReviewInstance.version}" value="${chartReviewInstance.version}">
						<input type="hidden" name="shipping-cart-id-${shippingCartInstance.id}" id="shipping-cart-id-${shippingCartInstance.id}" value="${shippingCartInstance.id}">
						<input type="hidden" name="shipping-cart-version-${shippingCartInstance.version}" id="shipping-cart-version-${shippingCartInstance.version}" value="${shippingCartInstance.version}">

						<!--<td><a class="add-to-cart" href="#">Add</a></td>-->
						<td><button class="btn btn-primary btn-small add-to-cart">Add</button></td> <!-- Clicking this button fires an event listener defined in the embedded script tag at the bottom of the viewChartReviewsToAdd.gsp -->
						</tr>
					</g:each>
				</tbody>
			</table>

			<input type="hidden" name="from-date" id="from-date" value="${fromDate}">
			<input type="hidden" name="to-date" id="to-date" value="${toDate}">
			<input type="hidden" name="primary-anatomic-site-id" id="primary-anatomic-site-id" value="${primaryAnatomicSiteId}">
			<input type="hidden" name="tissue-type-id" id="tissue-type-id" value="${tissueTypeId}">