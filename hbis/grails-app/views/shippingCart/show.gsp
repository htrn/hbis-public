
<%@ page import="hbis.ShippingCart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shippingCart.label', default: 'ShippingCart')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-shippingCart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-shippingCart" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ul class="property-list shippingCart">
			<g:if test="${shippingCartInstance.status.description == 'Shipped'}">
				<h3>Download Confirmed Packing Slip:</h3>
				<g:jasperReport
   						jasper="confirmed-packing-slip"
   						name="confirmed-packing-slip-${new Date().format('MMddyy')}"
   						controller="shippingCart"
   						action="confirmedPackingSlip"
   						format="pdf, xls"
   						description=" "
   						delimiter=" ">
   							<g:hiddenField name="shippingCartId" value="${shippingCartInstance?.id}" />
   						</g:jasperReport>
   			</g:if>
   			<g:else>
   				<h3>Download Pre-Packing Slip:</h3>
					<g:jasperReport
   						jasper="pre-packing-slip"
   						name="pre-packing-slip-${new Date().format('MMddyy')}"
   						controller="shippingCart"
   						action="prePackingSlip"
   						format="pdf, xls"
   						description=" "
   						delimiter=" ">
   							<g:hiddenField name="shippingCartId" value="${shippingCartInstance?.id}" />
   						</g:jasperReport>
   			</g:else>
   			</ul>
			<g:render template="/showDomainTemplates/shippingCart" />
			
			<g:form>
				<fieldset class="buttons" style="clear:both">
					<g:hiddenField name="id" value="${shippingCartInstance?.id}" />
					<!-- 
					<g:link class="edit" action="edit" id="${shippingCartInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					 -->
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
