
<%@ page import="hbis.ShippingCart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shippingCart.label', default: 'ShippingCart')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-shippingCart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-shippingCart" class="content scaffold-list" role="main">
			<h1>Pending Shipping Carts</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${shippingCartInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${shippingCartInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<div class="allowTableOverflow">
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'shippingCart.id.label', default: 'ID')}" />
						
						<th><g:message code="shippingCart.status.label" default="Status" /></th>
						
						<g:sortableColumn property="dateCreated" title="${message(code: 'shippingCart.dateCreated.label', default: 'Date Created')}" />
						
						<g:sortableColumn property="investigator" title="${message(code: 'shippingCart.investigator.label', default: 'Investigator')}" />
						
						<g:sortableColumn property="description" title="${message(code: 'shippingCart.description.label', default: 'Description')}" />
						
						<th><g:message code="shippingCart.qcStatusFilter.label" default="QC State" /></th>
						
						<th><g:message code="shippingCart.institutionsAllowed.label" default="Inst. Allowed" /></th>
						
						<th><g:message code="subspecimen.number.label" default="# Subspecimens" /></th>
						
						<th><g:message code="subspecimen.number.label" default="# Chart Reviews" /></th>
					
						<th><g:message code="shippingCart.viewCartLink.label" default="" /></th>
						
						<th><g:message code="shippingCart.viewChartReviewsLink.label" default="" /></th>
						
						<th><g:message code="shippingCart.confirmLink.label" default="" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${shippingCartInstanceList}" status="i" var="shippingCartInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${shippingCartInstance.id}">${fieldValue(bean: shippingCartInstance, field: "id")}</g:link></td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "status")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${shippingCartInstance.dateCreated}"/></td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "investigator")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "description")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "qcStatusFilter")}</td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "institutionsAllowed")}</td>
						
						<td>${shippingCartInstance.getNumberOfSubspecimens()}</td>
						
						<td>${shippingCartInstance.getNumberOfChartReviews()}</td>
						
						<td><g:link
								action="viewSubspecimensToAdd"
								id="${shippingCartInstance.id}">Add/Remove Subspecimens</g:link>
						</td>
						
						<td><g:link
								action="viewChartReviewsToAdd"
								id="${shippingCartInstance.id}">Add/Remove Chart Reviews</g:link>
						</td>
						
						<td><g:link
								action="confirm"
								id="${shippingCartInstance.id}">Confirm/Ship</g:link>
						</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			</div>
		</div>
	</body>
</html>
