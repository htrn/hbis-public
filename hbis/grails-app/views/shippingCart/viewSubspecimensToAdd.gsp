
<%@ page import="hbis.ShippingCart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shippingCart.label', default: 'ShippingCart')}" />
		<title><g:message code="shippingCart.viewSubspecimensToAdd.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-shippingCart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<div id="show-shippingCart" class="content scaffold-show" role="main">
			<h1><g:message code="shippingCart.viewSubspecimensToAdd.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
		</div>
		
		<!-- The below form allows for the procurement date range to be updated -->
		<g:form action="viewSubspecimensToAdd">
			<g:hiddenField name="id" value="${shippingCartInstance?.id}" />
			<div>
				<label for="preliminaryPrimaryAnatomicSite">
					<g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" />
				</label>
				<g:select id="preliminaryPrimaryAnatomicSite" name="preliminaryPrimaryAnatomicSiteId" from="${hbis.AnatomicSite.list()}" optionKey="id" required="" value="${preliminaryPrimaryAnatomicSiteId}" class="many-to-one" noSelection="['null': '']"/>
			</div>
			<div>
				<label for="preliminaryTissueType">
					<g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" />
				</label>
				<g:select id="preliminaryTissueType" name="preliminaryTissueTypeId" from="${hbis.TissueType.list()}" optionKey="id" required="" value="${preliminaryTissueTypeId}" class="many-to-one" noSelection="['null': '']"/>
			</div>
			<div>
				<label for="fromDate">
					<g:message code="shippingCart.date.fromDate" default="Procurement Date From" />
				</label>
				<g:datePicker name="fromDate" precision="day"  value="${fromDate}" default="none" noSelection="['': '']" />
			</div>
			<div>
				<label for="toDate">
					<g:message code="shippingCart.date.toDate" default="Procurement Date To" />
				</label>
				<g:datePicker name="toDate" precision="day"  value="${toDate}" default="none" noSelection="['': '']" />
			</div>
			<fieldset class="buttons">
					<g:submitButton name="viewSubspecimensToAdd" class="save" value="${message(code: 'default.button.viewSubspecimensToAdd.label', default: 'Update Results with Above Criteria')}" />
			</fieldset>
		</g:form>
		<div class="allowTableOverflow">
			<div id="show-subspecimens" style="clear:both">
				<fieldset class="box"><legend>Subspecimens assigned to ${shippingCartInstance.investigator} that are ${shippingCartInstance.qcStatusFilter}
													<g:if test="${!fromDate.equals(null) && !toDate.equals(null)}">
														that were procured from <g:formatDate format="MM/dd/yyyy" date="${fromDate}"/> to <g:formatDate format="MM/dd/yyyy" date="${toDate}"/>
													</g:if>
													<g:elseif test="${!fromDate.equals(null) && toDate.equals(null)}">
														that were procured from <g:formatDate format="MM/dd/yyyy" date="${fromDate}"/> until now
													</g:elseif>
													<g:elseif test="${fromDate.equals(null) && !toDate.equals(null)}">
														that were procured from the beginning of time until <g:formatDate format="MM/dd/yyyy" date="${toDate}"/>
													</g:elseif>
									  </legend>
					<g:render template="assignedSubspecimens" />
				</fieldset>
			</div>
			
			<div id="show-subspecimensInCart" style="clear:both">
				<fieldset class="box"><legend>Subspecimens in this cart</legend>
					<g:render template="subspecimensInCart" />
				</fieldset>
			</div>
		</div>
		Download Pre-Packing Slip:
					<g:jasperReport
   						jasper="pre-packing-slip"
   						name="pre-packing-slip-${new Date().format('MMddyy')}"
   						controller="shippingCart"
   						action="prePackingSlip"
   						format="pdf, xls"
   						description=" "
   						delimiter=" ">
   							<g:hiddenField name="shippingCartId" value="${shippingCartInstance?.id}" />
   						</g:jasperReport>
		<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${shippingCartInstance?.id}" />
					<g:actionSubmit class="save" action="viewChartReviewsToAdd" value="${message(code: 'shippingCart.button.viewChartReviewsToAdd.label', default: 'View Chart Reviews to Add')}" />
					<g:actionSubmit class="save" action="confirm" value="${message(code: 'default.button.proceed.label', default: 'Proceed to Shipping Cart Confirmation')}" />
					
				</fieldset>
				
		</g:form>
		
		
	</body>
</html>
