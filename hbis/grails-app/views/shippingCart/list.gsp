
<%@ page import="hbis.ShippingCart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shippingCart.label', default: 'ShippingCart')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-shippingCart" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-shippingCart" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${shippingCartInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${shippingCartInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="id" title="${message(code: 'shippingCart.id.label', default: 'ID')}" />
						
						<g:sortableColumn property="dateCreated" title="${message(code: 'shippingCart.dateCreated.label', default: 'Date Created')}" />
						
						<g:sortableColumn property="investigator" title="${message(code: 'shippingCart.investigator.label', default: 'Investigator')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${shippingCartInstanceList}" status="i" var="shippingCartInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${shippingCartInstance.id}">${fieldValue(bean: shippingCartInstance, field: "id")}</g:link></td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${shippingCartInstance.dateCreated}"/></td>
						
						<td>${fieldValue(bean: shippingCartInstance, field: "investigator")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${shippingCartInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
