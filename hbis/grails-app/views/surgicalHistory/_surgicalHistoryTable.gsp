<table>
				<thead>
					<tr>
					
						<th><g:message code="surgicalHistory.forBiosample.label" default="For Biosample" /></th>
					
						<th><g:message code="surgicalHistory.procedure.label" default="Procedure" /></th>
					
						<th><g:message code="surgicalHistory.anesthetics.label" default="Anesthetics" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${chartReviewInstance.surgicalHistories}" status="i" var="surgicalHistoryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${surgicalHistoryInstance.id}">${fieldValue(bean: surgicalHistoryInstance, field: "forBiosample")}</g:link></td>
					
						<td>${fieldValue(bean: surgicalHistoryInstance, field: "procedure")}</td>
					
						<td>${fieldValue(bean: surgicalHistoryInstance, field: "anesthetics")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>