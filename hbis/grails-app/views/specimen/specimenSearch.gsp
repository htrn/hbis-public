<%@ page import="hbis.Specimen" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'specimen.label', default: 'Specimen')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	
	<body>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		
		<div>
			<fieldset class="box"> <legend><strong>Search for Specimen</strong></legend>
				MRN <g:remoteField name="mrn" controller="patient" action="performSearchMRN" paramName="mrn" update="divSearchMRN" value="" />
				<br/>
				Specimen Number <g:remoteField name="specimenChtnId" controller="specimen" action="performSearchChtnId" paramName="chtnId" update="divSearchChtnId" value="" />
			</fieldset>
		</div>
		
		<div id="divSearchMRN">
			<g:render template="/patient/searchMRNResult" var="patientInstance" collection="${patientList}" />
		</div>
		
		<div id="divSearchChtnId">
			<g:render template="searchChtnIdResult" var="specimenInstance" collection="${specimenList}" />
		</div>
	</body>
</html>