<%@ page import="hbis.Specimen" %>


<div class="leftFormColumn">

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'specimenChtnId', 'error')} required">
	<label for="specimenChtnId">
		<g:message code="specimen.specimenChtnId.label" default="Specimen Chtn ID" />
		<span class="required-indicator">*</span>
	</label>
	<g:if test="${!(specimenInstance.procedure.procedureEventInstitution.description == 'OSU')}">
		<g:textField tabindex="1" name="specimenChtnId" required="" value="${specimenInstance?.specimenChtnId}" />
		<g:hiddenField name="setNextSpecimenNumber" value="${'false'}"/>
	</g:if>
	<g:elseif test="${!specimenInstance.specimenChtnId || setNextSpecimenNumber == 'true'}">
		Specimen CHTN ID will be generated when record is saved
		<g:hiddenField name="specimenChtnId" value="${specimenInstance?.specimenChtnId}"/>
		<g:hiddenField name="setNextSpecimenNumber" value="${'true'}"/>
	</g:elseif>
	<g:else>
		${specimenInstance?.specimenChtnId}
		<g:hiddenField name="specimenChtnId" value="${specimenInstance?.specimenChtnId}"/>
		<g:hiddenField name="setNextSpecimenNumber" value="${setNextSpecimenNumber}"/>
	</g:else>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'techInitials', 'error')} required">
	<label for="techInitials">
		<g:message code="specimen.techInitials.label" default="Tech Initials" />
		<span class="required-indicator">*</span>
	</label>
	<g:select tabindex="1" id="techInitials" name="techInitials.id" from="${hbis.ProcurementTechInitials.list()}" optionKey="id" required="" value="${specimenInstance?.techInitials?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'specimenCategory', 'error')}">
	<label for="specimenCategory">
		<g:message code="specimen.specimenCategory.label" default="Specimen Category" />
	</label>
	<!-- Do not want the user to be able to make the Specimen Category blank if it already has a value coming into create/edit, but if it is blank, user can leave it blank -->
	<g:if test="${specimenInstance.specimenCategory}">
		<g:select 	tabindex="1" 
					id="specimenCategory" 
					name="specimenCategory.id" 
					from="${hbis.SpecimenCategory.list()}" 
					optionKey="id" 
					value="${specimenInstance?.specimenCategory?.id}" 
					class="many-to-one"
					onchange="${remoteFunction(
            		controller:'specimenCategory', 
            		action:'ajaxPreliminaryTissueType',
					update:[success:'preliminaryTissueTypeContainer'],
            		params:'\'specimenCategoryId=\' + this.value', 
            		)};
            		${remoteFunction(
            		controller:'specimenCategory', 
            		action:'ajaxPreliminaryPrimaryAnatomicSite',
					update:[success:'preliminaryPrimaryAnatomicSiteContainer'],
            		params:'\'specimenCategoryId=\' + this.value', 
            		)}"/>
	</g:if>
	<g:else> 
		<g:select 	tabindex="1" 
					id="specimenCategory" 
					name="specimenCategory.id" 
					from="${hbis.SpecimenCategory.list()}" 
					optionKey="id" 
					value="${specimenInstance?.specimenCategory?.id}" 
					class="many-to-one" 
					noSelection="['null': '']"
					onchange="${remoteFunction(
            		controller:'specimenCategory', 
            		action:'ajaxPreliminaryTissueType',
					update:[success:'preliminaryTissueTypeContainer'],
            		params:'\'specimenCategoryId=\' + this.value', 
            		)};
            		${remoteFunction(
            		controller:'specimenCategory', 
            		action:'ajaxPreliminaryPrimaryAnatomicSite',
					update:[success:'preliminaryPrimaryAnatomicSiteContainer'],
            		params:'\'specimenCategoryId=\' + this.value', 
            		)}"/>
	</g:else>
</div>
	<script>
		$(function () {
			var specimenCategoryArr = ['BLOOD', 'BUFFY COAT', 'PLASMA', 'SERUM', 'URINE'];
			$('#specimenCategory').change(function () {
				var selectedVar = $('#specimenCategory option:selected').text();
				if (specimenCategoryArr.indexOf(selectedVar) !== -1) {
					$('#qcMethod option').filter(function () {
						return $(this).text() == 'No QC - NTA';
					}).prop('selected', true)
				}else {
					$('#qcMethod option').filter(function () {
						return $(this).text() == 'Pathologist Review';
					}).prop('selected', true)
				}
			});
		});
	</script>
<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'preliminaryTissueType', 'error')} required">
	<label for="preliminaryTissueType">
		<g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" />
		<span class="required-indicator">*</span>
	</label>
	<div id="preliminaryTissueTypeContainer">
		<g:render template="selectPreliminaryTissueType" />
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'preliminaryDiagnosis', 'error')}">
	<label for="preliminaryDiagnosis">
		<g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diagnosis" />
	</label>
	<g:select tabindex="1" id="preliminaryDiagnosis" name="preliminaryDiagnosis.id" from="${hbis.Diagnosis.list()}" optionKey="id" value="${specimenInstance?.preliminaryDiagnosis?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'primaryOrMets', 'error')}">
	<label for="primaryOrMets">
		<g:message code="specimen.primaryOrMets.label" default="Primary Or Mets" />
	</label>
	<g:select 	id="primaryOrMets" tabindex="1" 
				name="primaryOrMets.id" 
				from="${hbis.PrimaryMets.list()}" 
				optionKey="id"
				value="${specimenInstance?.primaryOrMets?.id}" 
				class="many-to-one"
				noSelection="['null': '']"
				onchange="${remoteFunction(
            		controller:'primaryMets', 
            		action:'ajaxMetsToAnatomic',
					update:[success:'metsToAnatomicSiteContainer'],
            		params:'\'primaryMetsId=\' + this.value', 
            		)}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'metsToAnatomicSite', 'error')} ">
	<label for="metsToAnatomicSite">
		<g:message code="specimen.metsToAnatomicSite.label" default="Mets To Anatomic Site" />
	</label>
	<div id="metsToAnatomicSiteContainer">
		<g:if test="${specimenInstance.primaryOrMets?.description == "Metastatic"}">
			<g:render template="selectMetsToAnatomicSite" />
		</g:if>
		<g:else>
			Choose Metastatic to display these options
		</g:else>
	</div>
</div>


<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'preliminarySubDiagnosis', 'error')} ">
	<label for="preliminarySubDiagnosis">
		<g:message code="specimen.preliminarySubDiagnosis.label" default="Prelim. Sub Diagnosis" />
	</label>
	<g:select tabindex="1" id="preliminarySubDiagnosis" name="preliminarySubDiagnosis.id" from="${hbis.SubDiagnosis.list()}" optionKey="id" value="${specimenInstance?.preliminarySubDiagnosis?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'preliminaryModDiagnosis', 'error')} ">
	<label for="preliminaryModDiagnosis">
		<g:message code="specimen.preliminaryModDiagnosis.label" default="Prelim. Mod Diagnosis" />
	</label>
	<g:select tabindex="1" id="preliminaryModDiagnosis" name="preliminaryModDiagnosis.id" from="${hbis.ModDiagnosis.list()}" optionKey="id" value="${specimenInstance?.preliminaryModDiagnosis?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

</div> <!-- End Left Form Column -->

<div class="rightFormColumn">

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'procurementDate', 'error')} required">
	<label for="procurementDate">
		<g:message code="specimen.procurementDate.label" default="Procurement Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker tabindex="2" name="procurementDate" precision="day"  value="${specimenInstance?.procurementDate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'preliminaryPrimaryAnatomicSite', 'error')} required">
	<label for="preliminaryPrimaryAnatomicSite">
		<g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" />
		<span class="required-indicator">*</span>
	</label>
	<div id="preliminaryPrimaryAnatomicSiteContainer">
		<g:render template="selectPreliminaryPrimaryAnatomicSite" />
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'side', 'error')} ">
	<label for="side">
		<g:message code="specimen.side.label" default="Side" />
	</label>
	<g:select tabindex="1" id="side" name="side.id" from="${hbis.Side.list()}" optionKey="id" value="${specimenInstance?.side?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'qcMethod', 'error')} required">
	<label for="qcMethod">
		<g:message code="specimen.qcMethod.label" default="QC Method" />
		<span class="required-indicator">*</span>
	</label>
	<g:if test="${specimenInstance.qcBatch}">
		<g:hiddenField name="qcMethod.id" value="${specimenInstance.qcMethod.id}" />
		${specimenInstance.qcMethod} - This Specimen is in a QC Batch, so this value can't be changed.
	</g:if>
	<g:else>
		<g:select tabindex="1" id="qcMethod" name="qcMethod.id" from="${hbis.QcMethod.list()}" optionKey="id" required="" value="${specimenInstance?.qcMethod?.id}" class="many-to-one" />
	</g:else>
	
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'timeReceived', 'error')} ">
	<label for="timeReceived">
		<g:message code="specimen.timeReceived.label" default="Time Received (HHmm, example 1452)" />
	</label>
	<g:textField tabindex="1" type="date" name="timeReceived" id="timeReceived" value="${formatDate(format:'HHmm',date:specimenInstance?.timeReceived)}" />
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'timePrepared', 'error')} ">
	<label for="timePrepared">
		<g:message code="specimen.timePrepared.label" default="Time Prepared (HHmm, example 1452)" />
	</label>
	<g:textField tabindex="1" type="date" name="timePrepared" id="timePrepared" value="${formatDate(format:'HHmm',date:specimenInstance?.timePrepared)}" />
	<label for="isOvernight">Overnight(No more than 24 hrs): <g:field type="checkbox" name="isOvernight" checked="${previousCheckedStatus == false ? previousCheckedStatus : specimenInstance?.isOvernight}" /> </label>
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'alternateSpecimenID', 'error')} ">
	<label for="alternateSpecimenID">
		<g:message code="specimen.alternateSpecimenID.label" default="Alternate Specimen ID" />
	</label>
	<g:textField tabindex="1" name="alternateSpecimenID" value="${specimenInstance?.alternateSpecimenID}" />
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'hoursPostAutopsy', 'error')} ">
	<label for="hoursPostAutopsy">
		<g:message code="specimen.hoursPostAutopsy.label" default="Hours Post Autopsy" />
	</label>
	<g:field tabindex="1" name="hoursPostAutopsy" type="number" min="0" value="${specimenInstance.hoursPostAutopsy}" />
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'comments', 'error')} ">
	<label for="comments">
		<g:message code="specimen.comments.label" default="Comments" />
		
	</label>
	<g:textField tabindex="1" name="comments" value="${specimenInstance?.comments}"/>
</div>

<g:hiddenField name="procedure.id" value="${specimenInstance?.procedure?.id}"/>
</div> <!-- End Right Form Column -->
