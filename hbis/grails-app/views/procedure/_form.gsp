<%@ page import="hbis.Procedure" %>

<script type="text/javascript">
	$(document).ready(function() {
			$("#dateOfDeath").datepicker({dateFormat: 'mm/dd/yy'});
		})
</script>
<div class="leftFormColumn">
<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'procedureName', 'error')} required">
	<label for="procedureName">
		<g:message code="procedure.procedureName.label" default="Procedure Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:select tabindex="1" id="procedureName" name="procedureName.id" from="${hbis.ProcedureName.list()}" optionKey="id" required="" value="${procedureInstance?.procedureName?.id}" class="many-to-one" noSelection="['null': '']"/>	
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'subProcedureName', 'error')}">
	<label for="subProcedureName">
		<g:message code="procedure.subProcedureName.label" default="Sub Procedure Name" />
	</label>
	<g:select tabindex="1" id="subProcedureName" name="subProcedureName.id" from="${hbis.SubProcedureName.list()}" optionKey="id" value="${procedureInstance?.subProcedureName?.id}" class="many-to-one" noSelection="['null': '']"/>	
</div>


<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'procedureDate', 'error')} required">
	<label for="procedureDate">
		<g:message code="procedure.procedureDate.label" default="Procedure Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField tabindex="1" type="date" name="procedureDate" id="procedureDate" value="${formatDate(format:'MM/dd/yyyy',date:procedureInstance?.procedureDate)}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'endTimeOfProcedure', 'error')}">
	<label for="endTimeOfProcedure">
		<g:message code="procedure.endTimeOfProcedure.label" default="End Time of Procedure(HHmm, example 1452)" />
	</label>
	<g:textField tabindex="1" type="date" name="endTimeOfProcedure" id="endTimeOfProcedure" value="${formatDate(format:'HHmm',date:procedureInstance?.endTimeOfProcedure)}"/>
</div>



<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'procurementResult', 'error')} ">
	<label for="procurementResult">
		<g:message code="procedure.procurementResult.label" default="Procurement Result" />
	</label>
	<g:select tabindex="1" id="procurementResult" name="procurementResult.id" from="${hbis.ProcurementResult.list()}" optionKey="id" value="${procedureInstance?.procurementResult?.id}" class="many-to-one" default="none" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'patientAge', 'error')}">
	<label for="patientAge">
		<g:message code="procedure.patientAge.label" default="Patient Age" />
	</label>
	<g:field tabindex="1" name="patientAge" type="text" value="${procedureInstance.patientAge}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'patientAgeUnit', 'error')} ">
	<label for="patientAgeUnit">
		<g:message code="procedure.patientAgeUnit.label" default="Patient Age Unit" />
	</label>
	<g:select tabindex="1" id="patientAgeUnit" name="patientAgeUnit.id" from="${hbis.AgeUnit.list()}" optionKey="id" value="${procedureInstance?.patientAgeUnit?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'chemoHistory', 'error')} ">
	<label for="chemoHistory">
		<g:message code="procedure.chemoHistory.label" default="Chemo History" />
	</label>
	<g:select tabindex="1" id="chemoHistory" name="chemoHistory.id" from="${hbis.YesNo.list()}" optionKey="id" value="${procedureInstance?.chemoHistory?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'chemoHistoryComments', 'error')}">
	<label for="chemoHistoryComments">
		<g:message code="procedure.chemoHistoryComments.label" default="Chemo History Comments" />
	</label>
	<g:textField tabindex="1" name="chemoHistoryComments" value="${procedureInstance?.chemoHistoryComments}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'radiationHistory', 'error')} ">
	<label for="radiationHistory">
		<g:message code="procedure.radiationHistory.label" default="Radiation History" />
	</label>
	<g:select tabindex="1" id="radiationHistory" name="radiationHistory.id" from="${hbis.YesNo.list()}" optionKey="id" value="${procedureInstance?.radiationHistory?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'radiationHistoryComments', 'error')} ">
	<label for="radiationHistoryComments">
		<g:message code="procedure.radiationHistoryComments.label" default="Radiation History Comments" />
	</label>
	<g:textField tabindex="1" name="radiationHistoryComments" value="${procedureInstance?.radiationHistoryComments}"/>
</div>

</div> <!-- End Left Form Column -->

<div class="rightFormColumn">

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'roomNumber', 'error')} ">
	<label for="roomNumber">
		<g:message code="procedure.roomNumber.label" default="Room Number" />
		
	</label>
	<g:textField tabindex="1" name="roomNumber" value="${procedureInstance?.roomNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'dateOfDeath', 'error')} ">
	<label for="dateOfDeath">
		<g:message code="procedure.dateOfDeath.label" default="Date Of Death" />
	</label>
	<!--<g:datePicker name="dateOfDeath" precision="day"  value="${procedureInstance?.dateOfDeath}" default="none" noSelection="['null': '']" />-->
	<g:textField tabindex="1" type="date" name="dateOfDeath" id="dateOfDeath" value="${formatDate(format:'MM/dd/yyyy',date:procedureInstance?.dateOfDeath)}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'procedureType', 'error')} required">
	<label for="procedureType">
		<g:message code="procedure.procedureType.label" default="Procedure Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select tabindex="1" id="procedureType" name="procedureType.id" from="${hbis.ProcedureType.list()}" optionKey="id" required="" value="${procedureInstance?.procedureType?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'procedureEventInstitution', 'error')} required">
	<label for="procedureEventInstitution">
		<g:message code="procedure.procedureEventInstitution.label" default="Procedure Event Institution" />
		<span class="required-indicator">*</span>
	</label>
	<!-- Default value of OSU -->
	<g:select tabindex="1" id="procedureEventInstitution" name="procedureEventInstitution.id"  from="${hbis.Institution.list()}" optionKey="id" required="" value="${procedureInstance?.procedureEventInstitution?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'building', 'error')} ">
	<label for="building">
		<g:message code="procedure.building.label" default="Building" />
		
	</label>
	<g:textField tabindex="1" name="building" value="${procedureInstance?.building}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'origin', 'error')} ">
	<label for="origin">
		<g:message code="procedure.origin.label" default="Origin" />
		
	</label>
	<g:select tabindex="1" id="origin" name="origin.id" from="${hbis.Origin.list()}" optionKey="id" value="${procedureInstance?.origin?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procedureInstance, field: 'medComments', 'error')} ">
	<label for="medComments">
		<g:message code="procedure.medComments.label" default="Med Comments" />
		
	</label>
	<g:textField tabindex="1" name="medComments" value="${procedureInstance?.medComments}"/>
</div>

<g:hiddenField name="patient.id" value="${procedureInstance?.patient?.id}"/>
</div> <!-- End Right Form Column -->

