
<%@ page import="hbis.Procedure" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<g:set var="parentEntityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#patient-button').click(function () {
    			$('#show-patient').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#show-procedure" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<div class="buttons">
			<button id="patient-button">Toggle Patient</button>
		</div>
		
		<div id="show-patient" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
				<g:render template="/showDomainTemplates/patient" />
		</div>
		
		<div id="show-procedure" class="content scaffold-show" role="main" style="clear:both">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:render template="/showDomainTemplates/procedure" />
			<g:form>
				<fieldset class="buttons" style="clear:both">
					<g:hiddenField name="id" value="${procedureInstance?.id}" />
					<g:link class="edit" action="edit" id="${procedureInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					<g:link class="save" controller="specimen" action="create" params='["procedure.id": "${procedureInstance.id}"]'>Create Specimen for this Procedure</g:link>
					<g:link controller="procedure" action="amProcedureList">Go to AM Procedures List</g:link>
					<g:link controller="procedure" action="caseResults">Go to Case Results</g:link>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
