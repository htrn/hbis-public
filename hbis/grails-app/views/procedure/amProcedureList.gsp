
<%@ page import="hbis.Procedure" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-procedure" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-procedure" class="content scaffold-list" role="main">
			<h1>Procedures Added Today</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<div class="allowTableOverflow"> <!-- Want to make sure table is scrollable on lower res monitors -->
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="procedureDate" title="${message(code: 'procedure.procedureDate.label', default: 'Procedure Date')}" />
						
						<th><g:message code="patient.mrn.label" default="MRN" /></th>
						
						<th><g:message code="patient.lastName.label" default="Last Name" /></th>
					
						<th><g:message code="procedure.procedureType.label" default="Procedure Type" /></th>
					
						<th><g:message code="procedure.procedureName.label" default="Procedure Name" /></th>
						
						<th><g:message code="procedure.subProcedureName.label" default="Sub Procedure Name" /></th>
						
						<th><g:message code="procedure.roomNumber.label" default="Room" /></th>
						
						<th><g:message code="procedure.endTimeOfProcedure.label" default="End Time" /></th>
					
						<th><g:message code="procedure.procurementResult.label" default="Procurement Result" /></th>
						
						<th><g:message code="procedure.medComments.label" default="Med Comments" /></th>
						
						<th><g:message code="procedure.origin.label" default="Origin" /></th>
						
						<th><g:message code="patient.consent.label" default="Consent" /></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${procedureInstanceList}" status="i" var="procedureInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${procedureInstance.id}"><g:formatDate format="MM/dd/yyyy" date="${procedureInstance.procedureDate}"/></g:link></td>
						
						<td><g:link controller="patient" action="show" id="${procedureInstance.patient.id}">${fieldValue(bean: procedureInstance, field: "patient.mrn")}</g:link></td>
						
						<td>${fieldValue(bean: procedureInstance, field: "patient.lastName")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procedureType")}</td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procedureName")}</td>
						
						<td>${fieldValue(bean: procedureInstance, field: "subProcedureName")}</td>
						
						<td>${fieldValue(bean: procedureInstance, field: "roomNumber")}</td>
						
						<td><g:formatDate format="hh:mm aa" date="${procedureInstance.endTimeOfProcedure}"/></td>
					
						<td>${fieldValue(bean: procedureInstance, field: "procurementResult")}</td>
						
						<td>${fieldValue(bean: procedureInstance, field: "medComments")}</td>
						
						<td>${fieldValue(bean: procedureInstance, field: "origin")}</td>
						
						<td><g:if test="${procedureInstance?.patient?.consents}">
								<g:each in="${procedureInstance?.patient?.consents}" var="c">
									<g:if test="${c.protocols}">
										<g:each in="${c.protocols}" var="p">
											<g:link controller="protocol" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link><br>
										</g:each>
									</g:if>
								</g:each>
							</g:if></td>
						
						<td><g:link action="edit" id="${procedureInstance.id}">Edit</g:link></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			</div>
		</div>
		
		<div id="morningListReport">
			<h2>Morning List Report:</h2>
			<g:jasperReport
   						jasper="morning-list"
   						name="morning-list-${new Date().format('MMddyy')}"
   						controller="procedure"
   						action="morningListReport"
   						format="pdf, xls"
   						description=" "
   						delimiter=" ">
   						</g:jasperReport>
		</div>
		
		<g:form controller="patient" action="create">
		<div>
			<fieldset class="box"> <legend><strong>Search for Patient</strong></legend>
				MRN <g:remoteField name="mrn" controller="patient" action="performSearchMRN" paramName="mrn" update="divSearchMRN" value="" tabindex="1" />
			</fieldset>
		</div>
		
		<fieldset class="box"><legend><strong>Patient Search Results</strong></legend>
		<div id="divSearchMRN">
					<g:render template="/patient/searchMRNResult" var="patientInstance" collection="${patientList}" />
		</div>
		</fieldset>
		<div id="addNewPatient">
			<input type="submit" value="Add New Patient">
		</div>
		</g:form>
	</body>
</html>
