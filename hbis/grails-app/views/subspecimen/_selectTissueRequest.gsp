<g:if test="${tissueRequestList}">
<!-- The optionValue accesses the toString property of the TissueRequest domain class -->
<g:select 	id="tissueRequest" tabindex="1" 
			name="tissueRequest.id" 
			from="${tissueRequestList}" 
			optionKey="id" 
			optionValue="${toString}"
			value="${subspecimenInstance?.tissueRequest?.id}" 
			class="many-to-one" 
			noSelection="['null': '']"
			/>
</g:if>
<g:else>
	This Investigator does not have any Tissue Requests
</g:else>
<g:actionSubmit name="addNewTissueRequest" action="addNewTissueRequest" class="save" value="${message(code: 'default.button.custom.label', default: 'Add New Tissue Request')}" />