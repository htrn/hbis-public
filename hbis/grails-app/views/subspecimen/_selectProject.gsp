<g:select 	id="project" 
			name="projectId" 
			from="${projectList}" 
			optionKey="id" 
			optionValue="projectId" 
			value="${subspecimenInstance?.project?.id}" 
			class="many-to-one" 
			noSelection="['null': '']"
			onchange="${remoteFunction(
            		controller:'project', 
            		action:'ajaxGetTissueRequestsForProject',
					update:[success:'tissueRequestContainer'],
            		params:'\'projectDbId=\' + this.value', 
            		)}"
			/>