
<%@ page import="hbis.Subspecimen" %>
<%@ page import="hbis.Price" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />

		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
          		$("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
		
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>Update Subspecimen Pricing</h1>
			
			<g:form action="editPrices" >
				<fieldset class="form">
				<div>
					<label for="fromDate">
						<g:message code="default.date.fromDate" default="From Date" />
					</label>
					<g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
				</div>
				<div>
					<label for="toDate">
						<g:message code="default.date.toDate" default="To Date" />
					</label>
					<g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
				</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="editPrices" class="save" value="${message(code: 'default.button.setProcurementDateRange.label', default: 'Set Procurement Date Range')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<div id="show-subspecimens" style="clear:both">
				<fieldset class="box"><legend>Subspecimens 
												<g:if test="${!fromDate.equals(null) && !toDate.equals(null)}">
													that were procured from <g:formatDate format="MM/dd/yyyy" date="${fromDate}"/> to <g:formatDate format="MM/dd/yyyy" date="${toDate}"/>
												</g:if>
												<g:elseif test="${!fromDate.equals(null) && toDate.equals(null)}">
													that were procured from <g:formatDate format="MM/dd/yyyy" date="${fromDate}"/> until now
												</g:elseif>
												<g:elseif test="${fromDate.equals(null) && !toDate.equals(null)}">
													that were procured from the beginning of time until <g:formatDate format="MM/dd/yyyy" date="${toDate}"/>
												</g:elseif>
												</legend>
			<g:form method="post" action="updateAllPrices">
				<table>
					<thead>
						<tr>
							<th><g:message code="patient.mrn.label" default="MRN" /></th>
						
							<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
						
							<th><g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen ID" /></th>
						
							<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
						
							<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
						
							<th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>
						
							<th><g:message code="subspecimen.preparationType.label" default="Preparation Media" /></th>
						
							<th><g:message code="subspecimen.investigator.label" default="Investigator" /></th>
					
							<th><g:message code="subspecimen.status.label" default="Status" /></th>
						
							<th><g:message code="subspecimen.price.label" default="Current Price" /></th>
						
							<th><g:message code="subspecimen.datePriceUpdated.label" default="Date Price Updated" /></th>
						
							<th><g:message code="subspecimen.price.label" default="Price To Update" /></th>
						</tr>
					</thead>
					<tbody>
						<g:each in="${subspecimens}" status="i" var="subspecimenInstance">
							<g:if test="${!(subspecimenInstance.shippingCart?.invoice?.billingStatus?.description == 'Invoice Sent') &&
									  !(subspecimenInstance.subspecimenChtnId == 'Q' && subspecimenInstance.investigator == null)}">
							
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							
								<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.mrn")}</td>
							
								<td>${fieldValue(bean: subspecimenInstance, field: "specimen.specimenChtnId")}</td>
						
								<td><g:link action="show" id="${subspecimenInstance.id}">${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</g:link></td>
							
								<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryPrimaryAnatomicSite")}</td>
							
								<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryTissueType")}</td>
							
								<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
							
								<td>${fieldValue(bean: subspecimenInstance, field: "preparationMedia")}</td>
							
								<td>${fieldValue(bean: subspecimenInstance, field: "investigator")}</td>
						
								<td>${fieldValue(bean: subspecimenInstance, field: "status")}</td>
							
								<td><g:formatNumber number="${subspecimenInstance.price}" type="currency" currencyCode="USD" /></td>
							
								<td><g:formatDate date='${subspecimenInstance.datePriceUpdated}' format="MM/dd/yyyy hh:mmaa"/></td>
							
								<td><g:select name="subspecimens.price.${subspecimenInstance.id}" value="${subspecimenInstance?.price}" from="${ Price.getCurrentPriceList() }" optionKey="price" optionValue="price" noSelection="['null': '']" /></td>
						
							</tr>
							</g:if>
						</g:each>
					</tbody>
				</table>
			</fieldset>
			</div>
			<g:hiddenField name="fromDate" value="${fromDateTextField}" />
			<g:hiddenField name="toDate" value="${toDateTextField}" />
			<fieldset class="buttons">
					<g:submitButton name="updateAllPrices" class="save" value="${message(code: 'subspecimen.button.updateAllPrices.label', default: 'Update All Prices')}" />
			</fieldset>
			</g:form>
		</div>
	</body>
</html>
