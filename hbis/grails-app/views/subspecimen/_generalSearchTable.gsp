<table>
	<thead>
	<tr>

		<g:if test="${showSpecimenChtnId}">
			<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
		</g:if>

		<th><g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen ID" /></th>

		<g:if test="${showMrn}">
			<th><g:message code="patient.mrn.label" default="MRN" /></th>
		</g:if>

		<g:if test="${showGender}">
			<th><g:message code="patient.gender.label" default="Gender" /></th>
		</g:if>

		<g:if test="${showRace}">
			<th><g:message code="patient.race.label" default="Race" /></th>
		</g:if>

		<g:if test="${showOriginInstitution}">
			<th><g:message code="patient.originInstitution.label" default="Institution" /></th>
		</g:if>

		<g:if test="${showPatientAge}">
			<th><g:message code="procedure.patientAge.label" default="Patient Age" /></th>

			<th><g:message code="procedure.patientAgeUnit.label" default="Unit" /></th>
		</g:if>

		<g:if test="${showProcedureType}">
			<th><g:message code="procedure.procedureType.label" default="Procedure Type" /></th>
		</g:if>

		<g:if test="${showInvestigatorChtnId}">
			<th><g:message code="investigator.chtnIdPrefix.label" default="Invest. ID Prefix" /></th>

			<th><g:message code="investigator.chtnId.label" default="Invest. ID" /></th>
		</g:if>

		<g:if test="${showInvestigatorFirstName}">
			<th><g:message code="investigator.firstName.label" default="Invest. First Name" /></th>
		</g:if>

		<g:if test="${showInvestigatorLastName}">
			<th><g:message code="investigator.lastName.label" default="Invest. Last Name" /></th>
		</g:if>

		<g:if test="${showInvestigatorInstitutionType}">
			<th><g:message code="investigator.institutionType.label" default="Invest. Inst. Type" /></th>
		</g:if>

		<g:if test="${showProcurementDate}">
			<th><g:message code="specimen.procurementDate.label" default="Procurement Date" /></th>
		</g:if>

        <g:if test="${showTimePreparedDate}">
            <th><g:message code="specimen.timePrepared.label" default="Time Prepared" /></th>
        </g:if>

		<g:if test="${showTimeReceivedDate}">
			<th><g:message code="specimen.timeReceived.label" default="Time Received" /></th>
		</g:if>

        <g:if test="${showSpecimenCategory}">
            <th><g:message code="specimen.showSpecimenCategory.label" default="Specimen Category" /></th>
        </g:if>

        <g:if test="${showMetsToAnatomicSite}">
            <th><g:message code="specimen.metsToAnatiomicSite.label" default="Mets to Anatomic Site" /></th>
        </g:if>

        <g:if test="${showProcurementTechInitials}">
            <th><g:message code="specimen.procurementTechInitials.label" default="Procurement Tech Initials" /></th>
        </g:if>

		<g:if test="${showSubspecimenStatus}">
			<th><g:message code="subspecimen.status.label" default="Status" /></th>
		</g:if>

        <g:if test="${showSubspecimenLabelComment}">
            <th><g:message code="subspecimen.labelComment.label" default="Subspecimen Label Comment" /></th>
        </g:if>

		<g:if test="${showShipDate}">
			<th><g:message code="shippingCart.shipDate.label" default="Ship Date" /></th>
		</g:if>

		<g:if test="${showInvoiceDate}">
			<th><g:message code="invoice.invoiceDate.label" default="Invoice Date" /></th>
		</g:if>

		<g:if test="${showPreliminaryPrimaryAnatomicSite}">
			<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
		</g:if>

		<g:if test="${showFinalPrimaryAnatomicSite}">
			<th><g:message code="qcResult.finalPrimaryAnatomicSite.label" default="Final Primary Anatomic Site" /></th>
		</g:if>

		<g:if test="${showPreliminaryTissueType}">
			<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
		</g:if>

		<g:if test="${showPrimaryOrMets}">
			<th><g:message code="specimen.primaryOrMets.label" default="Specimen PrimaryMets" /></th>
		</g:if>

		<g:if test="${showFinalTissueType}">
			<th><g:message code="qcResult.finalTissueType.label" default="Final Tissue Type" /></th>
		</g:if>

		<g:if test="${showPreliminaryDiagnosis}">
			<th><g:message code="specimen.preliminaryDiagnosis.label" default="Prelim. Diagnosis" /></th>
		</g:if>

		<g:if test="${showPreliminarySubDiagnosis}">
			<th><g:message code="specimen.preliminarySubDiagnosis.label" default="Prelim. Sub Diagnosis" /></th>
		</g:if>

		<g:if test="${showPreliminaryModDiagnosis}">
			<th><g:message code="specimen.preliminaryModDiagnosis.label" default="Prelim. Mod Diagnosis" /></th>
		</g:if>

		<g:if test="${showFinalDiagnosis}">
			<th><g:message code="qcResult.finalDiagnosis.label" default="Final Diagnosis" /></th>
		</g:if>

		<g:if test="${showFinalSubDiagnosis}">
			<th><g:message code="qcResult.finalSubDiagnosis.label" default="Final Sub Diagnosis" /></th>
		</g:if>

		<g:if test="${showFinalModDiagnosis}">
			<th><g:message code="qcResult.finalModDiagnosis.label" default="Final Mod Diagnosis" /></th>
		</g:if>

		<g:if test="${showTissueQcMatch}">
			<th><g:message code="qcResult.tissueQcMatch.label" default="QC Result" /></th>
		</g:if>

        <g:if test="${showQCFollowUpAction}">
            <th><g:message code="qcResult.qcFollowUpAction.label" default="QC Follow Up Action" /></th>
        </g:if>

		<g:if test="${showPreparationType}">
			<th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>
		</g:if>

		<g:if test="${showPreparationMedia}">
			<th><g:message code="subspecimen.preparationMedia.label" default="Preparation Media" /></th>
		</g:if>

		<g:if test="${showWeight}">
			<th><g:message code="subspecimen.weight.label" default="Weight" /></th>

			<th><g:message code="subspecimen.weightUnit.label" default="Weight Unit" /></th>
		</g:if>

		<g:if test="${showVolume}">
			<th><g:message code="subspecimen.volume.label" default="Volume" /></th>

			<th><g:message code="subspecimen.volumeUnit.label" default="Volume Unit" /></th>
		</g:if>

		<g:if test="${showSizeLength}">
			<th><g:message code="subspecimen.sizeLength.label" default="Length" /></th>

			<th><g:message code="subspecimen.sizeUnit.label" default="Units" /></th>
		</g:if>

		<g:if test="${showSizeWidth}">
			<th><g:message code="subspecimen.sizeWidth.label" default="Width" /></th>

			<th><g:message code="subspecimen.sizeUnit.label" default="Units" /></th>
		</g:if>

		<g:if test="${showSizeHeight}">
			<th><g:message code="subspecimen.sizeHeight.label" default="Height" /></th>

			<th><g:message code="subspecimen.sizeUnit.label" default="Units" /></th>
		</g:if>

		<g:if test="${showFinalMolecularStatusEr}">
			<th><g:message code="qcResult.finalMolecularStatusEr.label" default="Final Molecular Status ER" /></th>
		</g:if>

		<g:if test="${showFinalMolecularStatusPr}">
			<th><g:message code="qcResult.finalMolecularStatusPr.label" default="Final Molecular Status PR" /></th>
		</g:if>

		<g:if test="${showFinalMolecularStatusHer2}">
			<th><g:message code="qcResult.finalMolecularStatusHer2.label" default="Final Molecular Status HER2" /></th>
		</g:if>

		<g:if test="${showTissueRequestTissueQuestId}">
			<th><g:message code="subspecimen.tissueRequest.tissueRequestTissueQuestId.label" default="Tissue Quest ID" /></th>
		</g:if>
	</tr>
	</thead>
	<tbody>
	<g:each in="${subspecimens}" status="i" var="subspecimenInstance">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

			<g:if test="${showSpecimenChtnId}">
				<td><g:link controller="specimen" action="show" id="${subspecimenInstance.specimen.id}">
					${fieldValue(bean: subspecimenInstance, field: "specimen.specimenChtnId")}
				</g:link>
				</td>
			</g:if>

			<td><g:link controller="subspecimen" action="show" id="${subspecimenInstance.id}">
				${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}
			</g:link>
			</td>

			<g:if test="${showMrn}">
				<td><g:link controller="patient" action="show" id="${subspecimenInstance.specimen.procedure.patient.id}">
					${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.mrn")}
				</g:link>
				</td>
			</g:if>

			<g:if test="${showGender}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.gender")}</td>
			</g:if>

			<g:if test="${showRace}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.race")}</td>
			</g:if>

			<g:if test="${showOriginInstitution}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.originInstitution")}</td>
			</g:if>

			<g:if test="${showPatientAge}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patientAge")}</td>

				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patientAgeUnit")}</td>
			</g:if>

			<g:if test="${showProcedureType}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.procedureType.description")}</td>
			</g:if>

			<g:if test="${showInvestigatorChtnId}">
				<td>${subspecimenInstance.investigator?.chtnIdPrefix}</td>

				<td><g:link controller="investigator" action="show" id="${subspecimenInstance.investigator?.id}">
					${subspecimenInstance.investigator?.chtnId}
				</g:link>
				</td>
			</g:if>

			<g:if test="${showInvestigatorFirstName}">
				<td>${subspecimenInstance.investigator?.firstName}</td>
			</g:if>

			<g:if test="${showInvestigatorLastName}">
				<td>${subspecimenInstance.investigator?.lastName}</td>
			</g:if>

			<g:if test="${showInvestigatorInstitutionType}">
				<td>${subspecimenInstance.investigator?.institutionType}</td>
			</g:if>

			<g:if test="${showProcurementDate}">
				<td><g:formatDate date='${subspecimenInstance.specimen.procurementDate}' format="MM/dd/yyyy"/></td>
			</g:if>

            <g:if test="${showTimeReceivedDate}">
                <td><g:formatDate date='${subspecimenInstance.specimen.timeReceived}' type="time" style="SHORT"/></td>
            </g:if>

			<g:if test="${showTimePreparedDate}">
				<td><g:formatDate date='${subspecimenInstance.specimen.timePrepared}' type="time" style="SHORT"/></td>
			</g:if>

            <g:if test="${showSpecimenCategory}">
                <td>${subspecimenInstance.specimen.specimenCategory}</td>
            </g:if>

            <g:if test="${showMetsToAnatomicSite}">
                <td>${subspecimenInstance.specimen.metsToAnatomicSite}</td>
            </g:if>

            <g:if test="${showProcurementTechInitials}">
                <td>${subspecimenInstance.specimen.techInitials}</td>
            </g:if>

            <g:if test="${showSubspecimenStatus}">
				<td>${fieldValue(bean: subspecimenInstance, field: "status")}</td>
			</g:if>

            <g:if test="${showSubspecimenLabelComment}">
                <td>${fieldValue(bean: subspecimenInstance, field: "labelComment")}</td>
            </g:if>

			<g:if test="${showShipDate}">
				<td><g:link controller="shippingCart" action="show" id="${subspecimenInstance.shippingCart?.id}"><g:formatDate date='${subspecimenInstance.shippingCart?.shipDate}' format="MM/dd/yyyy"/></g:link></td>
			</g:if>

			<g:if test="${showInvoiceDate}">
				<td><g:link controller="invoice" action="show" id="${subspecimenInstance.shippingCart?.invoice?.id}"><g:formatDate date='${subspecimenInstance.shippingCart?.invoice?.invoiceDate}' format="MM/dd/yyyy"/></g:link></td>
			</g:if>

			<g:if test="${showPreliminaryPrimaryAnatomicSite}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryPrimaryAnatomicSite")}</td>
			</g:if>

			<g:if test="${showFinalPrimaryAnatomicSite}">
				<td>${subspecimenInstance.specimen.qcResult?.finalPrimaryAnatomicSite}</td>
			</g:if>

			<g:if test="${showPreliminaryTissueType}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryTissueType")}</td>
			</g:if>

			<g:if test="${showPrimaryOrMets}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.primaryOrMets")}</td>
			</g:if>

			<g:if test="${showFinalTissueType}">
				<td>${subspecimenInstance.specimen.qcResult?.finalTissueType}</td>
			</g:if>

			<g:if test="${showPreliminaryDiagnosis}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryDiagnosis")}</td>
			</g:if>

			<g:if test="${showPreliminarySubDiagnosis}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminarySubDiagnosis")}</td>
			</g:if>

			<g:if test="${showPreliminaryModDiagnosis}">
				<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryModDiagnosis")}</td>
			</g:if>

			<g:if test="${showFinalDiagnosis}">
				<td>${subspecimenInstance.specimen.qcResult?.finalDiagnosis}</td>
			</g:if>

			<g:if test="${showFinalSubDiagnosis}">
				<td>${subspecimenInstance.specimen.qcResult?.finalSubDiagnosis}</td>
			</g:if>

			<g:if test="${showFinalModDiagnosis}">
				<td>${subspecimenInstance.specimen.qcResult?.finalModDiagnosis}</td>
			</g:if>

			<g:if test="${showTissueQcMatch}">
				<td>${subspecimenInstance.specimen.qcResult?.tissueQcMatch}</td>
			</g:if>

            <g:if test="${showQCFollowUpAction}">
                <td>${subspecimenInstance.specimen.qcResult?.qcFollowUpAction}</td>
            </g:if>

			<g:if test="${showPreparationType}">
				<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
			</g:if>

			<g:if test="${showPreparationMedia}">
				<td>${fieldValue(bean: subspecimenInstance, field: "preparationMedia")}</td>
			</g:if>

			<g:if test="${showWeight}">
				<td>${fieldValue(bean: subspecimenInstance, field: "weight")}</td>

				<td>${fieldValue(bean: subspecimenInstance, field: "weightUnit")}</td>
			</g:if>

			<g:if test="${showVolume}">
				<td>${fieldValue(bean: subspecimenInstance, field: "volume")}</td>

				<td>${fieldValue(bean: subspecimenInstance, field: "volumeUnit")}</td>
			</g:if>

			<g:if test="${showSizeLength}">
				<td>${fieldValue(bean: subspecimenInstance, field: "sizeLength")}</td>

				<td>${fieldValue(bean: subspecimenInstance, field: "sizeUnit")}</td>
			</g:if>

			<g:if test="${showSizeWidth}">
				<td>${fieldValue(bean: subspecimenInstance, field: "sizeWidth")}</td>

				<td>${fieldValue(bean: subspecimenInstance, field: "sizeUnit")}</td>
			</g:if>

			<g:if test="${showSizeHeight}">
				<td>${fieldValue(bean: subspecimenInstance, field: "sizeHeight")}</td>

				<td>${fieldValue(bean: subspecimenInstance, field: "sizeUnit")}</td>
			</g:if>

			<g:if test="${showFinalMolecularStatusEr}">
				<td>${subspecimenInstance.specimen.qcResult?.finalMolecularStatusEr}</td>
			</g:if>

			<g:if test="${showFinalMolecularStatusPr}">
				<td>${subspecimenInstance.specimen.qcResult?.finalMolecularStatusPr}</td>
			</g:if>

			<g:if test="${showFinalMolecularStatusHer2}">
				<td>${subspecimenInstance.specimen.qcResult?.finalMolecularStatusHer2}</td>
			</g:if>

			<g:if test="${showTissueRequestTissueQuestId}">
				<td>${subspecimenInstance.tissueRequest?.tissueRequestTissueQuestId}</td>
			</g:if>

		</tr>
	</g:each>
	</tbody>
</table>

Export To CSV
<g:jasperReport
        jasper="general-subspecimen-search-results"
        name="general-subspecimen-search-results-${new Date().format('MMddyy')}"
        controller="subspecimen"
        action="generalSearchResultsToCSV"
        format="csv"
        description=" "
        delimiter=" ">
</g:jasperReport>