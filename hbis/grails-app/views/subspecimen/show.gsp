
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:set var="parentEntityName" value="${message(code: 'specimen.label', default: 'Specimen')}" />
		<g:set var="parentOfParentEntityName" value="${message(code: 'procedure.label', default: 'Procedure')}" />
		<g:set var="parentOfParentOfParentEntityName" value="${message(code: 'patient.label', default: 'Patient')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
		
		<g:javascript>
			$('#patient-button').click(function () {
    			$('#show-patient').toggle("slow");
			});
			
			$('#procedure-button').click(function () {
			  	$('#show-procedure').toggle("slow");
			});
			
			$('#specimen-button').click(function () {
			  	$('#show-specimen').toggle("slow");
			});
 		</g:javascript>
	</head>
	<body>
		<a href="#show-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div class="buttons">
			<button id="patient-button">Toggle Patient</button>
			<button id="procedure-button">Toggle Procedure</button>
			<button id="specimen-button">Toggle Specimen</button>
		</div>
		
		<div id="show-patient" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/patient" />
		</div>
		
		<div id="show-procedure" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentOfParentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/procedure" />
		</div>
		
		<div id="show-specimen" class="content scaffold-show" role="main" style="display:none;clear:both">
			<h1><g:message code="default.show.label" args="[parentEntityName]" /></h1>
			<g:render template="/showDomainTemplates/specimen" />
		</div>
		
		<div style="clear:both">
			<h1><g:message code="default.show.label" args="[entityName]" /> for ${specimenInstance.specimenChtnId}: ${specimenInstance.preliminaryPrimaryAnatomicSite}, ${specimenInstance.preliminaryTissueType}</h1>
		</div>
		
		<div style="clear:both">
			<g:link controller="specimen"
								action="createCopyDetails"
								id="${specimenInstance.id}"><button type="button">Copy Specimen Info Into New Specimen</button></g:link>
		</div>
		<div style="clear:both">
			<g:link controller="specimen"
								action="copySpecimenAndSubspecimens"
								id="${specimenInstance.id}"
								params="[view: 'createSubspecimen']"><button type="button">Copy Specimen and Subspecimens Into New Specimen</button></g:link>
		</div>
		
		<div id="show-subspecimens" class="allowTableOverflow" style="clear:both">
			<fieldset class="box"><legend>Subspecimens Procured for above Specimen</legend>
				<g:render template="subspecimenShowTable" />
			</fieldset>
		</div>
		
		<div id="show-subspecimen" class="content scaffold-show" role="main" style="clear:both">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:render template="/showDomainTemplates/subspecimen" />
			<g:form>
				<fieldset class="buttons" style="clear:both">
					<g:hiddenField name="id" value="${subspecimenInstance?.id}" />
					<g:link class="edit" action="edit" id="${subspecimenInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:link class="save" action="create" params='["specimen.id": "${specimenInstance.id}"]'><g:message code="default.button.addMoreSubspecimens.label" default="Add More Subspecimens to the parent Specimen" /></g:link>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
