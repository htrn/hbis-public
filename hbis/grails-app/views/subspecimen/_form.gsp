<%@ page import="org.apache.shiro.SecurityUtils; hbis.Subspecimen" %>
<%@ page import="hbis.Price" %>

<div class="leftFormColumn">

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'subspecimenChtnId', 'error')} required">
	<label for="subspecimenChtnId">
		<g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen Chtn ID" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField tabindex="1" name="subspecimenChtnId" value="${subspecimenInstance?.subspecimenChtnId}" style="width:50px"/>
	<g:if test="${displayQcPreset}">
		<g:actionSubmit name="qcPresets" action="qcPresets" value="${message(code: 'default.button.custom.label', default: 'QC Presets')}" />
	</g:if>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'preparationType', 'error')} required">
	<label for="preparationType">
		<g:message code="subspecimen.preparationType.label" default="Preparation Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select 	tabindex="1" 
				id="preparationType" 
				name="preparationType.id" 
				from="${hbis.PreparationType.list()}" 
				optionKey="id" value="${subspecimenInstance?.preparationType?.id}" 
				class="many-to-one" 
				noSelection="['null': '']"
				onchange="${remoteFunction(
								controller:'preparationMedia',
								action:'ajaxUpdatePreparationMedia',
								params:'\'preparationTypeId=\' + this.value',
								update:'preparationMediaContainer',
								) }"/>
</div>

<g:if test="${subspecimenInstance.id}">
<script type="text/javascript">
		var subspecimenInstanceId = ${subspecimenInstance.id}
</script>
<g:radioGroup name="measurementChoice"
              labels="['Weight','Volume','Size Dimensions']"
              values="['weight','volume','size']"
              default="weight"
              onchange="${remoteFunction(
								controller:'subspecimen',
								action:'ajaxUpdateMeasurementDisplay',
								params:'\'measurementType=\' + this.value + \'&subspecimenId=\' + subspecimenInstanceId',
								update:'measurementContainer',
								) }">
<p>${it.label} ${it.radio}</p>
</g:radioGroup>
</g:if>
<g:else>
<g:radioGroup name="measurementChoice"
              labels="['Weight','Volume','Size Dimensions']"
              values="['weight','volume','size']"
              default="weight"
              onchange="${remoteFunction(
								controller:'subspecimen',
								action:'ajaxUpdateMeasurementDisplay',
								params:'\'measurementType=\' + this.value',
								update:'measurementContainer',
								) }">
<p>${it.label} ${it.radio}</p>
</g:radioGroup>
</g:else>


<div id="measurementContainer">
	<g:if test="${subspecimenInstance?.volume}">
		<g:render template="volume" />
	</g:if>
	<g:elseif test="${subspecimenInstance?.sizeWidth || subspecimenInstance?.sizeLength || subspecimenInstance?.sizeHeight}">
		<g:render template="size" />
	</g:elseif>
	<g:else> <!-- If neither weight or size were found, then use weight by default -->
		<g:render template="weight" />
	</g:else>
	
</div>
 
</div> <!-- End Left Form Column -->

<div class="rightFormColumn">

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="subspecimen.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>

	<g:if test="${subspecimenInstance.status?.description == 'Shipped' && !SecurityUtils.subject.hasRole('ROLE_ADMIN')}">
		${subspecimenInstance.status} - status can't be updated once Shipped.		
	</g:if>
	
	<g:elseif test="${subspecimenInstance.status?.description == 'Shipped' && SecurityUtils.subject.hasRole('ROLE_ADMIN')}">
		<g:select tabindex="1" id="status" name="status.id" from="${hbis.SubspecimenStatus.findAll()}" optionKey="id" required="" value="${subspecimenInstance?.status?.id}" class="many-to-one"/>
	</g:elseif>

	<g:else>
		<g:select tabindex="1" id="status" name="status.id" from="${hbis.SubspecimenStatus.findAllByDescriptionNotEqual("Shipped")}" optionKey="id" required="" value="${subspecimenInstance?.status?.id}" class="many-to-one"/>
	</g:else>
	
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'preparationMedia', 'error')} ">
	<label for="preparationMedia">
		<g:message code="subspecimen.preparationMedia.label" default="Preparation Media" />
	</label>
	<div id="preparationMediaContainer">
		<g:if test="${preparationMediaList?.size() == 0 || preparationMediaList == null}">
			Select a Preparation Type to see choices
		</g:if>
		<g:else>
			<g:select tabindex="1" id="preparationMedia" name="preparationMedia.id" from="${preparationMediaList}" optionKey="id" value="${subspecimenInstance?.preparationMedia?.id}" class="many-to-one" noSelection="['null': '']"/>
		</g:else>
	</div>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'comments', 'error')} ">
	<label for="comments">
		<g:message code="subspecimen.comments.label" default="Comments" />
	</label>
	<g:textField tabindex="1" name="comments" value="${subspecimenInstance?.comments}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'investigator', 'error')} ">
	<label for="investigator">
		<g:message code="subspecimen.investigator.label" default="Investigator" />
		
	</label>
	<g:select 	tabindex="1" 
				id="investigator" 
				name="investigator.id" 
				from="${hbis.Investigator.list()}" 
				optionKey="id" 
				value="${subspecimenInstance?.investigator?.id}" 
				class="many-to-one" 
				noSelection="['null': '']"
				onchange="${remoteFunction(
            		controller:'investigator', 
            		action:'ajaxGetTissueRequestsForInvestigator',
					update:[success:'tissueRequestContainer'],
            		params:'\'investigatorId=\' + this.value', 
            		)}"
				/>
	<g:actionSubmit name="addNewInvestigator" action="addNewInvestigator" class="save" value="${message(code: 'default.button.custom.label', default: 'Add New Investigator')}" />
</div>

<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'tissueRequest', 'error')} ">
	<label for="tissueRequest">
		<g:message code="subspecimen.tissueRequest.label" default="Tissue Request" />
	</label>
	<div id="tissueRequestContainer">
		<g:if test="${subspecimenInstance.investigator}">
			<g:if test="${tissueRequestList}">
				<g:render template="selectTissueRequest" />
			</g:if>
			<g:else>
				This Investigator does not have any Tissue Requests
				<g:actionSubmit name="addNewTissueRequest" action="addNewTissueRequest" class="save" value="${message(code: 'default.button.custom.label', default: 'Add New Tissue Request')}" />
			</g:else>
			
		</g:if>
		<g:else>
			Choose an Investigator to display associated Tissue Requests
		</g:else>
	</div>
</div>
 
<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'labelComment', 'error')} ">
	<label for="labelComment">
		<g:message code="subspecimen.labelComment.label" default="Label Comment" />
	</label>
	<g:textField tabindex="1" name="labelComment" value="${subspecimenInstance?.labelComment}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'price', 'error')}">
	<label for="price">
		<g:message code="subspecimen.price.label" default="Price" />
	</label>
	<g:if test="${subspecimenInstance.shippingCart?.invoice?.billingStatus?.description == 'Invoice Sent' && !SecurityUtils.subject.hasRole('ROLE_ADMIN')}">
		${subspecimenInstance.price} - Since this subspecimen belongs to an Invoice that is sent, the price can't be updated.
	</g:if>
	<g:else>
	  <g:select tabindex="1" name="price" value="${subspecimenInstance?.price}" from="${ Price.getCurrentPriceList() }" optionKey="price" optionValue="price" />
	</g:else>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'needsChartReview', 'error')} ">
	<label for="needsChartReview">
		<g:message code="subspecimen.needsChartReview.label" default="Needs Chart Review" />
	</label>
	<g:checkBox name="needsChartReview" value="${subspecimenInstance?.needsChartReview}" />
</div>

<g:hiddenField name="specimen.id" value="${subspecimenInstance?.specimen?.id}"/>
</div><!-- End Form Right Column -->

<!-- 

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'shippingCart', 'error')} ">
	<label for="shippingCart">
		<g:message code="subspecimen.shippingCart.label" default="Shipping Cart" />
		
	</label>
	<g:select id="shippingCart" name="shippingCart.id" from="${hbis.ShippingCart.list()}" optionKey="id" value="${subspecimenInstance?.shippingCart?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'tissueRequest', 'error')} ">
	<label for="tissueRequest">
		<g:message code="subspecimen.tissueRequest.label" default="Tissue Request" />
		
	</label>
	<g:select id="tissueRequest" name="tissueRequest.id" from="${hbis.TissueRequest.list()}" optionKey="id" value="${subspecimenInstance?.tissueRequest?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
 -->

