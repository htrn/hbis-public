<g:select 	tabindex="1" 
			id="preparationMedia" 
			name="preparationMedia.id" 
			from="${preparationMediaList}" 
			optionKey="id" 
			value="${subspecimenInstance?.preparationMedia?.id ?: defaultSelectionId}" 
			class="many-to-one" 
			noSelection="['': '']"/>