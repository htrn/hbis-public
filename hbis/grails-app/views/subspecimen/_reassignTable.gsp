			<table>
				<thead>
					<tr>
						<th><g:message code="patient.mrn.label" default="MRN" /></th>
						
						<th><g:message code="specimen.specimenChtnId.label" default="Specimen ID" /></th>
						
						<th><g:message code="subspecimen.subspecimenChtnId.label" default="Subspecimen ID" /></th>
						
						<th><g:message code="qcResult.tissueQcMatch.label" default="QC Result" /></th>
						
						<th><g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Prelim. Primary Anatomic Site" /></th>
						
						<th><g:message code="specimen.preliminaryTissueType.label" default="Prelim. Tissue Type" /></th>
						
						<th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>
						
						<th><g:message code="subspecimen.investigator.label" default="Investigator" /></th>
					
						<th><g:message code="subspecimen.status.label" default="Status" /></th>
						
						<th><g:message code="subspecimen.statusChangeDate.label" default="Status Change Date" /></th>
						
						<th><g:message code="subspecimen.tissueRequest.label" default="Request" /></th>
						
						<th><g:message code="subspecimen.toReassign.label" default="To Reassign" /></th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${subspecimens}" status="i" var="subspecimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.procedure.patient.mrn")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.specimenChtnId")}</td>
					
						<td><g:link action="show" id="${subspecimenInstance.id}">${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</g:link></td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.qcResult.tissueQcMatch")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryPrimaryAnatomicSite")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "specimen.preliminaryTissueType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "investigator")}</td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "status")}</td>
						
						<td><g:formatDate format="MM/dd/yyyy" date="${subspecimenInstance.statusChangeDate}"/></td>
						
						<td>${fieldValue(bean: subspecimenInstance, field: "tissueRequest")}</td>
						
						<td><g:checkBox name="reassignBox.${subspecimenInstance.id}" value="on" checked="false" /></td>
					
						</tr>
					</g:each>
				</tbody>
			</table>