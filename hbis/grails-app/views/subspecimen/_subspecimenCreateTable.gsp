			<table>
				<thead>
					<tr>
						<th><g:message code="subspecimen.subspecimenChtnId.label" default="CHTN ID" /></th>
					
						<th><g:message code="subspecimen.investigator.label" default="Investigator" /></th>
					
						<th><g:message code="subspecimen.preparationType.label" default="Prep Type" /></th>
						
						<th><g:message code="subspecimen.preparationMedia.label" default="Prep Media" /></th>
						
						<th><g:message code="subspecimen.weight.label" default="Weight" /></th>
						
						<th><g:message code="subspecimen.weightUnit.label" default="W. Unit" /></th>
						
						<th><g:message code="subspecimen.volume.label" default="Volume" /></th>
							
						<th><g:message code="subspecimen.volumeUnit.label" default="V. Unit" /></th>
						
						<th><g:message code="subspecimen.sizeWidth.label" default="Width" /></th>
							
						<th><g:message code="subspecimen.sizeWidth.label" default="Length" /></th>
							
						<th><g:message code="subspecimen.sizeWidth.label" default="Height" /></th>
							
						<th><g:message code="subspecimen.volumeUnit.label" default="S. Unit" /></th>
						
						<th><g:message code="subspecimen.labelComment.label" default="Label Comment" /></th>
						
						<th></th>
						
						<th></th>
						
						<th></th>
						
						<th></th>
						
						<th></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${subspecimens}" status="i" var="subspecimenInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<g:form action="update">
						<g:hiddenField name="id" value="${subspecimenInstance?.id}" />
						<g:hiddenField name="version" value="${subspecimenInstance?.version}" />
						<g:hiddenField name="view" value="create" />
						<td><g:textField name="subspecimenChtnId" value="${subspecimenInstance?.subspecimenChtnId}" style="width:50px"/></td>
						
						
						<td><!--${fieldValue(bean: subspecimenInstance, field: "investigator")}-->
							<g:select
								id="investigator" 
								name="investigator.id" 
								from="${hbis.Investigator.list()}" 
								optionKey="id" 
								value="${subspecimenInstance?.investigator?.id}" 
								class="many-to-one" 
								noSelection="['null': '']"
							/></td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
								
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationMedia")} </td>
					
						<td>
							<g:field name="weight" type="text" value="${subspecimenInstance?.weight}" style="width:50px"/>
						</td>
						
						<td>
							<g:select id="weightUnit" 
								name="weightUnit.id" 
								from="${hbis.WeightMeasurement.list()}" 
								optionKey="id" 
								value="${subspecimenInstance?.weightUnit?.id}" 
								class="many-to-one" 
								noSelection="['null': '']"/>
						</td>
						
						<td>
							<g:field name="volume" type="text" value="${subspecimenInstance?.volume}" style="width:50px"/>
						</td>
							
						<td>
							<g:select id="volumeUnit" 
								name="volumeUnit.id" 
								from="${hbis.VolumeMeasurement.list()}" 
								optionKey="id" 
								value="${subspecimenInstance?.volumeUnit?.id}" 
								class="many-to-one" 
								noSelection="['null': '']"/>
						</td>
						
						<td>
							<g:field name="sizeWidth" type="text" value="${subspecimenInstance?.sizeWidth}" style="width:50px"/>
						</td>
							
						<td>
							<g:field name="sizeLength" type="text" value="${subspecimenInstance?.sizeLength}" style="width:50px"/>
						</td>
							
						<td>
							<g:field name="sizeHeight" type="text" value="${subspecimenInstance?.sizeHeight}" style="width:50px"/>
						</td>
							
						<td>
							<g:select id="sizeUnit" 
								name="sizeUnit.id" 
								from="${hbis.SizeMeasurement.list()}" 
								optionKey="id" 
								value="${subspecimenInstance?.sizeUnit?.id}" 
								class="many-to-one" 
								noSelection="['null': '']"/>
						</td>
						
						<td><g:textField name="labelComment" value="${subspecimenInstance?.labelComment}" style="width:150px"/></td>
						
						<td><g:submitButton name="update" value="Update" /></td>
						</g:form>
						
						<td><g:link controller="subspecimen" 
								action="copyFromCreate"
								id="${subspecimenInstance.id}">Copy</g:link>
								</td>
								
						<td><g:link controller="subspecimen" 
								action="edit"
								id="${subspecimenInstance.id}"
								params="[view: 'create']">Edit</g:link>
								</td>
						
						<td><g:link controller="subspecimen" 
								action="deleteFromCreate"
								id="${subspecimenInstance.id}"
								onclick="return confirm('${message(code: 'subspecimen.button.delete.confirm.message', default: 'Are you sure you want to delete this subspecimen?')}');">Delete</g:link>
								</td>
								
						<td><g:jasperReport
   						jasper="subspecimen-label"
   						name="subspecimen-label-${new Date().format('MMddyy')}"
   						controller="subspecimen"
   						action="subspecimenLabel"
   						format="pdf"
   						description="Label"
   						delimiter=" ">
   							<g:hiddenField name="subspecimenId" value="${subspecimenInstance?.id}" />
   						</g:jasperReport></td>
					
						</tr>
					</g:each>
				</tbody>
			</table>