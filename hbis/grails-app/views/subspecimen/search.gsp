
<%@page import="hbis.MolecularStatusEr"%>
<%@ page import="hbis.Subspecimen" %>
<%@ page import="hbis.ProcedureType" %>
<%@ page import="hbis.Institution" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
    <g:javascript library="jquery" />

    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#invoiceDateFromTextField").datepicker({dateFormat: 'mm/dd/yy'});
            $("#invoiceDateToTextField").datepicker({dateFormat: 'mm/dd/yy'});
            $("#shipDateFromTextField").datepicker({dateFormat: 'mm/dd/yy'});
            $("#shipDateToTextField").datepicker({dateFormat: 'mm/dd/yy'});
            $("#procurementDateFromTextField").datepicker({dateFormat: 'mm/dd/yy'});
            $("#procurementDateToTextField").datepicker({dateFormat: 'mm/dd/yy'});
        })
    </script>

    <title>General Subspecimen Search</title>
</head>
<body>
<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

<div id="list-subspecimen" class="content scaffold-list" role="main">
    <h1>General Subspecimen Search</h1>

    <g:form action="search" >
        <fieldset class="form">
            <div class="fieldcontain">
                <label for="mrn">
                    <g:message code="patient.mrn.label" default="MRN" />
                </label>
                <g:textField name="mrn" value="${mrn}" />
                <g:checkBox name="showMrn" value="${showMrn}" />
                <strong>Show MRN in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="gender">
                    <g:message code="patient.gender.label" default="Gender" />
                </label>
                <g:select id="gender" name="genderId" from="${hbis.Gender.list()}" optionKey="id" value="${genderId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showGender" value="${showGender}" />
                <strong>Show Gender in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="race">
                    <g:message code="patient.race.label" default="Race" />
                </label>
                <g:select id="race" name="raceId" from="${hbis.Race.list()}" optionKey="id" value="${raceId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showRace" value="${showRace}" />
                <strong>Show Race in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="originInstitution">
                    <g:message code="patient.originInstitution.label" default="Institution" />
                </label>
                <g:select id="originInstitution"
                          name="originInstitutionId"
                          from="${Institution.list()}"
                          optionKey="id"
                          value="${ originInstitutionId }"
                          noSelection="['': '']"
                />
                <g:checkBox name="showOriginInstitution" value="${showOriginInstitution}" />
                <strong>Show Origin Institution in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="patientAge">
                    <g:message code="procedure.patientAge.label" default="Patient Age" />
                </label>
                <g:textField name="patientAge" value="${patientAge}" />
                <g:select id="patientAgeUnit" name="patientAgeUnitId" from="${hbis.AgeUnit.list()}" optionKey="id" value="${patientAgeUnitId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showPatientAge" value="${showPatientAge}" />
                <strong>Show Patient Age in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="procedureType">
                    <g:message code="procedure.procedureType.label" default="Procedure Type" />
                </label>
                <g:select id="procedureType"
                          name="procedureTypeId"
                          from="${ProcedureType.list()}"
                          optionKey="id"
                          value="${ procedureTypeId }"
                          noSelection="['': '']"
                />
                <g:checkBox name="showProcedureType" value="${showProcedureType}" />
                <strong>Show Procedure Type in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="investigatorChtnId">
                    <g:message code="investigator.chtnId.label" default="Investigator ID" />
                </label>
                <g:select	id="investigatorChtnIdPrefix"
                             name="investigatorChtnIdPrefixId"
                             from="${hbis.InvestigatorChtnIdPrefix.list()}"
                             optionKey="id"
                             value="${params.list('investigatorChtnIdPrefixId')*.toLong()}"
                             class="many-to-one"
                             multiple="true"
                />
                <g:textField name="investigatorChtnId" value="${investigatorChtnId}" />
                <g:checkBox name="showInvestigatorChtnId" value="${showInvestigatorChtnId}" />
                <strong>Show Investigator ID in results table</strong>
            </div>
            <div class="fieldcontain">
                <label for="investigatorFirstName">
                    <g:message code="investigator.firstName.label" default="Investigator First Name" />
                </label>
                <g:textField name="investigatorFirstName" value="${investigatorFirstName}" />
                <g:checkBox name="showInvestigatorFirstName" value="${showInvestigatorFirstName}" />
                <strong>Show Investigator First Name in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="investigatorLastName">
                    <g:message code="investigator.lastName.label" default="Investigator Last Name" />
                </label>
                <g:textField name="investigatorLastName" value="${investigatorLastName}" />
                <g:checkBox name="showInvestigatorLastName" value="${showInvestigatorLastName}" />
                <strong>Show Investigator Last Name in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="investigatorInstitutionType">
                    <g:message code="investigator.institutionType.label" default="Investigator Institution Type" />
                </label>
                <g:select id="institutionType" name="institutionTypeId" from="${hbis.InstitutionType.list()}" optionKey="id" value="${institutionTypeId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showInvestigatorInstitutionType" value="${showInvestigatorInstitutionType}" />
                <strong>Show Investigator Institution Type in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="specimenChtnId">
                    <g:message code="specimen.specimenChtnId.label" default="Specimen ID" />
                </label>
                <g:textField name="specimenChtnId" value="${specimenChtnId}" />
                <g:checkBox name="showSpecimenChtnId" value="${showSpecimenChtnId}" />
                <strong>Show Specimen ID in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="procurementDateFrom">
                    <g:message code="default.procurementDate.fromDate" default="Specimen Procurement Date From" />
                </label>
                <g:textField type="date" name="procurementDateFromTextField" id="procurementDateFromTextField" value="${procurementDateFromTextField}" />
            </div>

            <div class="fieldcontain">
                <label for="procurementDateTo">
                    <g:message code="default.procurementDate.toDate" default="Specimen Procurement Date To" />
                </label>
                <g:textField type="date" name="procurementDateToTextField" id="procurementDateToTextField" value="${procurementDateToTextField}" />
                <g:checkBox name="showProcurementDate" value="${showProcurementDate}" />
                <strong>Show Procurement Date in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="timeReceivedFrom">
                    <g:message code="default.timeReceived.fromDate" default="Specimen Time Received From (hhmm, example: 13:30 for 1:30 PM)" />
                </label>
                <g:textField type="date" name="timeReceivedFrom" id="timeReceivedDateFromTextField" value="${timeReceivedFrom}" />
            </div>

            <div class="fieldcontain">
                <label for="timeReceivedTo">
                    <g:message code="default.timeReceived.toDate" default="Specimen Time Received To (hhmm, example: 16:30 for 4:30 PM)" />
                </label>
                <g:textField type="date" name="timeReceivedTo" id="timeReceivedDateToTextField" value="${timeReceivedTo}" />
                <g:checkBox name="showTimeReceivedDate" value="${showTimeReceivedDate}" />
                <strong>Show Specimen Time Received in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="timePreparedFrom">
                    <g:message code="default.timePrepared.fromDate" default="Specimen Time Prepared From (hhmm, example: 08:30 for 8:30 AM)" />
                </label>
                <g:textField type="date" name="timePreparedFrom" id="timePreparedDateFromTextField" value="${timePreparedFrom}" />
            </div>

            <div class="fieldcontain">
                <label for="timePreparedTo">
                    <g:message code="default.timePrepared.toDate" default="Specimen Time Prepared To (hhmm, example: 14:30 for 2:30 PM)" />
                </label>
                <g:textField type="date" name="timePreparedTo" id="timePreparedDateToTextField" value="${timePreparedTo}" />
                <g:checkBox name="showTimePreparedDate" value="${showTimePreparedDate}" />
                <strong>Show Specimen Time Prepared in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="preliminaryPrimaryAnatomicSite">
                    <g:message code="specimen.preliminaryPrimaryAnatomicSite.label" default="Preliminary Primary Anatomic Site" />
                </label>
                <g:textField name="preliminaryPrimaryAnatomicSite" value="${preliminaryPrimaryAnatomicSite}" />
                <g:checkBox name="showPreliminaryPrimaryAnatomicSite" value="${showPreliminaryPrimaryAnatomicSite}" />
                <strong>Show Preliminary Primary Anatomic Site in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="finalPrimaryAnatomicSite">
                    <g:message code="specimen.finalPrimaryAnatomicSite.label" default="Final Primary Anatomic Site" />
                </label>
                <g:textField name="finalPrimaryAnatomicSite" value="${finalPrimaryAnatomicSite}" />
                <g:checkBox name="showFinalPrimaryAnatomicSite" value="${showFinalPrimaryAnatomicSite}" />
                <strong>Show Final Primary Anatomic Site in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="preliminaryTissueType">
                    <g:message code="specimen.preliminaryTissueType.label" default="Preliminary Tissue Type" />
                </label>
                <g:select id="preliminaryTissueType" name="preliminaryTissueTypeId" from="${hbis.TissueType.list()}" optionKey="id" value="${preliminaryTissueTypeId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showPreliminaryTissueType" value="${showPreliminaryTissueType}" />
                <strong>Show Preliminary Tissue Type in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="primaryOrMets">
                    <g:message code="specimen.primaryOrMets.label" default="Specimen Primary/Metastatic" />
                </label>
                <g:select id="primaryOrMets" name="primaryOrMetsId" from="${hbis.PrimaryMets.list()}" optionKey="id" value="${primaryOrMetsId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showPrimaryOrMets" value="${showPrimaryOrMets}" />
                <strong>Show Primary Or Metastatic in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="specimenCategoryId">
                    <g:message code="specimen.category.label" default="Specimen Category" />
                </label>
                <g:select id="specimenCategoryId" name="specimenCategoryId" from="${hbis.SpecimenCategory.list()}" optionKey="id" value="${specimenCategoryId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showSpecimenCategory" value="${showSpecimenCategory}" />
                <strong>Show Specimen Category in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="metsToAnatomicSite">
                    <g:message code="specimen.metsToAnatomicSite.label" default="Mets To Anatomic Site" />
                </label>
                <g:select id="metsToAnatomicSite" name="metsToAnatomicSite" from="${hbis.MetsToAnatomicSite.list()}" optionKey="id" value="${metsToAnatomicSite}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showMetsToAnatomicSite" value="${showMetsToAnatomicSite}" />
                <strong>Show Mets To Anatomic Site in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="techInitialsId">
                    <g:message code="specimen.techInitials.label" default="Procurement Tech Initials" />
                </label>
                <g:select id="techInitialsId" name="techInitialsId" from="${hbis.ProcurementTechInitials.list()}" optionKey="id" value="${techInitialsId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showProcurementTechInitials" value="${showProcurementTechInitials}" />
                <strong>Show Procurement Tech Initials in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="finalTissueType">
                    <g:message code="specimen.finalTissueType.label" default="Final Tissue Type" />
                </label>
                <g:select id="finalTissueType" name="finalTissueTypeId" from="${hbis.TissueType.list()}" optionKey="id" value="${finalTissueTypeId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showFinalTissueType" value="${showFinalTissueType}" />
                <strong>Show Final Tissue Type in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="preliminaryDiagnosis">
                    <g:message code="specimen.preliminaryDiagnosis.label" default="Preliminary Diagnosis" />
                </label>
                <g:textField name="preliminaryDiagnosis" value="${preliminaryDiagnosis}" />
                <g:checkBox name="showPreliminaryDiagnosis" value="${showPreliminaryDiagnosis}" />
                <strong>Show Preliminary Diagnosis in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="preliminarySubDiagnosis">
                    <g:message code="specimen.preliminarySubDiagnosis.label" default="Preliminary Sub Diagnosis" />
                </label>
                <g:textField name="preliminarySubDiagnosis" value="${preliminarySubDiagnosis}" />
                <g:checkBox name="showPreliminarySubDiagnosis" value="${showPreliminarySubDiagnosis}" />
                <strong>Show Preliminary Sub Diagnosis in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="preliminaryModDiagnosis">
                    <g:message code="specimen.preliminaryModDiagnosis.label" default="Preliminary Mod Diagnosis" />
                </label>
                <g:textField name="preliminaryModDiagnosis" value="${preliminaryModDiagnosis}" />
                <g:checkBox name="showPreliminaryModDiagnosis" value="${showPreliminaryModDiagnosis}" />
                <strong>Show Preliminary Mod Diagnosis in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="finalDiagnosis">
                    <g:message code="specimen.finalDiagnosis.label" default="Final Diagnosis" />
                </label>
                <g:textField name="finalDiagnosis" value="${finalDiagnosis}" />
                <g:checkBox name="showFinalDiagnosis" value="${showFinalDiagnosis}" />
                <strong>Show Final Diagnosis in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="finalSubDiagnosis">
                    <g:message code="specimen.finalSubDiagnosis.label" default="Final Sub Diagnosis" />
                </label>
                <g:textField name="finalSubDiagnosis" value="${finalSubDiagnosis}" />
                <g:checkBox name="showFinalSubDiagnosis" value="${showFinalSubDiagnosis}" />
                <strong>Show Final Sub Diagnosis in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="finalModDiagnosis">
                    <g:message code="specimen.finalModDiagnosis.label" default="Final Mod Diagnosis" />
                </label>
                <g:textField name="finalModDiagnosis" value="${finalModDiagnosis}" />
                <g:checkBox name="showFinalModDiagnosis" value="${showFinalModDiagnosis}" />
                <strong>Show Final Mod Diagnosis in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="qcResult">
                    <g:message code="qcResult.tissueQcMatch.label" default="QC Result" />
                </label>
                <g:select name="tissueQcMatchPassFail" from="${["", "Pass", "Fail"]}" value="${tissueQcMatchPassFail}" />
                <g:checkBox name="showTissueQcMatch" value="${showTissueQcMatch}" />
                <strong>Show QC Result in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="qcFollowUpAction">
                    <g:message code="qcResult.qcFollowUpAction.label" default="QC Follow Up Action" />
                </label>
                <g:select name="qcFollowUpAction" from="${hbis.QcFollowUpAction.list()}" optionKey="id" value="${qcFollowUpAction}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showQCFollowUpAction" value="${showQCFollowUpAction}" />
                <strong>Show QC Follow Up Action in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="preparationType">
                    <g:message code="subspecimen.preparationType.label" default="Preparation Type" />
                </label>
                <g:select id="preparationType" name="preparationTypeId" from="${hbis.PreparationType.list()}" optionKey="id" value="${preparationTypeId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showPreparationType" value="${showPreparationType}" />
                <strong>Show Preparation Type in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="preparationMedia">
                    <g:message code="subspecimen.preparationMedia.label" default="Preparation Media" />
                </label>
                <g:select id="preparationMedia" name="preparationMediaId" from="${hbis.PreparationMedia.list()}" optionKey="id" value="${preparationMediaId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showPreparationMedia" value="${showPreparationMedia}" />
                <strong>Show Preparation Media in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="weight">
                    <g:message code="subspecimen.weight.label" default="Weight" />
                </label>
                <g:textField name="weight" value="${weight}" />
                <g:checkBox name="showWeight" value="${showWeight}" />
                <strong>Show Weight in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="volume">
                    <g:message code="subspecimen.volume.label" default="Volume" />
                </label>
                <g:textField name="volume" value="${volume}" />
                <g:checkBox name="showVolume" value="${showVolume}" />
                <strong>Show Volume in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="sizeLength">
                    <g:message code="subspecimen.sizeLength.label" default="Length" />
                </label>
                <g:textField name="sizeLength" value="${sizeLength}" />
                <g:checkBox name="showSizeLength" value="${showSizeLength}" />
                <strong>Show Length in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="sizeWidth">
                    <g:message code="subspecimen.sizeWidth.label" default="Width" />
                </label>
                <g:textField name="sizeWidth" value="${sizeWidth}" />
                <g:checkBox name="showSizeWidth" value="${showSizeWidth}" />
                <strong>Show Width in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="sizeHeight">
                    <g:message code="subspecimen.sizeHeight.label" default="Height" />
                </label>
                <g:textField name="sizeHeight" value="${sizeHeight}" />
                <g:checkBox name="showSizeHeight" value="${showSizeHeight}" />
                <strong>Show Height in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="finalMolecularStatusEr">
                    <g:message code="specimen.qcResult.finalMolecularStatusEr.label" default="Molecular Status ER" />
                </label>
                <g:select id="finalMolecularStatusEr" name="finalMolecularStatusErId" from="${hbis.MolecularStatusEr.list()}" optionKey="id" value="${finalMolecularStatusErId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showFinalMolecularStatusEr" value="${showFinalMolecularStatusEr}" />
                <strong>Show Molecular Status ER in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="finalMolecularStatusPr">
                    <g:message code="specimen.qcResult.finalMolecularStatusPr.label" default="Molecular Status PR" />
                </label>
                <g:select id="finalMolecularStatusPr" name="finalMolecularStatusPrId" from="${hbis.MolecularStatusPr.list()}" optionKey="id" value="${finalMolecularStatusPrId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showFinalMolecularStatusPr" value="${showFinalMolecularStatusPr}" />
                <strong>Show Molecular Status PR in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="finalMolecularStatusHer2">
                    <g:message code="specimen.qcResult.finalMolecularStatusHer2.label" default="Molecular Status HER2" />
                </label>
                <g:select id="finalMolecularStatusHer2" name="finalMolecularStatusHer2Id" from="${hbis.MolecularStatusHer2.list()}" optionKey="id" value="${finalMolecularStatusHer2Id}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showFinalMolecularStatusHer2" value="${showFinalMolecularStatusHer2}" />
                <strong>Show Molecular Status HER2 in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="status">
                    <g:message code="subspecimen.status.label" default="Subspecimen Status" />
                </label>
                <g:select id="subspecimenStatus" name="subspecimenStatusId" from="${hbis.SubspecimenStatus.list()}" optionKey="id" value="${subspecimenStatusId}" class="many-to-one" noSelection="['': '']"/>
                <g:checkBox name="showSubspecimenStatus" value="${showSubspecimenStatus}" />
                <strong>Show Subspecimen Status in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="subspecimenLabelComment">
                    <g:message code="subspecimen.labelComment.label" default="Subspecimen Label Comment" />
                </label>
                <g:textField id="subspecimenLabelComment" name="subspecimenLabelComment" value="${subspecimenLabelComment}" />
                <g:checkBox name="showSubspecimenLabelComment" value="${showSubspecimenLabelComment}" />
                <strong>Show Subspecimen Label Comment in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="status">
                    <g:message code="subspecimen.tissueRequest.tissueRequestTissueQuestId.label" default="Tissue Request Tissue Quest ID" />
                </label>
                <g:textField name="tissueRequestTissueQuestId" value="${tissueRequestTissueQuestId}" />
                <g:checkBox name="showTissueRequestTissueQuestId" value="${showTissueRequestTissueQuestId}" />
                <strong>Show Tissue Request Tissue Quest ID in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="shipDateFrom">
                    <g:message code="default.shipDate.fromDate" default="Ship Date From" />
                </label>
                <g:textField type="date" name="shipDateFromTextField" id="shipDateFromTextField" value="${shipDateFromTextField}" />
            </div>

            <div class="fieldcontain">
                <label for="shipDateTo">
                    <g:message code="default.shipDate.toDate" default="Ship Date To" />
                </label>
                <g:textField type="date" name="shipDateToTextField" id="shipDateToTextField" value="${shipDateToTextField}" />
                <g:checkBox name="showShipDate" value="${showShipDate}" />
                <strong>Show Ship Date in results table</strong>
            </div>

            <div class="fieldcontain">
                <label for="invoiceDateFrom">
                    <g:message code="default.invoiceDate.fromDate" default="Invoice Date From" />
                </label>
                <g:textField type="date" name="invoiceDateFromTextField" id="invoiceDateFromTextField" value="${invoiceDateFromTextField}" />
            </div>

            <div class="fieldcontain">
                <label for="invoiceDateTo">
                    <g:message code="default.invoiceDate.toDate" default="Invoice Date To" />
                </label>
                <g:textField type="date" name="invoiceDateToTextField" id="invoiceDateToTextField" value="${invoiceDateToTextField}" />
                <g:checkBox name="showInvoiceDate" value="${showInvoiceDate}" />
                <strong>Show Invoice Date in results table</strong>
        </fieldset>
        <fieldset class="buttons">
            <g:hiddenField name="formSubmitted" value="true" />
            <g:submitButton name="save" class="save" value="${message(code: 'subspecimen.button.search.label', default: 'Update Results')}" />
        </fieldset>
    </g:form>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${subspecimenInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${subspecimenInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:if test="${subspecimens}">
        <div id="show-subspecimens" class="allowTableOverflow" style="clear:both">
            <fieldset class="box">
                <legend>
                    <strong>Subspecimens</strong>
                </legend><g:render template="generalSearchTable" />
            </fieldset>
        </div>
    </g:if>
    <g:else>
        There are no results to display
    </g:else>
</div>
</body>
</html>