<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'sizeWidth', 'error')} ">
	<label for="sizeWidth">
		<g:message code="subspecimen.sizeWidth.label" default="Size Width" />
	</label>
	<g:field tabindex="1" name="sizeWidth" type="text" value="${subspecimenInstance?.sizeWidth}" style="width:50px"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'sizeLength', 'error')} ">
	<label for="sizeLength">
		<g:message code="subspecimen.sizeLength.label" default="Size Length" />
	</label>
	<g:field tabindex="1" name="sizeLength" type="text" value="${subspecimenInstance?.sizeLength}" style="width:50px"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'sizeHeight', 'error')} ">
	<label for="sizeHeight">
		<g:message code="subspecimen.sizeHeight.label" default="Size Height" />
	</label>
	<g:field tabindex="1" name="sizeHeight" type="text" value="${subspecimenInstance?.sizeHeight}" style="width:50px"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'sizeUnit', 'error')}">
	<label for="sizeUnit">
		<g:message code="subspecimen.sizeUnit.label" default="Size Unit" />
	</label>
	<g:select tabindex="1" id="sizeUnit" name="sizeUnit.id" from="${hbis.SizeMeasurement.list()}" optionKey="id" value="${subspecimenInstance?.sizeUnit?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>