<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'volume', 'error')} ">
	<label for="volume">
		<g:message code="subspecimen.volume.label" default="Volume" />
	</label>
	<g:field tabindex="1" name="volume" type="text" value="${subspecimenInstance?.volume}" style="width:50px"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subspecimenInstance, field: 'volumeUnit', 'error')} ">
	<label for="volumeUnit">
		<g:message code="subspecimen.volumeUnit.label" default="Volume Unit" />
	</label>
	<g:if test="${volumeUnitId}">
		<g:select tabindex="1" id="volumeUnit" name="volumeUnit.id" from="${hbis.VolumeMeasurement.list()}" optionKey="id" value="${volumeUnitId}" class="many-to-one" noSelection="['null': '']"/>
	</g:if>
	<g:else>
		<g:select tabindex="1" id="volumeUnit" name="volumeUnit.id" from="${hbis.VolumeMeasurement.list()}" optionKey="id" value="${subspecimenInstance?.volumeUnit?.id}" class="many-to-one" noSelection="['null': '']"/>
	</g:else>
</div>