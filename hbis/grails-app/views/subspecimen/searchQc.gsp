
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:javascript library="jquery" />
		
		<script type="text/javascript">
        	$(document).ready(function()
        	{
          		$("#fromDate").datepicker({dateFormat: 'mm/dd/yy'});
          		$("#toDate").datepicker({dateFormat: 'mm/dd/yy'});
       	 	})
    	</script>
		
		<title>Search QC</title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>Search QC</h1>
			
			<g:form action="searchQc" >
				<fieldset class="form">
					<div>
						<label for="qcResultId">
							<g:message code="qcResult.tissueQcMatchId.label" default="QC Result" />
						</label>
						<g:select name="tissueQcMatch" from="${["Pass", "Fail"]}" value="${tissueQcMatch}" />
					</div>
					
					<div>
						<label for="fromDate">
							<g:message code="default.procurementDate.fromDate" default="Procurement From Date" />
						</label>
						<g:textField type="date" name="fromDate" id="fromDate" value="${fromDateTextField}" />
					</div>
					
					<div>
						<label for="toDate">
							<g:message code="default.procurementDate.toDate" default="Procurement To Date" />
						</label>
						<g:textField type="date" name="toDate" id="toDate" value="${toDateTextField}" />
					</div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="reassign" class="save" value="${message(code: 'subspecimen.button.reassignQuery.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			
				<g:if test="${subspecimens}">
					<div id="show-subspecimens" class="allowTableOverflow" style="clear:both">
						<fieldset class="box"><legend><strong>Subspecimens</strong>
												</legend>
							<g:render template="searchQcTable" />
						</fieldset>
					</div>
					<div>
					Download Excel Report
							<g:jasperReport
   								jasper="search-qc"
   								name="search-qc-${new Date().format('MMddyy')}"
   								controller="subspecimen"
   								action="searchQcReport"
   								format="xls"
   								description=" "
   								delimiter=" ">
   									<g:hiddenField name="fromDate" value="${fromDateTextField}" />
   									<g:hiddenField name="toDate" value="${toDateTextField}" />
   									<g:hiddenField name="tissueQcMatch" value="${tissueQcMatch}" />
   							</g:jasperReport>
   					</div>
				</g:if>
			</div>
		</div>
	</body>
</html>