
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:javascript library="jquery" />
		
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>Histology Worksheet</h1>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<!-- 
			<g:form action="histologyWorksheet" >
				<fieldset class="form">
				<div>
					<label for="procurementDate">
						<g:message code="specimen.procurementDate.label" default="Procurement Date" />
					</label>
					<g:datePicker name="procurementDate" precision="day"  value="${procurementDate}" default="none" noSelection="['': '']" />
				</div>
				
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="histologyWorksheet" class="save" value="${message(code: 'subspecimen.button.histologyWorksheet.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			
			 -->
			 
			<g:jasperReport
   						jasper="histology-worksheet"
   						name="histology-worksheet-${new Date().format('MMddyy')}"
   						controller="subspecimen"
   						action="histologyWorksheetReport"
   						format="pdf, xls"
   						description="Select Date: "
   						delimiter=" ">
   							<g:datePicker name="procurementDate" precision="day"  value="${procurementDate}" default="none" noSelection="['': '']" />
   						</g:jasperReport>
			
			
		</div>
	</body>
</html>