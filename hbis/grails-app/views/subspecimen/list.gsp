
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="subspecimenChtnID" title="${message(code: 'subspecimen.subspecimenChtnId.label', default: 'Subspecimen Chtn ID')}" />
					
						<th><g:message code="subspecimen.status.label" default="Status" /></th>
					
						<th><g:message code="subspecimen.preparationType.label" default="Preparation Type" /></th>
					
						<g:sortableColumn property="preparationMedia" title="${message(code: 'subspecimen.preparationMedia.label', default: 'Preparation Media')}" />
					
						<g:sortableColumn property="weight" title="${message(code: 'subspecimen.weight.label', default: 'Weight')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${subspecimenInstanceList}" status="i" var="subspecimenInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${subspecimenInstance.id}">${fieldValue(bean: subspecimenInstance, field: "subspecimenChtnId")}</g:link></td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "status")}</td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationType")}</td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "preparationMedia")}</td>
					
						<td>${fieldValue(bean: subspecimenInstance, field: "weight")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			
		</div>
	</body>
</html>
