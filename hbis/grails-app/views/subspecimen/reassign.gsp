
<%@ page import="hbis.Subspecimen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subspecimen.label', default: 'Subspecimen')}" />
		<g:javascript library="jquery" />
		
		<title>Reassign Subspecimens</title>
	</head>
	<body>
		<a href="#list-subspecimen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="list-subspecimen" class="content scaffold-list" role="main">
			<h1>Reassign Subspecimens</h1>
			
			<g:form action="reassign" >
				<fieldset class="form">
				<div>
					<label for="investigator">
						<g:message code="subspecimen.investigator.label" default="Investigator" />
					</label>
					<g:select id="investigator" name="investigatorId" from="${hbis.Investigator.list()}" optionKey="id" value="${investigatorId}" class="many-to-one" noSelection="['null': '']"/>
				</div>
				<div>
					<label for="qcResultId">
						<g:message code="qcResult.tissueQcMatchId.label" default="QC Result" />
					</label>
					<g:select name="tissueQcMatch" from="${["Pass", "Fail"]}" value="${tissueQcMatch}" noSelection="['': '']"/>
				</div>
				<div>
					<label for="specimenChtnId">
						<g:message code="specimen.specimenChtnId.label" default="Specimen ID" />
					</label>
					<g:textField name="specimenChtnId" value="${specimenChtnId}" />
				</div>
				
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="reassign" class="save" value="${message(code: 'subspecimen.button.reassignQuery.label', default: 'Update Results')}" />
				</fieldset>
			</g:form>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${subspecimenInstance}">
				<ul class="errors" role="alert">
					<g:eachError bean="${subspecimenInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
			</g:hasErrors>
			<g:form action="performReassign">
			<div id="show-subspecimens" class="allowTableOverflow" style="clear:both">
				<fieldset class="box"><legend><strong>Subspecimens</strong>
												</legend>
					<g:render template="reassignTable" />
				</fieldset>
				
				<div class="fieldcontain" >
					<label for="investigator">
						<g:message code="subspecimen.investigator.label" default="Investigator" />
					</label>
					<g:select id="investigatorToAssign" 
								name="investigatorToAssignId" 
								from="${hbis.Investigator.list()}" 
								optionKey="id" 
								value="${investigatorToAssignId}" 
								class="many-to-one" 
								noSelection="['null': '']"
								onchange="${remoteFunction(
            							controller:'investigator', 
            							action:'ajaxGetTissueRequestsForInvestigator',
										update:[success:'tissueRequestContainer'],
            							params:'\'investigatorId=\' + this.value', 
            						)}"
						/>
				</div>

				<div class="fieldcontain ${hasErrors(bean: specimenInstance, field: 'tissueRequest', 'error')} ">
					<label for="tissueRequest">
						<g:message code="subspecimen.tissueRequest.label" default="Tissue Request" />
					</label>
					<div id="tissueRequestContainer">
						Choose an Investigator to display associated Tissue Requests
					</div>
				</div>
				
			</div>
				<fieldset class="buttons">
					<g:hiddenField name="investigatorId" value="${investigatorId}" />
					<g:hiddenField name="tissueQcMatch" value="${tissueQcMatch}" />
					<g:hiddenField name="specimenChtnId" value="${specimenChtnId}" />
					<g:submitButton name="performReassign" class="save" value="${message(code: 'subspecimen.button.reassignQuery.label', default: 'Reassign Checked Subspecimens')}" />
				</fieldset>
			</g:form>
			
		</div>
	</body>
</html>