<ul class="property-list consent">
	<li class="fieldcontain">
		<span id="consentType-label" class="property-label"><g:message code="consent.consentType.label" default="Consent Type: " /></span>
		<span class="property-value" aria-labelledby="consentType-label"><g:fieldValue bean="${consentInstance}" field="consentType"/></span>
		
		<span id="status-label" class="property-label"><g:message code="consent.status.label" default="Status: " /></span>
		<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${consentInstance}" field="status"/></span>
		
		<span id="method-label" class="property-label"><g:message code="consent.method.label" default="Method: " /></span>
		<span class="property-value" aria-labelledby="method-label"><g:fieldValue bean="${consentInstance}" field="method"/></span>
		
		<g:if test="${consentInstance?.protocols}">
			<li class="fieldcontain">
				<span id="protocols-label" class="property-label"><g:message code="consent.protocols.label" default="Protocols" /></span>
				<g:each in="${consentInstance?.protocols?}" var="p">
					<span class="property-value" aria-labelledby="protocols-label"><g:link controller="protocol" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
				</g:each>
			</li>
		</g:if>
	</li>
			
	<li class="fieldcontain">
		<span id="consentDate-label" class="property-label"><g:message code="consent.consentDate.label" default="Consent Date: " /></span>
		<span class="property-value" aria-labelledby="consentDate-label"><g:formatDate date="${consentInstance?.consentDate}" /></span>
	</li>
	
	<li class="fieldcontain">
		<span id="consenterFirstName-label" class="property-label"><g:message code="consent.consenterFirstName.label" default="Consenter First Name: " /></span>
		<span class="property-value" aria-labelledby="consenterFirstName-label"><g:fieldValue bean="${consentInstance}" field="consenterFirstName"/></span>
		
		<span id="consenterLastName-label" class="property-label"><g:message code="consent.consenterLastName.label" default="Consenter Last Name: " /></span>
		<span class="property-value" aria-labelledby="consenterLastName-label"><g:fieldValue bean="${consentInstance}" field="consenterLastName"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="consenterPhoneNumber-label" class="property-label"><g:message code="consent.consenterPhoneNumber.label" default="Consenter Phone Number: " /></span>
		<span class="property-value" aria-labelledby="consenterPhoneNumber-label"><g:fieldValue bean="${consentInstance}" field="consenterPhoneNumber"/></span>
		
		<span id="consenterEmail-label" class="property-label"><g:message code="consent.consenterEmail.label" default="Consenter Email: " /></span>
		<span class="property-value" aria-labelledby="consenterEmail-label"><g:fieldValue bean="${consentInstance}" field="consenterEmail"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="location-label" class="property-label"><g:message code="consent.location.label" default="Location: " /></span>
		<span class="property-value" aria-labelledby="location-label"><g:fieldValue bean="${consentInstance}" field="location"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="statusChangeDate-label" class="property-label"><g:message code="consent.statusChangeDate.label" default="Status Change Date: " /></span>
		<span class="property-value" aria-labelledby="statusChangeDate-label"><g:formatDate date="${consentInstance?.statusChangeDate}" /></span>
	</li>
			
	<li class="fieldcontain">
		<span id="patient-label" class="property-label"><g:message code="consent.patient.label" default="Patient: " /></span>
		<span class="property-value" aria-labelledby="patient-label"><g:link controller="patient" action="show" id="${consentInstance?.patient?.id}">${consentInstance?.patient?.encodeAsHTML()}</g:link></span>
	</li>
			
</ul>