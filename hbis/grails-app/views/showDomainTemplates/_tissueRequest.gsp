<ul class="property-list tissueRequest">
	<li class="fieldcontain">
		<span id="name-label" class="property-label"><g:message code="tissueRequest.name.label" default="Name" /></span>
		<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${tissueRequestInstance}" field="name"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="status-label" class="property-label"><g:message code="tissueRequest.status.label" default="Status" /></span>
		<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${tissueRequestInstance}" field="status"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="tissueRequestTissueQuestId-label" class="property-label"><g:message code="tissueRequest.tissueRequestTissueQuestId.label" default="Tissue Request Tissue Quest ID" /></span>
		<span class="property-value" aria-labelledby="tissueRequestTissueQuestId-label"><g:fieldValue bean="${tissueRequestInstance}" field="tissueRequestTissueQuestId"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="projectTissueQuestId-label" class="property-label"><g:message code="tissueRequest.projectTissueQuestId.label" default="Project Tissue Quest ID" /></span>
		<span class="property-value" aria-labelledby="projectTissueQuestId-label">${tissueRequestInstance?.projectTissueQuestId}</span>
	</li>
			
	<li class="fieldcontain">
		<span id="contactMethod-label" class="property-label"><g:message code="tissueRequest.contactMethod.label" default="Contact Method" /></span>
		<span class="property-value" aria-labelledby="contactMethod-label"><g:fieldValue bean="${tissueRequestInstance}" field="contactMethod"/></span>
	</li>
</ul>