<div class="leftShowColumn">
<ul class="property-list protocol">
	<li class="fieldcontain">
		<span id="description-label" class="property-label"><g:message code="protocol.description.label" default="Description" /></span>
		<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${protocolInstance}" field="description"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="principalInvestigatorName-label" class="property-label"><g:message code="protocol.principalInvestigatorName.label" default="Principal Investigator Name" /></span>
		<span class="property-value" aria-labelledby="principalInvestigatorName-label"><g:fieldValue bean="${protocolInstance}" field="principalInvestigatorName"/></span>
	</li>
</ul>
</div> <!-- End Left Show Column -->