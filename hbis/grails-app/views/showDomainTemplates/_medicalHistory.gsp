<div class="leftShowColumn">
<ul class="property-list medicalHistory">
	<li class="fieldcontain">
		<span id="cancer-label" class="property-label"><g:message code="medicalHistory.cancer.label" default="Cancer" /></span>
		<span class="property-value" aria-labelledby="cancer-label"><g:link controller="primaryMets" action="show" id="${medicalHistoryInstance?.cancer?.id}">${medicalHistoryInstance?.cancer?.encodeAsHTML()}</g:link></span>
	</li>
	<li class="fieldcontain">
		<span id="status-label" class="property-label"><g:message code="medicalHistory.status.label" default="Status" /></span>
		<span class="property-value" aria-labelledby="status-label"><g:link controller="medicalStatus" action="show" id="${medicalHistoryInstance?.status?.id}">${medicalHistoryInstance?.status?.encodeAsHTML()}</g:link></span>
	</li>
	<li class="fieldcontain">
		<span id="diagnosis-label" class="property-label"><g:message code="medicalHistory.diagnosis.label" default="Diagnosis" /></span>
		<span class="property-value" aria-labelledby="diagnosis-label"><g:link controller="diagnosis" action="show" id="${medicalHistoryInstance?.diagnosis?.id}">${medicalHistoryInstance?.diagnosis?.encodeAsHTML()}</g:link></span>
	</li>
	<li class="fieldcontain">
		<span id="stage-label" class="property-label"><g:message code="medicalHistory.stage.label" default="Stage" /></span>
		<span class="property-value" aria-labelledby="stage-label"><g:fieldValue bean="${medicalHistoryInstance}" field="stage"/></span>
	</li>
	<li class="fieldcontain">
		<span id="chartReview-label" class="property-label"><g:message code="medicalHistory.chartReview.label" default="Chart Review" /></span>
		<span class="property-value" aria-labelledby="chartReview-label"><g:link controller="chartReview" action="show" id="${medicalHistoryInstance?.chartReview?.id}">${medicalHistoryInstance?.chartReview?.encodeAsHTML()}</g:link></span>
	</li>
</ul>
</div>