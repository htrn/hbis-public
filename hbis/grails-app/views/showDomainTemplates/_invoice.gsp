<ul class="property-list invoice">
	<li class="fieldcontain">
		<span id="investigator-label" class="property-label"><g:message code="invoice.investigator.label" default="Investigator" /></span>
		<span class="property-value" aria-labelledby="investigator-label"><g:link controller="investigator" action="show" id="${invoiceInstance?.investigator?.id}">${invoiceInstance?.investigator?.encodeAsHTML()}</g:link></span>
	</li>
	
	<li class="fieldcontain">
		<span id="attentionTo-label" class="property-label"><g:message code="invoice.attentionTo.label" default="Attention To" /></span>
		<span class="property-value" aria-labelledby="attentionTo-label"><g:fieldValue bean="${invoiceInstance}" field="attentionTo"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="institution-label" class="property-label"><g:message code="invoice.institution.label" default="Institution" /></span>
		<span class="property-value" aria-labelledby="institution-label"><g:fieldValue bean="${invoiceInstance}" field="institution"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="department-label" class="property-label"><g:message code="invoice.department.label" default="Department" /></span>
		<span class="property-value" aria-labelledby="department-label"><g:fieldValue bean="${invoiceInstance}" field="department"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="streetAddress1-label" class="property-label"><g:message code="invoice.streetAddress1.label" default="Street Address 1" /></span>
		<span class="property-value" aria-labelledby="streetAddress1-label"><g:fieldValue bean="${invoiceInstance}" field="streetAddress1"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="streetAddress2-label" class="property-label"><g:message code="invoice.streetAddress2.label" default="Street Address 2" /></span>
		<span class="property-value" aria-labelledby="streetAddress2-label"><g:fieldValue bean="${invoiceInstance}" field="streetAddress2"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="city-label" class="property-label"><g:message code="invoice.city.label" default="City" /></span>
		<span class="property-value" aria-labelledby="city-label"><g:fieldValue bean="${invoiceInstance}" field="city"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="state-label" class="property-label"><g:message code="invoice.state.label" default="State" /></span>
		<span class="property-value" aria-labelledby="state-label"><g:fieldValue bean="${invoiceInstance}" field="state"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="zipCode-label" class="property-label"><g:message code="invoice.zipCode.label" default="Zip Code" /></span>
		<span class="property-value" aria-labelledby="zipCode-label"><g:fieldValue bean="${invoiceInstance}" field="zipCode"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="country-label" class="property-label"><g:message code="invoice.country.label" default="Country" /></span>
		<span class="property-value" aria-labelledby="country-label"><g:fieldValue bean="${invoiceInstance}" field="country"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="invoiceDate-label" class="property-label"><g:message code="invoice.invoiceDate.label" default="Invoice Date" /></span>
		<span class="property-value" aria-labelledby="invoiceDate-label"><g:formatDate date="${invoiceInstance?.invoiceDate}" /></span>
	</li>
	
	<li class="fieldcontain">
		<span id="lastInvoiceSentDate-label" class="property-label"><g:message code="invoice.lastInvoiceSentDate.label" default="Last Invoice Sent Date" /></span>
		<span class="property-value" aria-labelledby="lastInvoiceSentDate-label"><g:formatDate date="${invoiceInstance?.lastInvoiceSentDate}" /></span>
	</li>
	
	<li class="fieldcontain">
		<span id="billingStatus-label" class="property-label"><g:message code="invoice.billingStatus.label" default="Billing Status" /></span>
		<span class="property-value" aria-labelledby="billingStatus-label"><g:fieldValue bean="${invoiceInstance}" field="billingStatus"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="paymentStatus-label" class="property-label"><g:message code="invoice.paymentStatus.label" default="Payment Status" /></span>
		<span class="property-value" aria-labelledby="paymentStatus-label"><g:fieldValue bean="${invoiceInstance}" field="paymentStatus"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="paymentReceivedDate-label" class="property-label"><g:message code="invoice.paymentReceivedDate.label" default="Payment Received Date" /></span>
		<span class="property-value" aria-labelledby="paymentReceivedDate-label"><g:formatDate date="${invoiceInstance?.paymentReceivedDate}" /></span>
	</li>
	
	<li class="fieldcontain">
		<span id="paymentReceivedBy-label" class="property-label"><g:message code="invoice.paymentReceivedBy.label" default="Payment Received By" /></span>
		<span class="property-value" aria-labelledby="paymentReceivedBy-label"><g:fieldValue bean="${invoiceInstance}" field="paymentReceivedBy" /></span>
	</li>
	 
	<li class="fieldcontain">
		<span id="shippingCarts-label" class="property-label"><g:message code="invoice.shippingCarts.label" default="Shipping Carts" /></span>
		<g:each in="${invoiceInstance.shippingCarts}" var="s">
			<span class="property-value" aria-labelledby="shippingCarts-label"><g:link controller="shippingCart" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>	
	
</ul>