<div class="leftShowColumn">
<ul class="property-list investigator">
	<li class="fieldcontain">
		<span id="chtnIdPrefix-label" class="property-label"><g:message code="investigator.chtnIdPrefix.label" default="Chtn ID Prefix" /></span>
		<span class="property-value" aria-labelledby="chtnIdPrefix-label"><g:fieldValue bean="${investigatorInstance}" field="chtnIdPrefix"/></span>
	</li>
	
	<li class="fieldcontain">
		<span id="chtnId-label" class="property-label"><g:message code="investigator.chtnId.label" default="Chtn ID" /></span>
		<span class="property-value" aria-labelledby="chtnId-label"><g:fieldValue bean="${investigatorInstance}" field="chtnId"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="firstName-label" class="property-label"><g:message code="investigator.firstName.label" default="First Name" /></span>
		<span class="property-value" aria-labelledby="firstName-label"><g:fieldValue bean="${investigatorInstance}" field="firstName"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="middleName-label" class="property-label"><g:message code="investigator.middleName.label" default="Middle Name" /></span>
		<span class="property-value" aria-labelledby="middleName-label"><g:fieldValue bean="${investigatorInstance}" field="middleName"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="lastName-label" class="property-label"><g:message code="investigator.lastName.label" default="Last Name" /></span>
		<span class="property-value" aria-labelledby="lastName-label"><g:fieldValue bean="${investigatorInstance}" field="lastName"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="chtnDivision-label" class="property-label"><g:message code="investigator.chtnDivision.label" default="Chtn Division" /></span>
		<span class="property-value" aria-labelledby="chtnDivision-label"><g:fieldValue bean="${investigatorInstance}" field="chtnDivision"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="externalVsInternal-label" class="property-label"><g:message code="investigator.externalVsInternal.label" default="External or Internal" /></span>
		<span class="property-value" aria-labelledby="externalVsInternal-label"><g:fieldValue bean="${investigatorInstance}" field="externalVsInternal"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="institutionType-label" class="property-label"><g:message code="investigator.institutionType.label" default="Institution Type" /></span>
		<span class="property-value" aria-labelledby="institutionType-label"><g:fieldValue bean="${investigatorInstance}" field="institutionType"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="serviceProgramType-label" class="property-label"><g:message code="investigator.serviceProgramType.label" default="Service Program Type" /></span>
		<span class="property-value" aria-labelledby="serviceProgramType-label"><g:fieldValue bean="${investigatorInstance}" field="serviceProgramType"/></span>
	</li>
				
</ul>
</div> <!-- End Left Show Column -->
				
<div class="middleShowColumn">
<ul class="property-list investigator">
			
	<li class="fieldcontain">
		<span id="primaryInstitution-label" class="property-label"><g:message code="investigator.primaryInstitution.label" default="Primary Institution" /></span>
		<span class="property-value" aria-labelledby="primaryInstitution-label"><g:fieldValue bean="${investigatorInstance}" field="primaryInstitution"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="primaryDepartment-label" class="property-label"><g:message code="investigator.primaryDepartment.label" default="Primary Department" /></span>
		<span class="property-value" aria-labelledby="primaryDepartment-label"><g:fieldValue bean="${investigatorInstance}" field="primaryDepartment"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="primaryStreetAddress1-label" class="property-label"><g:message code="investigator.primaryStreetAddress1.label" default="Primary Street Address1" /></span>
		<span class="property-value" aria-labelledby="primaryStreetAddress1-label"><g:fieldValue bean="${investigatorInstance}" field="primaryStreetAddress1"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="primaryStreetAddress2-label" class="property-label"><g:message code="investigator.primaryStreetAddress2.label" default="Primary Street Address2" /></span>
		<span class="property-value" aria-labelledby="primaryStreetAddress2-label"><g:fieldValue bean="${investigatorInstance}" field="primaryStreetAddress2"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="primaryCity-label" class="property-label"><g:message code="investigator.primaryCity.label" default="Primary City" /></span>
		<span class="property-value" aria-labelledby="primaryCity-label"><g:fieldValue bean="${investigatorInstance}" field="primaryCity"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="primaryState-label" class="property-label"><g:message code="investigator.primaryState.label" default="Primary State" /></span>
		<span class="property-value" aria-labelledby="primaryState-label"><g:fieldValue bean="${investigatorInstance}" field="primaryState"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="primaryZipCode-label" class="property-label"><g:message code="investigator.primaryZipCode.label" default="Primary Zip Code" /></span>
		<span class="property-value" aria-labelledby="primaryZipCode-label"><g:fieldValue bean="${investigatorInstance}" field="primaryZipCode"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="primaryCountry-label" class="property-label"><g:message code="investigator.primaryCountry.label" default="Primary Country" /></span>
		<span class="property-value" aria-labelledby="primaryCountry-label"><g:fieldValue bean="${investigatorInstance}" field="primaryCountry"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="billingInstitution-label" class="property-label"><g:message code="investigator.billingInstitution.label" default="Billing Institution" /></span>
		<span class="property-value" aria-labelledby="billingInstitution-label"><g:fieldValue bean="${investigatorInstance}" field="billingInstitution"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="billingDepartment-label" class="property-label"><g:message code="investigator.billingDepartment.label" default="Billing Department" /></span>
		<span class="property-value" aria-labelledby="billingDepartment-label"><g:fieldValue bean="${investigatorInstance}" field="billingDepartment"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="billingStreetAddress1-label" class="property-label"><g:message code="investigator.billingStreetAddress1.label" default="Billing Street Address1" /></span>
		<span class="property-value" aria-labelledby="billingStreetAddress1-label"><g:fieldValue bean="${investigatorInstance}" field="billingStreetAddress1"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="billingStreetAddress2-label" class="property-label"><g:message code="investigator.billingStreetAddress2.label" default="Billing Street Address2" /></span>
		<span class="property-value" aria-labelledby="billingStreetAddress2-label"><g:fieldValue bean="${investigatorInstance}" field="billingStreetAddress2"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="billingCity-label" class="property-label"><g:message code="investigator.billingCity.label" default="Billing City" /></span>
		<span class="property-value" aria-labelledby="billingCity-label"><g:fieldValue bean="${investigatorInstance}" field="billingCity"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="billingState-label" class="property-label"><g:message code="investigator.billingState.label" default="Billing State" /></span>
		<span class="property-value" aria-labelledby="billingState-label"><g:fieldValue bean="${investigatorInstance}" field="billingState"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="billingZipCode-label" class="property-label"><g:message code="investigator.billingZipCode.label" default="Billing Zip Code" /></span>
		<span class="property-value" aria-labelledby="billingZipCode-label"><g:fieldValue bean="${investigatorInstance}" field="billingZipCode"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="billingCountry-label" class="property-label"><g:message code="investigator.billingCountry.label" default="Billing Country" /></span>
		<span class="property-value" aria-labelledby="billingCountry-label"><g:fieldValue bean="${investigatorInstance}" field="billingCountry"/></span>
	</li>
				
</ul>
</div> <!-- End Middle Show Column -->
				
<div class="rightShowColumn">
<ul class="property-list investigator">
			
	<li class="fieldcontain">
		<span id="shippingInstitution-label" class="property-label"><g:message code="investigator.shippingInstitution.label" default="Shipping Institution" /></span>
		<span class="property-value" aria-labelledby="shippingInstitution-label"><g:fieldValue bean="${investigatorInstance}" field="shippingInstitution"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shippingDepartment-label" class="property-label"><g:message code="investigator.shippingDepartment.label" default="Shipping Department" /></span>
		<span class="property-value" aria-labelledby="shippingDepartment-label"><g:fieldValue bean="${investigatorInstance}" field="shippingDepartment"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shippingStreetAddress1-label" class="property-label"><g:message code="investigator.shippingStreetAddress1.label" default="Shipping Street Address1" /></span>
		<span class="property-value" aria-labelledby="shippingStreetAddress1-label"><g:fieldValue bean="${investigatorInstance}" field="shippingStreetAddress1"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shippingStreetAddress2-label" class="property-label"><g:message code="investigator.shippingStreetAddress2.label" default="Shipping Street Address2" /></span>
		<span class="property-value" aria-labelledby="shippingStreetAddress2-label"><g:fieldValue bean="${investigatorInstance}" field="shippingStreetAddress2"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shippingCity-label" class="property-label"><g:message code="investigator.shippingCity.label" default="Shipping City" /></span>
		<span class="property-value" aria-labelledby="shippingCity-label"><g:fieldValue bean="${investigatorInstance}" field="shippingCity"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shippingState-label" class="property-label"><g:message code="investigator.shippingState.label" default="Shipping State" /></span>
		<span class="property-value" aria-labelledby="shippingState-label"><g:fieldValue bean="${investigatorInstance}" field="shippingState"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shippingZipCode-label" class="property-label"><g:message code="investigator.shippingZipCode.label" default="Shipping Zip Code" /></span>
		<span class="property-value" aria-labelledby="shippingZipCode-label"><g:fieldValue bean="${investigatorInstance}" field="shippingZipCode"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="shippingCountry-label" class="property-label"><g:message code="investigator.shippingCountry.label" default="Shipping Country" /></span>
		<span class="property-value" aria-labelledby="shippingCountry-label"><g:fieldValue bean="${investigatorInstance}" field="shippingCountry"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="courierName-label" class="property-label"><g:message code="investigator.courierName.label" default="Courier Name" /></span>
		<span class="property-value" aria-labelledby="courierName-label"><g:fieldValue bean="${investigatorInstance}" field="courierName"/></span>
	</li>
			
	<li class="fieldcontain">
		<span id="courierNumber-label" class="property-label"><g:message code="investigator.courierNumber.label" default="Courier Number" /></span>
		<span class="property-value" aria-labelledby="courierNumber-label"><g:fieldValue bean="${investigatorInstance}" field="courierNumber"/></span>
	</li>
				
	<li class="fieldcontain">
		<span id="tissueRequests-label" class="property-label"><g:message code="investigator.projects.label" default="Tissue Requests" /></span>
		<g:each in="${investigatorInstance.tissueRequests}" var="tr">
			<span class="property-value" aria-labelledby="projects-label"><g:link controller="tissueRequest" action="show" id="${tr.id}">${tr?.encodeAsHTML()}</g:link></span>
		</g:each>
	</li>
				
</ul>
</div> <!-- End Right Show Column -->