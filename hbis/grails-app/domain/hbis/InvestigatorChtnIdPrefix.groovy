package hbis

class InvestigatorChtnIdPrefix {
	String prefix
	
	String toString() {
		prefix
	}

    static constraints = {
    }
	
	static mapping = {
		sort prefix: "asc"
	}
}
