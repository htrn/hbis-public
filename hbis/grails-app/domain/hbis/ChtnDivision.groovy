package hbis

class ChtnDivision {
	String divisionName
	String primaryVsNonPrimary
	
	String toString() {
		divisionName
	}

    static constraints = {
		primaryVsNonPrimary(inList:["Primary", "Non-Primary"])
    }
	
	static mapping = {
		sort divisionName: "asc"
	}
}
