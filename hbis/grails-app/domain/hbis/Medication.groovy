package hbis

class Medication {
	String medicationName
	Integer doseMg
	String doseOther
	String frequency
	MedicalStatus status
	
	static belongsTo = [chartReview: ChartReview]

    static constraints = {
    }
}
