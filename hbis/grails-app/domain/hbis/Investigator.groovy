package hbis

class Investigator {
	InvestigatorChtnIdPrefix chtnIdPrefix
	String chtnId
	ChtnDivision chtnDivision
	ExternalVsInternal externalVsInternal
	InstitutionType institutionType
	ServiceProgramType serviceProgramType
	
	InvestigatorStatus status
	
	String firstName
	String middleName
	String lastName
	
	String primaryInstitution
	String primaryDepartment
	String primaryStreetAddress1
	String primaryStreetAddress2
	String primaryCity
	String primaryState
	String primaryZipCode
	String primaryCountry
	
	String billingInstitution
	String billingDepartment
	String billingStreetAddress1
	String billingStreetAddress2
	String billingCity
	String billingState
	String billingZipCode
	String billingCountry
	
	String shippingInstitution
	String shippingDepartment
	String shippingStreetAddress1
	String shippingStreetAddress2
	String shippingCity
	String shippingState
	String shippingZipCode
	String shippingCountry
	
	ShipMode courierName
	String courierNumber
	
	//hasMany TissueRequests?
	static hasMany = [tissueRequests: TissueRequest]
	
	String toString(){
		"$lastName, $firstName: $chtnIdPrefix$chtnId"
	}

    static constraints = {
		chtnIdPrefix()
		chtnId() //It should be unique for the associated chtnIdPrefix, but is not currently
		chtnDivision(nullable:true)
		externalVsInternal(nullable:true)
		institutionType(nullable:true)
		serviceProgramType(nullable:true)
		status(nullable:true)
		
		firstName(blank:false)
		middleName(nullable:true)
		lastName(blank:false)
		
		primaryInstitution(nullable:true)
		primaryDepartment(nullable:true)
		primaryStreetAddress1(nullable:true)
		primaryStreetAddress2(nullable:true)
		primaryCity(nullable:true)
		primaryState(nullable:true)
		primaryZipCode(nullable:true)
		primaryCountry(nullable:true)
		
		billingInstitution(nullable:true)
		billingDepartment(nullable:true)
		billingStreetAddress1(nullable:true)
		billingStreetAddress2(nullable:true)
		billingCity(nullable:true)
		billingState(nullable:true)
		billingZipCode(nullable:true)
		billingCountry(nullable:true)
		
		shippingInstitution(nullable:true)
		shippingDepartment(nullable:true)
		shippingStreetAddress1(nullable:true)
		shippingStreetAddress2(nullable:true)
		shippingCity(nullable:true)
		shippingState(nullable:true)
		shippingZipCode(nullable:true)
		shippingCountry(nullable:true)
		
		courierName(nullable:true)
		courierNumber(nullable:true)
    }
	static mapping = {
		sort lastName: "asc"
	}
}
