package hbis

class FamilyHistory {
	String relative
	Diagnosis diagnosis
	
	static belongsTo = [chartReview: ChartReview]

    static constraints = {
    }
}
