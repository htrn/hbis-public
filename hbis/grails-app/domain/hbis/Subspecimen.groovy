package hbis

class Subspecimen {
	String subspecimenChtnId
	SubspecimenStatus status
	Date statusChangeDate
	PreparationType preparationType
	PreparationMedia preparationMedia
	BigDecimal weight
	WeightMeasurement weightUnit
	BigDecimal volume
	VolumeMeasurement volumeUnit
	BigDecimal sizeWidth
	BigDecimal sizeLength
	BigDecimal sizeHeight
	SizeMeasurement sizeUnit
	String labelComment
	String comments

	BigDecimal price
	Date datePriceUpdated
	
	boolean needsChartReview
	boolean called
	boolean emailed
	
	boolean deidentified
	Date dateDeidentified
	boolean transferredToIw
	Date dateTransferredToIw
	
	Date dateCreated
	
	ShippingCart shippingCart
	
	Investigator investigator
	//Project is not part of this class for now
	//Project project
	TissueRequest tissueRequest
	
	//needs to be associated with User
	static belongsTo = [specimen: Specimen]
	//static hasOne = [investigator: Investigator] //revisit this later, also should tie Project
	
	String toString(){
		"$subspecimenChtnId : $preparationType, $investigator, $status"
	}

	static constraints = {
		subspecimenChtnId(blank:false)
		status()
		preparationType()
		preparationMedia(nullable:true)
		weight(nullable:true, scale: 2)
		weightUnit(nullable:true)
		volume(nullable:true, scale: 2)
		volumeUnit(nullable:true)
		sizeWidth(nullable:true, scale: 2)
		sizeLength(nullable:true, scale: 2)
		sizeHeight(nullable:true, scale: 2)
		sizeUnit(nullable:true)
		labelComment(nullable:true)
		comments(nullable:true)

		price(nullable:true, scale: 2)
		investigator(nullable:true)
		shippingCart(nullable:true)
		
		datePriceUpdated(nullable:true)
		
		deidentified(nullable:true)
		dateDeidentified(nullable:true)
		transferredToIw(nullable:true)
		dateTransferredToIw(nullable:true)
		statusChangeDate(nullable:true)
		called(nullable:true)
		emailed(nullable:true)
		tissueRequest(nullable:true)
	}
	
	static mapping = {
		specimen(sort:'specimenChtnId', order:'asc')
		sort "subspecimenChtnId"
	}
}
