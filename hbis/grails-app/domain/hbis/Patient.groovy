package hbis

class Patient { //change class name to Patient
	String mrn
	String firstName
	String middleInitial
	String lastName
	Gender gender
	Race race
	Date dateOfBirth
	Institution originInstitution
	
	static hasMany = [procedures: Procedure, consents:Consent]
	String toString() {
		"${mrn}" + " " + "${lastName}"
	}

    static constraints = {
		mrn(blank: false, unique: true, matches: /\d{9}/)  //Must be 9 digits
		firstName(nullable:true)
		middleInitial(nullable:true)
		lastName()
		gender(nullable:true)
		race(nullable:true)
		dateOfBirth(nullable:true)
		originInstitution(nullable:true)
    }
	
	static mapping = {
		procedures(sort:'procedureDate', order:'asc')
		consents(sort:'consentDate', order:'asc')
	}
}
