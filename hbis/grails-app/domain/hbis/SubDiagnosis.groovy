package hbis

class SubDiagnosis {
	String description
	
	String toString() {
		description
	}

	static constraints = {
	}
	
	static mapping = {
		sort description: "asc"
	}
}
