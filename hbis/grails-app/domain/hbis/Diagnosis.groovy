package hbis

class Diagnosis {
	String description
	
	String toString() {
		description
	}

    static constraints = {
    }
	
	static mapping = {
		sort description: "asc"
	}
}
