package hbis

class Protocol {
	String description //Really protocol name/number, may display as that on front end
	String principalInvestigatorName
	
	String toString() {
		def toStringValue = description
		if(principalInvestigatorName) toStringValue += ": $principalInvestigatorName"
		return toStringValue
	}

    static constraints = {
		description()
		principalInvestigatorName(nullable:true)
    }
	
	static mapping = {
		sort description: "asc"
	}
}
