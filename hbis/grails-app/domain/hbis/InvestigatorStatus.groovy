package hbis

class InvestigatorStatus {
	String description
	
	String toString() {
		description
	}
	
    static constraints = {
    }
	
	static mapping = {
		sort description: "asc"
	}
}
