package hbis

class PreparationMedia {
	String description
	String fixed
	String fresh
	String frozen

	String toString() {
		description
	}

    static constraints = {
		fixed nullable: true
		fresh nullable: true
		frozen nullable: true
    }
}
