package hbis

class ProcedureName {
	String description
	
	String toString() {
		description
	}

    static constraints = {
    }
	
	static mapping = {
		sort description: "asc"
	}
}
