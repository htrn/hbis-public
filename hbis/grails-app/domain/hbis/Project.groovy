package hbis

class Project {
	String name
	String projectId
	ProjectStatus status
	
	String toString(){
		"$projectId"
	}
	
	//static belongsTo = [investigator: Investigator]
	//static hasMany = [tissueRequests: TissueRequest]

    static constraints = {
		name()
		projectId()
		status()
    }
}
