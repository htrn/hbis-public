package hbis

class SurgicalHistory {
	YesNo forBiosample
	ProcedureName procedure
	String anesthetics
	
	static belongsTo = [chartReview: ChartReview]

    static constraints = {
    }
}
