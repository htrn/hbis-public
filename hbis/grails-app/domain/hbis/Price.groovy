package hbis

class Price {
	
	String name
	BigDecimal price
	Boolean display
	Date dateCreated
	Date lastUpdated
	
	static mapping = {
	}
	

    static constraints = {
		name blank: false, nullable: true
		price min:0.00, blank: false
		display blank:false
    }
	
	static def getCurrentPriceList() {
		def prices = Price.findAllDisplay(sort:"price")
	}
}
