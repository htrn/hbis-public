package hbis
import java.text.SimpleDateFormat

class Procedure {
	ProcedureName procedureName //was surgeryName
	SubProcedureName subProcedureName
	ProcedureType procedureType
	Date procedureDate = new Date().clearTime()
	Date endTimeOfProcedure //case results
	Date dateOfDeath
	Institution procedureEventInstitution
	String building
	String roomNumber //case results
	ProcurementResult procurementResult //case results
	Origin origin
	Integer patientAge
	AgeUnit patientAgeUnit
	YesNo chemoHistory
	String chemoHistoryComments
	YesNo radiationHistory
	String radiationHistoryComments
	String medComments //this may be tied to Chart Review entity later
	ChartReview chartReview
	Date dateCreated
	
	static belongsTo = [patient: Patient]
	static hasMany = [specimens: Specimen]
	//static hasOne = [chartReview: ChartReview]
	
	String toString() {
		def sdf = new SimpleDateFormat("MM/dd/yyyy")
		"${id}: " + "${sdf.format(procedureDate)}, ${procedureName}"
	}
	
	
	static constraints = {
		procedureDate()
		procedureType()
		procedureName()
		subProcedureName(nullable:true)
		procedureEventInstitution()
		procurementResult(nullable:true)
		patientAge(nullable:true, min: 0)
		patientAgeUnit(nullable:true)
		dateOfDeath(nullable:true)
		chemoHistory(nullable: true)
		radiationHistory(nullable: true)
		endTimeOfProcedure(nullable:true)
		chartReview(nullable:true)
		origin(nullable:true)
	}
	
	static mapping = {
		table 'clinical_procedure' 
		specimens(sort:'specimenChtnId', order:'asc')
	}
	
	Integer getNumberOfSubspecimens() {
		def numberOfSubspecimens = 0
		this.specimens.each { specimen ->
			numberOfSubspecimens += specimen.getNumberOfSubspecimens()
		}
		return numberOfSubspecimens
	}
}
