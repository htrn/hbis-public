package hbis

class Invoice {
	String attentionTo
	String institution
	String department
	String streetAddress1
	String streetAddress2
	String city
	String state
	String zipCode
	String country
	
	Date invoiceDate
	
	InvoiceBillingStatus billingStatus
	InvoicePaymentStatus paymentStatus
	PaymentMethod paymentMethod
	//String otherPaymentMethod? //Asked Tina about paymentMethod, and she suggested having an Other choice with text area
	Date paymentReceivedDate
	String paymentReceivedBy
	BigDecimal amountPaid
	Date lastInvoiceSentDate
	
	Investigator investigator
	
	static hasMany = [shippingCarts: ShippingCart]

    static constraints = {
		attentionTo(nullable:true)
		institution(nullable:true)
		department(nullable:true)
		streetAddress1(nullable:true)
		streetAddress2(nullable:true)
		city(nullable:true)
		state(nullable:true)
		zipCode(nullable:true)
		country(nullable:true)
		invoiceDate(nullable:true)
		billingStatus(nullable:true)
		paymentStatus(nullable:true)
		paymentMethod(nullable:true)
		paymentReceivedDate(nullable:true)
		paymentReceivedBy(nullable:true)
		amountPaid(nullable:true)
		lastInvoiceSentDate(nullable:true)
    }
	
	static mapping = {
		shippingCarts(sort:'id', order:'asc')
	}
	
	BigDecimal getTotalPrice() {
		def totalPrice = 0.00;
		this.shippingCarts.each { shippingCart ->
			totalPrice += shippingCart.getTotalPrice()
		}
		return totalPrice
	}
	
	Integer getNumberOfShippingCarts() {
		def numberOfShippingCarts = 0
		this.shippingCarts.each {
			numberOfShippingCarts++
		}
		return numberOfShippingCarts
	}
}