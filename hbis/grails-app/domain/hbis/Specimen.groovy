package hbis

class Specimen {
	String specimenChtnId
	String alternateSpecimenID
	Date procurementDate
	Date timeReceived //needs to be a time type
	Date timePrepared //needs to be a time type
	TissueType preliminaryTissueType
	AnatomicSite preliminaryPrimaryAnatomicSite
	PrimaryMets primaryOrMets
	MetsToAnatomicSite metsToAnatomicSite
	Diagnosis preliminaryDiagnosis
	SubDiagnosis preliminarySubDiagnosis
	ModDiagnosis preliminaryModDiagnosis
	Integer hoursPostAutopsy
	String comments
	ProcurementTechInitials techInitials
	Side side
	SpecimenCategory specimenCategory
	
	Date dateCreated
	
	QcMethod qcMethod
	QcBatch qcBatch
	static belongsTo = [procedure: Procedure]
	static hasMany = [subspecimens: Subspecimen]
	static hasOne = [qcResult: QcResult]
	
	String toString(){
		"$specimenChtnId :$preliminaryTissueType"
	}
	
	static constraints = {
		specimenChtnId(blank:false, unique:true)
		alternateSpecimenID(nullable:true)
		procurementDate()
		preliminaryPrimaryAnatomicSite()
		preliminaryTissueType()
		primaryOrMets(nullable:true)
		metsToAnatomicSite(nullable:true)
		preliminaryDiagnosis(nullable:true)
		preliminarySubDiagnosis(nullable:true)
		preliminaryModDiagnosis(nullable:true)
		timeReceived(nullable:true)
		timePrepared(nullable:true)
		hoursPostAutopsy(nullable:true, min: 0)
		comments(nullable:true)
		qcResult(nullable:true)
		techInitials()
		side(nullable:true)
		qcMethod()
		qcBatch(nullable:true)
		specimenCategory(nullable:true)
	}
	
	static mapping = {
		sort "specimenChtnId"
		subspecimens(sort:'subspecimenChtnId', order:'asc')
	}
	
	Integer getNumberOfSubspecimens() {
		def numberOfSubspecimens = 0
		this.subspecimens.each {
			numberOfSubspecimens++
		}
		return numberOfSubspecimens
	}
	
	String getPrepTime() {
		if(this.timeReceived && this.timePrepared) {
			use(groovy.time.TimeCategory) {
				def duration
				if(this.isOvernight){
					duration = this.timePrepared + 24.hours - this.timeReceived
					return duration.hours + " hrs " + duration.minutes + " mins "
				} else {
					duration = this.timePrepared - this.timeReceived
					return duration.hours + " hrs " + duration.minutes + " mins "
				}
			}
		}
	}

	boolean getIsOvernight(){
		return this.timePrepared < this.timeReceived
	}
	
	String setNextSpecimenNumber() {
		if(this.specimenChtnId) {
			def newSpecimenChtnId
			def specimenCriteria = Specimen.createCriteria()
			def specimenList = specimenCriteria.list {
				ilike("specimenChtnId", this.specimenChtnId[0..this.specimenChtnId.length() - 2] + "%")
			}
			/**/
			def char greatestChar // Getting the character with the highest value (assuming the last letter will be capitalized always)
			specimenList.each() { specimen ->
				def char lastChar = specimen.specimenChtnId[specimen.specimenChtnId.length() - 1]
				if(greatestChar < lastChar) {
					greatestChar = lastChar
				}
			}
			//Will use the greatestChar to see what the next character should be
			//this.specimenChtnId[this.specimenChtnId.length() - 1]
			switch(greatestChar) {
				case "Z": //Need to get what real behavior should be if this actually happens, but this seems intuitive
					newSpecimenChtnId = this.specimenChtnId[0..this.specimenChtnId.length() - 2] + "AA"
				break
				default:
					def char nextChar = greatestChar.next()
					newSpecimenChtnId = this.specimenChtnId[0..this.specimenChtnId.length() - 2] + nextChar
				break				
			}
			this.specimenChtnId = newSpecimenChtnId
		}
		else { //No specimenChtnId, so need to create it
			def String nextSpecimenNumber
			def calendar = Calendar.instance
			def year = calendar.getAt(Calendar.YEAR)
			def institutionNumber = this.procedure.procedureEventInstitution.number
			def nextNumber
			def String nextNumberString
			def specimenNumberTrackerCriteria = SpecimenNumberTracker.createCriteria()
			def specimenNumberTrackerInstance = specimenNumberTrackerCriteria.get {
				eq("year", year)
				eq("institutionNumber", institutionNumber)
			}
			if(!specimenNumberTrackerInstance) { // Entry for this institution for this year has not been created yet
				//no lastNumber, so make one in db and set to 0
				specimenNumberTrackerInstance = new SpecimenNumberTracker(year:year, institutionNumber:institutionNumber, lastNumber:0).save(failOnError:true)
			}
			nextNumber = specimenNumberTrackerInstance.lastNumber + 1
			def yearString = Integer.toString(year)
			//Start construction of the nextSpecimenNumber string - note the only last two digits of the year are needed
			nextSpecimenNumber = "M" + Integer.toString(institutionNumber) + yearString[yearString.length()-2..yearString.length()-1]
			//Depending on how long the nextNumber integer is (as a string), add the appropriate number of 0s
			switch(Integer.toString(nextNumber).length()) {
				case 1:
					nextSpecimenNumber += "000"
				break
				case 2:
					nextSpecimenNumber += "00"
				break
				case 3:
				nextSpecimenNumber += "0"
				break
				case 4:
					//Don't do anything, as it already is 4 characters long
				break
				default:
					// Shouldn't get any longer, so also don't add any by default
				break
			}
			//Finish off constructing the string with the nextNumber and A, since that is always the first specimen letter
			nextSpecimenNumber += nextNumber + "A"
			specimenNumberTrackerInstance.lastNumber = nextNumber
			//When the next number is set, it needs to be saved right away in case there are errors in this Specimen instance
			//	while the controller tries to save it to the db.  Since the specimenChtnId is already generated at this point,
			//	it won't get called again.
			specimenNumberTrackerInstance.save(flush:true) //Flush is true so the object gets persisted in the db right away
			this.specimenChtnId = nextSpecimenNumber
		}
		
	}
}
