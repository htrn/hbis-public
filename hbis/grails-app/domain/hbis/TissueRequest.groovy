package hbis

class TissueRequest {
	String tissueRequestTissueQuestId
	String projectTissueQuestId
	Integer oldTissueRequestId
	String name
	TissueRequestStatus status
	TissueRequestContactMethod contactMethod
	
	
	static belongsTo = [investigator: Investigator]
	
	String toString(){
		def toStringValue = "$tissueRequestTissueQuestId"
		if(name != null) toStringValue += ": $name"
		return toStringValue
	}

    static constraints = {
		name(nullable:true)
		status()
		tissueRequestTissueQuestId(unique:true)
		projectTissueQuestId(nullable:true)
		oldTissueRequestId(nullable:true)
		contactMethod()
    }
}
