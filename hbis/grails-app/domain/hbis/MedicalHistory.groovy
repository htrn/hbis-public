package hbis

class MedicalHistory {
	Diagnosis diagnosis
	String stage
	PrimaryMets cancer
	MedicalStatus status
	
	static belongsTo = [chartReview: ChartReview]

    static constraints = {
		cancer()
		status()
    }
}
