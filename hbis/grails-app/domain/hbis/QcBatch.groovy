package hbis

class QcBatch {
	String description
	QcBatchStatus status
	Date dateScanningComplete
	Date dateImageAnalysisComplete
	Date datePathologistConfirmationComplete
	Date dateComplete
	
	Date dateCreated
	Date lastUpdated
	
	static hasMany = [specimens: Specimen]

    static constraints = {
		description(nullable:true)
		status(nullable:true)
		dateScanningComplete(nullable:true)
		dateImageAnalysisComplete(nullable:true)
		datePathologistConfirmationComplete(nullable:true)
		dateComplete(nullable:true)
    }
	
	static mapping = {
		specimens(sort:'specimenChtnId', order:'asc')
	}
	
	Integer getNumberOfSpecimens() {
		return this.specimens.size()
	}
}
