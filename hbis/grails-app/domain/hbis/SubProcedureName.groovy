package hbis

class SubProcedureName {
	String description

	String toString() {
		description
	}
	
    static constraints = {
		
    }
	
	static mapping = {
		sort description: "asc"
	}
}
