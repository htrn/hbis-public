package hbis

class ModDiagnosis {
	String description
	
	String toString() {
		description
	}

	static constraints = {
	}
	
	static mapping = {
		sort description: "asc"
	}
}
