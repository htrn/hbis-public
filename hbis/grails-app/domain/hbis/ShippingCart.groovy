package hbis

class ShippingCart {
	ShippingCartStatus status
	String description
	String attentionTo
	String institution
	String department
	
	String streetAddress1
	String streetAddress2
	String city
	String state
	String zipCode
	String country
	String phone
	
	String eRampNumber
	String poOrRefNumber
	
	String commentsPackingSlip 	//Comments that will appear on the packing slip
	String commentsOther //Comments that won't appear on the packing slip
	
	ShippingCartQcStatus qcStatusFilter
	
	Date shipDate
	ShipMode shipMode
	String deliveryMethodAccount
	String trackingNumber
	BigDecimal shippingCost
	
	ShippingCartInstitutionsAllowed institutionsAllowed //track which institution's subspecimens will be displayed to Add
	
	Investigator investigator
	Invoice invoice
	Date dateCreated
	
	static hasMany = [subspecimens: Subspecimen, chartReviews: ChartReview]
	static transients = ['subspecimenList']

	List subspecimenList = []

    static constraints = {
		institutionsAllowed()
		attentionTo(nullable:true)
		institution(nullable:true)
		department(nullable:true)
		streetAddress1(nullable:true)
		streetAddress2(nullable:true)
		city(nullable:true)
		state(nullable:true)
		zipCode(nullable:true)
		country(nullable:true)
		phone(nullable:true)
		eRampNumber(nullable:true)
		
		shipDate(nullable:true)
		shipMode(nullable:true)
		trackingNumber(nullable:true)
		status(nullable:true)
		invoice(nullable:true)
		deliveryMethodAccount(nullable:true)
		shippingCost(nullable:true)
		poOrRefNumber(nullable:true)
		commentsPackingSlip(nullable:true)
		commentsOther(nullable:true)
    }
	
	static mapping = {
		subspecimens(sort:'specimen', order:'asc')
	}
	
	Integer getNumberOfSubspecimens() {
		def numberOfSubspecimens = 0
		this.subspecimens.each {
			numberOfSubspecimens++
		}
		return numberOfSubspecimens
	}
	
	Integer getNumberOfChartReviews() {
		def numberOfChartReviews = 0
		this.chartReviews.each {
			numberOfChartReviews++
		}
		return numberOfChartReviews
	}
	
	BigDecimal getTotalPrice() {
		def totalPrice = 0.00
		this.subspecimens.each { subspecimen ->
			if(subspecimen.price) {
				totalPrice += subspecimen.price
			}
		}
		
		this.chartReviews.each { chartReview ->
			totalPrice += chartReview.price
		}
		
		if(this.shippingCost) {
			totalPrice += this.shippingCost
		}
		
		return totalPrice
	}
}
