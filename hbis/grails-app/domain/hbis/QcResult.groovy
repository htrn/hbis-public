package hbis

class QcResult {
	String qcCode
	String qcPathologist //reviewed by
	Integer regionOfInterest
	BigDecimal regionOfInterestArea
	Integer percentNecrosis
	String linkPathReport
	String linkOverlayImage
	String linkSvsImage
	
	TissueQcMatch tissueQcMatch
	TissueType finalTissueType
	AnatomicSite finalPrimaryAnatomicSite
	Diagnosis finalDiagnosis
	SubDiagnosis finalSubDiagnosis
	ModDiagnosis finalModDiagnosis
	boolean goodForTma
	String pathologistComments
	Grade finalGrade
	Stage finalStage
	MolecularStatusEr finalMolecularStatusEr
	MolecularStatusPr finalMolecularStatusPr
	MolecularStatusHer2 finalMolecularStatusHer2
	QcFollowUpAction qcFollowUpAction
	
	Date dateCreated //check if they want to capture an actual completion date (other than insertion in the db)
	
	Specimen specimen
	//static belongsTo = [specimen: Specimen]
	
    static constraints = {
		qcCode(nullable:true)
		qcPathologist(nullable:true)
		regionOfInterest(nullable:true, min:0, max:100)
		regionOfInterestArea(nullable:true, scale:0)
		percentNecrosis(nullable:true, min:0, max:100)
		linkPathReport(nullable:true)
		linkOverlayImage(nullable:true)
		linkSvsImage(nullable:true)
		tissueQcMatch(nullable:true)
		finalPrimaryAnatomicSite(nullable:true)
		finalTissueType(nullable:true)
		finalDiagnosis(nullable:true)
		finalSubDiagnosis(nullable:true)
		finalModDiagnosis(nullable:true)
		goodForTma(nullable:true)
		pathologistComments(nullable:true)
		finalGrade(nullable:true)
		finalStage(nullable:true)
		finalMolecularStatusEr(nullable:true)
		finalMolecularStatusPr(nullable:true)
		finalMolecularStatusHer2(nullable:true)
		qcFollowUpAction(nullable:true)
    }
	
}
