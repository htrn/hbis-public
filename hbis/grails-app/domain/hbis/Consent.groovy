package hbis

class Consent {//Consent history as an entity?
	Date consentDate
	ConsentType consentType
	ConsentStatus status
	ConsentMethod method
	Date statusChangeDate
	String location
	String consenterFirstName
	String consenterLastName
	String consenterPhoneNumber
	String consenterEmail
	
	Patient patient
	static hasMany = [protocols: Protocol]

	String toString() {
		def toStringValue = "${id}" + ": " + "${consentType}"
		def protocolPrinted = false
		protocols.each { protocol ->
			if(protocolPrinted) { //Adding another protocol to the previous printed ones
				toStringValue += ", "
			}
			else { // No protocol printed yet
				toStringValue += ": "
			}
			toStringValue += protocol.toString()
			protocolPrinted = true
		}
		return toStringValue
		
	}
	
    static constraints = {
		consentType()
		status()
		method()
		statusChangeDate(nullable:true)
		location(nullable:true)
		consenterFirstName(nullable:true)
		consenterLastName(nullable:true)
		consenterPhoneNumber(nullable:true)
		consenterEmail(nullable:true)
    }
	
	static mapping = {
		//protocols(sort:'description', order:'asc') //Get error - default sort for associations[Consent->protocols] are not supported with unidirectional one to many relationships
	}

}
