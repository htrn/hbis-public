package hbis

class SpecimenNumberTracker {
	Integer year
	Integer institutionNumber
	Integer lastNumber

    static constraints = {
		year()
		institutionNumber()
		lastNumber()
    }
}
