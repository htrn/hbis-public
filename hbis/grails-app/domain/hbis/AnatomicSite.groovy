package hbis

class AnatomicSite {
	String description
	
	String toString() {
		description
	}

    static constraints = {
    }
	
	static mapping = {
		sort description: "asc"
	}
}
