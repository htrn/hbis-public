package hbis

class ChartReview {
	Integer yearOfBirth
	Integer yearOfDeath
	String height
	BigDecimal weight
	//female only
	String menstrualHistory
	Date lastMenstrualPeriod
	String currentlyPregnant
	Integer numberOfPregnancies
	Integer numberOfLiveBirths
	//SpecialDiet
	SpecialDiet specialDietStatus
	String specialDietOtherSpecify
	String specialDietNotes
	//SmokingHistory
	SubstanceUseStatus smokingHistoryStatus
	String smokingHistoryWhenQuit
	BigDecimal smokingHistoryAmountPacksPerDay
	Integer smokingHistoryDuration
	//AlcoholConsumption
	SubstanceUseStatus alcoholConsumptionStatus
	String alcoholConsumptionWhenQuit
	Integer alcoholConsumptionAmountPerWeek
	Integer alcoholConsumptionDuration
	//Recreational Drugs
	SubstanceUseStatus recreationalDrugsStatus
	String recreationalDrugsNotes
	
	String additionalNotes
	String requestVerifiedBy
	Date dateVerified
	String requestCompletedBy
	Date dateCompleted
	
	Date dateCreated

	BigDecimal price = 60.00
	
	Procedure procedure
	//May want to revisit how to define the many-to-many relationship between Chart Review and Shipping Cart
	static belongsTo = ShippingCart
	static hasMany = [shippingCarts: ShippingCart, medicalHistories: MedicalHistory, surgicalHistories: SurgicalHistory, medications: Medication, familyHistories: FamilyHistory]
	
	String toString() {
		"Chart Review: " + "${id}"
	}

	static constraints = {
		yearOfBirth(nullable:true)
		yearOfDeath(nullable:true)
		weight(nullable:true)
		lastMenstrualPeriod(nullable:true)
		numberOfPregnancies(nullable:true)
		numberOfLiveBirths(nullable:true)
		dateVerified(nullable:true)
		dateCompleted(nullable:true)
		specialDietStatus(nullable:true)
		smokingHistoryStatus(nullable:true)
		smokingHistoryAmountPacksPerDay(nullable:true)
		smokingHistoryDuration(nullable:true)
		alcoholConsumptionStatus(nullable:true)
		alcoholConsumptionAmountPerWeek(nullable:true)
		alcoholConsumptionDuration(nullable:true)
		recreationalDrugsStatus(nullable:true)
		price nullable:false, matches: "^([1-9]{1}[0-9]{0,2}(\\,[0-9]{3})*(\\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\\.[0-9]{0,2})?|0(\\.[0-9]{0,2})?|(\\.[0-9]{1,2})?)\$"
	}
	
	static mapping = {
		//shippingCarts(sort:'description', order:'asc') //Issues with many-to-many sort
		medicalHistories(sort:'diagnosis', order:'asc')
		surgicalHistories(sort:'procedure', order:'asc')
		medications(sort:'medicationName', order:'asc')
		familyHistories(sort:'relative', order:'asc')
	}

}
