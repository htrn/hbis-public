class ShiroUser {
    String username
	String firstName
	String lastName
	String email
    String passwordHash
    
    static hasMany = [ roles: ShiroRole, permissions: String ]

    static constraints = {
        username(nullable: false, blank: false, unique: true)
		firstName(nullable:true)
		lastName(nullable:true)
		email(nullable:true)
    }

    String toString(){
    	username
    }
}
