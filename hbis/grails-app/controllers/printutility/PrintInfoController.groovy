package printutility

import utilities.PrintUtility
import javax.print.PrintService

class PrintInfoController {

	

       def index() {
		PrintUtility pu=new PrintUtility();

        PrintService [] allServices=pu.getPrintServices();
        allServices.each() { print " ${it.getName()}" }; println "";
        String theList=""
        def allServicesList= allServices.collect{ it.getName()}

        allServicesList="All Services ${allServicesList}<p/>"



        PrintService defaultPrintService
        defaultPrintService = pu.getDefaultPrintService()

        allServicesList="${allServicesList} default: ${defaultPrintService.getName()}"
        [allServicesList:allServicesList]

    }

    def print(){
		PrintUtility pu=new PrintUtility();
        def printDoc=params.doc
        pu.printDocWithPrintService(printDoc,pu.defaultPrintService.getName())
        render(view:"index");
    }
}
