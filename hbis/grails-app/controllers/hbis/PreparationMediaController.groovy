package hbis

class PreparationMediaController {

	def subspecimenService

    def index() { }

	def ajaxUpdatePreparationMedia() {
		def defaultSelectionId
		def preparationType

		//Set the defaultSelectionId
		if(!(params.preparationTypeId == "null")) {
			preparationType = PreparationType.read(params.preparationTypeId)
			switch(preparationType.description) {
				case 'Fixed':
				defaultSelectionId = PreparationMedia.findByDescription("Floating in NBF")?.id
					break
				case 'Fresh':
					break
				
				case 'Frozen':
					defaultSelectionId = PreparationMedia.findByDescription("LN2 Immersed")?.id
					break
				default:
					break
			}
		}
		else { //preparation type is null
			//Should not be able to choose Preparation Media without first selecting a required Preparation Type
			render("Select a Preparation Type to see choices")
			return
		}


		def preparationMediaList = subspecimenService.getPreparationMediaList(preparationType.description)

		render(template:"/subspecimen/selectPreparationMedia", model:[preparationMediaList: preparationMediaList, defaultSelectionId: defaultSelectionId])
		return
		
	}
}
