package hbis

import org.springframework.dao.DataIntegrityViolationException
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import java.text.SimpleDateFormat

class InvoiceController {
	def invoiceService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [invoiceInstanceList: Invoice.list(params), invoiceInstanceTotal: Invoice.count()]
    }
	
	def jasperService
	def invoiceReport() {
		def invoiceCriteria = Invoice.createCriteria()
		def invoices = invoiceCriteria.list {
			eq('id', params.long('invoiceId'))
			//fetchMode("subspecimens", org.hibernate.FetchMode.JOIN)
		}
		//As per the above query, there will only be one invoice, but jasper service
		//  seems to require a collection
		def numberOfSubspecimens = 0
		def numberOfChartReviews = 0
		def totalShippingCost = 0
		def poOrRefNumber
		invoices.each {invoice ->
			params.totalPrice = invoice.getTotalPrice()
			invoice.shippingCarts.each { shippingCart ->
				shippingCart.discard()

				List subspecimenList = []

				// Remove all subspecimens with a needsChartReview property of false
				shippingCart.chartReviews.each { chartReview ->
					chartReview.procedure.specimens.each { specimen ->
						specimen.subspecimens.removeAll { !it.needsChartReview }
					}
				}

				// Remove all specimens that no longer have any subspecimens
				shippingCart.chartReviews.each { chartReview ->
					chartReview.procedure.specimens.removeAll { it.subspecimens.size() == 0 }
				}

				subspecimenList.addAll(shippingCart.subspecimens)

				subspecimenList.sort { a, b ->
					a.specimen.specimenChtnId <=> b.specimen.specimenChtnId ?: a.subspecimenChtnId <=> b.subspecimenChtnId
				}

				shippingCart.subspecimenList.addAll(subspecimenList)

				numberOfSubspecimens += shippingCart.getNumberOfSubspecimens()
				numberOfChartReviews += shippingCart.getNumberOfChartReviews()
				if(shippingCart.poOrRefNumber) {
					poOrRefNumber = shippingCart.poOrRefNumber
				}
				if(shippingCart.shippingCost) {
					totalShippingCost += shippingCart.shippingCost
				}
			}
		}
		params.numberOfSubspecimens = numberOfSubspecimens
		params.numberOfChartReviews = numberOfChartReviews
		params.totalShippingCost = totalShippingCost
		params.poOrRefNumber = poOrRefNumber
		params.SUBREPORT_DIR = "${servletContext.getRealPath('/reports')}/"
		params.CHTN_IMAGE_DIR = "${servletContext.getRealPath('/reports/images')}/"
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:invoices])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	
	def lateInvoiceReport() {
		
		def invoiceInstance = Invoice.get(params.int('invoiceId')) //Using get so lastInvoiceSentDate can be updated
		def invoiceCriteria = Invoice.createCriteria()
		def invoices = invoiceCriteria.list {
			eq('id', params.long('invoiceId'))
			//fetchMode("subspecimens", org.hibernate.FetchMode.JOIN)
		}
		//As per the above query, there will only be one invoice, but jasper service
		//  seems to require a collection
		def numberOfSubspecimens = 0
		def numberOfChartReviews = 0
		invoices.each {invoice ->
			params.totalPrice = invoice.getTotalPrice()
			params.daysLate = new Date().clearTime() - 30 - invoice.invoiceDate.clearTime()
			invoice.shippingCarts.each { shippingCart ->
				numberOfSubspecimens += shippingCart.getNumberOfSubspecimens()
				numberOfChartReviews += shippingCart.getNumberOfChartReviews()
			}
			invoice.lastInvoiceSentDate = new Date()
		}
		
		params.numberOfSubspecimens = numberOfSubspecimens
		params.numberOfChartReviews = numberOfChartReviews
		params.SUBREPORT_DIR = "${servletContext.getRealPath('/reports')}/"
		params.CHTN_IMAGE_DIR = "${servletContext.getRealPath('/reports/images')}/"
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:invoices])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}

	def paymentsReceivedReport() {
		def invoiceList = invoiceService.investigatorInvoiceQueryHelper(params.fromDate, params.toDate, params.long("investigatorId"))

		def sumTotalPrice = 0.00
		def sumAmountPaid = 0.00

		invoiceList.each { invoice ->
			if(invoice.amountPaid){
				BigDecimal amountPaid = new BigDecimal(invoice.amountPaid)
				sumAmountPaid += amountPaid
			}

			sumTotalPrice += invoice.totalPrice
		}

		invoiceList.sort new OrderBy([
			{ it.investigator.lastName },
			{ it.invoiceDate }
		])

		def sdf = new SimpleDateFormat("MM/dd/yyyy")

		params.invoiceDateFrom = params.fromDate ? sdf.parse(params.fromDate) : sdf.parse('01/01/1970')
		params.invoiceDateTo = params.toDate ? sdf.parse(params.toDate) : new Date()
		params.sumTotalPrice = sumTotalPrice
		params.sumAmountPaid = sumAmountPaid
		params.investigatorName = params.investigatorId ? invoiceList[0].investigator.toString() : "All Investigators"

		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:invoiceList])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
	}
	
	def outstandingPaymentsReport() {
		def sdf = new SimpleDateFormat("MM/dd/yyyy")
		def fromDate
		def toDate
		if(params.fromDate) {
			fromDate = sdf.parse(params.fromDate)
		}
		
		if(params.toDate) {
			toDate = sdf.parse(params.toDate)
		}
		
		def invoiceCriteria = Invoice.createCriteria()
		def invoiceInstanceList
		
		if(fromDate || toDate) {
			invoiceInstanceList = invoiceCriteria.list {
				eq("billingStatus", InvoiceBillingStatus.findByDescription("Invoice Sent"))
				or {
					not {eq("paymentStatus", InvoicePaymentStatus.findByDescription("Full Payment Received"))}
					isNull("paymentStatus")
				}

				if(fromDate && !toDate) {
					ge("invoiceDate", fromDate.clearTime())
				}
				else if (!fromDate && toDate) {
					le("invoiceDate", toDate.clearTime())
				}
				else { //Both fromDate and toDate are provided
					between("invoiceDate", fromDate.clearTime(), toDate.clearTime())
				}
				order("invoiceDate", "asc")
			}
		}
		
		params.invoiceDateFrom = fromDate
		params.invoiceDateTo = toDate
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:invoiceInstanceList])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	
	def viewPending(Integer max) {
		def invoiceInstanceList
		def invoiceCriteria = Invoice.createCriteria()
		invoiceInstanceList = invoiceCriteria.list() {
			not{eq("billingStatus", InvoiceBillingStatus.findByDescription("Invoice Sent"))}
		}
		[invoiceInstanceList: invoiceInstanceList]
	}

    def create() {
		def investigatorCriteria = Investigator.createCriteria()
		def investigators = investigatorCriteria.list {
			not {
				eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("E"))
			}
			order("lastName", "asc")
			order("firstName", "asc")
			order("chtnIdPrefix", "asc")
			order("chtnId", "asc")
		}
		
		def shippingCartCriteria = ShippingCart.createCriteria()
		
		//Making list of not just shipped carts, but some other Investigator filtering as well
		def availableShippingCarts = shippingCartCriteria.list {
			//eq("investigator", investigator)
			isNull("invoice")
			eq("status", ShippingCartStatus.findByDescription("Shipped"))
			// Do not display shipping carts for E investigators
			investigator {
				not { eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("E")) }
			}
			order("id", "asc")
		}
        [invoiceInstance: new Invoice(params), investigators: investigators, availableShippingCarts: availableShippingCarts]
    }

    def save() {
        def invoiceInstance = new Invoice(params)
        if (!invoiceInstance.save(flush: true)) {
            render(view: "create", model: [invoiceInstance: invoiceInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'invoice.label', default: 'Invoice'), invoiceInstance.id])
        redirect(action: "show", id: invoiceInstance.id)
    }
	
	def saveAndAddShippingCarts() {
		if(params.investigator.id == "null") {
			flash.message = "Please choose an Investigator before clicking Proceed to Add Shipping Carts"
			redirect(action:"create")
			return
		}
		def investigator = Investigator.read(params.int('investigator.id'))
		def availableShippingCarts
		def shippingCartCriteria = ShippingCart.createCriteria()
		
		availableShippingCarts = shippingCartCriteria.list {
			eq("investigator", investigator)
			isNull("invoice")
			eq("status", ShippingCartStatus.findByDescription("Shipped"))
			//If Investigator is a C, then check if subspecimens are OSU
			if(investigator.chtnIdPrefix == InvestigatorChtnIdPrefix.findByPrefix("C")) {
				eq("institutionsAllowed", ShippingCartInstitutionsAllowed.findByDescription("Non-OSU only"))
			}
		}
		if(!availableShippingCarts) {
			flash.message = "The selected investigator does not have any associated Shipping Carts available to add to the Invoice.  Please choose a different Investigator."
			redirect(action: "create", params:[investigatorId: params.int('investigator.id')])
			return
		}
		
		def invoiceInstance = new Invoice(params)
		invoiceInstance.billingStatus = InvoiceBillingStatus.findByDescription("Invoice Not Sent")
		if (!invoiceInstance.save(flush: true)) {
			render(view: "create", model: [invoiceInstance: invoiceInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'invoice.label', default: 'Invoice'), invoiceInstance.id])
		redirect(action: "viewShippingCartsToAdd", id: invoiceInstance.id)
	}

    def show(Long id) {
        def invoiceInstance = Invoice.get(id)
        if (!invoiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
            redirect(action: "list")
            return
        }

        [invoiceInstance: invoiceInstance]
    }

    def edit(Long id) {
        def invoiceInstance = Invoice.get(id)
        if (!invoiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
            redirect(action: "list")
            return
        }

        [invoiceInstance: invoiceInstance]
    }

    def update(Long id, Long version) {
        def invoiceInstance = Invoice.get(id)
        if (!invoiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (invoiceInstance.version > version) {
                invoiceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'invoice.label', default: 'Invoice')] as Object[],
                          "Another user has updated this Invoice while you were editing")
				if(params.view == "accountsReceivable") {
					render(view:"accountsReceivable", model: [invoiceInstance: invoiceInstance,
																invoiceId: params.long('invoiceId'), 
																investigatorId: params.long('investigatorId')])
					return
				}
				else if(params.view == "confirm") {
					render(view:"confirm", model: [invoiceInstance: invoiceInstance, poOrRefNumber: params.poOrRefNumber])
					return
				}
                render(view: "edit", model: [invoiceInstance: invoiceInstance])
                return
            }
        }

        invoiceInstance.properties = params
		invoiceInstance.clearErrors() //Need to clear errors here so 'invoiceDate must be a valid Date' does not get returned after date is properly set below.
		
		if(params.view == "confirm") {
			if(params.invoiceDate) {
				def sdf = new SimpleDateFormat("MM/dd/yyyy")
				invoiceInstance.invoiceDate = sdf.parse(params.invoiceDate)
				invoiceInstance.lastInvoiceSentDate = invoiceInstance.invoiceDate
			}
			else { //Invoice Date is not supplied while in confirm view, so need to throw an error
				invoiceInstance.errors.rejectValue("invoiceDate", "default.invoiceDate.required",
					[message(code: 'invoice.label', default: 'Invoice')] as Object[],
					"Field invoiceDate is required.")
				render(view:"confirm", model: [invoiceInstance: invoiceInstance, poOrRefNumber: params.poOrRefNumber])
				return
			}
			if(params.invoiceBillingStatus == "Invoice Sent") {
				invoiceInstance.billingStatus = InvoiceBillingStatus.findByDescription("Invoice Sent")
				invoiceInstance.paymentStatus = InvoicePaymentStatus.findByDescription("Not Received")
			}
		}
		else if (params.view == "accountsReceivable") {
			if (invoiceInstance.amountPaid) {
				if(invoiceInstance.amountPaid < invoiceInstance.getTotalPrice()) {
					invoiceInstance.paymentStatus = InvoicePaymentStatus.findByDescription("Partial Payment Received")
				}
				else if (invoiceInstance.amountPaid >= invoiceInstance.getTotalPrice()) {
					invoiceInstance.paymentStatus = InvoicePaymentStatus.findByDescription("Full Payment Received")
				}
			}
		}

        if (!invoiceInstance.save(flush: true)) {
			if(params.view == "accountsReceivable") {
				render(view:"accountsReceivable", model: [invoiceInstance: invoiceInstance,
																invoiceId: params.long('invoiceId'), 
																investigatorId: params.long('investigatorId')])
				return
			}
			else if(params.view == "confirm") {
				render(view:"confirm", model: [invoiceInstance: invoiceInstance, poOrRefNumber: params.poOrRefNumber])
				return
			}
            render(view: "edit", model: [invoiceInstance: invoiceInstance])
            return
        }
		
		if(params.poOrRefNumber) {
			invoiceInstance.shippingCarts.each { shippingCart ->
				shippingCart.poOrRefNumber = params.poOrRefNumber
			}
		}

        flash.message = message(code: 'default.updated.message', args: [message(code: 'invoice.label', default: 'Invoice'), invoiceInstance.id])
		if(params.view == "accountsReceivable") {
			redirect(action:"accountsReceivable", params: [invoiceInstance: invoiceInstance,
														invoiceId: params.long('invoiceId'),
														investigatorId: params.long('investigatorId')])
			return
		}
		else if(params.view == "confirm") {
			redirect(action: "show", id: invoiceInstance.id)
			return
		}
		redirect(action: "show", id: invoiceInstance.id)
    }

    def delete(Long id) {
        def invoiceInstance = Invoice.get(id)
        if (!invoiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
            redirect(action: "list")
            return
        }

        try {
            invoiceInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def viewShippingCartsToAdd(Long id) {
		def invoiceInstance = Invoice.get(id)
		def availableShippingCarts
		def shippingCartCriteria = ShippingCart.createCriteria()
		
		//availableShippingCarts = ShippingCart.findAllByInvestigatorAndStatusAndInvoiceIsNull(invoiceInstance.investigator, ShippingCartStatus.findByDescription("Shipped"))
		availableShippingCarts = shippingCartCriteria.list {
			eq("investigator", invoiceInstance.investigator)
			isNull("invoice")
			eq("status", ShippingCartStatus.findByDescription("Shipped"))
			//If Investigator is a C, then check if subspecimens are OSU
			if(invoiceInstance.investigator.chtnIdPrefix == InvestigatorChtnIdPrefix.findByPrefix("C")) {
				eq("institutionsAllowed", ShippingCartInstitutionsAllowed.findByDescription("Non-OSU only"))
			}
		}
		[invoiceInstance: invoiceInstance, availableShippingCarts: availableShippingCarts]
	}
	
	def addRemoveShippingCart() {
		def invoiceInstance = Invoice.get(params.long('invoiceId'))
		def shippingCartInstance = ShippingCart.get(params.long('shippingCartId'))
		
		def invoiceVersion = params.long('invoiceVersion')
		def shippingCartVersion = params.long('shippingCartVersion')
		def hasError = false
		
		if (invoiceVersion != null) {
			if (invoiceInstance.version > invoiceVersion) {
				invoiceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'invoice.label', default: 'Invoice')] as Object[],
						  "Another user has updated this Invoice while you were editing")
				hasError = true
			}
		}
		
		if (shippingCartVersion != null) {
			if (shippingCartInstance.version > shippingCartVersion) {
				shippingCartInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'shippingCart.label', default: 'Shipping Cart')] as Object[],
						  "Another user has updated this Shipping Cart while you were editing")
				hasError = true
			}
		}
		
		if(hasError) {
			Integer max
			params.max = Math.min(max ?: 10, 100)
			render(view: "list", model: [invoiceInstance: invoiceInstance,
											shippingCartInstance: shippingCartInstance,
											invoiceInstanceList: Invoice.list(params),
											invoiceInstanceTotal: Invoice.count()])
			return
		}
		if (params.addRemove == 'Add') {
			invoiceInstance.addToShippingCarts(shippingCartInstance)
		}
		else if (params.addRemove == 'Remove') {
			invoiceInstance.removeFromShippingCarts(shippingCartInstance)
		}
		
		invoiceInstance.save(flush:true)
		
		redirect(action: "viewShippingCartsToAdd", id: invoiceInstance.id)
	}
	
	def confirm(Long id) {
		def invoiceInstance = Invoice.get(id)
		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "list")
			return
		}
		
		//Make sure there is at least 1 ShippingCart when going to this view, otherwise return to pendingInvoices with message
		if(invoiceInstance.shippingCarts.size() < 1) {
			flash.message = "There needs to be at least one Shipping Cart in the Invoice before proceeding to confirmation."
			redirect(action:"viewPending")
			return
		}
		
		//Get poOrReferenceNumber from first ShippingCart now that we know there is at least 1 ShippingCart
		def firstShippingCart = invoiceInstance.shippingCarts.first()
		def poOrRefNumber = firstShippingCart.poOrRefNumber

		[invoiceInstance: invoiceInstance, poOrRefNumber: poOrRefNumber]
	}
	
	def accountsReceivable() {
		def invoiceId = params.long('invoiceId')
		def investigatorId = params.long('investigatorId')
		def institution = params.institution
		
		def invoiceCriteria = Invoice.createCriteria()
		def invoiceInstanceList 
		
		if(invoiceId || investigatorId || institution) {
			invoiceInstanceList= invoiceCriteria.list {
				eq("billingStatus", InvoiceBillingStatus.findByDescription("Invoice Sent"))
				not { eq("paymentStatus", InvoicePaymentStatus.findByDescription("Full Payment Received")) }
				if(invoiceId) {
					eq("id", invoiceId)
				}
				if(investigatorId) {
					eq("investigator", Investigator.read(investigatorId))
				}
				if(institution) {
					ilike("institution", "%" + institution + "%")
				}
			}
		}
		invoiceInstanceList?.removeAll{
			it.getTotalPrice() == 0.00
		}
		[invoiceInstanceList: invoiceInstanceList, invoiceId: invoiceId, investigatorId: investigatorId, institution: institution]
		
	}
	
	def latePayment() {
		def investigatorId, daysSinceLastInvoiceSent
		if(params.long('investigatorId')) {
			investigatorId = params.long('investigatorId')
		}
		
		if(params.int('daysSinceLastInvoiceSent') >= 0) {
			daysSinceLastInvoiceSent = params.int('daysSinceLastInvoiceSent')
		}
		
		def invoiceCriteria = Invoice.createCriteria()
		def invoiceInstanceList
		if(investigatorId || (daysSinceLastInvoiceSent != null && daysSinceLastInvoiceSent >= 0) ) {
			invoiceInstanceList = invoiceCriteria.list {
				eq("billingStatus", InvoiceBillingStatus.findByDescription("Invoice Sent"))
				or {
					not {eq("paymentStatus", InvoicePaymentStatus.findByDescription("Full Payment Received"))}
					isNull("paymentStatus")
				}
				if(investigatorId) {
					eq("investigator", Investigator.read(investigatorId))
				}
				if(daysSinceLastInvoiceSent) {
					def dateToCheck = new Date().clearTime() - daysSinceLastInvoiceSent
					le("lastInvoiceSentDate", dateToCheck)
				}
				order("invoiceDate", "asc")
			}
		}
		
		[invoiceInstanceList: invoiceInstanceList, daysSinceLastInvoiceSent: daysSinceLastInvoiceSent, investigatorId: investigatorId]
	}
	
	def outstandingPayments() {
		def sdf = new SimpleDateFormat("MM/dd/yyyy")
		def fromDate
		def toDate
		if(params.fromDate) {
			fromDate = sdf.parse(params.fromDate)
		}
		
		if(params.toDate) {
			toDate = sdf.parse(params.toDate)
		}
		
		def invoiceCriteria = Invoice.createCriteria()
		def invoiceInstanceList
		
		if(fromDate || toDate) {
			invoiceInstanceList = invoiceCriteria.list {
				eq("billingStatus", InvoiceBillingStatus.findByDescription("Invoice Sent"))
				or {
					not {eq("paymentStatus", InvoicePaymentStatus.findByDescription("Full Payment Received"))}
					isNull("paymentStatus")
				}

				if(fromDate && !toDate) {
					ge("invoiceDate", fromDate.clearTime())
				}
				else if (!fromDate && toDate) {
					le("invoiceDate", toDate.clearTime())
				}
				else { //Both fromDate and toDate are provided
					between("invoiceDate", fromDate.clearTime(), toDate.clearTime())
				}
				order("invoiceDate", "asc")
			}
		}
		[invoiceInstanceList: invoiceInstanceList, fromDateTextField: params.fromDate, toDateTextField: params.toDate]
	}

	def investigatorInvoices() {
		if(params.fromDate || params.toDate || params.investigatorId){
			def invoiceList = invoiceService.investigatorInvoiceQueryHelper(params.fromDate, params.toDate, params.long("investigatorId"))

			def sumTotalPrice = 0.00
			def sumAmountPaid = 0.00

			invoiceList.each { invoice ->
				if(invoice.amountPaid){
					BigDecimal amountPaid = new BigDecimal(invoice.amountPaid)
					sumAmountPaid += amountPaid
				}

				sumTotalPrice += invoice.totalPrice
			}

			invoiceList.sort new OrderBy([
				{ it.investigator.lastName },
				{ it.invoiceDate }
			])

			[
				invoiceList: invoiceList,
				investigatorId: params.long("investigatorId"),
				fromDateTextField: params.fromDate,
				toDateTextField: params.toDate,
				sumTotalPrice: sumTotalPrice,
				sumAmountPaid: sumAmountPaid
			]
		}else{
			render view: 'investigatorInvoices'
		}
	}	
}
