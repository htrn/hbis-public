package hbis

import java.util.Date;
import java.text.SimpleDateFormat
import org.springframework.dao.DataIntegrityViolationException

class SpecimenController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [specimenInstanceList: Specimen.list(params), specimenInstanceTotal: Specimen.count()]
    }
	
	def specimensProcuredToday() {
		Date today = new Date().clearTime()
		def specimenCriteria = Specimen.createCriteria()
		//def specimenInstanceList = Specimen.findAllByDateCreatedGreaterThanEquals(today, [sort:"specimenChtnId"])
		def specimenInstanceList = specimenCriteria.list {
			ge("dateCreated", today)
			procedure {
				eq("procedureEventInstitution", Institution.findByDescription("OSU"))
			}
			order("specimenChtnId", "asc")
		}
		[specimenInstanceList: specimenInstanceList]
	}
	
	def specimenSearch() {
		
	}
	
	def performSearchChtnId() {
		def specimenList = Specimen.findAllBySpecimenChtnIdLike("%" + params.chtnId + "%", [sort:'specimenChtnId', order:'asc'])
		render(template:'searchChtnIdResult', collection:specimenList, var:'specimenInstance')
	}

    def create() {
		def specimenInstance = new Specimen(params)
		
		//Removing the default value for primaryOrMets, but in case they want it reverted back the old line is below
		//specimenInstance.primaryOrMets = PrimaryMets.findByDescription("Primary")
		specimenInstance.side = Side.findByDescription("Not Specified")
		specimenInstance.specimenCategory = SpecimenCategory.findByDescription("TISSUE")
		specimenInstance.procurementDate = specimenInstance.procedure.procedureDate
		
		if(specimenInstance.procedure.procedureType.description == "Autopsy") {
			//specimenInstance.alternateSpecimenID = "Generate Alternate ID"
			//Should an alternate ID be generated?
		}
		
        [specimenInstance: specimenInstance, procedureInstance:specimenInstance.procedure, patientInstance:specimenInstance.procedure.patient]
    }
	
	def createCopyDetails(Long id) {
		def incomingSpecimen = Specimen.read(id)
		def newSpecimen = new Specimen()
		newSpecimen.procedure = incomingSpecimen.procedure
		newSpecimen.specimenChtnId = incomingSpecimen.specimenChtnId
		//newSpecimen.preliminaryPrimaryAnatomicSite = incomingSpecimen.preliminaryPrimaryAnatomicSite
		//newSpecimen.preliminaryTissueType = incomingSpecimen.preliminaryTissueType
		newSpecimen.preliminaryDiagnosis = incomingSpecimen.preliminaryDiagnosis
		newSpecimen.procurementDate = incomingSpecimen.procurementDate
		newSpecimen.timeReceived = incomingSpecimen.timeReceived
		newSpecimen.timePrepared = incomingSpecimen.timePrepared
		newSpecimen.primaryOrMets = incomingSpecimen.primaryOrMets
		newSpecimen.metsToAnatomicSite = incomingSpecimen.metsToAnatomicSite
		newSpecimen.preliminarySubDiagnosis = incomingSpecimen.preliminarySubDiagnosis
		newSpecimen.preliminaryModDiagnosis = incomingSpecimen.preliminaryModDiagnosis
		newSpecimen.hoursPostAutopsy = incomingSpecimen.hoursPostAutopsy
		newSpecimen.comments = incomingSpecimen.comments
		newSpecimen.techInitials = incomingSpecimen.techInitials
		newSpecimen.side = incomingSpecimen.side
		newSpecimen.qcMethod = incomingSpecimen.qcMethod
		
		newSpecimen.preliminaryPrimaryAnatomicSite = null
		newSpecimen.preliminaryTissueType = null
		
		render(view: "create", model:[specimenInstance: newSpecimen, 
										procedureInstance: newSpecimen.procedure, 
										patientInstance: newSpecimen.procedure.patient,
										setNextSpecimenNumber: "true"])
		return
	}
	
	def copySpecimenAndSubspecimens(Long id) {
		def incomingSpecimen = Specimen.read(id)
		def newSpecimen = new Specimen()
		newSpecimen.procedure = incomingSpecimen.procedure
		
		newSpecimen.specimenChtnId = incomingSpecimen.specimenChtnId
		newSpecimen.setNextSpecimenNumber()
		
		newSpecimen.preliminaryPrimaryAnatomicSite = incomingSpecimen.preliminaryPrimaryAnatomicSite
		newSpecimen.preliminaryTissueType = incomingSpecimen.preliminaryTissueType
		newSpecimen.preliminaryDiagnosis = incomingSpecimen.preliminaryDiagnosis
		newSpecimen.procurementDate = incomingSpecimen.procurementDate
		newSpecimen.timeReceived = incomingSpecimen.timeReceived
		newSpecimen.timePrepared = incomingSpecimen.timePrepared
		newSpecimen.primaryOrMets = incomingSpecimen.primaryOrMets
		newSpecimen.metsToAnatomicSite = incomingSpecimen.metsToAnatomicSite
		newSpecimen.preliminarySubDiagnosis = incomingSpecimen.preliminarySubDiagnosis
		newSpecimen.preliminaryModDiagnosis = incomingSpecimen.preliminaryModDiagnosis
		newSpecimen.hoursPostAutopsy = incomingSpecimen.hoursPostAutopsy
		newSpecimen.comments = incomingSpecimen.comments
		newSpecimen.techInitials = incomingSpecimen.techInitials
		newSpecimen.side = incomingSpecimen.side
		newSpecimen.qcMethod = incomingSpecimen.qcMethod
		
		newSpecimen.save()
		//tempSubspecimens.addAll(incomingSpecimen.subspecimens)
		incomingSpecimen.subspecimens.each { subspecimen ->
			def temp = new Subspecimen()
			//temp.properties = subspecimen.properties 
			temp.subspecimenChtnId = subspecimen.subspecimenChtnId
			if(subspecimen.status.description == "Shipped") {
				temp.status = SubspecimenStatus.findByDescription("Assigned")
			}
			else {
				temp.status = subspecimen.status
			}
			temp.preparationType = subspecimen.preparationType
			temp.preparationMedia = subspecimen.preparationMedia
			//temp.weight = subspecimen.weight
			temp.weightUnit = subspecimen.weightUnit
			//temp.volume = subspecimen.volume
			temp.volumeUnit = subspecimen.volumeUnit
			//temp.sizeWidth = subspecimen.sizeWidth
			//temp.sizeLength = subspecimen.sizeLength
			//temp.sizeHeight = subspecimen.sizeHeight
			temp.sizeUnit = subspecimen.sizeUnit
			temp.labelComment = subspecimen.labelComment
			temp.comments = subspecimen.comments
			temp.price = subspecimen.price
			temp.datePriceUpdated = subspecimen.datePriceUpdated
			temp.needsChartReview = subspecimen.needsChartReview
			temp.investigator = subspecimen.investigator
			temp.tissueRequest = subspecimen.tissueRequest
			
			newSpecimen.addToSubspecimens(temp)
		}
		newSpecimen.save(flush:true)
		newSpecimen.preliminaryPrimaryAnatomicSite = null
		newSpecimen.preliminaryTissueType = null
		
		//incomingSpecimen.primaryAnatomicSite.
		flash.message = "Please check the Preliminary Primary Anatomic Site and Preliminary Tissue Type before proceeding."
		render(view:"edit", model:[specimenInstance: newSpecimen, 
									procedureInstance: newSpecimen.procedure, 
									patientInstance: newSpecimen.procedure.patient,
									view: params.view,
									setNextSpecimenNumber: "false"])
	}

    def save() {
        def specimenInstance = new Specimen(params)
		specimenInstance.clearErrors()
		if(params.timeReceived) {
			def error = false
			if(!(params.timeReceived.length() == 4)) { //TimeReceived needs to have a length of 4
				error = true
			}
			else {
				def sdf = new SimpleDateFormat("HHmm")
				try {
					specimenInstance.timeReceived = sdf.parse(params.timeReceived)
				} catch (Exception e) {
					error = true
				}
			}
			if(error) {
				specimenInstance.errors.rejectValue("timeReceived", "validation.specimen.timeReceived.unparsable",
					[message(code: 'specimen.label', default: 'Specimen')] as Object[],
					"Time Received must be in HHmm format (example: 1452)")
				render(view: "create", model: [specimenInstance: specimenInstance, 
											procedureInstance:specimenInstance.procedure, 
											patientInstance:specimenInstance.procedure.patient,
											setNextSpecimenNumber: params.setNextSpecimenNumber])
				return
			}
		}
		if(params.timePrepared) {
			def error = false
			if(!(params.timePrepared.length() == 4)) { // Time Prepared must have a length of 4
				error = true
			}
			else {
				def sdf = new SimpleDateFormat("HHmm")
				try {
					specimenInstance.timePrepared = sdf.parse(params.timePrepared)
				} catch (Exception e) {
					error = true
				}
			}
			if(error) {
				specimenInstance.errors.rejectValue("timePrepared", "validation.specimen.timePrepared.unparsable",
					[message(code: 'specimen.label', default: 'Specimen')] as Object[],
					"Time Prepared must be in HHmm format (example: 1452)")
				render(view: "create", model: [specimenInstance: specimenInstance,
											procedureInstance:specimenInstance.procedure,
											patientInstance:specimenInstance.procedure.patient,
											setNextSpecimenNumber: params.setNextSpecimenNumber])
				return
			}

			if(params.int('timeReceived') > params.int('timePrepared') && params.isOvernight == null ){
				specimenInstance.discard()
				specimenInstance.errors.rejectValue("timePrepared", "validation.specimen.timePrepared.earlyThanReceived",
						[message(code: 'specimen.label', default: 'Specimen')] as Object[],
						'"Time Prepared" must be later than "Time Received". Please click "Overnight" checkbox if it is a overnight procurement scenario.')
				return render(view: "edit", model: [specimenInstance: specimenInstance,
													procedureInstance:specimenInstance.procedure,
													patientInstance:specimenInstance.procedure.patient,
													setNextSpecimenNumber: params.setNextSpecimenNumber, previousCheckedStatus: false])

			}
		}
		
		if(params.setNextSpecimenNumber == "true") {
			specimenInstance.setNextSpecimenNumber()
		}
		
        if (!specimenInstance.save(flush: true)) {
            render(view: "create", model: [specimenInstance: specimenInstance, 
											procedureInstance:specimenInstance.procedure, 
											patientInstance:specimenInstance.procedure.patient,
											setNextSpecimenNumber: "false"])
            return
        }
		
		specimenInstance.procedure.procurementResult = ProcurementResult.findByDescription("Procured")
		
		if(specimenInstance.qcMethod.description == "Frozen Section") {
			if(!specimenInstance.qcResult) {
				QcResult qcResult = new QcResult()
				qcResult.specimen = specimenInstance
				qcResult.tissueQcMatch = TissueQcMatch.findByDescription("Pass")
				qcResult.finalTissueType = specimenInstance.preliminaryTissueType
				qcResult.finalPrimaryAnatomicSite = specimenInstance.preliminaryPrimaryAnatomicSite
				qcResult.finalDiagnosis = specimenInstance.preliminaryDiagnosis
				qcResult.finalSubDiagnosis = specimenInstance.preliminarySubDiagnosis
				qcResult.finalModDiagnosis = specimenInstance.preliminaryModDiagnosis
				if(!qcResult.save(flush:true)) {
					println("Saving qcResult failed in Specimen controller, save action")
				}
				else {
				}
			}
		}

		flash.message = message(code: 'specimen.created.message', args: [message(code: 'specimen.label', default: 'Specimen'), specimenInstance.id, specimenInstance.specimenChtnId])
		if(params.saveAndCreateSubspecimen) {
			redirect(controller:"subspecimen", action: "create", params: ['specimen.id':specimenInstance.id])
			return
		}
        redirect(action: "show", id: specimenInstance.id)
    }
	
    def show(Long id) {
        def specimenInstance = Specimen.get(id)
        if (!specimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'specimen.label', default: 'Specimen'), id])
            redirect(action: "list")
            return
        }
		def procedureInstance = specimenInstance.procedure
		def patientInstance = procedureInstance.patient

        [specimenInstance: specimenInstance, procedureInstance:procedureInstance, patientInstance:patientInstance]
    }

    def edit(Long id) {
        def specimenInstance = Specimen.get(id)
        if (!specimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'specimen.label', default: 'Specimen'), id])
            redirect(action: "list")
            return
        }
		def procedureInstance = specimenInstance.procedure
		def patientInstance = procedureInstance.patient

        [specimenInstance: specimenInstance, procedureInstance:procedureInstance, patientInstance:patientInstance, view: params.view]
    }

    def update(Long id, Long version) {
        def specimenInstance = Specimen.get(id)
        if (!specimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'specimen.label', default: 'Specimen'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (specimenInstance.version > version) {
                specimenInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'specimen.label', default: 'Specimen')] as Object[],
                          "Another user has updated this Specimen while you were editing")
				def procedureInstance = specimenInstance.procedure
				def patientInstance = procedureInstance.patient
                render(view: "edit", model: [specimenInstance: specimenInstance, 
												procedureInstance:procedureInstance, 
												patientInstance:patientInstance,
												view: params.view])
                return
            }
        }
		
		//Handle qcMethod - check if it is going to change and make sure it should be allowed
		//	Also handle associated qcResult (if it exists) depending on what new value is
		//	Need to see what the new qcMethod is going to be prior to binding it to the specimenInstance
		if(params.qcMethod.id) {
			def newQcMethod = QcMethod.read(params.long('qcMethod.id'))
			def error = false //Keep track if there is an error here, then return to edit if true
			//In form view, already keeping track if already in a batch, 
			//	but still checking that here just in case it is possible
			// Main use cases here are if going to frozen section from no qc, or vice versa
			
			if(specimenInstance.qcMethod.description == "Frozen Section" && newQcMethod.description != "Frozen Section") {
				if(specimenInstance.qcResult) {
					def qcResult = specimenInstance.qcResult
					specimenInstance.qcResult = null
					specimenInstance.save(flush: true)
					qcResult.delete(flush: true)
				}
			}
			else if (specimenInstance.qcMethod.description != "Frozen Section" && newQcMethod.description == "Frozen Section"){
				if(!specimenInstance.qcResult) {
					QcResult qcResult = new QcResult()
					qcResult.specimen = specimenInstance
					qcResult.tissueQcMatch = TissueQcMatch.findByDescription("Pass")
					qcResult.finalTissueType = specimenInstance.preliminaryTissueType
					qcResult.finalPrimaryAnatomicSite = specimenInstance.preliminaryPrimaryAnatomicSite
					qcResult.finalDiagnosis = specimenInstance.preliminaryDiagnosis
					qcResult.finalSubDiagnosis = specimenInstance.preliminarySubDiagnosis
					qcResult.finalModDiagnosis = specimenInstance.preliminaryModDiagnosis
					if(!qcResult.save(flush:true)) {
						println("Saving qcResult failed in Specimen controller, update action")
					}
					else {
					}
				}
			}
			else if(specimenInstance.qcMethod.description == "Pathologist Review" && newQcMethod.description != "Pathologist Review") {
				if(specimenInstance.qcBatch) { //If already in a batch, then do not let it be changed
					specimenInstance.errors.rejectValue("qcMethod", "validation.specimen.qcMethod.illegalChange",
						[message(code: 'specimen.label', default: 'Specimen')] as Object[],
						"QC Method cannot be changed from 'Pathologist Review' if it is already in a QC Batch")
					render(view: "edit", model: [specimenInstance: specimenInstance,
					procedureInstance:specimenInstance.procedure,
					patientInstance:specimenInstance.procedure.patient,
					setNextSpecimenNumber: params.setNextSpecimenNumber])
				return
					
				}
			}
			
		}
		
		specimenInstance.properties = params
		specimenInstance.clearErrors()
		
		if(params.timeReceived) {
			def error = false
			if(!(params.timeReceived.length() == 4)) { //TimeReceived needs to have a length of 4
				error = true
			}
			else {
				def sdf = new SimpleDateFormat("HHmm")
				try {
					specimenInstance.timeReceived = sdf.parse(params.timeReceived)
				} catch (Exception e) {
					error = true
				}
			}
			if(error) {
				specimenInstance.errors.rejectValue("timeReceived", "validation.specimen.timeReceived.unparsable",
					[message(code: 'specimen.label', default: 'Specimen')] as Object[],
					"Time Received must be in HHmm format (example: 1452)")
				render(view: "edit", model: [specimenInstance: specimenInstance, 
											procedureInstance:specimenInstance.procedure, 
											patientInstance:specimenInstance.procedure.patient,
											setNextSpecimenNumber: params.setNextSpecimenNumber])
				return
			}
		}
		if(params.timePrepared) {
			def error = false
			if(!(params.timePrepared.length() == 4)) { // Time Prepared must have a length of 4
				error = true
			}
			else {
				def sdf = new SimpleDateFormat("HHmm")
				try {
					specimenInstance.timePrepared = sdf.parse(params.timePrepared)
				} catch (Exception e) {
					error = true
				}
			}
			if(error) {
				specimenInstance.errors.rejectValue("timePrepared", "validation.specimen.timePrepared.unparsable",
					[message(code: 'specimen.label', default: 'Specimen')] as Object[],
					"Time Prepared must be in HHmm format (example: 1452)")
				render(view: "edit", model: [specimenInstance: specimenInstance,
											procedureInstance:specimenInstance.procedure,
											patientInstance:specimenInstance.procedure.patient,
											setNextSpecimenNumber: params.setNextSpecimenNumber])
				return
			}

			if(params.int('timeReceived') > params.int('timePrepared') && params.isOvernight == null ){
				specimenInstance.discard()
				specimenInstance.errors.rejectValue("timePrepared", "validation.specimen.timePrepared.earlyThanReceived",
						[message(code: 'specimen.label', default: 'Specimen')] as Object[],
						'"Time Prepared" must be later than "Time Received". Please click "Overnight" checkbox if it is a overnight procurement scenario.')
				return render(view: "edit", model: [specimenInstance: specimenInstance,
											 procedureInstance:specimenInstance.procedure,
											 patientInstance:specimenInstance.procedure.patient,
											 setNextSpecimenNumber: params.setNextSpecimenNumber, previousCheckedStatus: false])

			}
		}
		
		//Handle primaryOrMets - if it is no longer Mets, then associated metsToAnatomicSite should be cleared
		if(params.primaryOrMets.id == "null") {
			specimenInstance.metsToAnatomicSite = null
		}
		else if (params.primaryOrMets.id) {
			def primaryMetsValue = PrimaryMets.read(params.primaryOrMets.id)
			if(primaryMetsValue.description == "Primary") {
				specimenInstance.metsToAnatomicSite = null
			}
		}
		

        if (!specimenInstance.save(flush: true)) {
			def procedureInstance = specimenInstance.procedure
			def patientInstance = procedureInstance.patient
            render(view: "edit", model: [specimenInstance: specimenInstance, 
											procedureInstance:procedureInstance, 
											patientInstance:patientInstance,
											view: params.view])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'specimen.label', default: 'Specimen'), specimenInstance.id])
		if(params.view == "imageAnalysis") {
			redirect(controller:"qcBatch", action:"viewBatchForImageAnalysis", id:params.qcBatchId)
			return
		}
		else if(params.view == "createSubspecimen") {
			redirect(controller:"subspecimen", action:"create", params:['specimen.id': specimenInstance.id])
			return
		}
        redirect(action: "show", id: specimenInstance.id)
    }
	
	def updateAllImageAnalysis() {
		def boolean error = false
		flash.message = ""
		
		params.specimens.regionOfInterest.each {specimenId, regionOfInterest ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(regionOfInterest == 'null')) {
				def regionOfInterestInteger = regionOfInterest.toInteger()
				if(regionOfInterestInteger >= 0 || regionOfInterestInteger <= 100) {
					specimenInstance.qcResult.regionOfInterest = regionOfInterestInteger
				}
				else { 
					if(!params.saveProgress) { //If not saving progress, then trying to push to next state
						flash.message += "Specimen " + specimenInstance.specimenChtnId + " ROI % needs to be within 0-100.  "
						error = true
					}
					
				}
				
			}
			else { // Region of interest is blank
				if(!params.saveProgress) { //If not saving progress, then trying to push to next state
					//this field is required, and if here it is 'null'/blank selection
					flash.message += "Specimen " + specimenInstance.specimenChtnId + " ROI % needs to have a value within 0-100.  "
					error = true
				}
			}
			
		}
		
		params.specimens.regionOfInterestArea.each {specimenId, regionOfInterestArea ->
			def specimenInstance = Specimen.get(specimenId)
			try {
				if(regionOfInterestArea.isEmpty()) {
					regionOfInterestArea=0
				}
				specimenInstance.qcResult.regionOfInterestArea = regionOfInterestArea.toBigDecimal()
			}
			catch(GroovyCastException) {
				if(!params.saveProgress) {
					flash.message += "Specimen " + specimenInstance.specimenChtnId + " ROI Area needs to be an Integer value.  "
					error = true
				}
				//Not sure if I need the below blocks
				if(!(regionOfInterestArea == 'null')) {
					if(!params.saveProgress) {
						
					}
				}
				else {
					
				}
			}	
		}
		
		params.specimens.percentNecrosis.each {specimenId, percentNecrosis ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(percentNecrosis == 'null')) {
				specimenInstance.qcResult.percentNecrosis = percentNecrosis.toInteger()
			}
			
		}
		
		params.specimens.linkPathReport.each {specimenId, linkPathReport ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(linkPathReport == 'null')) {
				specimenInstance.qcResult.linkPathReport = linkPathReport
			}
			
		}
		
		params.specimens.linkOverlayImage.each {specimenId, linkOverlayImage ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(linkOverlayImage == 'null')) {
				specimenInstance.qcResult.linkOverlayImage = linkOverlayImage
			}
			
		}
		
		params.specimens.linkSvsImage.each {specimenId, linkSvsImage ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(linkSvsImage == 'null')) {
				specimenInstance.qcResult.linkSvsImage = linkSvsImage
			}
			
		}
		
		//Instead, forward to controller:qcBatch, action:update
		if(params.saveProgress || error) {
			if(params.saveProgress) {
				flash.message += "Your progress is now saved.  "
			}
			forward(controller:"qcBatch", action:"viewBatchForImageAnalysis", params: ['id': params.qcBatchId])
			return
		}
		//Didn't save progress and return, so go ahead and forward to update QC Batch
		forward(controller:"qcBatch", action:"update", params: ['id': params.qcBatchId, 'version': params.qcBatchVersion, 'view': params.view, 'status': params.status, 'dateImageAnalysisComplete': params.dateImageAnalysisComplete])
	}
	
	def updateAllPathologistReview() {
		def boolean error = false
		flash.message = ""
		
		params.specimens.primaryOrMets.each {specimenId, primaryOrMets ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(primaryOrMets == 'null')) {
				def primaryOrMetsObj = PrimaryMets.read(primaryOrMets)
				specimenInstance.primaryOrMets = primaryOrMetsObj
			}
			else { //primaryOrMets field is 'null', that is, blank, so set to null
				specimenInstance.primaryOrMets = null
			}
			
		}
		
		params.specimens.regionOfInterest.each {specimenId, regionOfInterest ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(regionOfInterest == 'null')) {
				def regionOfInterestInteger = regionOfInterest.toInteger()
				if(regionOfInterestInteger >= 0 || regionOfInterestInteger <= 100) {
					specimenInstance.qcResult.regionOfInterest =regionOfInterestInteger
				}
				else {
					if(!params.saveProgress) { //If not saving progress, then trying to push to next state
						flash.message += "Specimen " + specimenInstance.specimenChtnId + " ROI % needs to be within 0-100.  "
						error = true
					}
					
				}
				
			}
			else { // Region of interest has blank
				if(!params.saveProgress) { //If not saving progress, then trying to push to next state
					//this field is required, and if here it is 'null'/blank selection
					flash.message += "Specimen " + specimenInstance.specimenChtnId + " ROI % needs to have a value within 0-100.  "
					error = true
				}
			}
			
		}
		
		params.specimens.percentNecrosis.each {specimenId, percentNecrosis ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(percentNecrosis == 'null')) {
				specimenInstance.qcResult.percentNecrosis = percentNecrosis.toInteger()
			}
			else {
				specimenInstance.qcResult.percentNecrosis = null
			}
			
		}
		
		params.specimens.tissueQcMatch.each {specimenId, tissueQcMatch ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(tissueQcMatch == 'null')) {
				def tissueQcMatchObj = TissueQcMatch.read(tissueQcMatch)
				specimenInstance.qcResult.tissueQcMatch = tissueQcMatchObj
			}
			else { //tissueQcMatch field is 'null', that is, blank, so set to null
				specimenInstance.qcResult.tissueQcMatch = null
				if(!params.saveProgress) {
					//This is a required field, so if not just saving progress, want to keep user from
					//  being able to push to next state
					flash.message += "Specimen " + specimenInstance.specimenChtnId + " needs a Tissue QC Match value.  "
					error = true
				}
			}
			
		}
		
		params.specimens.finalPrimaryAnatomicSite.each {specimenId, finalPrimaryAnatomicSite ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalPrimaryAnatomicSite == 'null')) {
				def anatomicSiteObj = AnatomicSite.read(finalPrimaryAnatomicSite)
				specimenInstance.qcResult.finalPrimaryAnatomicSite = anatomicSiteObj
			}
			else {
				specimenInstance.qcResult.finalPrimaryAnatomicSite = null
			}
			
		}
		
		params.specimens.finalTissueType.each {specimenId, finalTissueType ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalTissueType == 'null')) {
				def tissueTypeObj = TissueType.read(finalTissueType)
				specimenInstance.qcResult.finalTissueType = tissueTypeObj
			}
			else {
				specimenInstance.qcResult.finalTissueType = null
			}
			
		}
		
		params.specimens.finalDiagnosis.each {specimenId, finalDiagnosis ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalDiagnosis == 'null')) {
				def diagnosisObj = Diagnosis.read(finalDiagnosis)
				specimenInstance.qcResult.finalDiagnosis = diagnosisObj
			}
			else {
				specimenInstance.qcResult.finalDiagnosis = null
			}
		}
			
		params.specimens.finalSubDiagnosis.each {specimenId, finalSubDiagnosis ->
				def specimenInstance = Specimen.get(specimenId)
				if(!(finalSubDiagnosis == 'null')) {
					def subDiagnosisObj = SubDiagnosis.read(finalSubDiagnosis)
					specimenInstance.qcResult.finalSubDiagnosis = subDiagnosisObj
				}
				else {
					specimenInstance.qcResult.finalSubDiagnosis = null
				}
			
		}
		
		params.specimens.finalModDiagnosis.each {specimenId, finalModDiagnosis ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalModDiagnosis == 'null')) {
				def modDiagnosisObj = ModDiagnosis.read(finalModDiagnosis)
				specimenInstance.qcResult.finalModDiagnosis = modDiagnosisObj
			}
			else {
				specimenInstance.qcResult.finalModDiagnosis = null
			}
		}
		
		//Getting the IDs that have the checkbox checked
		def goodForTmaSpecimenIds = params.list('goodForTma').get(0)
		goodForTmaSpecimenIds.each {specimenId ->
			//println("specimenId: " + specimenId)
			//println("Specimen Key isLong() = " + specimenId.key.isLong())
			if (specimenId.key.isLong()) {
				def specimenInstance = Specimen.get(specimenId.key.toLong())
				//It is unintuitive, but the way the hiddenField and checkBox are
				//  working, the "on" being false seems to indicate the correct
				//  values of the checkBoxes. When it is checked, the value
				//  gets very jarbled.  When it is not checked, the value
				//  stays as "on" - leaving as is for now since it works
				if(!"on".equals(specimenId.value)) {
					specimenInstance.qcResult.goodForTma = true
				}
				else { //If it does say "on" then it is off
					specimenInstance.qcResult.goodForTma = false
				}
			}
			
		}

		params.specimens.pathologistComments.each {specimenId, pathologistComments ->
			def specimenInstance = Specimen.get(specimenId)
			specimenInstance.qcResult.pathologistComments = pathologistComments
		}
		
		params.specimens.finalGrade.each {specimenId, finalGrade ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalGrade == 'null')) {
				def gradeObj = Grade.read(finalGrade)
				specimenInstance.qcResult.finalGrade = gradeObj
			}
			else {
				specimenInstance.qcResult.finalGrade = null
			}
		}
		
		params.specimens.finalStage.each {specimenId, finalStage ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalStage == 'null')) {
				def stageObj = Stage.read(finalStage)
				specimenInstance.qcResult.finalStage = stageObj
			}
			else {
				specimenInstance.qcResult.finalStage = null
			}
		}
		
		params.specimens.finalMolecularStatusEr.each {specimenId, finalMolecularStatusEr ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalMolecularStatusEr == 'null')) {
				def molecularStatusErObj = MolecularStatusEr.read(finalMolecularStatusEr)
				specimenInstance.qcResult.finalMolecularStatusEr = molecularStatusErObj
			}
			else {
				specimenInstance.qcResult.finalMolecularStatusEr = null
			}
		}
		
		params.specimens.finalMolecularStatusPr.each {specimenId, finalMolecularStatusPr ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalMolecularStatusPr == 'null')) {
				def molecularStatusPrObj = MolecularStatusPr.read(finalMolecularStatusPr)
				specimenInstance.qcResult.finalMolecularStatusPr = molecularStatusPrObj
			}
			else {
				specimenInstance.qcResult.finalMolecularStatusPr = null
			}
		}
		
		params.specimens.finalMolecularStatusHer2.each {specimenId, finalMolecularStatusHer2 ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(finalMolecularStatusHer2 == 'null')) {
				def molecularStatusHer2Obj = MolecularStatusHer2.read(finalMolecularStatusHer2)
				specimenInstance.qcResult.finalMolecularStatusHer2 = molecularStatusHer2Obj
			}
			else {
				specimenInstance.qcResult.finalMolecularStatusHer2 = null
			}
		}
		
		params.specimens.qcFollowUpAction.each {specimenId, qcFollowUpAction ->
			def specimenInstance = Specimen.get(specimenId)
			if(!(qcFollowUpAction == 'null')) {
				def qcFollowUpActionObj = QcFollowUpAction.read(qcFollowUpAction)
				specimenInstance.qcResult.qcFollowUpAction = qcFollowUpActionObj
			}
			else {
				specimenInstance.qcResult.qcFollowUpAction = null
			}
		}
		
		if(params.saveProgress || error) {
			if(params.saveProgress) {
				flash.message += "Your progress is now saved.  "
			}
			forward(controller:"qcBatch", action:"viewBatchForPathologistReview", params: ['id': params.qcBatchId])
			return
		}
		//forward to qcBatch update with the qcBatch hidden fields from viewPathologistReview
		forward(controller:"qcBatch", action:"update", params: ['id': params.qcBatchId, 'version': params.qcBatchVersion, 'view': params.view, 'status': params.status])
	}

    def delete(Long id) {
        def specimenInstance = Specimen.get(id)
        if (!specimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'specimen.label', default: 'Specimen'), id])
            redirect(action: "specimensProcuredToday")
            return
        }

        try {
			def procedureInstance = specimenInstance.procedure
            specimenInstance.delete(flush: true)
			if(procedureInstance.specimens.size() == 0) {
				if(procedureInstance.procurementResult.description == "Procured") {
					procedureInstance.procurementResult = null
				}
			}
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'specimen.label', default: 'Specimen'), id])
            redirect(action: "specimensProcuredToday")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'specimen.label', default: 'Specimen'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def pathologyReportList() {
		def sdf = new SimpleDateFormat("MM/dd/yyyy"), fromDate, toDate

		fromDate = params.fromDate ? sdf.parse(params.fromDate) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')
		toDate = params.toDate ? sdf.parse(params.toDate) : new Date()

		def specimenCriteria = Specimen.createCriteria()
		def specimenInstanceList
		if(fromDate && toDate) {
			specimenInstanceList = specimenCriteria.list {
				procedure {
					between("procedureDate", fromDate, toDate)
					procedureEventInstitution {
						eq("description", "OSU")
					}
					order("procedureDate", "asc")
				}
				order("specimenChtnId", "asc")
			}
		}
		else { //Both dates need to be provided
			flash.message = "Both Procedure From Date and To Date need to be provided"
		}
		
		
		[specimenInstanceList: specimenInstanceList, fromDate: fromDate, fromDateTextField: params.fromDate, toDate: toDate, toDateTextField: params.toDate]
	}
	
}
