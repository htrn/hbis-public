package hbis

import org.springframework.dao.DataIntegrityViolationException
import java.text.SimpleDateFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class ProcedureController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }
	
	def jasperService
	def morningListReport() {
		def procedureDate = new Date().clearTime()
		def procedureCriteria = Procedure.createCriteria()
		def procedures = procedureCriteria.list {
			ge("procedureDate", procedureDate)
			eq("procedureEventInstitution", Institution.findByDescription("OSU"))
			order("endTimeOfProcedure", "asc")
		}
		params.procedureDate = procedureDate
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:procedures])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [procedureInstanceList: Procedure.list(params), procedureInstanceTotal: Procedure.count()]
    }
	
	def amProcedureList() {
		Date today = new Date().clearTime()
		def procedureCriteria = Procedure.createCriteria()
		def procedureInstanceList = procedureCriteria.list {
			ge("procedureDate", today)
			eq("procedureEventInstitution", Institution.findByDescription("OSU"))
			order("endTimeOfProcedure", "asc")
		}
		[procedureInstanceList: procedureInstanceList]
	}
	
	def caseResults() {
		Date today = new Date().clearTime()
		def procedureCriteria = Procedure.createCriteria()
		def procedureInstanceList = procedureCriteria.list { //Procedure.findAllByDateCreatedGreaterThanEquals(today)
			ge("procedureDate", today)
			eq("procedureEventInstitution", Institution.findByDescription("OSU"))
			order("endTimeOfProcedure", "asc")		
		}
		
		[procedureInstanceList: procedureInstanceList]
	}
	
	def searchCaseResults() {
		def sdf = new SimpleDateFormat("MM/dd/yyyy")
		def procedureDate
		def mrn = params.mrn
		def lastName = params.lastName
		def procedureCriteria = Procedure.createCriteria()
		def procedureInstanceList
		
		if(params.procedureDate) {
			procedureDate = sdf.parse(params.procedureDate)
		}
		
		if(procedureDate || mrn || lastName) {
			procedureInstanceList = procedureCriteria.list {
				if(procedureDate) {
					eq("procedureDate", procedureDate)
				}
				patient {
					if(mrn) {
						ilike("mrn", "%" + mrn + "%")
					}
					if(lastName) {
						ilike("lastName", "%" + lastName + "%")
					}
					
				}
				order("procedureDate", "asc")
			}
		}
		
		
		[procedureInstanceList: procedureInstanceList, procedureDate: params.procedureDate, mrn: params.mrn, lastName: params.lastName]
	}

    def create() {
		def procedureInstance = new Procedure(params)
		
		if(procedureInstance.patient.originInstitution) { //Bring the institution over from the parent Patient
			procedureInstance.procedureEventInstitution = procedureInstance.patient.originInstitution
		}
		else { //If no parent Patient institution, make it default to OSU
			def institutionOsu = Institution.findByDescription("OSU")
			procedureInstance.procedureEventInstitution = institutionOsu
		}
		if(!procedureInstance.patientAgeUnit) {
			procedureInstance.patientAgeUnit = AgeUnit.findByDescription("Years")
		}
		
		procedureInstance.chemoHistory = YesNo.findByDescription("Unknown")
		procedureInstance.radiationHistory = YesNo.findByDescription("Unknown")
		
        [procedureInstance: procedureInstance, patientInstance:procedureInstance.patient]
    }

    def save() {
        def procedureInstance = new Procedure(params)
		procedureInstance.clearErrors()
		
		if(params.procedureDate) {
			def error = false
			def sdf = new SimpleDateFormat("MM/dd/yyyy")
			if(!(params.procedureDate.length() == 10)) {
				error = true
			}
			else {
				try {
					procedureInstance.procedureDate = sdf.parse(params.procedureDate)
				} catch (Exception e) { // Unable to parse procedureDate
					error = true
				}
			}
			if(error) {
				procedureInstance.errors.rejectValue("procedureDate", "validation.procedure.procedureDate.unparsable",
					[message(code: 'procedure.label', default: 'Procedure')] as Object[],
					"Procedure Date must be in MM/dd/yyyy format (example: 01/01/2014)")
				render(view: "create", model: [procedureInstance: procedureInstance, patientInstance:procedureInstance.patient])
				return
			}
		}
		if(params.endTimeOfProcedure) {
			def error = false
			if(!(params.endTimeOfProcedure.length() == 4)) { //There is unexpected behavior if given all numbers with greater than 4 length
				error = true
			}
			else {
				def sdf = new SimpleDateFormat("HHmm")
				try {
					procedureInstance.endTimeOfProcedure = sdf.parse(params.endTimeOfProcedure)
				} catch (Exception e) { // Unable to parse endTime
					error = true
				}
			}
			if(error) {
				procedureInstance.errors.rejectValue("endTimeOfProcedure", "validation.procedure.endTimOfProcedure.unparsable",
					[message(code: 'procedure.label', default: 'Procedure')] as Object[],
					"End Time must be in HHmm format (example: 1452)")
				render(view: "create", model: [procedureInstance: procedureInstance, patientInstance:procedureInstance.patient])
				return
			}
		}
		if(params.dateOfDeath) {
			def sdf = new SimpleDateFormat("MM/dd/yyyy")
			def error = false
			if(!(params.dateOfDeath.length() == 10)) {
				error = true
			}
			else {
				try {
					procedureInstance.dateOfDeath = sdf.parse(params.dateOfDeath)
				} catch (Exception e) { // Unable to parse procedureDate
					error = true
				}
			}
			if(error) {
				procedureInstance.errors.rejectValue("dateOfDeath", "validation.procedure.dateOfDeath.unparsable",
					[message(code: 'procedure.label', default: 'Procedure')] as Object[],
					"Date of Death must be in MM/dd/yyyy format (example: 01/01/2014)")
				render(view: "create", model: [procedureInstance: procedureInstance, patientInstance:procedureInstance.patient])
				return
			}
		}
		
        if (!procedureInstance.save(flush: true)) {
			def patientInstance = procedureInstance.patient
            render(view: "create", model: [procedureInstance: procedureInstance, patientInstance:patientInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'procedure.label', default: 'Procedure'), procedureInstance.id])
		// Depending on which button is pressed, redirect to the appropriate place
		if(params.saveAndReturnAmProcedureList) {
			redirect(action: "amProcedureList")
			return
		}
		if(params.saveAndCreateSpecimen) {
			redirect(controller:"specimen", action: "create", params:['procedure.id': procedureInstance.id])
			return
		}
		if(params.saveAndCreateChartReview) {
			redirect(controller:"chartReview", action: "create", params:['procedure.id': procedureInstance.id])
			return
		}
		redirect(action: "show", id: procedureInstance.id)
    }
	
    def show(Long id) {
        def procedureInstance = Procedure.get(id)
        if (!procedureInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procedure.label', default: 'Procedure'), id])
            redirect(action: "list")
            return
        }
		def patientInstance = procedureInstance.patient

        [procedureInstance: procedureInstance, patientInstance: patientInstance]
    }

    def edit(Long id) {
        def procedureInstance = Procedure.get(id)
        if (!procedureInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procedure.label', default: 'Procedure'), id])
            redirect(action: "list")
            return
        }
		def patientInstance = procedureInstance.patient

        [procedureInstance: procedureInstance, patientInstance: patientInstance, view: params.view]
    }

    def update(Long id, Long version) {
        def procedureInstance = Procedure.get(id)
        if (!procedureInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procedure.label', default: 'Procedure'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (procedureInstance.version > version) {
                procedureInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'procedure.label', default: 'Procedure')] as Object[],
                          "Another user has updated this Procedure while you were editing")
                render(view: "edit", model: [procedureInstance: procedureInstance, patientInstance:procedureInstance.patient])
                return
            }
        }

        procedureInstance.properties = params
		procedureInstance.clearErrors()
		
		if(params.procedureDate) {
			def error = false
			def sdf = new SimpleDateFormat("MM/dd/yyyy")
			if(!(params.procedureDate.length() == 10)) {
				error = true
			}
			else {
				try {
					procedureInstance.procedureDate = sdf.parse(params.procedureDate)
				} catch (Exception e) { // Unable to parse procedureDate
					error = true
				}
			}
			if(error) {
				procedureInstance.errors.rejectValue("procedureDate", "validation.procedure.procedureDate.unparsable",
					[message(code: 'procedure.label', default: 'Procedure')] as Object[],
					"Procedure Date must be in MM/dd/yyyy format (example: 01/01/2014)")
				render(view: "edit", model: [procedureInstance: procedureInstance, patientInstance:procedureInstance.patient])
				return
			}
		}
		if(params.endTimeOfProcedure) {
			def error = false
			if(!(params.endTimeOfProcedure.length() == 4)) { //There is unexpected behavior if given all numbers with greater than 4 length
				error = true
			}
			else {
				def sdf = new SimpleDateFormat("HHmm")
				try {
					procedureInstance.endTimeOfProcedure = sdf.parse(params.endTimeOfProcedure)
				} catch (Exception e) { // Unable to parse endTime
					error = true
				}
			}
			if(error) {
				procedureInstance.errors.rejectValue("endTimeOfProcedure", "validation.procedure.endTimOfProcedure.unparsable",
					[message(code: 'procedure.label', default: 'Procedure')] as Object[],
					"End Time must be in HHmm format (example: 1452)")
				render(view: "edit", model: [procedureInstance: procedureInstance, patientInstance:procedureInstance.patient])
				return
			}
		}
		if(params.dateOfDeath) {
			def sdf = new SimpleDateFormat("MM/dd/yyyy")
			def error = false
			if(!(params.dateOfDeath.length() == 10)) {
				error = true
			}
			else {
				try {
					procedureInstance.dateOfDeath = sdf.parse(params.dateOfDeath)
				} catch (Exception e) { // Unable to parse dateOfDeath
					error = true
				}
			}
			if(error) {
				procedureInstance.errors.rejectValue("dateOfDeath", "validation.procedure.dateOfDeath.unparsable",
					[message(code: 'procedure.label', default: 'Procedure')] as Object[],
					"Date of Death must be in MM/dd/yyyy format (example: 01/01/2014)")
				render(view: "edit", model: [procedureInstance: procedureInstance, patientInstance:procedureInstance.patient])
				return
			}
		}	
		
        if (!procedureInstance.save(flush: true)) {
			def patientInstance = procedureInstance.patient
            render(view: "edit", model: [procedureInstance: procedureInstance, patientInstance:patientInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'procedure.label', default: 'Procedure'), procedureInstance.id])
		if(params.view == "caseResults") { //If edit pressed from Case Results page, want to go directly back there
			redirect(action:"caseResults")
			return
		}
        redirect(action: "show", id: procedureInstance.id)
    }
	
	def updateFromCaseResults(Long id, Long version) {
		def procedureInstance = Procedure.get(id)
		if (!procedureInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'procedure.label', default: 'Procedure'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (procedureInstance.version > version) {
				procedureInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'procedure.label', default: 'Procedure')] as Object[],
						  "Another user has updated this Procedure while you were editing")
				Date today = new Date().clearTime()
				def procedureInstanceList = Procedure.findAllByDateCreatedGreaterThanEquals(today)
				
				render(view: "caseResults", model: [procedureInstance: procedureInstance, procedureInstanceList: procedureInstanceList, numberOfSegsList: numberOfSegsList])
				return
			}
		}
		
		ProcurementResult procurementResult
		Origin origin
		if(params.fieldUpdated.equals("procurementResult")) {
			procurementResult = ProcurementResult.get(params.procurementResult.id)
			procedureInstance.procurementResult = procurementResult
		}
		if (params.fieldUpdated.equals("origin")) {
			origin = Origin.get(params.origin.id)
			procedureInstance.origin = origin
		}
		
		

		if (!procedureInstance.save(flush: true)) {
			Date today = new Date().clearTime()
				def procedureInstanceList = Procedure.findAllByDateCreatedGreaterThanEquals(today)
				render(view: "caseResults", model: [procedureInstance: procedureInstance, procedureInstanceList: procedureInstanceList, numberOfSegsList: numberOfSegsList])
				return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'procedure.label', default: 'Procedure'), procedureInstance.id])
		redirect(action: "caseResults")
	}
	
	def updateAllCaseResults() {
		def boolean error = false
		flash.message = ""
		
		params.procedures.procurementResult.each {procedureId, procurementResult ->
			def procedureInstance = Procedure.get(procedureId)
			if(!(procurementResult == 'null')) {
				def procurementResultObj = ProcurementResult.read(procurementResult)
				procedureInstance.procurementResult = procurementResultObj
			}
			else { //tissueQcMatch field is 'null', that is, blank, so set to null
				procedureInstance.procurementResult = null
			}
			
		}
		
		params.procedures.medComments.each {procedureId, medComments ->
			def procedureInstance = Procedure.get(procedureId)
			procedureInstance.medComments = medComments
		}
		
		params.procedures.origin.each {procedureId, origin ->
			def procedureInstance = Procedure.get(procedureId)
			if(!(origin == 'null')) {
				def originObj = Origin.read(origin)
				procedureInstance.origin = originObj
			}
			else { //tissueQcMatch field is 'null', that is, blank, so set to null
				procedureInstance.origin = null
			}
			
		}
		
		flash.message = "Procedures are now updated"
		redirect(action: "caseResults")
	}

    def delete(Long id) {
        def procedureInstance = Procedure.get(id)
        if (!procedureInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procedure.label', default: 'Procedure'), id])
            redirect(action: "list")
            return
        }
		else if (procedureInstance.chartReview) {
			flash.message = message(code: 'procedure.notDeleted.hasChartReview.message', args: [message(code: 'procedure.label', default: 'Procedure'), id])
			redirect(action: "show", id:id)
			return
		}

        try {
            procedureInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'procedure.label', default: 'Procedure'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'procedure.label', default: 'Procedure'), id])
            redirect(action: "show", id: id)
        }
    }
}
