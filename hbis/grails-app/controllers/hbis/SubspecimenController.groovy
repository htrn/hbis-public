package hbis

import java.text.SimpleDateFormat

import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import grails.converters.JSON

import org.springframework.dao.DataIntegrityViolationException

class SubspecimenController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def subspecimenService, sdf = new SimpleDateFormat("MM/dd/yyyy")

    def index() {
        redirect(action: "list", params: params)
    }
	def jasperService
	def subspecimenLabel() {
		def subspecimenCriteria = Subspecimen.createCriteria()
		def subspecimens = subspecimenCriteria.list {
			eq('id', params.long('subspecimenId'))
		}

		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:subspecimens])
        
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	def histologyWorksheetReport() {
		def procurementDate = params.date('procurementDate').clearTime()
		def paraffinBlock = PreparationMedia.findByDescription("Paraffin Block (NBF)")
		def subspecimenCriteria = Subspecimen.createCriteria()
		def subspecimens = subspecimenCriteria.list {
			or {
				eq('preparationMedia', paraffinBlock)
				and {
					preparationType {
						not { eq("description", "Frozen") }
					}
					preparationMedia {
						ilike("description", "%" + "slide" + "%")
					}
				}
			}
			specimen {
				procedure {
					eq("procedureEventInstitution", Institution.findByDescription("OSU"))
				}
				eq("procurementDate", procurementDate)
				order("specimenChtnId", "asc")
			}
			
		}
		params.procurementDate = procurementDate
		params.oneHeValue = 'X'
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:subspecimens])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	
	def searchQcReport() {
		def subspecimens = subspecimenService.searchQcReport(params)

		params.fromDate = sdf.parse(params.fromDate)
		params.toDate = sdf.parse(params.toDate)
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:subspecimens])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	
	def subcontractReport() {
		subspecimens = subspecimenService.subcontractReport(params)

		params.organization = organization
		params.fromDate = sdf.parse(params.fromDate)
		params.toDate = sdf.parse(params.toDate)
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:subspecimens])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}

	def generalSearchResultsToCSV() {
		def subspecimens = subspecimenService.generalSearch(session['generalSubspecimenSearchParams'])
		def fields = subspecimenService.findSelectedFields(session['generalSubspecimenSearchParams'])

		response.setHeader("Content-disposition", "attachment; filename=" + 'general-subspecimen-search-results-' + new Date() + '.csv');
		response.contentType = "text/csv"
		response.characterEncoding = "UTF-8"

		response.outputStream << subspecimenService.generateCSVHeader(fields)

		subspecimens.each { subspecimen ->
			response.outputStream << subspecimenService.generateCSVContent(subspecimen, fields)
		}

		response.outputStream.flush()
		response.outputStream.close()
	}

	def apqiSearchResultsToCSV() {
		def apqiSearchResults = subspecimenService.apqiSearch(session['apqiSearchParams'])

		response.setHeader("Content-disposition", "attachment; filename=" + 'apqi-search-results-' + new Date() + '.csv');
		response.contentType = "text/csv"
		response.characterEncoding = "UTF-8"

		response.outputStream << 'Distinct Patients,Total QC Performed,Number QC Passed,Number QC Failed,Parts Per Case,Total Subspecimens Procured,Distinct Autopsy Patients,Total Autopsy Subspecimens Procured\n'
		response.outputStream << "${apqiSearchResults.distinctPatients},${apqiSearchResults.totalQcPerformed},${apqiSearchResults.numberQcPassed}" +
				",${apqiSearchResults.numberQcFailed},${apqiSearchResults.partsPerCase},${apqiSearchResults.totalSubspecimensProcured}" +
				",${apqiSearchResults.distinctAutopsyPatients},${apqiSearchResults.totalAutopsySubspecimens}\n"
		response.outputStream << ',\n'

		response.outputStream << 'Procurement Result,Quantity\n'
		apqiSearchResults.procurementResultQuantityList.each { it ->
			response.outputStream << "${it.procurementResultDescription},${it.quantity}\n"
		}
		response.outputStream << ',\n'

		response.outputStream << 'Procedure Name,Sub Procedure Name,Procurement Result,Quantity\n'
		apqiSearchResults.procedureNameSubNameByResultQuantityList.each { it ->
			response.outputStream << "${it.procedureNameDescription},${it.subProcedureNameDescription},${it.procurementResultDescription}" +
					",${it.quantity}\n"
		}
		response.outputStream << ',\n'

		response.outputStream << 'Origin,Quantity\n'
		apqiSearchResults.originQuantityList.each { it ->
			response.outputStream << "${it.originDescription},${it.quantity}\n"
		}

		response.outputStream.flush()
		response.outputStream.close()
	}

	def internalBillingReport() {
		def subspecimens = subspecimenService.internalBillingReport(params)

		params.organization = organization
		params.fromDate = sdf.parse(params.fromDate)
		params.toDate = sdf.parse(params.toDate)
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:subspecimens])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	
	def searchProcurementReport() {
		def subspecimens, fromDate, toDate, cal = Calendar.getInstance()

		fromDate = params.fromDate ? sdf.parse(params.fromDate) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')

		toDate = params.toDate ? sdf.parse(params.toDate) : new Date()
		cal.setTime(toDate)
		toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")

		subspecimens = subspecimenService.searchProcurementReport(params.investigatorId, fromDate, toDate)
		
		params.fromDate = fromDate
		params.toDate = toDate
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:subspecimens])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	
	def searchDistributionReport() {
		params.fromDate = sdf.parse(params.fromDate)
		params.toDate = sdf.parse(params.toDate)

		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:subspecimens])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	
	def histologyWorksheet() {
		[procurementDate: new Date()-1]
	}

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [subspecimenInstanceList: Subspecimen.list(params), subspecimenInstanceTotal: Subspecimen.count()]
    }

    def create() {
		def subspecimenInstance
		def subspecimens
		def subspecimenCriteria
		if(params.subspecimenParamMap) {
			//Need to parse the saved paramMap from String to JSON so it can be used to fill the data fields again
			def subspecimenParamMap = JSON.parse(params.subspecimenParamMap)
			subspecimenInstance = new Subspecimen(subspecimenParamMap)
		}
		else {
			subspecimenInstance = new Subspecimen(params)
			subspecimenInstance.weightUnit = WeightMeasurement.findByDescription("grams")
		}
		
		def tissueRequestList
		if(subspecimenInstance.investigator) {
			def tissueRequestCriteria = TissueRequest.createCriteria()
			tissueRequestList =  tissueRequestCriteria.list {
				eq("investigator", subspecimenInstance.investigator)
			}
		}
		
		subspecimenCriteria = Subspecimen.createCriteria()
		subspecimens = subspecimenCriteria.list {
			eq("specimen", subspecimenInstance.specimen)
			order("subspecimenChtnId", "asc")
		}

		def preparationMediaList = subspecimenService.getPreparationMediaList(subspecimenInstance?.preparationType?.description)

        [subspecimenInstance: subspecimenInstance, specimenInstance:subspecimenInstance.specimen,
			procedureInstance:subspecimenInstance.specimen.procedure, patientInstance:subspecimenInstance.specimen.procedure.patient, 
			subspecimens:subspecimens, tissueRequestList: tissueRequestList, displayQcPreset: true, preparationMediaList: preparationMediaList]
    }
	
	def copyFromCreate(Long id) {
		def incomingSubspecimen = Subspecimen.get(id)
		def subspecimenInstance = new Subspecimen()
		//copying each of the properties manually
		//Handling if the copied/incoming subspecimen has a status of Shipped
		if(incomingSubspecimen.status.description == "Shipped") {
			subspecimenInstance.status = SubspecimenStatus.findByDescription("Assigned")
		}
		else {
			subspecimenInstance.status = incomingSubspecimen.status
		}
		subspecimenInstance.preparationType = incomingSubspecimen.preparationType
		def preparationTypeDescription = subspecimenInstance.preparationType.description
		def preparationMediaList = subspecimenService.getPreparationMediaList(preparationTypeDescription)

		subspecimenInstance.preparationMedia = incomingSubspecimen.preparationMedia
		//subspecimenInstance.weight = incomingSubspecimen.weight
		subspecimenInstance.weightUnit = incomingSubspecimen.weightUnit
		//subspecimenInstance.volume = incomingSubspecimen.volume
		subspecimenInstance.volumeUnit = incomingSubspecimen.volumeUnit
		//subspecimenInstance.sizeWidth = incomingSubspecimen.sizeWidth
		//subspecimenInstance.sizeLength = incomingSubspecimen.sizeLength
		//subspecimenInstance.sizeHeight = incomingSubspecimen.sizeHeight
		subspecimenInstance.sizeUnit = incomingSubspecimen.sizeUnit
		subspecimenInstance.labelComment = incomingSubspecimen.labelComment
		subspecimenInstance.price = incomingSubspecimen.price
		subspecimenInstance.needsChartReview = incomingSubspecimen.needsChartReview
		subspecimenInstance.specimen = incomingSubspecimen.specimen
		subspecimenInstance.investigator = incomingSubspecimen.investigator
		subspecimenInstance.tissueRequest = incomingSubspecimen.tissueRequest
		
		def tissueRequestList
		if(subspecimenInstance.investigator) {
			def tissueRequestCriteria = TissueRequest.createCriteria()
			tissueRequestList =  tissueRequestCriteria.list {
				eq("investigator", subspecimenInstance.investigator)
			}
		}
		
		render(view: "create", model: [subspecimenInstance: subspecimenInstance, specimenInstance: subspecimenInstance.specimen,
										procedureInstance: subspecimenInstance.specimen.procedure, patientInstance: subspecimenInstance.specimen.procedure.patient, 
										subspecimens: subspecimenInstance.specimen.subspecimens, tissueRequestList: tissueRequestList, displayQcPreset: true, preparationMediaList:preparationMediaList])
		return
	}
	
	def qcPresets() {
		def subspecimenInstance = new Subspecimen(params)
		// setting each of the QC preset fields
		subspecimenInstance.subspecimenChtnId = 'Q'
		subspecimenInstance.status = SubspecimenStatus.findByDescription('QC')
		def description = 'Fixed'
		subspecimenInstance.preparationType = PreparationType.findByDescription(description)
		subspecimenInstance.preparationMedia = PreparationMedia.findByDescription('Paraffin Block (NBF)')
		subspecimenInstance.weightUnit = WeightMeasurement.findByDescription('grams')
		
		def specimenInstance = subspecimenInstance.specimen
		def procedureInstance = specimenInstance.procedure
		def patientInstance = procedureInstance.patient
		def subspecimens = specimenInstance.subspecimens

		def preparationMediaList = subspecimenService.getPreparationMediaList(description)
		
		render(view: "create", model: [subspecimenInstance: subspecimenInstance, specimenInstance:specimenInstance, procedureInstance:procedureInstance, patientInstance:patientInstance, subspecimens:subspecimens, displayQcPreset: true, preparationMediaList:preparationMediaList])
		return
	}

    def save() {
        def subspecimenInstance = new Subspecimen(params)
        if (!subspecimenInstance.save(flush: true)) {
			def specimenInstance = subspecimenInstance.specimen
			def procedureInstance = specimenInstance.procedure
			def patientInstance = procedureInstance.patient
			def subspecimens = specimenInstance.subspecimens
            render(view: "create", model: [subspecimenInstance: subspecimenInstance, specimenInstance:specimenInstance, procedureInstance:procedureInstance, patientInstance:patientInstance, subspecimens:subspecimens, displayQcPreset: true])
            return
        }

        flash.message = message(code: 'subspecimen.created.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), subspecimenInstance.id, subspecimenInstance.specimen.specimenChtnId, subspecimenInstance.subspecimenChtnId])
        redirect(action: "show", id: subspecimenInstance.id)
    }
	
	def saveAndCreateAnotherSubspecimen() {
		def subspecimenInstance = new Subspecimen(params)
		if (!subspecimenInstance.save(flush: true)) {
			def specimenInstance = subspecimenInstance.specimen
			def procedureInstance = specimenInstance.procedure
			def patientInstance = procedureInstance.patient
			def subspecimens = specimenInstance.subspecimens
			def tissueRequestList
			if(subspecimenInstance.investigator) {
				def tissueRequestCriteria = TissueRequest.createCriteria()
				tissueRequestList =  tissueRequestCriteria.list {
					eq("investigator", subspecimenInstance.investigator)
				}
			}
			render(view: "create", model: [subspecimenInstance: subspecimenInstance, 
											specimenInstance:specimenInstance, 
											procedureInstance:procedureInstance, 
											patientInstance:patientInstance, 
											subspecimens:subspecimens,
											tissueRequestList: tissueRequestList, 
											displayQcPreset: true])
			return
		}
		
		flash.message = message(code: 'subspecimen.created.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), subspecimenInstance.id, subspecimenInstance.specimen.specimenChtnId, subspecimenInstance.subspecimenChtnId])
		redirect(action: "create", params: ['specimen.id':subspecimenInstance.specimen.id])
		
	}

    def show(Long id) {
        def subspecimenInstance = Subspecimen.get(id)
        if (!subspecimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
            redirect(action: "list")
            return
        }
		def specimenInstance = subspecimenInstance.specimen
		def procedureInstance = specimenInstance.procedure
		def patientInstance = procedureInstance.patient

        [subspecimenInstance: subspecimenInstance, specimenInstance:specimenInstance,
			procedureInstance:procedureInstance, patientInstance:patientInstance]
    }

    def edit(Long id) {
        def subspecimenInstance = Subspecimen.get(id)
        if (!subspecimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
            redirect(action: "list")
            return
        }
		
		def tissueRequestList
		if(subspecimenInstance.investigator) {
			def tissueRequestCriteria = TissueRequest.createCriteria()
			tissueRequestList =  tissueRequestCriteria.list {
				eq("investigator", subspecimenInstance.investigator)
			}
		}

		def preparationMediaList = subspecimenService.getPreparationMediaList(subspecimenInstance?.preparationType?.description)

        [subspecimenInstance: subspecimenInstance, specimenInstance:subspecimenInstance.specimen, 
			procedureInstance:subspecimenInstance.specimen.procedure, patientInstance:subspecimenInstance.specimen.procedure.patient,
			tissueRequestList: tissueRequestList, view: params.view, preparationMediaList: preparationMediaList]
    }

    def update(Long id, Long version) {
        def subspecimenInstance = Subspecimen.get(id)
        if (!subspecimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (subspecimenInstance.version > version) {
                subspecimenInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'subspecimen.label', default: 'Subspecimen')] as Object[],
                          "Another user has updated this Subspecimen while you were editing")
				def specimenInstance = subspecimenInstance.specimen
				def procedureInstance = specimenInstance.procedure
				def patientInstance = procedureInstance.patient
				if(params.view == "contactExternalFresh") {
					redirect(action:"contactExternalFresh")
					return
				}
				else if(params.view == "contactInternalFresh") {
					redirect(action:"contactInternalFresh")
					return
				}
				else if(params.view == "contactEmail") {
					redirect(action:"contactEmail")
					return
				}
				def tissueRequestList
				if(subspecimenInstance.investigator) {
					def tissueRequestCriteria = TissueRequest.createCriteria()
					tissueRequestList =  tissueRequestCriteria.list {
						eq("investigator", subspecimenInstance.investigator)
					}
				}
                render(view: "edit", model: [subspecimenInstance: subspecimenInstance, 
												specimenInstance:specimenInstance, 
												procedureInstance:procedureInstance, 
												patientInstance:patientInstance,
												tissueRequestList: tissueRequestList,
												view: params.view])
                return
            }
        }
		
		
		//If the investigator is updated, but there is no Tissue Request, then the old tissue request should be removed
		//  Need to know if the incoming investigator is different from the existing one, since the drop down will 
		//		be populated and technically send that as part of the request parameters
		def boolean checkForTissueRequest = false
		if(params.investigator) {
			def incomingInvestigator = Investigator.read(params.int('investigator.id'))
			//If the investigator is changing, then mark it to check for an incoming Tissue Request parameter
			if(incomingInvestigator != subspecimenInstance.investigator) {
				checkForTissueRequest = true
			}
		}

        subspecimenInstance.properties = params
		
		
		if(checkForTissueRequest) {
			//Investigator has changed, so if there is no tissue request parameter, then it should be cleared
			//	(otherwise it won't be updated to null, since there won't be a drop down displayed 
			//	in the case of an Investigator with no tissue requests
			if(!params.tissueRequest) {
				subspecimenInstance.tissueRequest = null
			}
		}

        if (!subspecimenInstance.save(flush: true)) {
			def specimenInstance = subspecimenInstance.specimen
			def procedureInstance = specimenInstance.procedure
			def patientInstance = procedureInstance.patient
			if(params.view == "contactExternalFresh") {
				redirect(action:"contactExternalFresh")
				return
			}
			else if(params.view == "contactInternalFresh") {
				redirect(action:"contactInternalFresh")
				return
			}
			else if(params.view == "contactEmail") {
				redirect(action:"contactEmail")
				return
			}
			def tissueRequestList
			if(subspecimenInstance.investigator) {
				def tissueRequestCriteria = TissueRequest.createCriteria()
				tissueRequestList =  tissueRequestCriteria.list {
					eq("investigator", subspecimenInstance.investigator)
				}
			}
			render(view: "edit", model: [subspecimenInstance: subspecimenInstance,
											specimenInstance:specimenInstance,
											procedureInstance:procedureInstance,
											patientInstance:patientInstance,
											tissueRequestList: tissueRequestList,
											view: params.view])
			return
        }

        flash.message = message(code: 'subspecimen.updated.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), subspecimenInstance.id, subspecimenInstance.specimen.specimenChtnId, subspecimenInstance.subspecimenChtnId])
		if(params.view == "contactExternalFresh") {
			redirect(action:"contactExternalFresh")
			return
		}
		else if(params.view == "contactInternalFresh") {
			redirect(action:"contactInternalFresh")
			return
		}
		else if(params.view == "contactEmail") {
			redirect(action:"contactEmail")
			return
		}
		else if(params.view == "create") {
			redirect(action:"create", params:['specimen.id': subspecimenInstance.specimen.id])
			return
		}
		redirect(action: "show", id: subspecimenInstance.id)
    }
	
	def editPrices() {
		//want to initially find all from yesterday
		def fromDate
		def toDate
		if(params.fromDate) {
			fromDate = sdf.parse(params.fromDate)
		}
		if(params.toDate) {
			toDate = sdf.parse(params.toDate)
		}
		def yesterday = new Date().clearTime()-1
		def today = new Date()
		def subspecimenCriteria = Subspecimen.createCriteria()
		def subspecimens = subspecimenCriteria.list {
			specimen {
				if(!fromDate && !toDate) {
					ge("procurementDate", yesterday)
				}
				else {
					if(fromDate && !toDate) {
						ge("procurementDate", fromDate)
					}
					else if (!fromDate && toDate) {
						le("procurementDate", toDate)
					}
					else { //Both fromDate and toDate are provided
						between("procurementDate", fromDate, toDate)
					}
				}
				order("specimenChtnId", "asc")
				procedure {
					patient {
						order("mrn", "asc")
					}
				}
			}
			order("subspecimenChtnId", "asc")
		}
		// fromDate and toDate are for the page reporting what dates it is searching for
		// fromDateTextField and toDateTextField are for preserving the user's criteria selections
		[subspecimens: subspecimens, fromDate: fromDate, fromDateTextField: params.fromDate, toDate: toDate, toDateTextField: params.toDate]
	}
	
	def updatePrice(Long id, Long version, float price) {
		def subspecimenInstance = Subspecimen.get(id)
        if (!subspecimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
            redirect(action: "editPrices")
            return
        }
		
		if (version != null) {
			if (subspecimenInstance.version > version) {
				subspecimenInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'subspecimen.label', default: 'Subspecimen')] as Object[],
						  "Another user has updated this Subspecimen while you were editing")
				//Need to essentially process the same as editPrices action because it is just being rendered below, not redirected
				//  This code snippet should be moved to its own private function, but no time right now.  It may also
				//		Change due to possibility of implementing an 'Update All' button instead of each row separately
				def fromDate
				def toDate
				if(params.fromDate) {
					fromDate = sdf.parse(params.fromDate)
				}
				if(params.toDate) {
					toDate = sdf.parse(params.toDate)
				}
				def yesterday = new Date().clearTime()-1
				def today = new Date()
				def subspecimenCriteria = Subspecimen.createCriteria()
				def subspecimens = subspecimenCriteria.list {
					specimen {
						if(!fromDate && !toDate) {
							ge("procurementDate", yesterday)
						}
						else {
							if(fromDate && !toDate) {
								ge("procurementDate", fromDate)
							}
							else if (!fromDate && toDate) {
								le("procurementDate", toDate)
							}
							else { //Both fromDate and toDate are provided
								between("procurementDate", fromDate, toDate)
							}
						}
						order("specimenChtnId", "asc")
						procedure {
							patient {
								order("mrn", "asc")
							}
						}
					}
					order("subspecimenChtnId", "asc")
				}
				
				render(view: "editPrices", model: [subspecimens: subspecimens, subspecimenInstance: subspecimenInstance, 
														fromDate: fromDate, toDate: toDate,
														fromDateTextField: params.fromDate, toDateTextField: params.toDate])
				return
			}
		}
		
		subspecimenInstance.price = price
		subspecimenInstance.datePriceUpdated = new Date()
		
		if (!subspecimenInstance.save(flush: true)) {
			def specimenInstance = subspecimenInstance.specimen
			def procedureInstance = specimenInstance.procedure
			def patientInstance = procedureInstance.patient
			render(view: "edit", model: [subspecimenInstance: subspecimenInstance, specimenInstance:specimenInstance, procedureInstance:procedureInstance, patientInstance:patientInstance])
			return
		}

		flash.message = message(code: 'subspecimen.updated.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), subspecimenInstance.id, subspecimenInstance.specimen.specimenChtnId, subspecimenInstance.subspecimenChtnId])
		
		//Since redirecting to editPrices, just need to pass along the dates as they were received
		redirect(action: "editPrices", params:[fromDate: params.fromDate, toDate: params.toDate])
	}
	
	def UpdateAllPrices() {
		def boolean error = false
		flash.message = ""
		
		params.subspecimens.price.each {subspecimenId, price ->
			def subspecimenInstance = Subspecimen.get(subspecimenId)
			if(!(price == 'null')) {
				if(subspecimenInstance.price != price.toBigDecimal()) {
					subspecimenInstance.price = price.toBigDecimal()
					subspecimenInstance.datePriceUpdated = new Date()
				}
				
			}
		}
		
		//Since redirecting to editPrices, just need to pass along the dates as they were received
		redirect(action: "editPrices", params:[fromDate: params.fromDate, toDate: params.toDate])
	}
	

    def delete(Long id) {
        def subspecimenInstance = Subspecimen.get(id)
        if (!subspecimenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
            redirect(action: "list")
            return
        }

        try {
            subspecimenInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def deleteFromCreate(Long id) {
		def subspecimenInstance = Subspecimen.get(id)
		if (!subspecimenInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
			redirect(controller: "specimen", action: "specimensProcuredToday")
			return
		}

		try {
			subspecimenInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
			redirect(action: "create", params: ['specimen.id':subspecimenInstance.specimen.id])
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'subspecimen.label', default: 'Subspecimen'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def reassign() {
		def specimenChtnId = params.specimenChtnId
		def investigatorId = params.long('investigatorId')
		def tissueQcMatchString = params.tissueQcMatch  // named tissueQcMatchString here to avoid errors with referencing tissueQcMatch in the criteria below
		
		def subspecimenCriteria = Subspecimen.createCriteria()
		def subspecimens
		if(specimenChtnId || investigatorId || tissueQcMatchString) {
			subspecimens = subspecimenCriteria.list {
				or {
					not { eq("status", SubspecimenStatus.findByDescription("Shipped")) }
					not { eq("status", SubspecimenStatus.findByDescription("Destroyed")) }
				}
				isNull("shippingCart")
				if(investigatorId) {
					eq("investigator", Investigator.read(investigatorId))
				}
				specimen {
					if(specimenChtnId) {
						ilike("specimenChtnId", "%" + specimenChtnId + "%")
					}

					if(tissueQcMatchString) {
						qcResult {
							tissueQcMatch {
								ilike("description", tissueQcMatchString + "%")
							}
						}
					}
				}
			}
		}
		
		[subspecimens: subspecimens, specimenChtnId: specimenChtnId, 
										investigatorId: investigatorId, 
										tissueQcMatch: tissueQcMatchString]
	}
	
	def performReassign() {
		// Request parameters from reassign, so they are intact during the redirect
		def specimenChtnId = params.specimenChtnId
		def investigatorId = params.long('investigatorId')
		def tissueQcMatch = params.tissueQcMatch
		//Request paramaters for performing the reassign
		def investigatorToAssignId = params.long('investigatorToAssignId')
		def investigatorToAssign
		if(investigatorToAssignId) {
			investigatorToAssign = Investigator.get(investigatorToAssignId)
		}
		else { // If there is not a valid investigatorId (greater than 0), then redirect back with a message
			flash.message = "Please choose an investigator to reassign to."
			redirect(action:"reassign", params:[investigatorId: investigatorId,
												tissueQcMatch: tissueQcMatch,
												specimenChtnId: specimenChtnId])
			return
		}
		
		def requestId = params.long('tissueRequest.id')
		def requestToAssign
		if(requestId) {
			requestToAssign = TissueRequest.get(requestId)
		}
		
		def alreadyShippedSubspecimens = new ArrayList()
		def reassignedSubspecimens = new ArrayList()
		def subspecimenIds = params.list('reassignBox').get(0)
		subspecimenIds.each { subspecimenId ->
			if (subspecimenId.key.isLong() && "on".equals(subspecimenId.value)) {
				def subspecimenInstance = Subspecimen.get(subspecimenId.key.toLong())
				def subspecimenCriteria = Subspecimen.createCriteria()
				def subspecimenShippedList = subspecimenCriteria.list {
					eq("specimen", subspecimenInstance.specimen)
					eq("investigator", investigatorToAssign)
					eq("status", SubspecimenStatus.findByDescription("Shipped"))
				}
				if(subspecimenShippedList) {
					// Investigator already has a subspecimen shipped from the same specimen, so do not want to reassign
					subspecimenShippedList.each { subspecimen -> // Add each subspecimen to the list to be printed later
						// Only add if not already in the list
						if(!alreadyShippedSubspecimens.contains(subspecimen)) {
							alreadyShippedSubspecimens.add(subspecimen)
						}
					}
				}
				else { // Criteria result is empty, so can reassign
					if(investigatorToAssign.chtnIdPrefix == InvestigatorChtnIdPrefix.findByPrefix("G") && investigatorToAssign.chtnId == "825") {
						subspecimenInstance.price = null
						subspecimenInstance.datePriceUpdated = new Date()
					}
					subspecimenInstance.investigator = investigatorToAssign
					subspecimenInstance.status = SubspecimenStatus.findByDescription("Assigned")
					subspecimenInstance.statusChangeDate = new Date()
					//Need to update Tissue Request
					if(requestToAssign) {
						subspecimenInstance.tissueRequest = requestToAssign
					}
					else { // There is no tissue request, so make sure the old one gets cleared
						subspecimenInstance.tissueRequest = null
					}
					reassignedSubspecimens.add(subspecimenInstance)
				}
				
			}
		}
		//Create the flash.message if the Investigator already has 1+ subspecimens shipped from the same specimen
		def flashMessageString
		if(alreadyShippedSubspecimens) {
			flashMessageString = "Investigator " + investigatorToAssign + " already has the following Subspecimens Shipped: "
			alreadyShippedSubspecimens.eachWithIndex { subspecimen, index ->
				if(index > 0) { // Continuing the flashMessageString
					flashMessageString += " | " + subspecimen.specimen.specimenChtnId + " - " + subspecimen.subspecimenChtnId
				}
				else { // First element to add to the flashMessageString
					flashMessageString += subspecimen.specimen.specimenChtnId + " - " + subspecimen.subspecimenChtnId
				}
			}
			flashMessageString += ".  "
			flash.message = flashMessageString
		}
		if(reassignedSubspecimens) {
			flashMessageString = "Investigator " + investigatorToAssign + " has had the following subspecimens reassigned to him/her: "
			reassignedSubspecimens.eachWithIndex { subspecimen, index ->
				if(index > 0) { // Continuing the flashMessageString
					flashMessageString += " | " + subspecimen.specimen.specimenChtnId + " - " + subspecimen.subspecimenChtnId
				}
				else { // First element to add to the flashMessageString
					flashMessageString += subspecimen.specimen.specimenChtnId + " - " + subspecimen.subspecimenChtnId
				}
			}
			flashMessageString += ".  "
			if(flash.message) { //There is already stuff in the flash.message
				flash.message += flashMessageString
			}
			else { //There is no flash.message yet
				flash.message = flashMessageString
			}
			
		}
		
		
		redirect(action:"reassign", params:[investigatorId: investigatorId, 
											tissueQcMatch: tissueQcMatch,
											specimenChtnId: specimenChtnId])
	}
	
	def discard() {
		def specimenChtnId = params.specimenChtnId
		def investigatorId = params.long('investigatorId')
		def tissueQcMatchId = params.long('tissueQcMatchId')
		
		def subspecimenCriteria = Subspecimen.createCriteria()
		def subspecimens
		if(specimenChtnId || investigatorId || tissueQcMatchId) {
			subspecimens = subspecimenCriteria.list {
				or {
					not { eq("status", SubspecimenStatus.findByDescription("Shipped")) }
					not { eq("status", SubspecimenStatus.findByDescription("Destroyed")) }
				}
				isNull("shippingCart")
				if(investigatorId) {
					eq("investigator", Investigator.read(investigatorId))
				}
				specimen {
					if(specimenChtnId) {
						ilike("specimenChtnId", "%" + specimenChtnId + "%")
					}
					if(tissueQcMatchId) {
						qcResult {
							eq("tissueQcMatch", TissueQcMatch.read(tissueQcMatchId))
						}
					}
				}
			}
		}
		
		[subspecimens: subspecimens, specimenChtnId: specimenChtnId, investigatorId: investigatorId, tissueQcMatchId: tissueQcMatchId]
	}
	
	def performDiscard() {
		def subspecimenIds = params.list('discardBox').get(0)
		subspecimenIds.each { subspecimenId ->
			if (subspecimenId.key.isLong() && "on".equals(subspecimenId.value)) {
				def subspecimen = Subspecimen.get(subspecimenId.key.toLong())
				subspecimen.investigator = null
				subspecimen.tissueRequest = null
				subspecimen.status = SubspecimenStatus.findByDescription("Destroyed")
				subspecimen.statusChangeDate = new Date()
			  }
		}
		
		redirect(action:"discard", params:[specimenChtnId: params.specimenChtnId, investigatorId: params.investigatorId, tissueQcMatchId: params.tissueQcMatchId])
	}
	
	def contactExternalFresh() {
		def subspecimenCriteria = Subspecimen.createCriteria()
		def subspecimenInstanceList
		subspecimenInstanceList = subspecimenCriteria.list {
			eq("preparationType", PreparationType.findByDescription("Fresh"))
			or {
				eq("called", false)
				isNull("called")
			}
			isNotNull("investigator")
			investigator {
				or {
					eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("A"))
					eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("AC"))
				}
				not { eq("lastName", "susztak", [ignoreCase: true]) }
			}
			specimen {
				procedure {
					procedureEventInstitution {
						eq("description", "OSU")
					}
				}
			}
		}
		
		[subspecimenInstanceList: subspecimenInstanceList]
	}
	
	def contactInternalFresh() {
		def subspecimenCriteria = Subspecimen.createCriteria()
		def subspecimenInstanceList
		subspecimenInstanceList = subspecimenCriteria.list {
			eq("preparationType", PreparationType.findByDescription("Fresh"))
			or {
				eq("called", false)
				isNull("called")
			}
			isNotNull("investigator")
			investigator {
				not { eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("A")) }
				not { eq("chtnIdPrefix", InvestigatorChtnIdPrefix.findByPrefix("AC")) }
				not { eq("lastName", "christofi", [ignoreCase: true])}
			}
			specimen {
				procedure {
					procedureEventInstitution {
						eq("description", "OSU")
					}
				}
			}
		}
		
		[subspecimenInstanceList: subspecimenInstanceList]
	}
	
	def contactEmail() {
		
		def subspecimenCriteria = Subspecimen.createCriteria()
		def subspecimenInstanceList
		subspecimenInstanceList = subspecimenCriteria.list {
			isNotNull("investigator")
			isNotNull("tissueRequest")
			or {
				eq("emailed", false)
				isNull("emailed")
			}
			tissueRequest {
				eq("contactMethod", TissueRequestContactMethod.findByDescription("Email"))
			}
			specimen {
				procedure {
					procedureEventInstitution {
						eq("description", "OSU")
					}
				}
			}
		}
		
		[subspecimenInstanceList: subspecimenInstanceList]
	}
	
	def addNewInvestigator() {
		def subspecimenParamMap = params as JSON
		redirect(controller:"investigator", action:"create", params:[subspecimenParamMap:subspecimenParamMap])
		return
	}
	
	def addNewTissueRequest() {
		def subspecimenParamMap = params as JSON
		redirect(controller:"tissueRequest", action:"create", params:['investigator.id': params.investigator.id, subspecimenParamMap:subspecimenParamMap])
		return
	}
	
	def ajaxUpdateMeasurementDisplay() {
		def measurementType = params.measurementType
		def subspecimenId
		if(params.subspecimenId) {
			subspecimenId = params.long('subspecimenId')
			def subspecimenInstance = Subspecimen.read(subspecimenId)
			switch(measurementType) {
				case 'weight':
					render(template:"weight", model:[subspecimenInstance:subspecimenInstance])
					break
				case 'volume':
					render(template:"volume", model:[subspecimenInstance:subspecimenInstance])
					break
				case 'size':
					render(template:"size", model:[subspecimenInstance:subspecimenInstance])
					break
			}
			return
		}
		else {
			switch(measurementType) {
				case 'weight':
					render(template:"weight", model:[weightUnitId: WeightMeasurement.findByDescription("grams").id])
					break
				case 'volume':
					render(template:"volume", model:[volumeUnitId: VolumeMeasurement.findByDescription("milliliters").id])
					break
				case 'size':
					render(template:"size")
					break
			}
			return
		}
		
	}
	
	def searchQc() {
		def subspecimens = [], fromDate, toDate, cal = Calendar.getInstance()

		fromDate = params.fromDate ? sdf.parse(params.fromDate) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')

		toDate = params.toDate ? sdf.parse(params.toDate) : new Date()
		cal.setTime(toDate)
		// we have to take the date parsed from the request params and convert that into a format that will work properly
		// when the dateCreated column has a time portion other than 00:00:00
		toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")

		if(params.tissueQcMatch) {
			subspecimens = subspecimenService.searchQc(fromDate, toDate, params.tissueQcMatch)
		}
		// fromDate and toDate are for the page reporting what dates it is searching for
		// fromDateTextField and toDateTextField are for preserving the user's criteria selections
		[subspecimens: subspecimens, tissueQcMatch: params.tissueQcMatch, fromDate: fromDate, fromDateTextField: params.fromDate, toDate: toDate, toDateTextField: params.toDate]
	}
	
	def apqi() {
		session['apqiSearchParams'] = params

		def fromDate, toDate, results

		if(params.fromDate) {
			fromDate = sdf.parse(params.fromDate)
		}

		if(params.toDate) {
			toDate = sdf.parse(params.toDate)
		}

		if(fromDate && toDate){ //Both dates need to be provided
			results = subspecimenService.apqiSearch(params)

			[distinctPatients: results.distinctPatients, totalQcPerformed: results.totalQcPerformed, numberQcPassed: results.numberQcPassed,
			 numberQcFailed: results.numberQcFailed, totalSubspecimensProcured: results.totalSubspecimensProcured,
			 totalAutopsySubspecimens: results.totalAutopsySubspecimens,
			 distinctAutopsyPatients: results.distinctAutopsyPatients, partsPerCase: results.partsPerCase,
			 displayCounts: results.displayCounts, procurementResultQuantityList: results.procurementResultQuantityList,
			 procedureNameSubNameByResultQuantityList: results.procedureNameSubNameByResultQuantityList,
			 originQuantityList: results.originQuantityList,
			 fromDate: fromDate, fromDateTextField: params.fromDate, toDate: toDate, toDateTextField: params.toDate]

		} else {
			flash.message = "Both From Date and To Date need to be provided"
		}
		
	}
	
	def chtnAnnualReport() {
		def results = subspecimenService.chtnAnnualReport(params)
		
		[displayCounts: results.displayCounts,
			subspecimenCountListProcuredProcedureType: results.subspecimenCountListProcuredProcedureType,
			subspecimenDistribtedListChtnIdPrefix: results.subspecimenDistribtedListChtnIdPrefix,
			subspecimenDistribtedListCategoryPrepTypeMedia: results.subspecimenDistribtedListCategoryPrepTypeMedia,
			chartReviewShippedQuantity: results.chartReviewShippedQuantity,
			fromDate: results.fromDate, fromDateTextField: params.fromDate, toDate: results.toDate, toDateTextField: params.toDate]
	}
	
	def subcontractQuery() {
		def fromDate, toDate, organization, subspecimens = []

		if(params.fromDate) {
			fromDate = sdf.parse(params.fromDate)
		}
		
		if(params.toDate) {
			toDate = sdf.parse(params.toDate)
		}

		organization = params.organization

		if( organization && fromDate && toDate ) {
			subspecimens = subspecimenService.subcontractQuery(organization, fromDate, toDate)

			flash.message = ""
		}
		else { //Not all parameters were chosen
			flash.message = "Please select an organization and a full date range (To and From dates)"
		}
		
		[subspecimens: subspecimens, fromDate: fromDate, fromDateTextField: params.fromDate, 
			toDate: toDate, toDateTextField: params.toDate,
			organization: organization]
	}
	
	def internalBilling() {
		def fromDate
		def toDate
		if(params.fromDate) {
			fromDate = sdf.parse(params.fromDate)
		}

		if(params.toDate) {
			toDate = sdf.parse(params.toDate)
		}
		def organization = params.organization

		def subspecimens

		if( organization && fromDate && toDate ) {
			subspecimens = subspecimenService.internalBilling(organization, fromDate, toDate)
			flash.message = ""
		}
		else { //Not all parameters were chosen
			flash.message = "Please select an organization and a full date range (To and From dates)"
		}
		
		[subspecimens: subspecimens, fromDate: fromDate, fromDateTextField: params.fromDate,
			toDate: toDate, toDateTextField: params.toDate,
			organization: organization]
	}
	
	def searchProcurement() {
		def subspecimens = [], fromDate, toDate, cal = Calendar.getInstance()

		fromDate = params.fromDate ? sdf.parse(params.fromDate) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')

		toDate = params.toDate ? sdf.parse(params.toDate) : new Date()
		cal.setTime(toDate)
		toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")

		if( fromDate || toDate ) {
			subspecimens = subspecimenService.searchProcurement(params.investigatorId, fromDate, toDate)

			flash.message = ""
		}
		else { //Not all required parameters were chosen
			flash.message = "Please select a partial date range (To or From dates)"
		}
		
		[subspecimens: subspecimens, fromDate: fromDate, fromDateTextField: params.fromDate,
			toDate: toDate, toDateTextField: params.toDate,
			investigatorId: params.investigatorId]
	}
	
	def searchDistribution() {
		def fromDate
		def toDate
		if(params.fromDate) {
			fromDate = sdf.parse(params.fromDate)
		}
		
		if(params.toDate) {
			toDate = sdf.parse(params.toDate)
		}
		def investigatorId = params.long('investigatorId')

		def subspecimens
		if( investigatorId && (fromDate || toDate) ) {
			subspecimens = subspecimenService.searchDistribution(investigatorId, fromDate, toDate)

			flash.message = ""
		}
		else { //Not all required parameters were chosen
			flash.message = "Please select an investigator and a partial date range (To or From dates)"
		}
		
		[subspecimens: subspecimens, fromDate: fromDate, fromDateTextField: params.fromDate,
			toDate: toDate, toDateTextField: params.toDate,
			investigatorId: investigatorId]
	}

	def search() {
		session['generalSubspecimenSearchParams'] = params

		def subspecimens

		if(params.formSubmitted == 'true') {
			subspecimens = subspecimenService.generalSearch(params)
		}

		[
			subspecimens: subspecimens, mrn: params.mrn, genderId: params.genderId, raceId: params.raceId,
			originInstitutionId: params.originInstitutionId, patientAge: params.patientAge, patientAgeUnitId: params.patientAgeUnitId,
			procedureTypeId: params.procedureTypeId,
			investigatorChtnIdPrefixId: params.list('investigatorChtnIdPrefixId'), 
			investigatorChtnId: params.investigatorChtnId, 
			investigatorFirstName: params.investigatorFirstName, investigatorLastName: params.investigatorLastName,
			institutionTypeId: params.institutionTypeId,
			specimenChtnId: params.specimenChtnId, preliminaryPrimaryAnatomicSite: params.preliminaryPrimaryAnatomicSite,
			finalPrimaryAnatomicSite: params.finalPrimaryAnatomicSite,
			preliminaryTissueTypeId: params.preliminaryTissueTypeId, finalTissueTypeId: params.finalTissueTypeId,
			primaryOrMetsId: params.primaryOrMetsId,
			preliminaryDiagnosis: params.preliminaryDiagnosis, finalDiagnosis: params.finalDiagnosis,
			preliminarySubDiagnosis: params.preliminarySubDiagnosis, finalSubDiagnosis: params.finalSubDiagnosis,
			preliminaryModDiagnosis: params.preliminaryModDiagnosis, finalModDiagnosis: params.finalModDiagnosis,
			preparationTypeId: params.preparationTypeId,
			preparationMediaId: params.preparationMediaId, subspecimenStatusId: params.subspecimenStatusId,
			tissueQcMatchPassFail: params.tissueQcMatchPassFail, 
			shipDateFromTextField: params.shipDateFromTextField, shipDateToTextField: params.shipDateToTextField,
			procurementDateFromTextField: params.procurementDateFromTextField, procurementDateToTextField: params.procurementDateToTextField,
		 	timePreparedFrom: params.timePreparedFrom, timePreparedTo: params.timePreparedTo,
		 	timeReceivedFrom: params.timeReceivedFrom, timeReceivedTo: params.timeReceivedTo,
			techInitialsId: params.techInitialsId, qcFollowUpAction: params.qcFollowUpAction, specimenCategoryId: params.specimenCategoryId,
			invoiceDateFromTextField: params.invoiceDateFromTextField, invoiceDateToTextField: params.invoiceDateToTextField,
			weight: params.weight, volume: params.volume, sizeWidth: params.sizeWidth, sizeLength: params.sizeLength, sizeHeight: params.sizeHeight,
			tissueRequestTissueQuestId: params.tissueRequestTissueQuestId,
			finalMolecularStatusErId: params.finalMolecularStatusErId, finalMolecularStatusPrId: params.finalMolecularStatusPrId,
			finalMolecularStatusHer2Id: params.finalMolecularStatusHer2Id,
			cancerId: params.cancerId, metsToAnatomicSite: params.metsToAnatomicSite ? params.metsToAnatomicSite.toInteger() : params.metsToAnatomicSite,
			showMrn: params.showMrn, showGender: params.showGender, showRace: params.showRace, showPatientAge: params.showPatientAge,
			showOriginInstitution: params.showOriginInstitution,
			showProcedureType: params.showProcedureType, showInvestigatorChtnId: params.showInvestigatorChtnId,
			showInvestigatorFirstName: params.showInvestigatorFirstName, showInvestigatorLastName: params.showInvestigatorLastName,
			showInvestigatorInstitutionType: params.showInvestigatorInstitutionType,
			showSpecimenChtnId: params.showSpecimenChtnId, showProcurementDate: params.showProcurementDate,
		 	showTimePreparedDate: params.showTimePreparedDate,
		 	showTimeReceivedDate: params.showTimeReceivedDate,
			showPreliminaryPrimaryAnatomicSite: params.showPreliminaryPrimaryAnatomicSite,
			showFinalPrimaryAnatomicSite: params.showFinalPrimaryAnatomicSite,
			showPreliminaryTissueType: params.showPreliminaryTissueType, showFinalTissueType: params.showFinalTissueType, 
			showPrimaryOrMets: params.showPrimaryOrMets,
			showPreliminaryDiagnosis: params.showPreliminaryDiagnosis, showFinalDiagnosis: params.showFinalDiagnosis,
			showPreliminarySubDiagnosis: params.showPreliminarySubDiagnosis, showFinalSubDiagnosis: params.showFinalSubDiagnosis,
			showPreliminaryModDiagnosis: params.showPreliminaryModDiagnosis, showFinalModDiagnosis: params.showFinalModDiagnosis,
			showTissueQcMatch: params.showTissueQcMatch,
			showPreparationType: params.showPreparationType, showPreparationMedia: params.showPreparationMedia,
			showWeight: params.showWeight, showVolume: params.showVolume, showSizeWidth: params.showSizeWidth, showSizeLength: params.showSizeLength,
			showSizeHeight: params.showSizeHeight,
			showSubspecimenStatus: params.showSubspecimenStatus, showShipDate: params.showShipDate,
			showInvoiceDate: params.showInvoiceDate,
			showFinalMolecularStatusEr: params.showFinalMolecularStatusEr, showFinalMolecularStatusPr: params.showFinalMolecularStatusPr,
			showFinalMolecularStatusHer2: params.showFinalMolecularStatusHer2,
			showTissueRequestTissueQuestId: params.showTissueRequestTissueQuestId,
			showCancer: params.showCancer, showSpecimenCategory: params.showSpecimenCategory, showQCFollowUpAction: params.showQCFollowUpAction,
			showMetsToAnatomicSite: params.showMetsToAnatomicSite, showProcurementTechInitials: params.showProcurementTechInitials,
			showSubspecimenLabelComment: params.showSubspecimenLabelComment, subspecimenLabelComment: params.subspecimenLabelComment
		]
	}
	
}
