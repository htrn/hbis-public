package hbis

import org.springframework.dao.DataIntegrityViolationException

class ConsentController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [consentInstanceList: Consent.list(params), consentInstanceTotal: Consent.count()]
    }

    def create() {
		def consentInstance = new Consent(params)
		consentInstance.method = ConsentMethod.findByDescription("Paper Form")

        [consentInstance: consentInstance, patientInstance: consentInstance.patient]
    }
	
	def patientSearch() {
		
	}

    def save() {
        def consentInstance = new Consent(params)
        if (!consentInstance.save(flush: true)) {
            render(view: "create", model: [consentInstance: consentInstance, patientInstance: consentInstance.patient])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'consent.label', default: 'Consent'), consentInstance.id])
		// Depending on which button is pressed, redirect to the appropriate place
		if(params.saveAndCreateProcedure) {
			redirect(controller:"procedure", action: "create", params:['patient.id': consentInstance.patient.id])
			return
		}
		redirect(action: "show", id: consentInstance.id)
    }
	
    def show(Long id) {
        def consentInstance = Consent.get(id)
        if (!consentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consent.label', default: 'Consent'), id])
            redirect(action: "list")
            return
        }

        [consentInstance: consentInstance, patientInstance: consentInstance.patient]
    }

    def edit(Long id) {
        def consentInstance = Consent.get(id)
        if (!consentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consent.label', default: 'Consent'), id])
            redirect(action: "list")
            return
        }
		
		[consentInstance: consentInstance, patientInstance: consentInstance.patient]
    }

    def update(Long id, Long version) {
        def consentInstance = Consent.get(id)
        if (!consentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consent.label', default: 'Consent'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (consentInstance.version > version) {
                consentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'consent.label', default: 'Consent')] as Object[],
                          "Another user has updated this Consent while you were editing")
				
                render(view: "edit", model: [consentInstance: consentInstance, patientInstance: consentInstance.patient])
                return
            }
        }

        consentInstance.properties = params

        if (!consentInstance.save(flush: true)) {
			render(view: "edit", model: [consentInstance: consentInstance, patientInstance: consentInstance.patient])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'consent.label', default: 'Consent'), consentInstance.id])
        redirect(action: "show", id: consentInstance.id)
    }

    def delete(Long id) {
        def consentInstance = Consent.get(id)
        if (!consentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consent.label', default: 'Consent'), id])
            redirect(action: "list")
            return
        }

        try {
            consentInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'consent.label', default: 'Consent'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'consent.label', default: 'Consent'), id])
            redirect(action: "show", id: id)
        }
    }
}
