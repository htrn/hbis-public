package hbis
import grails.converters.JSON

class PrimaryMetsController {
	
	static scaffold = true

    def index() { }
	
	def ajaxMetsToAnatomic() {
		if(params.primaryMetsId != "null") {
			def primaryMetsDescription = PrimaryMets.read(params.primaryMetsId).description
			if(primaryMetsDescription == 'Metastatic') {
				def metsToAnatomicSiteList = hbis.MetsToAnatomicSite.list()
				render(template:"/specimen/selectMetsToAnatomicSite", model:[metsToAnatomicSiteList:metsToAnatomicSiteList])
				return
			}
			else {
				render("Choose Metastatic to display these options")
				return
			}
		}
		else {
			render("Choose Metastatic to display these options")
			return
		}
		
	}
}
