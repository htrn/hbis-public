package hbis

import org.springframework.dao.DataIntegrityViolationException

class SurgicalHistoryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [surgicalHistoryInstanceList: SurgicalHistory.list(params), surgicalHistoryInstanceTotal: SurgicalHistory.count()]
    }

    def create() {
		def surgicalHistoryInstance = new SurgicalHistory(params)
        [surgicalHistoryInstance: surgicalHistoryInstance,
			chartReviewInstance: surgicalHistoryInstance.chartReview,
			procedureInstance: surgicalHistoryInstance.chartReview.procedure,
			patientInstance: surgicalHistoryInstance.chartReview.procedure.patient]
    }

    def save() {
        def surgicalHistoryInstance = new SurgicalHistory(params)
        if (!surgicalHistoryInstance.save(flush: true)) {
            render(view: "create", model: [surgicalHistoryInstance: surgicalHistoryInstance,
											chartReviewInstance: surgicalHistoryInstance.chartReview,
											procedureInstance: surgicalHistoryInstance.chartReview.procedure,
											patientInstance: surgicalHistoryInstance.chartReview.procedure.patient])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'surgicalHistory.label', default: 'SurgicalHistory'), surgicalHistoryInstance.id])
		// Depending on which button is pressed, redirect to the appropriate place
		if(params.saveAndCreateAnotherSurgicalHistory) {
			redirect(action: "create", params: ['chartReview.id':surgicalHistoryInstance.chartReview.id])
			return
		}
		redirect(controller: "chartReview", action: "show", id: surgicalHistoryInstance.chartReview.id)
    }
	
    def show(Long id) {
        def surgicalHistoryInstance = SurgicalHistory.get(id)
        if (!surgicalHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'surgicalHistory.label', default: 'SurgicalHistory'), id])
            redirect(action: "list")
            return
        }

        [surgicalHistoryInstance: surgicalHistoryInstance,
			chartReviewInstance: surgicalHistoryInstance.chartReview,
			procedureInstance: surgicalHistoryInstance.chartReview.procedure,
			patientInstance: surgicalHistoryInstance.chartReview.procedure.patient]
    }

    def edit(Long id) {
        def surgicalHistoryInstance = SurgicalHistory.get(id)
        if (!surgicalHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'surgicalHistory.label', default: 'SurgicalHistory'), id])
            redirect(action: "list")
            return
        }

        [surgicalHistoryInstance: surgicalHistoryInstance,
			chartReviewInstance: surgicalHistoryInstance.chartReview,
			procedureInstance: surgicalHistoryInstance.chartReview.procedure,
			patientInstance: surgicalHistoryInstance.chartReview.procedure.patient]
    }

    def update(Long id, Long version) {
        def surgicalHistoryInstance = SurgicalHistory.get(id)
        if (!surgicalHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'surgicalHistory.label', default: 'SurgicalHistory'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (surgicalHistoryInstance.version > version) {
                surgicalHistoryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'surgicalHistory.label', default: 'SurgicalHistory')] as Object[],
                          "Another user has updated this SurgicalHistory while you were editing")
                render(view: "edit", model: [surgicalHistoryInstance: surgicalHistoryInstance,
												chartReviewInstance: surgicalHistoryInstance.chartReview,
												procedureInstance: surgicalHistoryInstance.chartReview.procedure,
												patientInstance: surgicalHistoryInstance.chartReview.procedure.patient])
                return
            }
        }

        surgicalHistoryInstance.properties = params

        if (!surgicalHistoryInstance.save(flush: true)) {
            render(view: "edit", model: [surgicalHistoryInstance: surgicalHistoryInstance,
											chartReviewInstance: surgicalHistoryInstance.chartReview,
											procedureInstance: surgicalHistoryInstance.chartReview.procedure,
											patientInstance: surgicalHistoryInstance.chartReview.procedure.patient])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'surgicalHistory.label', default: 'SurgicalHistory'), surgicalHistoryInstance.id])
        redirect(action: "show", id: surgicalHistoryInstance.id)
    }

    def delete(Long id) {
        def surgicalHistoryInstance = SurgicalHistory.get(id)
        if (!surgicalHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'surgicalHistory.label', default: 'SurgicalHistory'), id])
            redirect(action: "list")
            return
        }

        try {
            surgicalHistoryInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'surgicalHistory.label', default: 'SurgicalHistory'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'surgicalHistory.label', default: 'SurgicalHistory'), id])
            redirect(action: "show", id: id)
        }
    }
}
