package hbis

import org.springframework.dao.DataIntegrityViolationException
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import java.text.SimpleDateFormat

class QcBatchController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }
	
	def jasperService
	def qcReportAll() {
		def qcBatch = QcBatch.read(params.qcBatchId)

		//Just need to pass the specimens in the QC Batch to the report
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:qcBatch.specimens])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [qcBatchInstanceList: QcBatch.list(params), qcBatchInstanceTotal: QcBatch.count()]
    }
	
	def listBatchesInPreparation() {
		def qcBatchCriteria = QcBatch.createCriteria()
		def qcBatchInstanceList = qcBatchCriteria.list {
			eq("status", QcBatchStatus.findByDescription("Preparing to Send to Scan"))
			order("dateCreated", "asc")
			order("id", "asc")
		}
		[qcBatchInstanceList: qcBatchInstanceList]
	}
	
	def listBatchesToScan() {
		def qcBatchCriteria = QcBatch.createCriteria()
		def qcBatchInstanceList = qcBatchCriteria.list {
			eq("status", QcBatchStatus.findByDescription("Sent to Scan"))
			order("dateCreated", "asc")
			order("id", "asc")
		}
		[qcBatchInstanceList: qcBatchInstanceList]
	}
	
	def listBatchesForImageAnalysis() {
		def qcBatchCriteria = QcBatch.createCriteria()
		def qcBatchInstanceList = qcBatchCriteria.list {
			eq("status", QcBatchStatus.findByDescription("Scanning Complete"))
			order("dateCreated", "asc")
			order("id", "asc")
		}
		[qcBatchInstanceList: qcBatchInstanceList]
	}
	
	def listBatchesForPathologistReview() {
		def qcBatchCriteria = QcBatch.createCriteria()
		def qcBatchInstanceList = qcBatchCriteria.list {
			eq("status", QcBatchStatus.findByDescription("Image Analysis Complete"))
			order("dateCreated", "asc")
			order("id", "asc")
		}
		[qcBatchInstanceList: qcBatchInstanceList]
	}
	
	def listCompleteBatches() {
		def qcBatchCriteria = QcBatch.createCriteria()
		def qcBatchInstanceList = qcBatchCriteria.list {
			eq("status", QcBatchStatus.findByDescription("Complete"))
			order("dateCreated", "asc")
			order("id", "asc")
		}
		[qcBatchInstanceList: qcBatchInstanceList]
	}
	
	def viewBatchToScan(Long id) {
		def qcBatchInstance = QcBatch.get(id)
		qcBatchInstance.specimens.each { specimenInstance->
			if(!specimenInstance.qcResult) {
				QcResult qcResult = new QcResult()
				qcResult.specimen = specimenInstance
				qcResult.linkSvsImage = "file:///H:/CHTN%20QC/_CHTN%20QC%20Files/" + specimenInstance.specimenChtnId + ".svs"
				qcResult.linkOverlayImage = "file:///H:/CHTN%20QC/_CHTN%20QC%20Files/" + specimenInstance.specimenChtnId + ".jpg"
				qcResult.linkPathReport = "file:///H:/CHTN%20QC/_CHTN%20QC%20Files/" + specimenInstance.specimenChtnId[0..specimenInstance.specimenChtnId.length() - 2] + ".pdf"
				if(!qcResult.save(flush:true)) {
					println("Saving qcResult failed in QcBatch controller")
				}
				else {
				}
			}
		}
		[qcBatchInstance: qcBatchInstance]
	}
	
	def viewBatchForImageAnalysis(Long id) {
		def qcBatchInstance = QcBatch.read(id)
		
		[qcBatchInstance: qcBatchInstance]
	}
	
	def viewBatchForPathologistReview(Long id) {
		def qcBatchInstance = QcBatch.read(id)
		[qcBatchInstance: qcBatchInstance]
	}
	
	def viewCompleteBatch(Long id) {
		def qcBatchInstance = QcBatch.read(id)
		[qcBatchInstance: qcBatchInstance]
	}
	
	def viewBatch(Long id) {
		def qcBatchInstance = QcBatch.read(id)
		[qcBatchInstance: qcBatchInstance]
	}

    def create() {
        [qcBatchInstance: new QcBatch(params)]
    }

    def save() {
        def qcBatchInstance = new QcBatch(params)
        if (!qcBatchInstance.save(flush: true)) {
            render(view: "create", model: [qcBatchInstance: qcBatchInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), qcBatchInstance.id])
        redirect(action: "show", id: qcBatchInstance.id)
    }
	
	def saveAndAddSpecimens() {
		def specimenCriteria = Specimen.createCriteria()
		//Check if any specimens available with qcMethod of Pathologist Review that do not have a qcBatch
		//	with subspecimenId of Q and status of QC
		def resultQcSpecimens = specimenCriteria.list {
			isNull("qcBatch")
			eq("qcMethod", QcMethod.findByDescription("Pathologist Review"))
			subspecimens {
				eq("subspecimenChtnId", "Q", [ignoreCase:true])
				or {
					eq("status", SubspecimenStatus.findByDescription("QC"))
					eq("status", SubspecimenStatus.findByDescription("Assigned"))
				}
			}
			order("specimenChtnId", "asc")
			order("procurementDate", "asc")
		}
		if(!resultQcSpecimens) {
			flash.message = "All Specimens with a QC Method of Pathologist Review and a Q subspecimen with a status of QC or Assigned have been in a QC Batch"
			redirect(action:"listBatchesInPreparation")
			return
		}
		def qcBatchInstance = new QcBatch(params)
		qcBatchInstance.status = QcBatchStatus.findByDescription("Preparing to Send to Scan")
		if (!qcBatchInstance.save(flush: true)) {
			render(view: "listBatchesInPreparation", model: [qcBatchInstance: qcBatchInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), qcBatchInstance.id])
		redirect(action: "viewSpecimensToAdd", id: qcBatchInstance.id)
	}

    def show(Long id) {
        def qcBatchInstance = QcBatch.get(id)
        if (!qcBatchInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), id])
            redirect(action: "list")
            return
        }

        [qcBatchInstance: qcBatchInstance]
    }

    def edit(Long id) {
        def qcBatchInstance = QcBatch.get(id)
        if (!qcBatchInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), id])
            redirect(action: "list")
            return
        }

        [qcBatchInstance: qcBatchInstance]
    }

    def update(Long id, Long version) {
        def qcBatchInstance = QcBatch.get(id)
        if (!qcBatchInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (qcBatchInstance.version > version) {
                qcBatchInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'qcBatch.label', default: 'QcBatch')] as Object[],
                          "Another user has updated this QcBatch while you were editing")
				if(params.view == "scanning") {
					render(view:"listBatchesToScan", model: [qcBatchInstance: qcBatchInstance])
					return
				}
				else if (params.view == "preparation") {
					redirect(action:"listBatchesInPreparation", model: [qcBatchInstance: qcBatchInstance])
					return
				}
				else if (params.view == "imageAnalysis") {
					redirect(action:"listBatchesForImageAnalysis", model: [qcBatchInstance: qcBatchInstance])
					return
				}
				else if (params.view == "pathologistReview") {
					redirect(action:"listBatchesForPathologistReview", model: [qcBatchInstance: qcBatchInstance])
					return
				}
                render(view: "edit", model: [qcBatchInstance: qcBatchInstance])
                return
            }
        }

        qcBatchInstance.properties = params
		if(params.view == "scanning" || params.view == "preparation" || params.view == "imageAnalysis" || params.view == "pathologistReview") {
			if(params.qcBatchStatus) { //Reaching here from the respective views that are sending in an updated status, which is the description of the status needed
				qcBatchInstance.status = QcBatchStatus.findByDescription(params.qcBatchStatus)
			}
		}
		if (params.view == "pathologistReview") { //Fields specific to pathologistReview
			qcBatchInstance.datePathologistConfirmationComplete = new Date().clearTime()
		}

        if (!qcBatchInstance.save(flush: true)) {
            render(view: "edit", model: [qcBatchInstance: qcBatchInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), qcBatchInstance.id])
		if(params.view == "scanning") {
			redirect(action:"listBatchesToScan")
			return
		}
		else if (params.view == "preparation") {
			redirect(action:"listBatchesInPreparation")
			return
		}
		else if (params.view == "imageAnalysis") {
			redirect(action:"listBatchesForImageAnalysis")
			return
		}
		else if (params.view == "pathologistReview") {
			redirect(action:"listBatchesForPathologistReview")
			return
		}
        redirect(action: "show", id: qcBatchInstance.id)
    }

    def delete(Long id) {
        def qcBatchInstance = QcBatch.get(id)
        if (!qcBatchInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), id])
            redirect(action: "list")
            return
        }

        try {
            qcBatchInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'qcBatch.label', default: 'QcBatch'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def viewSpecimensToAdd(Long id) {
		def qcBatchInstance = QcBatch.read(id)
		def fromDate = params.date('fromDate')
		def toDate = params.date('toDate')
		def resultQcSpecimens
		
		def specimenCriteria = Specimen.createCriteria()
		resultQcSpecimens = specimenCriteria.list {
			subspecimens {
				eq("subspecimenChtnId", "Q", [ignoreCase:true])
				or {
					eq("status", SubspecimenStatus.findByDescription("QC"))
					eq("status", SubspecimenStatus.findByDescription("Assigned"))
				}
			}
			
			isNull("qcBatch")
			eq("qcMethod", QcMethod.findByDescription("Pathologist Review"))
			
			if(!fromDate && !toDate) {
				//ge("procurementDate", lastTwoWeeks)
			}
			else {
				if(fromDate && !toDate) {
					ge("procurementDate", fromDate)
				}
				else if (!fromDate && toDate) {
					le("procurementDate", toDate)
				}
				else { //Both fromDate and toDate are provided
					between("procurementDate", fromDate, toDate)
				}
			}
			order("specimenChtnId", "asc")
			order("procurementDate", "asc")
		}
		[qcBatchInstance: qcBatchInstance, resultQcSpecimens: resultQcSpecimens, fromDate: fromDate, toDate: toDate]
	}
	
	def addRemoveSpecimen() {
		def qcBatchInstance = QcBatch.get(params.long('qcBatchId')), specimens
		//def specimenInstance = Specimen.get(params.long('specimenId'))
		
		def qcBatchVersion = params.long('qcBatchVersion')
		//def specimenVersion = params.long('specimenVersion')
		def hasError = false
		
		if (qcBatchVersion != null) {
			if (qcBatchInstance.version > qcBatchVersion) {
				qcBatchInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'qcBatch.label', default: 'QcBatch')] as Object[],
						  "Another user has updated this QcBatch while you were editing")
				hasError = true
			}
		}
		
//		if (specimenVersion != null) {
//			if (specimenInstance.version > specimenVersion) {
//				specimenInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
//						  [message(code: 'specimen.label', default: 'Specimen')] as Object[],
//						  "Another user has updated this Specimen while you were editing")
//				hasError = true
//			}
//		}
		
		if(hasError) {
			Integer max
			params.max = Math.min(max ?: 10, 100)
			render(view: "list", model: [qcBatchInstance: qcBatchInstance,
											specimenInstance: specimenInstance,
											qcBatchInstanceList: QcBatch.list(params),
											qcBatchInstanceTotal: QcBatch.count()])
			return
		}
		
		if (params.addRemove == 'Add') {
			specimens = Specimen.getAll(params.list('specimensToAdd'))

			specimens.each { specimen ->
				qcBatchInstance.addToSpecimens(specimen)
			}
		}
		else if (params.addRemove == 'Remove') {
			specimens = Specimen.getAll(params.list('specimensToRemove'))

			specimens.each { specimen ->
				qcBatchInstance.removeFromSpecimens(specimen)
			}
		}
		
		qcBatchInstance.save(flush:true)
		
		redirect(action: "viewSpecimensToAdd", id: qcBatchInstance.id, params:[fromDate: params.fromDate, toDate: params.toDate])
		
	}
	
	def search() {
		def sdf = new SimpleDateFormat("MM/dd/yyyy"), fromDate, toDate, cal = Calendar.getInstance()

		fromDate = params.fromDate ? sdf.parse(params.fromDate) : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')

		toDate = params.toDate ? sdf.parse(params.toDate) : new Date()
		cal.setTime(toDate)
		// we have to take the date parsed from the request params and convert that into a format that will work properly
		// when the dateCreated column has a time portion other than 00:00:00
		toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")

		def qcBatchStatus
		if(params.qcBatchStatusId != "null") {
			qcBatchStatus = QcBatchStatus.read(params.long('qcBatchStatusId'))
		}

		def qcBatchCriteria = QcBatch.createCriteria()
		def qcBatchInstanceList
		if(toDate || fromDate || qcBatchStatus || params.description || params.specimenChtnId) {
			qcBatchInstanceList = qcBatchCriteria.listDistinct {
				between("dateCreated", fromDate, toDate)

				if(qcBatchStatus) {
					eq("status", qcBatchStatus)
				}
				if(params.description) {
					ilike("description", "%" + params.description + "%")
				}
				if(params.specimenChtnId) {
					specimens {
						ilike("specimenChtnId", "%" + params.specimenChtnId + "%")
					}
				}

			}
		}
		
		[qcBatchInstanceList: qcBatchInstanceList, fromDateTextField: params.fromDate, 
			toDateTextField: params.toDate, qcBatchStatusId: params.qcBatchStatusId,
			description: params.description, specimenChtnId: params.specimenChtnId]
	}
}
