package hbis

class SpecimenCategoryController {
	
	static scaffold = true

    def index() { }
	
	def ajaxPreliminaryTissueType() {
		if(params.specimenCategoryId != "null") {
			def specimenCategory = SpecimenCategory.read(params.specimenCategoryId)
			if(['BLOOD', 'BUFFY COAT', 'PLASMA', 'SERUM', 'URINE'].contains(specimenCategory.description)){
				return render(template:"/specimen/selectPreliminaryTissueType", model:[preliminaryTissueTypeId: TissueType.findByDescription("NAT - Normal Adjacent Tissue")?.id])
			}else {
				render(template:"/specimen/selectPreliminaryTissueType", model:[preliminaryTissueTypeId: null])
				return
			}
		}
		
		//render(template:"/specimen/selectPreliminaryTissueType")
		return
	}
	
	def ajaxPreliminaryPrimaryAnatomicSite() {
		if(params.specimenCategoryId != "null") {
			def specimenCategory = SpecimenCategory.read(params.specimenCategoryId)

			if(['BLOOD', 'BUFFY COAT', 'PLASMA', 'SERUM'].contains(specimenCategory.description)){
				return render(template:"/specimen/selectPreliminaryPrimaryAnatomicSite", model:[preliminaryPrimaryAnatomicSiteId: AnatomicSite.findByDescription("BLOOD")?.id])
			}else if(specimenCategory.description == "URINE") {
				return	render(template:"/specimen/selectPreliminaryPrimaryAnatomicSite", model:[preliminaryPrimaryAnatomicSiteId: AnatomicSite.findByDescription("URINE")?.id])
			}else {
				return	render(template:"/specimen/selectPreliminaryPrimaryAnatomicSite", model:[preliminaryPrimaryAnatomicSiteId: null])
			}
		}
		//render(template:"/specimen/selectPreliminaryPrimaryAnatomicSite")
		return
	}
}
