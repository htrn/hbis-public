package hbis

import org.springframework.dao.DataIntegrityViolationException

class ProjectController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [projectInstanceList: Project.list(params), projectInstanceTotal: Project.count()]
    }

    def create() {
		//println("In Project, Specimen Db Id: " + params.specimenDbId)
        [projectInstance: new Project(params), subspecimenParamMap:params.subspecimenParamMap/*specimenDbId: params.specimenDbId*/]
    }

    def save() {
        def projectInstance = new Project(params)
        if (!projectInstance.save(flush: true)) {
            render(view: "create", model: [projectInstance: projectInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'project.label', default: 'Project'), projectInstance.id])
		if(params.subspecimenParamMap) {
			redirect(controller:"subspecimen", action: "create", params: [subspecimenParamMap: params.subspecimenParamMap])
			return
		}
		redirect(action: "show", id: projectInstance.id)
    }
	
	def saveAndCreateTissueRequest() {
		def projectInstance = new Project(params)
		if (!projectInstance.save(flush: true)) {
			render(view: "create", model: [projectInstance: projectInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'project.label', default: 'Project'), projectInstance.id])
		redirect(controller:"tissueRequest", action: "create", params:['project.id': projectInstance.id, subspecimenParamMap:params.subspecimenParamMap /*'specimenDbId': params.specimenDbId*/])
	}

    def show(Long id) {
        def projectInstance = Project.get(id)
        if (!projectInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'project.label', default: 'Project'), id])
            redirect(action: "list")
            return
        }

        [projectInstance: projectInstance]
    }

    def edit(Long id) {
        def projectInstance = Project.get(id)
        if (!projectInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'project.label', default: 'Project'), id])
            redirect(action: "list")
            return
        }

        [projectInstance: projectInstance]
    }

    def update(Long id, Long version) {
        def projectInstance = Project.get(id)
        if (!projectInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'project.label', default: 'Project'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (projectInstance.version > version) {
                projectInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'project.label', default: 'Project')] as Object[],
                          "Another user has updated this Project while you were editing")
                render(view: "edit", model: [projectInstance: projectInstance])
                return
            }
        }

        projectInstance.properties = params

        if (!projectInstance.save(flush: true)) {
            render(view: "edit", model: [projectInstance: projectInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'project.label', default: 'Project'), projectInstance.id])
        redirect(action: "show", id: projectInstance.id)
    }

    def delete(Long id) {
        def projectInstance = Project.get(id)
        if (!projectInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'project.label', default: 'Project'), id])
            redirect(action: "list")
            return
        }

        try {
            projectInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'project.label', default: 'Project'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'project.label', default: 'Project'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def ajaxGetTissueRequestsForProject() {
		if(params.projectDbId == "null") {
			render("Choose a Project to display associated Tissue Requests")
			return
		}
		else {
			def projectInstance = Project.read(params.projectDbId)
			
			if(projectInstance) {
				def tissueRequestCriteria = TissueRequest.createCriteria()
				def tissueRequestList =  tissueRequestCriteria.list {
					eq("project", projectInstance)
				}
				if(tissueRequestList) {
					render(template:"/subspecimen/selectTissueRequest", model:[tissueRequestList:tissueRequestList])
					return
				}
				else { //tissueRequestList is empty {
					render("This Project does not have any Tissue Requests")
					return
				}
				
			}
			else {
				render("Choose an Investigator and Project to display associated Tissue Requests")
				return
			}
		}
		
		//If something weird has happened to get here, just use default sentence
		render("Choose an Investigator and Project to display associated Tissue Requests")
		return
		
	}
	
	
}
