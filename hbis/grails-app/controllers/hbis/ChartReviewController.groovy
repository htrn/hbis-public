package hbis

import org.springframework.dao.DataIntegrityViolationException

import java.text.SimpleDateFormat

class ChartReviewController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [chartReviewInstanceList: ChartReview.list(params), chartReviewInstanceTotal: ChartReview.count()]
    }

	def listSubspecimensNeedChartReview() {
		def fromDate, toDate, sdf = new SimpleDateFormat("MM/dd/yyyy"), cal = Calendar.getInstance()

        if(!params.fromDate && !params.toDate){
            fromDate = new Date() - 7
            toDate = new Date()
        } else {
            fromDate = params.fromDate ? params.fromDate : Date.parse('yyyy-MM-dd HH:mm:ss', '1970-01-01 00:00:00')

            toDate = params.toDate ? params.toDate : new Date()
            cal.setTime(toDate)
            toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH) + 1}-${cal.get(Calendar.DATE)} 23:59:59")
        }

		def mrn = params.mrn

		def subspecimens
		def subspecimenCriteria = Subspecimen.createCriteria()
		//It is not apparent here, but Subspecimens will be filtered further in the associated gsp page
		//  Only subspecimens that do not have an associated Chart Review already will be displayed
		//  Completed this way since trying to do so here proved difficult
		subspecimens = subspecimenCriteria.list {
			eq("needsChartReview", true)
			specimen {
				procedure {
					patient {
						if(mrn) {
							like ("mrn", "%" + mrn + "%")
						}

					}
				}

				between('procurementDate', fromDate, toDate)
			}
		}		
		
		[subspecimens: subspecimens, fromDate: fromDate, toDate: toDate, mrn: mrn]
	}

    def create() {
		def chartReviewInstance = new ChartReview(params)
		def procedureInstance = chartReviewInstance.procedure
		def patientInstance = procedureInstance.patient
        [chartReviewInstance: chartReviewInstance, procedureInstance:procedureInstance, patientInstance:patientInstance]
    }

    def save() {
        def chartReviewInstance = new ChartReview(params)
		
        if (!chartReviewInstance.save(flush: true)) {
            render(view: "create", model: [chartReviewInstance: chartReviewInstance, procedureInstance:chartReviewInstance.procedure, patientInstance:chartReviewInstance.procedure.patient])
            return
        }
		
		//Chart Review has saved successfully, so now the Chart Review property on the associated Procedure also has to be set
		//  This may not be optimal, but the hasOne and unique relationship was not working with updating a Procedure
		//	after it has a Chart Review associated with it.  Doing this now should work with how the application
		//  has been written.
		def procedureInstance = Procedure.get(params.int('procedure.id'))
		procedureInstance.chartReview = chartReviewInstance

        flash.message = message(code: 'default.created.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), chartReviewInstance.id])
		// Depending on which button is pressed, redirect to the appropriate place
		if(params.saveAndCreateSpecimen) {
			redirect(controller:"specimen", action: "create", params:['procedure.id': chartReviewInstance.procedure.id])
			return
		}
		if(params.saveAndCreateMedicalHistory) {
			redirect(controller:"medicalHistory", action: "create", params:['chartReview.id': chartReviewInstance.id])
			return
		}
		if(params.saveAndCreateSurgicalHistory) {
			redirect(controller:"surgicalHistory", action: "create", params:['chartReview.id': chartReviewInstance.id])
			return
		}
		if(params.saveAndCreateMedication) {
			redirect(controller:"medication", action: "create", params:['chartReview.id': chartReviewInstance.id])
			return
		}
		if(params.saveAndCreateFamilyHistory) {
			redirect(controller:"familyHistory", action: "create", params:['chartReview.id': chartReviewInstance.id])
			return
		}
		redirect(action: "show", id: chartReviewInstance.id)
    }
	
    def show(Long id) {
        def chartReviewInstance = ChartReview.get(id)
        if (!chartReviewInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), id])
            redirect(action: "list")
            return
        }
		def procedureInstance = chartReviewInstance.procedure
		def patientInstance = procedureInstance.patient

        [chartReviewInstance: chartReviewInstance, procedureInstance:procedureInstance, patientInstance:patientInstance]
    }

    def edit(Long id) {
        def chartReviewInstance = ChartReview.get(id)
        if (!chartReviewInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), id])
            redirect(action: "list")
            return
        }
		def procedureInstance = chartReviewInstance.procedure
		def patientInstance = procedureInstance.patient

        [chartReviewInstance: chartReviewInstance, procedureInstance:procedureInstance, patientInstance:patientInstance]
    }

    def update(Long id, Long version) {
        def chartReviewInstance = ChartReview.get(id)
        if (!chartReviewInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (chartReviewInstance.version > version) {
                chartReviewInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'chartReview.label', default: 'ChartReview')] as Object[],
                          "Another user has updated this ChartReview while you were editing")
				def procedureInstance = chartReviewInstance.procedure
				def patientInstance = procedureInstance.patient
                render(view: "edit", model: [chartReviewInstance: chartReviewInstance, procedureInstance:procedureInstance, patientInstance:patientInstance])
                return
            }
        }

        chartReviewInstance.properties = params

        if (!chartReviewInstance.save(flush: true)) {
			def procedureInstance = chartReviewInstance.procedure
			def patientInstance = procedureInstance.patient
            render(view: "edit", model: [chartReviewInstance: chartReviewInstance, procedureInstance:procedureInstance, patientInstance:patientInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), chartReviewInstance.id])
        redirect(action: "show", id: chartReviewInstance.id)
    }

    def delete(Long id) {
        def chartReviewInstance = ChartReview.get(id)
		def procedureInstance
        if (!chartReviewInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), id])
            redirect(controller:"procedure", action: "amProcedureList")
            return
        }
		else if(chartReviewInstance.shippingCarts) { //There is at least one shipping cart associated
			flash.message = message(code: 'chartReview.notDeleted.hasShippingCarts.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), chartReviewInstance.id])
			redirect(controller:"chartReview", action: "show", id:chartReviewInstance.id)
			return
		}
		else { //There is a chartReviewInstance
			//First need to remove the association from the procedure in order to delete the chart review
			procedureInstance = chartReviewInstance.procedure
			procedureInstance.chartReview = null
		}

        try {
            chartReviewInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), id])
            redirect(controller:"procedure", action: "amProcedureList")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'chartReview.label', default: 'ChartReview'), id])
            redirect(action: "show", id: id)
        }
    }
}
