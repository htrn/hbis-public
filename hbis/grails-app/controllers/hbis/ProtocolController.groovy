package hbis

import org.springframework.dao.DataIntegrityViolationException

class ProtocolController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [protocolInstanceList: Protocol.list(params), protocolInstanceTotal: Protocol.count()]
    }
	
	def search(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		if(!params.offset) params.offset = 0
		
		def protocolCriteria = Protocol.createCriteria()
		def protocolInstanceList = protocolCriteria.list(max:params.max, offset:params.offset) {
			if(params.description) {
				ilike("description", "%" + params.description + "%")
			}
			if(params.principalInvestigatorName) {
				ilike("principalInvestigatorName", "%" + params.principalInvestigatorName + "%")
			}
		}
		[protocolInstanceList: protocolInstanceList, protocolInstanceTotal: protocolInstanceList.totalCount,
			description: params.description, principalInvestigatorName: params.principalInvestigatorName]
	}

    def create() {
        [protocolInstance: new Protocol(params)]
    }

    def save() {
        def protocolInstance = new Protocol(params)
        if (!protocolInstance.save(flush: true)) {
            render(view: "create", model: [protocolInstance: protocolInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'protocol.label', default: 'Protocol'), protocolInstance.id])
        redirect(action: "show", id: protocolInstance.id)
    }

    def show(Long id) {
        def protocolInstance = Protocol.get(id)
        if (!protocolInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'protocol.label', default: 'Protocol'), id])
            redirect(action: "list")
            return
        }

        [protocolInstance: protocolInstance]
    }

    def edit(Long id) {
        def protocolInstance = Protocol.get(id)
        if (!protocolInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'protocol.label', default: 'Protocol'), id])
            redirect(action: "list")
            return
        }

        [protocolInstance: protocolInstance]
    }

    def update(Long id, Long version) {
        def protocolInstance = Protocol.get(id)
        if (!protocolInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'protocol.label', default: 'Protocol'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (protocolInstance.version > version) {
                protocolInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'protocol.label', default: 'Protocol')] as Object[],
                          "Another user has updated this Protocol while you were editing")
                render(view: "edit", model: [protocolInstance: protocolInstance])
                return
            }
        }

        protocolInstance.properties = params

        if (!protocolInstance.save(flush: true)) {
            render(view: "edit", model: [protocolInstance: protocolInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'protocol.label', default: 'Protocol'), protocolInstance.id])
        redirect(action: "show", id: protocolInstance.id)
    }

    def delete(Long id) {
        def protocolInstance = Protocol.get(id)
        if (!protocolInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'protocol.label', default: 'Protocol'), id])
            redirect(action: "list")
            return
        }

        try {
            protocolInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'protocol.label', default: 'Protocol'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'protocol.label', default: 'Protocol'), id])
            redirect(action: "show", id: id)
        }
    }
}
