package hbis

import org.springframework.dao.DataIntegrityViolationException

class InvestigatorController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [investigatorInstanceList: Investigator.list(params), investigatorInstanceTotal: Investigator.count()]
    }
	
	def search(Integer max) {
		params.max = Math.min(max ?: 15, 100)
		if(!params.offset) params.offset = 0
		
		def chtnIdPrefixList
		if(params.chtnIdPrefixId) {
			chtnIdPrefixList = InvestigatorChtnIdPrefix.getAll(params.list('chtnIdPrefixId'))
		}
		def chtnDivisionList
		if(params.chtnDivisionId) {
			chtnDivisionList = ChtnDivision.getAll(params.list('chtnDivisionId'))
		}
		def statusList
		if(params.statusId) {
			statusList = InvestigatorStatus.getAll(params.list('statusId'))
		}
		
		def investigatorCriteria = Investigator.createCriteria()
		def investigatorInstanceList = investigatorCriteria.list(max:params.max, offset:params.offset) {
			if(chtnIdPrefixList) {
				'in'("chtnIdPrefix", chtnIdPrefixList)
			}
			if(chtnDivisionList) {
				'in'("chtnDivision", chtnDivisionList)
			}
			if(statusList) {
				'in'("status", statusList)
			}
			if(params.chtnId) {
				ilike("chtnId", "%" + params.chtnId + "%")
			}
			if(params.firstName) {
				ilike("firstName", "%" + params.firstName + "%")
			}
			if(params.lastName) {
				ilike("lastName", "%" + params.lastName + "%")
			}
			if(params.sort && params.order) {
				order(params.sort, params.order)
			}
			else {//define a default order 
				order("chtnId", "asc")
			}
			
		}
		
		[investigatorInstanceList: investigatorInstanceList, investigatorInstanceTotal: investigatorInstanceList.totalCount,
			chtnIdPrefixId:params.list('chtnIdPrefixId'), chtnId: params.chtnId, 
			firstName: params.firstName, lastName: params.lastName,
			chtnDivisionId:params.list('chtnDivisionId'), statusId:params.list('statusId')]
	}

    def create() {
        //[investigatorInstance: new Investigator(params), specimenDbId: params.specimenDbId]
		[investigatorInstance: new Investigator(), subspecimenParamMap:params.subspecimenParamMap]
    }

    def save() {
        def investigatorInstance = new Investigator(params)
        if (!investigatorInstance.save(flush: true)) {
			if(params.subspecimenParamMap) {
				render(view: "create", model: [investigatorInstance: investigatorInstance, subspecimenParamMap: params.subspecimenParamMap])
				return
			}
            render(view: "create", model: [investigatorInstance: investigatorInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'investigator.label', default: 'Investigator'), investigatorInstance.id])
		if(params.saveAndCreateTissueRequest) {
			redirect(controller:"tissueRequest", action: "create", params:['investigator.id': investigatorInstance.id, subspecimenParamMap:params.subspecimenParamMap])
			return
		}
		if(params.subspecimenParamMap) {
			redirect(controller:"subspecimen", action: "create", params: [subspecimenParamMap: params.subspecimenParamMap])
			return
		}
        redirect(action: "show", id: investigatorInstance.id)
    }
	

    def show(Long id) {
        def investigatorInstance = Investigator.get(id)
        if (!investigatorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'investigator.label', default: 'Investigator'), id])
            redirect(action: "list")
            return
        }

        [investigatorInstance: investigatorInstance]
    }

    def edit(Long id) {
        def investigatorInstance = Investigator.get(id)
        if (!investigatorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'investigator.label', default: 'Investigator'), id])
            redirect(action: "list")
            return
        }

        [investigatorInstance: investigatorInstance]
    }

    def update(Long id, Long version) {
        def investigatorInstance = Investigator.get(id)
        if (!investigatorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'investigator.label', default: 'Investigator'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (investigatorInstance.version > version) {
                investigatorInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'investigator.label', default: 'Investigator')] as Object[],
                          "Another user has updated this Investigator while you were editing")
                render(view: "edit", model: [investigatorInstance: investigatorInstance])
                return
            }
        }

        investigatorInstance.properties = params

        if (!investigatorInstance.save(flush: true)) {
            render(view: "edit", model: [investigatorInstance: investigatorInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'investigator.label', default: 'Investigator'), investigatorInstance.id])
        redirect(action: "show", id: investigatorInstance.id)
    }

    def delete(Long id) {
        def investigatorInstance = Investigator.get(id)
        if (!investigatorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'investigator.label', default: 'Investigator'), id])
            redirect(action: "list")
            return
        }

        try {
            investigatorInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'investigator.label', default: 'Investigator'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'investigator.label', default: 'Investigator'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def ajaxGetTissueRequestsForInvestigator() {
		if(params.investigatorId == "null") {
			render("Choose an Investigator to display associated Tissue Requests")
			return
		}
		else {
			def investigatorInstance = Investigator.read(params.investigatorId)
			
			if(investigatorInstance) {
				def tissueRequestCriteria = TissueRequest.createCriteria()
				def tissueRequestList =  tissueRequestCriteria.list {
					eq("investigator", investigatorInstance)
				}
				//If the tissueRequestList is null, the drop down box just won't be displayed (logic written in the template)
				render(template:"/subspecimen/selectTissueRequest", model:[tissueRequestList:tissueRequestList])
				return
			}
			else {
				render("Choose an Investigator to display associated Tissue Requests")
				return
			}
		}
		//If something weird has happened to get here, just use default sentence
		render("Choose an Investigator to display associated Tissue Requests")
		return
	}
	
	def ajaxGetExternalVsInternal() {
		def externalVsInternalList
		if(!(params.chtnIdPrefixId == "null")) {
			switch(InvestigatorChtnIdPrefix.findById(params.chtnIdPrefixId).prefix) {
				case "A":
					externalVsInternalList = ExternalVsInternal.findByDescription("External")
				break
				case "AC":
					externalVsInternalList = ExternalVsInternal.findByDescription("External")
				break
				case "B":
					externalVsInternalList = ExternalVsInternal.findByDescription("Internal")
				break
				case "C":
					externalVsInternalList = ExternalVsInternal.findByDescription("Internal")
				break
				case "E":
					externalVsInternalList = ExternalVsInternal.findByDescription("Internal")
				break
				case "G":
					externalVsInternalList = ExternalVsInternal.findByDescription("Internal")
				break
				case "SP":
					externalVsInternalList = ExternalVsInternal.findByDescription("Internal")
				break
				default:
					render("Choose a valid CHTN ID Prefix")
					return
				break	
			}
		}
		else {
			render("Choose a CHTN ID Prefix")
			return
		}
		render(template:"selectExternalVsInternal", model:[externalVsInternalList: externalVsInternalList])
		return
	}
	def ajaxGetInstitutionType() {
		def institutionTypeCriteria = InstitutionType.createCriteria()
		def institutionTypeList
		if(!(params.chtnIdPrefixId == "null")) {
			switch(InvestigatorChtnIdPrefix.findById(params.chtnIdPrefixId).prefix) {
				case "A":
					institutionTypeList = institutionTypeCriteria.list {
						or {
							eq("description", "Academic/Non-Profit")
							eq("description", "Government")
						}
					}
				break
				case "AC":
					institutionTypeList = InstitutionType.findByDescription("Commercial")
				break
				case "B":
					institutionTypeList = InstitutionType.findByDescription("Academic/Non-Profit")
				break
				case "C":
					institutionTypeList = InstitutionType.findByDescription("Academic/Non-Profit")
				break
				case "E":
					institutionTypeList = InstitutionType.findByDescription("Academic/Non-Profit")
				break
				case "G":
					institutionTypeList = InstitutionType.findByDescription("Not Applicable")
				break
				case "SP":
					institutionTypeList = InstitutionType.findByDescription("Not Applicable")
				break
				default:
					render("Choose a valid CHTN ID Prefix")
					return
				break
			}
		}
		else {
			render("Choose a CHTN ID Prefix")
			return
		}
		render(template:"selectInstitutionType", model:[institutionTypeList: institutionTypeList])
		return
	}
	
	def ajaxGetServiceProgramType() {
		def serviceProgramTypeList
		if(!(params.chtnIdPrefixId == "null")) {
			switch(InvestigatorChtnIdPrefix.findById(params.chtnIdPrefixId).prefix) {
				case "A":
					serviceProgramTypeList = ServiceProgramType.findByDescription("CHTN only")
				break
				case "AC":
					serviceProgramTypeList = ServiceProgramType.findByDescription("CHTN only")
				break
				case "B":
					serviceProgramTypeList = ServiceProgramType.findByDescription("CHTN only")
				break
				case "C":
					serviceProgramTypeList = ServiceProgramType.findByDescription("CHTN and CCC")
				break
				case "E":
					serviceProgramTypeList = ServiceProgramType.findByDescription("CCC only")
				break
				case "G":
					serviceProgramTypeList = ServiceProgramType.findByDescription("Temporary Stock")
				break
				case "SP":
					serviceProgramTypeList = ServiceProgramType.findByDescription("Not Applicable")
				break
				default:
					render("Choose a valid CHTN ID Prefix")
					return
				break
			}
		}
		else {
			render("Choose a CHTN ID Prefix")
			return
		}
		render(template:"selectServiceProgramType", model:[serviceProgramTypeList: serviceProgramTypeList])
		return
	}
}
