package hbis

import org.springframework.dao.DataIntegrityViolationException

class TissueRequestController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [tissueRequestInstanceList: TissueRequest.list(params), tissueRequestInstanceTotal: TissueRequest.count()]
    }
	
	def search(Integer max) {
		params.max = Math.min(max ?: 15, 100)
		if(!params.offset) params.offset = 0
		
		def contactMethodList
		if(params.contactMethodId) {
			contactMethodList = TissueRequestContactMethod.getAll(params.list('contactMethodId'))
		}
		def statusList
		if(params.statusId) {
			statusList = TissueRequestStatus.getAll(params.list('statusId'))
		}
		
		def tissueRequestCriteria = TissueRequest.createCriteria()
		def tissueRequestInstanceList = tissueRequestCriteria.list(max:params.max, offset:params.offset) {
			if(contactMethodList) {
				'in'("contactMethod", contactMethodList)
			}
			if(statusList) {
				'in'("status", statusList)
			}
			if(params.name) {
				ilike("name", "%" + params.name + "%")
			}
			if(params.tissueRequestTissueQuestId) {
				ilike("tissueRequestTissueQuestId", "%" + params.tissueRequestTissueQuestId + "%")
			}
			if(params.projectTissueQuestId) {
				ilike("projectTissueQuestId", "%" + params.projectTissueQuestId + "%")
			}
			if(params.sort && params.order) {
				order(params.sort, params.order)
			}
			else {//define a default order
				order("tissueRequestTissueQuestId", "asc")
			}
		}
		
		
		[tissueRequestInstanceList: tissueRequestInstanceList, tissueRequestInstanceTotal: tissueRequestInstanceList.totalCount,
			name: params.name, tissueRequestTissueQuestId: params.tissueRequestTissueQuestId,
			projectTissueQuestId: params.projectTissueQuestId, statusId: params.list('statusId'),
			contactMethodId: params.list('contactMethodId')]
	}

    def create() {
		def tissueRequestInstance = new TissueRequest(params)
        [tissueRequestInstance: tissueRequestInstance, investigatorInstance: tissueRequestInstance.investigator,
			subspecimenParamMap:params.subspecimenParamMap]
    }

    def save() {
        def tissueRequestInstance = new TissueRequest(params)
        if (!tissueRequestInstance.save(flush: true)) {
            render(view: "create", model: [tissueRequestInstance: tissueRequestInstance, subspecimenParamMap: params.subspecimenParamMap])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'tissueRequest.label', default: 'TissueRequest'), tissueRequestInstance.id])
		if(params.subspecimenParamMap) {
			redirect(controller:"subspecimen", action: "create", params: [subspecimenParamMap: params.subspecimenParamMap])
			return
		}
		redirect(action: "show", id: tissueRequestInstance.id)
    }

    def show(Long id) {
        def tissueRequestInstance = TissueRequest.get(id)
        if (!tissueRequestInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tissueRequest.label', default: 'TissueRequest'), id])
            redirect(action: "list")
            return
        }

        [tissueRequestInstance: tissueRequestInstance, investigatorInstance: tissueRequestInstance.investigator]
    }

    def edit(Long id) {
        def tissueRequestInstance = TissueRequest.get(id)
        if (!tissueRequestInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tissueRequest.label', default: 'TissueRequest'), id])
            redirect(action: "list")
            return
        }

        [tissueRequestInstance: tissueRequestInstance, investigatorInstance: tissueRequestInstance.investigator]
    }

    def update(Long id, Long version) {
        def tissueRequestInstance = TissueRequest.get(id)
        if (!tissueRequestInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tissueRequest.label', default: 'TissueRequest'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (tissueRequestInstance.version > version) {
                tissueRequestInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'tissueRequest.label', default: 'TissueRequest')] as Object[],
                          "Another user has updated this TissueRequest while you were editing")
                render(view: "edit", model: [tissueRequestInstance: tissueRequestInstance])
                return
            }
        }

        tissueRequestInstance.properties = params

        if (!tissueRequestInstance.save(flush: true)) {
            render(view: "edit", model: [tissueRequestInstance: tissueRequestInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'tissueRequest.label', default: 'TissueRequest'), tissueRequestInstance.id])
        redirect(action: "show", id: tissueRequestInstance.id)
    }

    def delete(Long id) {
        def tissueRequestInstance = TissueRequest.get(id)
        if (!tissueRequestInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tissueRequest.label', default: 'TissueRequest'), id])
            redirect(action: "list")
            return
        }

        try {
            tissueRequestInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'tissueRequest.label', default: 'TissueRequest'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tissueRequest.label', default: 'TissueRequest'), id])
            redirect(action: "show", id: id)
        }
    }
	
}
