package hbis

import org.springframework.dao.DataIntegrityViolationException

class FamilyHistoryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [familyHistoryInstanceList: FamilyHistory.list(params), familyHistoryInstanceTotal: FamilyHistory.count()]
    }

    def create() {
		def familyHistoryInstance = new FamilyHistory(params)
        [familyHistoryInstance: familyHistoryInstance,
			chartReviewInstance: familyHistoryInstance.chartReview,
			procedureInstance: familyHistoryInstance.chartReview.procedure,
			patientInstance: familyHistoryInstance.chartReview.procedure.patient]
    }

    def save() {
        def familyHistoryInstance = new FamilyHistory(params)
        if (!familyHistoryInstance.save(flush: true)) {
            render(view: "create", model: [familyHistoryInstance: familyHistoryInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'familyHistory.label', default: 'FamilyHistory'), familyHistoryInstance.id])
		// Depending on which button is pressed, redirect to the appropriate place
		if(params.saveAndCreateAnotherFamilyHistory) {
			redirect(action: "create", params: ['chartReview.id':familyHistoryInstance.chartReview.id])
			return
		}
		redirect(controller: "chartReview", action: "show", id: familyHistoryInstance.chartReview.id)
    }
	
    def show(Long id) {
        def familyHistoryInstance = FamilyHistory.get(id)
        if (!familyHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'familyHistory.label', default: 'FamilyHistory'), id])
            redirect(action: "list")
            return
        }

        [familyHistoryInstance: familyHistoryInstance,
			chartReviewInstance: familyHistoryInstance.chartReview,
			procedureInstance: familyHistoryInstance.chartReview.procedure,
			patientInstance: familyHistoryInstance.chartReview.procedure.patient]
    }

    def edit(Long id) {
        def familyHistoryInstance = FamilyHistory.get(id)
        if (!familyHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'familyHistory.label', default: 'FamilyHistory'), id])
            redirect(action: "list")
            return
        }

        [familyHistoryInstance: familyHistoryInstance,
			chartReviewInstance: familyHistoryInstance.chartReview,
			procedureInstance: familyHistoryInstance.chartReview.procedure,
			patientInstance: familyHistoryInstance.chartReview.procedure.patient]
    }

    def update(Long id, Long version) {
        def familyHistoryInstance = FamilyHistory.get(id)
        if (!familyHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'familyHistory.label', default: 'FamilyHistory'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (familyHistoryInstance.version > version) {
                familyHistoryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'familyHistory.label', default: 'FamilyHistory')] as Object[],
                          "Another user has updated this FamilyHistory while you were editing")
                render(view: "edit", model: [familyHistoryInstance: familyHistoryInstance,
												chartReviewInstance: familyHistoryInstance.chartReview,
												procedureInstance: familyHistoryInstance.chartReview.procedure,
												patientInstance: familyHistoryInstance.chartReview.procedure.patient])
                return
            }
        }

        familyHistoryInstance.properties = params

        if (!familyHistoryInstance.save(flush: true)) {
            render(view: "edit", model: [familyHistoryInstance: familyHistoryInstance,
												chartReviewInstance: familyHistoryInstance.chartReview,
												procedureInstance: familyHistoryInstance.chartReview.procedure,
												patientInstance: familyHistoryInstance.chartReview.procedure.patient])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'familyHistory.label', default: 'FamilyHistory'), familyHistoryInstance.id])
        redirect(action: "show", id: familyHistoryInstance.id)
    }

    def delete(Long id) {
        def familyHistoryInstance = FamilyHistory.get(id)
        if (!familyHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'familyHistory.label', default: 'FamilyHistory'), id])
            redirect(action: "list")
            return
        }

        try {
            familyHistoryInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'familyHistory.label', default: 'FamilyHistory'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'familyHistory.label', default: 'FamilyHistory'), id])
            redirect(action: "show", id: id)
        }
    }
}
