package hbis

import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.dao.DataIntegrityViolationException

import java.text.NumberFormat

class ShippingCartController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [shippingCartInstanceList: ShippingCart.list(params), shippingCartInstanceTotal: ShippingCart.count()]
    }
	
	def jasperService
	def shippingCartService
	def prePackingSlip() {
		
		def shippingCartInstance = ShippingCart.read(params.int('shippingCartId'))

		def shippingCarts = shippingCartService.getShippingCartForPackingSlip(params.long('shippingCartId'))

		//As per the above query, there will only be one shippingCart, but jasper service
		//  seems to require a collection
		shippingCarts.each { shippingCart ->
			shippingCart.discard()

			// Remove all subspecimens with a needsChartReview property of false
			shippingCart.chartReviews.each { chartReview ->
				chartReview.procedure.specimens.each { specimen ->
					specimen.subspecimens.removeAll { !it.needsChartReview }
				}

				// Remove all specimens that no longer have any subspecimens
				chartReview.procedure.specimens.removeAll { it.subspecimens.size() == 0 }
			}

			params.totalPrice = shippingCart.getTotalPrice()
			params.numberOfSubspecimens = shippingCart.getNumberOfSubspecimens()
			params.numberOfChartReviews = shippingCart.getNumberOfChartReviews()
			params.shippingCartId = shippingCart.id
		}
		params.SUBREPORT_DIR = "${servletContext.getRealPath('/reports')}/"
		params.CHTN_IMAGE_DIR = "${servletContext.getRealPath('/reports/images')}/"
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:shippingCarts])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	def confirmedPackingSlip() {
		
		def shippingCartInstance = ShippingCart.read(params.int('shippingCartId'))

		def shippingCarts = shippingCartService.getShippingCartForPackingSlip(params.long('shippingCartId'))

		//As per the above query, there will only be one shippingCart, but jasper service
		//  seems to require a collection
		shippingCarts.each { shippingCart ->
			shippingCart.discard()

			// Remove all subspecimens with a needsChartReview property of false
			shippingCart.chartReviews.each { chartReview ->
				chartReview.procedure.specimens.each { specimen ->
					specimen.subspecimens.removeAll { !it.needsChartReview }
				}

				// Remove all specimens that no longer have any subspecimens
				chartReview.procedure.specimens.removeAll { it.subspecimens.size() == 0 }
			}

			params.totalPrice = shippingCart.getTotalPrice()
			params.numberOfSubspecimens = shippingCart.getNumberOfSubspecimens()
			params.numberOfChartReviews = shippingCart.getNumberOfChartReviews()
			params.shippingCartId = shippingCart.id
		}

		params.SUBREPORT_DIR = "${servletContext.getRealPath('/reports')}/"
		params.CHTN_IMAGE_DIR = "${servletContext.getRealPath('/reports/images')}/"
		JasperReportDef reportDef = jasperService.buildReportDefinition(params, request.getLocale(), [data:shippingCarts])
		// Non-inline reports (e.g. PDF)
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline)
		{
			response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
		}
		else
		{
			// Inline report (e.g. HTML)
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
		}
	}
	
	def viewPending() {
		def shippingCartInstanceList
		def shippingCartCriteria = ShippingCart.createCriteria()
		shippingCartInstanceList = shippingCartCriteria.list {
			eq("status", ShippingCartStatus.findByDescription("Pending"))
		}
		
		[shippingCartInstanceList: shippingCartInstanceList]
	}

    def create() {
        [shippingCartInstance: new ShippingCart(params)]
    }
	
	def save() {
		def investigator = Investigator.read(params.int('investigator.id'))
		def shippingCartInstitutionsAllowed = ShippingCartInstitutionsAllowed.read(params.int('institutionsAllowed.id'))
		def shippingCartQcStatusFilter = ShippingCartQcStatus.read(params.int('qcStatusFilter.id'))
		def fromDate = params.date('fromDate')
		def toDate = params.date('toDate')
		def preliminaryPrimaryAnatomicSiteId = params.int('preliminaryPrimaryAnatomicSiteId')
		def preliminaryTissueTypeId = params.int('preliminaryTissueTypeId')
		
		def subspecimenList
		def chartReviewList
		
		//C investigators can't be associated with "All" as the institutionsAllowed
		if(investigator.chtnIdPrefix.prefix == "C" && shippingCartInstitutionsAllowed.description == "All") {
			flash.message = "C investigators can't be selected when 'All' is selected as the Subspecimen Institutions Allowed in this Shipping Cart.  Please make different criteria selections and try again."
			redirect(controller: "shippingCart", action: "create", params:['investigator.id':params.int('investigator.id'),
																			'institutionsAllowed.id':params.int('institutionsAllowed.id'),
																			'qcStatusFilter.id':params.int('qcStatusFilter.id'),
																			description: params.description,
																			fromDate:params.date('fromDate'),
																			toDate:params.date('toDate'),
																			preliminaryPrimaryAnatomicSiteId: params.int('preliminaryPrimaryAnatomicSiteId'),
																			preliminaryTissueTypeId: params.int('preliminaryTissueTypeId')])
			return
		}
		
		if(params.saveAndAddSubspecimens) {
			def subspecimenCriteria = Subspecimen.createCriteria()
			subspecimenList = subspecimenCriteria.list {
				eq("status", SubspecimenStatus.findByDescription("Assigned"))
				eq("investigator", investigator)
				//Also check 'deidentified' is yes when that property is added
				isNull("shippingCart")
				specimen {
					procedure {
						procedureEventInstitution {
							switch(shippingCartInstitutionsAllowed.description) { //Only three discrete cases currently, so no default
								case "All":
									//Do nothing in this case, show subspecimens from all insttutions
								break
								case "OSU only":
									eq("description", "OSU")
								break
								case "Non-OSU only":
									not { eq("description", "OSU") }
								break
							}
						}
					}
					if(preliminaryPrimaryAnatomicSiteId) {
						eq("preliminaryPrimaryAnatomicSite", AnatomicSite.read(preliminaryPrimaryAnatomicSiteId))
					}
					if(preliminaryTissueTypeId) {
						eq("preliminaryTissueType", TissueType.read(preliminaryTissueTypeId))
					}
					if(!fromDate && !toDate) {
						//ge("procurementDate", lastTwoWeeks)
					}
					else {
						if(fromDate && !toDate) {
							ge("procurementDate", fromDate)
						}
						else if (!fromDate && toDate) {
							le("procurementDate", toDate)
						}
						else { //Both fromDate and toDate are provided
							between("procurementDate", fromDate, toDate)
						}
					}
					if(shippingCartQcStatusFilter == ShippingCartQcStatus.findByDescription("QC Complete")) {
						qcBatch {
							eq("status", QcBatchStatus.findByDescription("Complete"))
						}
					}
					order("specimenChtnId", "asc")
				}
			}
			if(!subspecimenList) { //If no subspecimens were retrieved, do not proceed
				flash.message = "The criteria selected does not include any Subspecimens available to add to the Shipping Cart.  Please make different criteria selections and try again."
				redirect(controller: "shippingCart", action: "create", params:['investigator.id':params.int('investigator.id'),
																			'institutionsAllowed.id':params.int('institutionsAllowed.id'),
																			'qcStatusFilter.id':params.int('qcStatusFilter.id'),
																			description: params.description,
																			fromDate:params.date('fromDate'),
																			toDate:params.date('toDate'),
																			preliminaryPrimaryAnatomicSiteId: params.int('preliminaryPrimaryAnatomicSiteId'),
																			preliminaryTissueTypeId: params.int('preliminaryTissueTypeId')])
				return
			}
		}
		
		if(params.saveAndAddChartReviews) {
			def chartReviewCriteria = ChartReview.createCriteria()
			chartReviewList = chartReviewCriteria.list {
				procedure {
					specimens {
						subspecimens {
							eq("needsChartReview", true)
							eq("investigator", investigator)
						}
					}
				}
			}
			if(!chartReviewList) {
				flash.message = "The criteria selected does not include any Chart Reviews available to add to the Shipping Cart.  Please make different criteria selections and try again."
				redirect(controller: "shippingCart", action: "create", params:['investigator.id':params.int('investigator.id'),
																			'institutionsAllowed.id':params.int('institutionsAllowed.id'),
																			'qcStatusFilter.id':params.int('qcStatusFilter.id'),
																			description: params.description,
																			fromDate:params.date('fromDate'),
																			toDate:params.date('toDate'),
																			preliminaryPrimaryAnatomicSiteId: params.int('preliminaryPrimaryAnatomicSiteId'),
																			preliminaryTissueTypeId: params.int('preliminaryTissueTypeId')])
				return
			}
		}
		
		def shippingCartInstance = new ShippingCart(params)
		shippingCartInstance.status = ShippingCartStatus.findByDescription("Pending")
		if (!shippingCartInstance.save(flush: true)) {
			render(view: "create", model: [shippingCartInstance: shippingCartInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), shippingCartInstance.id])
		if(params.saveAndAddSubspecimens) {
			redirect(action: "viewSubspecimensToAdd", id: shippingCartInstance.id,
				params:[fromDate: params.date('fromDate'), toDate: params.date('toDate'), preliminaryPrimaryAnatomicSiteId: params.int('preliminaryPrimaryAnatomicSiteId'), preliminaryTissueTypeId: params.int('preliminaryTissueTypeId')])
			return
		}
		if(params.saveAndAddChartReviews) {
			redirect(action: "viewChartReviewsToAdd", id: shippingCartInstance.id)
			return
		}
		
	}

    def show(Long id) {
        def shippingCartInstance = ShippingCart.get(id)
        if (!shippingCartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), id])
            redirect(action: "list")
            return
        }

        [shippingCartInstance: shippingCartInstance]
    }

    def edit(Long id) {
        def shippingCartInstance = ShippingCart.get(id)
        if (!shippingCartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), id])
            redirect(action: "list")
            return
        }

        [shippingCartInstance: shippingCartInstance]
    }

    def update(Long id, Long version) {
        def shippingCartInstance = ShippingCart.get(id)
        if (!shippingCartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (shippingCartInstance.version > version) {
                shippingCartInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'shippingCart.label', default: 'ShippingCart')] as Object[],
                          "Another user has updated this ShippingCart while you were editing")
				if(params.view == "confirm") {
					render(view:"confirm", model: [shippingCartInstance: shippingCartInstance])
					return
				}
				if(params.view == "editErampNumber") {
					render(view:"editErampNumber", model: [shippingCartInstance: shippingCartInstance,
															investigatorId: params.investigatorId,
															shippingCartId: params.shippingCartId])
					return
				}
                render(view: "edit", model: [shippingCartInstance: shippingCartInstance])
                return
            }
        }

        shippingCartInstance.properties = params
		if((params.view == "confirm") && (params.shippingCartStatus == "Shipped")) {
			shippingCartInstance.status = ShippingCartStatus.findByDescription("Shipped")
			def subspecimenStatusShipped = SubspecimenStatus.findByDescription("Shipped")
			shippingCartInstance.subspecimens.each { subspecimen ->
				subspecimen.status = subspecimenStatusShipped
			}
			
		}

        if (!shippingCartInstance.save(flush: true)) {
            render(view: "edit", model: [shippingCartInstance: shippingCartInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), shippingCartInstance.id])
		if(params.view == "confirm") {
			redirect(action: "show", id: shippingCartInstance.id)
			return
		}
		if(params.view == "editErampNumber") {
			redirect(action: "editErampNumber", params:[investigatorId: params.investigatorId,
															shippingCartId: params.shippingCartId])
			return
		}
		redirect(action: "show", id: shippingCartInstance.id)
    }

    def delete(Long id) {
        def shippingCartInstance = ShippingCart.get(id)
        if (!shippingCartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), id])
            redirect(action: "list")
            return
        }

        try {
			
			def subspecimens = Subspecimen.findAllByShippingCart(shippingCartInstance)
			subspecimens.each { subspecimen ->
				shippingCartInstance.removeFromSubspecimens(subspecimen)
			}
			shippingCartInstance.save(flush:true)

            shippingCartInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def viewSubspecimensToAdd(Long id) {
		def shippingCartInstance = ShippingCart.read(id)
		def specimens
		def subspecimens
		def fromDate = params.date('fromDate')
		def toDate = params.date('toDate')
		def preliminaryPrimaryAnatomicSiteId = params.int('preliminaryPrimaryAnatomicSiteId')
		def preliminaryTissueTypeId = params.int('preliminaryTissueTypeId')
		
		def subspecimenCriteria = Subspecimen.createCriteria()
		subspecimens = subspecimenCriteria.list {
			eq("status", SubspecimenStatus.findByDescription("Assigned"))
			eq("investigator", shippingCartInstance.investigator)
			//Also check 'deidentified' is yes when that property is added
			isNull("shippingCart")
			order("subspecimenChtnId", "asc")
			specimen {
				procedure {
					procedureEventInstitution {
							switch(shippingCartInstance.institutionsAllowed.description) { //Only three discrete cases currently, so no default
								case "All":
									//Do nothing in this case, showing everything
								break
								case "OSU only":
									eq("description", "OSU")
								break
								case "Non-OSU only":
									not { eq("description", "OSU") }
								break
							}
						}
				}
				if(preliminaryPrimaryAnatomicSiteId) {
					eq("preliminaryPrimaryAnatomicSite", AnatomicSite.read(preliminaryPrimaryAnatomicSiteId))
				}
				if(preliminaryTissueTypeId) {
					eq("preliminaryTissueType", TissueType.read(preliminaryTissueTypeId))
				}
				if(!fromDate && !toDate) {
					//ge("procurementDate", lastTwoWeeks)
				}
				else {
					if(fromDate && !toDate) {
						ge("procurementDate", fromDate)
					}
					else if (!fromDate && toDate) {
						le("procurementDate", toDate)
					}
					else { //Both fromDate and toDate are provided
						between("procurementDate", fromDate, toDate)
					}
				}
				
				if(shippingCartInstance.qcStatusFilter == ShippingCartQcStatus.findByDescription("QC Complete")) {
					qcBatch {
						eq("status", QcBatchStatus.findByDescription("Complete"))
					}
				}
				order("specimenChtnId", "asc")
				
			}
			
		}
		
		[shippingCartInstance:shippingCartInstance, fromDate: fromDate, toDate: toDate, 
			preliminaryPrimaryAnatomicSiteId: preliminaryPrimaryAnatomicSiteId, preliminaryTissueTypeId: preliminaryTissueTypeId,
			subspecimensInCart:shippingCartInstance.subspecimens, subspecimens:subspecimens]
	}
	
	def viewChartReviewsToAdd(Long id) {
		def shippingCartInstance = ShippingCart.read(id)
		def fromDate = params.date('fromDate')
		def toDate = params.date('toDate')
		def primaryAnatomicSiteId = params.int('primaryAnatomicSiteId')
		def tissueTypeId = params.int('tissueTypeId')
		
		def chartReviewList
		def chartReviewCriteria = ChartReview.createCriteria()
		chartReviewList = chartReviewCriteria.listDistinct {
			if(shippingCartInstance.chartReviews) {
				not { 'in'("id", shippingCartInstance.chartReviews*.id)}
			}
			procedure {
				specimens {
					subspecimens {
						eq("needsChartReview", true)
						eq("investigator", shippingCartInstance.investigator)
					}
				}
			}
		}
		
		println("Chart Review List: " + chartReviewList)
		
		[shippingCartInstance:shippingCartInstance, fromDate: fromDate, toDate: toDate,
			primaryAnatomicSiteId: primaryAnatomicSiteId, tissueTypeId: tissueTypeId,
			chartReviewsInCart:shippingCartInstance.chartReviews, chartReviewList:chartReviewList]
	}
	
	def addRemoveSubspecimen() {
		def shippingCartInstance = ShippingCart.get(params.long('shippingCartId')), subspecimens
		
		def shippingCartVersion = params.long('shippingCartVersion')
		def hasError = false
		
		if (shippingCartVersion != null) {
			if (shippingCartInstance.version > shippingCartVersion) {
				shippingCartInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'shippingCart.label', default: 'ShippingCart')] as Object[],
						  "Another user has updated this ShippingCart while you were editing")
				hasError = true
			}
		}

		if(hasError) {
			Integer max
			params.max = Math.min(max ?: 10, 100)
			render(view: "list", model: [shippingCartInstance: shippingCartInstance,
											subspecimenInstance: subspecimenInstance,
											shippingCartInstanceList: ShippingCart.list(params),
											shippingCartInstanceTotal: ShippingCart.count()])
			return
		}

		if (params.addRemove == 'Add') {
			subspecimens = Subspecimen.getAll(params.list('subspecimensToAdd'))

			subspecimens.each { subspecimen ->
				shippingCartInstance.addToSubspecimens(subspecimen)
			}
		}
		else if (params.addRemove == 'Remove') {
			subspecimens = Subspecimen.getAll(params.list('subspecimensToRemove'))

			subspecimens.each { subspecimen ->
				shippingCartInstance.removeFromSubspecimens(subspecimen)
			}
		}
		
		shippingCartInstance.save(flush:true)
		
		redirect(action: "viewSubspecimensToAdd", id: shippingCartInstance.id, params:[fromDate: params.fromDate, toDate: params.toDate, preliminaryPrimaryAnatomicSiteId: params.int('preliminaryPrimaryAnatomicSiteId'), preliminaryTissueTypeId: params.int('preliminaryTissueTypeId')])
	}
	
	def addRemoveChartReview() {
		def shippingCartInstance = ShippingCart.get(params.long('shippingCartId'))
		def chartReviewInstance = ChartReview.get(params.long('chartReviewId'))

		chartReviewInstance.price = params.float('price')

		def shippingCartVersion = params.long('shippingCartVersion')
		def chartReviewVersion = params.long('chartReviewVersion')
		def hasError = false
		
		if (shippingCartVersion != null) {
			if (shippingCartInstance.version > shippingCartVersion) {
				shippingCartInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'shippingCart.label', default: 'ShippingCart')] as Object[],
						  "Another user has updated this ShippingCart while you were editing")
				hasError = true
			}
		}
		
		if (chartReviewVersion != null) {
			if (chartReviewInstance.version > chartReviewVersion) {
				chartReviewInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'chartReview.label', default: 'ChartReview')] as Object[],
						  "Another user has updated this Chart Review while you were editing")
				hasError = true
			}
		}
		
		if(hasError) {
			Integer max
			params.max = Math.min(max ?: 10, 100)
			render(view: "list", model: [shippingCartInstance: shippingCartInstance,
											chartReviewInstance: chartReviewInstance,
											shippingCartInstanceList: ShippingCart.list(params),
											shippingCartInstanceTotal: ShippingCart.count()])
			return
		}
		if (params.addRemove == 'Add') {
			shippingCartInstance.addToChartReviews(chartReviewInstance)
		}
		else if (params.addRemove == 'Remove') {
			shippingCartInstance.removeFromChartReviews(chartReviewInstance)
		}
		
		shippingCartInstance.save(flush:true)

		if(request.format == 'json'){
			def specimens = chartReviewInstance.procedure.specimens.each { specimen ->
				specimen.subspecimens.removeAll { !it.needsChartReview }
			}

			specimens.removeAll { specimen ->
				specimen.subspecimens.size() == 0
			}

			render(contentType: "text/json") {
				chartReview(
						id: params.long('chartReviewId'),
						price: NumberFormat.getCurrencyInstance().format(chartReviewInstance.price),
						fromDate: params.fromDate,
						toDate: params.toDate,
						preliminaryPrimaryAnatomicSiteId: params.int('preliminaryPrimaryAnatomicSiteId'),
						preliminaryTissueTypeId: params.int('preliminaryTissueTypeId'),
						procedureDate: chartReviewInstance.procedure.procedureDate,
						specimens: specimens,
						chartReviewVersion: chartReviewInstance.version,
						shippingCartVersion: shippingCartInstance.version
				)
			}
		} else {
			redirect(action: "viewChartReviewsToAdd", id: shippingCartInstance.id, params:[fromDate: params.fromDate, toDate: params.toDate, preliminaryPrimaryAnatomicSiteId: params.int('preliminaryPrimaryAnatomicSiteId'), preliminaryTissueTypeId: params.int('preliminaryTissueTypeId')])
		}
	}
	
	def confirm(Long id) {
		def shippingCartInstance = ShippingCart.get(id)
		if (!shippingCartInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'shippingCart.label', default: 'ShippingCart'), id])
			redirect(action: "list")
			return
		}
		
		if(shippingCartInstance.subspecimens.size() < 1 && shippingCartInstance.chartReviews.size() < 1 ) {
			flash.message = "There needs to be at least one Subspecimen or Chart Review in the Shipping Cart before proceeding to confirmation."
			redirect(action:"viewPending")
			return
		}

		[shippingCartInstance: shippingCartInstance]
	}
	
	def editErampNumber() {
		def shippingCartInstanceList
		def investigatorId
		def shippingCartId
		//If there are any incoming parameters, then execute the query, else produce message to enter criteria
		def shippingCartCriteria = ShippingCart.createCriteria()
		if(params.investigatorId != "null") {
			investigatorId = params.long('investigatorId')
		}
		if(params.shippingCartId != "null") {
			shippingCartId = params.long('shippingCartId')
		}
		
		if(investigatorId || shippingCartId) {
			shippingCartInstanceList = shippingCartCriteria.list {
				eq("status", ShippingCartStatus.findByDescription("Shipped"))
				if(shippingCartId) {
					eq("id", shippingCartId)
				}
				if(investigatorId) {
					eq("investigator", Investigator.read(investigatorId))
				}
			}
			if(!shippingCartInstanceList) {
				flash.message="The criteria selected did not produce any results.  Try again with different criteria."
			}
		}
		
		[shippingCartInstanceList: shippingCartInstanceList, investigatorId: investigatorId, 
			shippingCartId: shippingCartId]
	}
}
