package hbis

import org.springframework.dao.DataIntegrityViolationException
import java.text.SimpleDateFormat

class PatientController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [patientInstanceList: Patient.list(params), patientInstanceTotal: Patient.count()]
    }

    def create() {
		def patientInstance = new Patient(params)
		if(!patientInstance.originInstitution) {
			patientInstance.originInstitution = Institution.findByDescription("OSU")
		}
        [patientInstance: patientInstance]
    }

	
    def save() {
        def patientInstance = new Patient(params)
		patientInstance.clearErrors()
		
		if(params.dateOfBirth) {
			def sdf = new SimpleDateFormat("MM/dd/yyyy")
			patientInstance.dateOfBirth = sdf.parse(params.dateOfBirth)
		}
		
        if (!patientInstance.save(flush: true)) {
            render(view: "create", model: [patientInstance: patientInstance])
            return
        }
		
        flash.message = message(code: 'patient.created.message', args: [message(code: 'patient.label', default: 'Patient'), patientInstance.id, patientInstance.mrn, patientInstance.lastName])
		if(params.saveAndCreateProcedure) {
			redirect(controller:"procedure", action: "create", params:['patient.id': patientInstance.id])
			return
		}
		if(params.saveAndCreateConsent) {
			redirect(controller:"consent", action: "create", params:['patient.id': patientInstance.id])
			return
		}
		redirect(action: "show", id: patientInstance.id)
    }

    def show(Long id) {
        def patientInstance = Patient.get(id)
        if (!patientInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'patient.label', default: 'Patient'), id])
            redirect(action: "list")
            return
        }

        [patientInstance: patientInstance]
    }
	
	def searchMRN() {
		
	}
	
	def performSearchMRN() {
		if(params.mrn.size() > 7) {
			
			def patientCriteria = Patient.createCriteria()
			def patientList
			patientList = patientCriteria.list {
				like("mrn", params.mrn + "%")
				order("mrn", "asc")
			}
			render(template:'searchMRNResult', collection:patientList, var:'patientInstance')
		}		
	}

    def edit(Long id) {
        def patientInstance = Patient.get(id)
        if (!patientInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'patient.label', default: 'Patient'), id])
            redirect(action: "list")
            return
        }

        [patientInstance: patientInstance]
    }

    def update(Long id, Long version) {
        def patientInstance = Patient.get(id)
        if (!patientInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'patient.label', default: 'Patient'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (patientInstance.version > version) {
                patientInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'patient.label', default: 'Patient')] as Object[],
                          "Another user has updated this Patient while you were editing")
                render(view: "edit", model: [patientInstance: patientInstance])
                return
            }
        }

        patientInstance.properties = params
		patientInstance.clearErrors() //Need to clear errors here so a 'not valid date' error does not get returned after dateOfBirth is properly set below
		
		if(params.dateOfBirth) {
			def sdf = new SimpleDateFormat("MM/dd/yyyy")
			patientInstance.dateOfBirth = sdf.parse(params.dateOfBirth)
		}

        if (!patientInstance.save(flush: true)) {
            render(view: "edit", model: [patientInstance: patientInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'patient.label', default: 'Patient'), patientInstance.id])
        redirect(action: "show", id: patientInstance.id)
    }

    def delete(Long id) {
        def patientInstance = Patient.get(id)
        if (!patientInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'patient.label', default: 'Patient'), id])
            redirect(action: "list")
            return
        }

        try {
            patientInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'patient.label', default: 'Patient'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'patient.label', default: 'Patient'), id])
            redirect(action: "show", id: id)
        }
    }
}
