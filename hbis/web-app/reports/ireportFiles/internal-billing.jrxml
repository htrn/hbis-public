<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="internal-billing" language="groovy" pageWidth="612" pageHeight="792" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="b22561b3-3872-403a-b2af-6f19ba726078">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="organization" class="java.lang.String"/>
	<parameter name="fromDate" class="java.util.Date"/>
	<parameter name="toDate" class="java.util.Date"/>
	<field name="investigator" class="java.lang.String"/>
	<field name="shippingCart.shipDate" class="java.util.Date"/>
	<field name="specimen.procurementDate" class="java.util.Date"/>
	<field name="specimen.specimenChtnId" class="java.lang.String"/>
	<field name="subspecimenChtnId" class="java.lang.String"/>
	<field name="preparationType" class="java.lang.String"/>
	<field name="specimen.qcResult.finalPrimaryAnatomicSite" class="java.lang.String"/>
	<field name="specimen.qcResult.finalTissueType" class="java.lang.String"/>
	<field name="shippingCart.eRampNumber" class="java.lang.String"/>
	<field name="price" class="java.math.BigDecimal"/>
	<variable name="investigator_count" class="java.lang.Integer" resetType="Group" resetGroup="Investigator" calculation="Count">
		<variableExpression><![CDATA[$F{investigator}]]></variableExpression>
	</variable>
	<variable name="price_sum" class="java.math.BigDecimal" resetType="Group" resetGroup="Investigator" calculation="Sum">
		<variableExpression><![CDATA[$F{price}]]></variableExpression>
	</variable>
	<group name="Investigator">
		<groupExpression><![CDATA[$F{investigator}]]></groupExpression>
		<groupHeader>
			<band height="41">
				<textField>
					<reportElement uuid="ba868f91-5042-4b55-85a2-608dbe5ab234" x="76" y="0" width="128" height="20"/>
					<textElement>
						<font size="12" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{investigator}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="f0d6efbb-4349-426d-87d7-08ddbba2573f" x="0" y="0" width="76" height="20"/>
					<textElement>
						<font size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Investigator: ]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="78e95d6e-509d-488e-b62b-9aae5449c2ce" x="0" y="20" width="57" height="20"/>
					<box>
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center"/>
					<text><![CDATA[Ship Date]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="01ccecc5-cb82-4e35-9206-bdd82e7be4d1" x="57" y="20" width="57" height="20"/>
					<box>
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center"/>
					<text><![CDATA[Proc Date]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="9f7e5c92-fc95-4bbb-ba53-86018cd04a3e" x="114" y="20" width="86" height="20"/>
					<box>
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center"/>
					<text><![CDATA[Specimen ID]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="2d974288-3bff-4c0c-9c62-45b204939d46" x="200" y="20" width="80" height="20"/>
					<box>
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center"/>
					<text><![CDATA[Prep Type]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="17b28231-2990-423d-a280-4cec7868023a" x="280" y="20" width="106" height="20"/>
					<box>
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center"/>
					<text><![CDATA[Fin. Prim. Ana. Site]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="fb69a76d-a688-4e27-81ed-5a21eee019a0" x="386" y="20" width="73" height="20"/>
					<box>
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center"/>
					<text><![CDATA[Final Tiss. Type]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="ad90a86d-af94-47b6-8608-218b122bfbfa" x="459" y="20" width="59" height="20"/>
					<box>
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center"/>
					<text><![CDATA[eRamp Num]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="ae303d90-01a3-4af8-905c-3ac99baae245" x="518" y="20" width="54" height="20"/>
					<box>
						<pen lineWidth="1.0"/>
						<topPen lineWidth="1.0"/>
						<leftPen lineWidth="1.0"/>
						<bottomPen lineWidth="1.0"/>
						<rightPen lineWidth="1.0"/>
					</box>
					<textElement textAlignment="Center"/>
					<text><![CDATA[Price]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField>
					<reportElement uuid="2fad266c-8c2a-4041-8a65-49e172111c2c" x="448" y="0" width="35" height="20"/>
					<textElement>
						<font size="12"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{investigator_count}]]></textFieldExpression>
				</textField>
				<textField pattern="">
					<reportElement uuid="434af3bc-2baa-4994-a684-980cffd60882" x="518" y="0" width="54" height="20"/>
					<textElement>
						<font size="12" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{price_sum}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="fb688d93-ee75-4337-b64d-54eec80ad9f1" x="483" y="0" width="35" height="20"/>
					<textElement>
						<font size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Total:]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="fb688d93-ee75-4337-b64d-54eec80ad9f1" x="391" y="0" width="57" height="20"/>
					<textElement>
						<font size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Quantity:]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="40" splitType="Stretch">
			<staticText>
				<reportElement uuid="f602f879-dc43-4855-9106-6cc6a611e102" x="36" y="0" width="140" height="20"/>
				<textElement>
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[ - Internal Billing Report]]></text>
			</staticText>
			<textField>
				<reportElement uuid="3810f0b4-547d-44fa-8fdf-874c4140973d" x="0" y="0" width="36" height="20"/>
				<textElement>
					<font size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{organization}]]></textFieldExpression>
			</textField>
			<textField pattern="MM/dd/yyyy">
				<reportElement uuid="9e03f8b8-cce9-476b-a588-62a72f879d4f" x="68" y="20" width="67" height="20"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{fromDate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="f602f879-dc43-4855-9106-6cc6a611e102" x="0" y="20" width="68" height="20"/>
				<textElement>
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[From Date: ]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f602f879-dc43-4855-9106-6cc6a611e102" x="135" y="20" width="52" height="20"/>
				<textElement>
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[To Date: ]]></text>
			</staticText>
			<textField pattern="MM/dd/yyyy">
				<reportElement uuid="9e03f8b8-cce9-476b-a588-62a72f879d4f" x="187" y="20" width="67" height="20"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{toDate}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField pattern="MM/dd/yyyy">
				<reportElement uuid="1e38cf47-7dfa-4af7-a4ce-8e7588d08896" x="0" y="0" width="57" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{shippingCart.shipDate}]]></textFieldExpression>
			</textField>
			<textField pattern="MM/dd/yyyy" isBlankWhenNull="false">
				<reportElement uuid="107f14e8-ae42-4496-a861-2c9f642be1af" x="57" y="0" width="57" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{specimen.procurementDate}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="76fdf33b-ed16-41e7-bf0e-f7f2bb73b03d" x="386" y="0" width="73" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{specimen.qcResult.finalTissueType}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="3acc4100-b816-469d-9094-48988e421aae" x="459" y="0" width="59" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{shippingCart.eRampNumber}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="f5f7cc50-4878-452b-bfe9-5bf49705a70e" x="518" y="0" width="54" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{price}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="7fff6dfe-551f-4a81-9949-ebaf9cf45d76" x="114" y="0" width="65" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{specimen.specimenChtnId}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="fbdd7355-de0b-4968-af46-ba5f83989514" x="179" y="0" width="21" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{subspecimenChtnId}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="adde444e-a7d8-4574-91eb-7adfd44621d2" x="200" y="0" width="80" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{preparationType}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="db64b981-ea6c-4666-a194-2e199d4e9285" x="280" y="0" width="106" height="20"/>
				<box>
					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{specimen.qcResult.finalPrimaryAnatomicSite}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="29" splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="42" splitType="Stretch"/>
	</summary>
</jasperReport>
