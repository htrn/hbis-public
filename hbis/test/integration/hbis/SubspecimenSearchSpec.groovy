package hbis

import grails.plugin.spock.IntegrationSpec

class SubspecimenSearchSpec extends IntegrationSpec {
    def subspecimenService

    void "subspecimen search should be able to search by specimen time_prepared for times between 7:00 to 10:00"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timePreparedFrom and timePreparedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.timePreparedFrom = '0700'
        subspecimenController.params.timePreparedTo = '1000'

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 6

        def preparedFromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 07:00:00")
        def preparedToDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 10:00:00")

        // try and find a specimen that has a timePrepared time outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.timePrepared < preparedFromDate || subspecimen.specimen.timePrepared > preparedToDate
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen time_prepared with a value for timePreparedFrom but not timePreparedTo"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timePreparedFrom but not timePreparedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.timePreparedFrom = '1330'

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 111
    }

    void "subspecimen search should be able to search by specimen time_prepared with values 13:00 to 19:00"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timePreparedFrom and timePreparedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.timePreparedFrom = '1300'
        subspecimenController.params.timePreparedTo = '1900'

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 9

        def preparedFromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 13:00:00")
        def preparedToDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 19:00:00")

        // try and find a specimen that has a timePrepared time outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.timePrepared < preparedFromDate || subspecimen.specimen.timePrepared > preparedToDate
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen time_prepared with a value for timePreparedTo but not timePreparedFrom"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timePreparedFrom but not timePreparedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.timePreparedTo = '1130'

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 111
    }

    void "subspecimen search should be able to search by specimen time_prepared if there are no values for timePreparedTo or timePreparedFrom"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timePreparedFrom but not timePreparedTo'
        subspecimenController.params.formSubmitted = "true"

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 111
    }

    void "subspecimen search should be able to search by specimen time_received with values 09:00 to 13:00"(){
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timeReceivedFrom and timeReceivedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.timeReceivedFrom = '0900'
        subspecimenController.params.timeReceivedTo = '1300'

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 18

        def receivedFromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 09:00:00")
        def receivedToDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 13:00:00")

        // try and find a specimen that has a timeReceived date outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.timeReceived < receivedFromDate || subspecimen.specimen.timeReceived > receivedToDate
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen time_received with a value for timeReceivedFrom but not timeReceivedTo"(){
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timeReceivedFrom but not timeReceivedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.timeReceivedFrom = '1400'

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 111
    }

    void "subspecimen search should be able to search by specimen time_received with values 06:00 to 09:00"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timeReceivedFrom and timeReceivedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.timeReceivedFrom = '0600'
        subspecimenController.params.timeReceivedTo = '0900'

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 7

        def receivedFromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 06:00:00")
        def receivedToDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 09:00:00")

        // try and find a specimen that has a timeReceived date outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.timeReceived < receivedFromDate || subspecimen.specimen.timeReceived > receivedToDate
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen time_received with a value for timeReceivedTo but not timeReceivedFrom"(){
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timeReceivedTo but not timeReceivedFrom'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.timeReceivedTo = '1200'

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 111
    }
    
    void "subspecimen search should be able to search by specimen time_received if there are no values for timeReceivedTo or timeReceivedFrom"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timeReceivedFrom but not timeReceivedTo'
        subspecimenController.params.formSubmitted = "true"

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 111
    }

    void "subspecimen search should be able to search by specimen category #8 (BUFFY COAT)"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for specimen category'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.specimenCategoryId = 8

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 3

        // make sure each specimen has the correct value for metsToAnatomicSite
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.specimenCategory.toString() != 'BUFFY COAT'
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen category #3 (BLOOD)"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for specimen category'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.specimenCategoryId = 3

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 16

        // make sure each specimen has the correct value for metsToAnatomicSite
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.specimenCategory.toString() != 'BLOOD'
        }

        subspecimen == null
    }
    
    void "subspecimen search should be able to search by metsToAnatomicSite #3 (ANUS)"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for specimen metsToAnatomicSite'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.metsToAnatomicSite = 3

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 3

        // make sure each specimen has the correct value for metsToAnatomicSite
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.metsToAnatomicSite.toString() != 'ANUS'
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by metsToAnatomicSite #2 (ADRENAL GLAND)"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for specimen metsToAnatomicSite'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.metsToAnatomicSite = 2

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 8

        // make sure each specimen has the correct value for metsToAnatomicSite
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.metsToAnatomicSite.toString() != 'ADRENAL GLAND'
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by tech initials 'JH'"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for specimen tech initials'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.techInitialsId = 2

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 78

        // make sure each specimen has the correct value for techInitialsId
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.techInitials.toString() != 'JH'
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by tech initials 'AD'"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for specimen tech initials'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.techInitialsId = 1

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 4

        // make sure each specimen has the correct value for techInitialsId
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.techInitials.toString() != 'AD'
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by label comment"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for subspecimen comment label'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.subspecimenLabelComment = "hoverboard maker Swagway"

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct properties'
        results.subspecimens.size() == 2

        // make sure each subspecimen has the correct value for label comment
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.labelComment.contains("hoverboard maker Swagway") == false
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by label comment case insensitive"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for subspecimen comment label'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.subspecimenLabelComment = "Hoverboard Maker swagway"

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct properties'
        results.subspecimens.size() == 2

        // make sure each subspecimen has the correct value for label comment
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.labelComment.contains("hoverboard maker Swagway") == false
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by qc follow up action 'Failed - To Destroy'"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for subspecimen comment label'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.qcFollowUpAction = 2

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain a subspecimen object with the proper follow up action'
        results.subspecimens.size() == 7

        // make sure each specimen has the correct value for qc result follow up action
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.qcResult.qcFollowUpAction.toString() != "Failed - To Destroy"
        }

        subspecimen == null
    }

    void "subspecimen search should find failing tissueQcMatch values"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for subspecimen comment label'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.tissueQcMatchPassFail = "Fail"

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain a subspecimen object with the proper follow up action'
        results.subspecimens.size() == 2

        // make sure each specimen has the correct value for tissue qc match
        def subspecimen = results.subspecimens.find { subspecimen ->
            !subspecimen.specimen.qcResult.tissueQcMatch.description.toString().contains("Fail")
        }

        subspecimen == null
    }

    void "subspecimen search should find passing tissueQcMatch values"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include a value for subspecimen comment label'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.tissueQcMatchPassFail = "Pass"

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain a subspecimen object with the proper follow up action'
        results.subspecimens.size() == 38

        // make sure each specimen has the correct value for tissue qc match
        def subspecimen = results.subspecimens.find { subspecimen ->
            !subspecimen.specimen.qcResult.tissueQcMatch.description.toString().contains("Pass")
        }

        subspecimen == null
    }

    void "subspecimen search results should be sorted by specimen chtn id then subspecimen chtn id"() {
        given: 'a subspecimen service'
        assert subspecimenService != null

        def params = [:]

        params.procurementDateFromTextField = '04/01/2016'
        params.procurementDateToTextField = '04/30/2016'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each result should be sorted by specimen id then subspecimen chtn id'
        //sort the results manually, then compare that to the returned result set
        def sorted = results.sort { a, b ->
            a.specimen.specimenChtnId <=> b.specimen.specimenChtnId ?: a.subspecimenChtnId <=> b.subspecimenChtnId
        }

        results == sorted
    }

    void "subspecimen search should be able to search by ship date for dates between 11/01/2015 to 11/30/2015"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timePreparedFrom and timePreparedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.shipDateFromTextField = "11/01/2015"
        subspecimenController.params.shipDateToTextField = "11/30/2015"

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 10

        def shipDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-01 00:00:00")
        def shipDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-30 00:00:00")

        // try and find a specimen that has a timePrepared time outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.shippingCart.shipDate < shipDateFrom || subspecimen.shippingCart.shipDate > shipDateTo
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by invoice date for dates between 02/01/2014 to 02/28/2014"() {
        given: 'SubspecimenController'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for timePreparedFrom and timePreparedTo'
        subspecimenController.params.formSubmitted = "true"
        subspecimenController.params.invoiceDateFromTextField = "02/01/2014"
        subspecimenController.params.invoiceDateToTextField = "02/28/2014"

        when: 'search is called'
        def results = subspecimenController.search()

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.subspecimens.size() == 5

        def invoiceDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2014-02-01 00:00:00")
        def invoiceDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2014-02-28 00:00:00")

        // try and find a specimen that has a timePrepared time outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.shippingCart.invoice.invoiceDate < invoiceDateFrom || subspecimen.shippingCart.invoice.invoiceDate > invoiceDateTo
        }

        subspecimen == null
    }
}