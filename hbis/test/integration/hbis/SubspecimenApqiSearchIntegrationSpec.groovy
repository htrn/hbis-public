package hbis

import grails.plugin.spock.IntegrationSpec
import spock.lang.Ignore

class SubspecimenApqiSearchIntegrationSpec extends IntegrationSpec {
    void "subspecimen apqi search should be able to search by specimen procurement date from and to"() {
        given: 'Subspecimen Controller'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for fromDate and toDate'
        subspecimenController.params.fromDate = '12/23/2000'
        subspecimenController.params.toDate = '12/23/2015'

        when: 'apqi() is called'
        def results = subspecimenController.apqi()

        then: 'apqi search results are returned'
        results.distinctPatients == 8
        results.totalQcPerformed == 13
        results.partsPerCase == 25
        results.procurementResultQuantityList[0].quantity == 13
        results.procedureNameSubNameByResultQuantityList[0].quantity == 7
        results.procedureNameSubNameByResultQuantityList[0].procedureNameDescription == 'Adrenalectomy'
    }

    @Ignore
    void "subspecimen apqi search should be able to search by specimen procurement date from 04/01/2016 to 04/30/2016"() {
        given: 'Subspecimen Controller'
        def subspecimenController = new SubspecimenController()

        and: 'search parameters that include values for fromDate and toDate'
        subspecimenController.params.fromDate = '04/01/2016'
        subspecimenController.params.toDate = '04/30/2016'

        when: 'apqi() is called'
        def results = subspecimenController.apqi()

        then: 'apqi search results are returned'
        results.distinctPatients == 0
        results.totalQcPerformed == 0
        results.numberQcPassed == 0
        results.numberQcFailed == 0
        results.partsPerCase == 0
        results.totalSubspecimensProcured == 9
        results.procurementResultQuantityList[0].quantity == 1
        results.procedureNameSubNameByResultQuantityList[0].quantity == 1
        results.procedureNameSubNameByResultQuantityList[0].procedureNameDescription == 'Adrenalectomy'
    }
}
