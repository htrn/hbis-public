package hbis

import grails.plugin.spock.IntegrationSpec

class SubspecimenServiceIntegrationSpec extends IntegrationSpec{
    def subspecimenService

    void "the subspecimen service should return a list of subspecimens in a date range for the searchQcReport"(){
        given: 'a subspecimen service'
        assert subspecimenService != null

        def params = [:]

        params.fromDate = '04/01/2016'
        params.toDate = '04/23/2016'
        params.tissueQcMatch = 'Pass'

        when: 'searchQcReport is called'
        def results = subspecimenService.searchQcReport(params)

        then: 'there should be no subspecimens outside the selected date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-23 00:00:00")

        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < fromDate || subspecimen.specimen.procurementDate > toDate
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of subspecimens in a date range for the subcontractReport"(){
        def params = [:]

        params.fromDate = '11/01/2015'
        params.toDate = '11/30/2015'
        params.organization = 'CCF'

        when: 'subcontractReport is called'
        def results = subspecimenService.subcontractReport(params)

        then: 'there should be no subspecimens that belong to shipping carts outside the selected date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-30 00:00:00")

        def subspecimen = results.find { subspecimen ->
            subspecimen.shippingCart.shipDate < fromDate || subspecimen.shippingCart.shipDate > toDate
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of subspecimens in a date range for the internalBillingReport"(){
        def params = [:]

        params.fromDate = '11/01/2015'
        params.toDate = '11/30/2015'
        params.organization = 'CCC'

        when: 'internalBillingReport is called'
        def results = subspecimenService.internalBillingReport(params)

        then: 'there should be no subspecimens that belong to shipping carts outside the selected date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-30 00:00:00")

        def subspecimen = results.find { subspecimen ->
            subspecimen.shippingCart.shipDate < fromDate || subspecimen.shippingCart.shipDate > toDate
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of subspecimens in a date range for the searchDistributionReport"(){
        def params = [:]

        params.fromDate = '11/01/2015'
        params.toDate = '11/30/2015'
        params.investigatorId = '351'

        when: 'searchDistributionReport is called'
        def results = subspecimenService.searchDistributionReport(params)

        then: 'there should be no subspecimens that belong to specimens with procurement dates outside the selected date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-30 00:00:00")

        def subspecimen = results.find { subspecimen ->
            subspecimen.shippingCart.shipDate < fromDate || subspecimen.shippingCart.shipDate > toDate
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of subspecimens in a date range for the searchDistributionReport 2"(){
        def params = [:]

        params.fromDate = '11/01/2015'
        params.toDate = '11/17/2015'
        params.investigatorId = '351'

        when: 'searchDistributionReport is called'
        def results = subspecimenService.searchDistributionReport(params)

        then: 'there should be no subspecimens that belong to specimens with procurement dates outside the selected date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-17 00:00:00")

        def subspecimen = results.find { subspecimen ->
            subspecimen.shippingCart.shipDate < fromDate || subspecimen.shippingCart.shipDate > toDate
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of subspecimens in a date range for the searchQc"(){
        def fromDate = Date.parse('MM/dd/yyyy', '04/01/2016'), toDate = Date.parse('MM/dd/yyyy', '04/30/2016'), tissueQcMatch = 'Pass'

        when: 'searchDistributionReport is called'
        def results = subspecimenService.searchQc(fromDate, toDate, tissueQcMatch)

        then: 'there should be no subspecimens that belong to specimens with procurement dates outside the selected date range'
        def from = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")
        def to = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-30 00:00:00")

        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < from || subspecimen.specimen.procurementDate > to
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of subspecimens in a date range for the searchProcurementReport"(){
        def fromDate = Date.parse('MM/dd/yyyy', '04/01/2015'), toDate = Date.parse('MM/dd/yyyy', '04/20/2015'), investigatorId = '351'

        when: 'searchDistributionReport is called'
        def results = subspecimenService.searchProcurementReport(investigatorId, fromDate, toDate)

        then: 'there should be no subspecimens that belong to specimens with procurement dates outside the selected date range'
        def from = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-04-01 00:00:00")
        def to = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-04-20 00:00:00")

        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < from || subspecimen.specimen.procurementDate > to
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of subspecimens in a date range for the chtnAnnualReport"(){
        def params = [:]

        params.fromDate = '11/01/2015'
        params.toDate = '11/30/2015'
        params.investigatorId = '351'

        when: 'searchDistributionReport is called'
        def results = subspecimenService.chtnAnnualReport(params)

        then: 'the chart review shipped quantity count should not include shipping carts outside the date range'
        results.chartReviewShippedQuantity == 4
    }

    void "the subspecimen service should return a list of subspecimens for a subcontract query"() {
        when: 'subcontractQuery is called'
        def subspecimens = subspecimenService.subcontractQuery('CCF',
                Date.parse('MM/dd/yyyy', '11/01/2015'),
                Date.parse('MM/dd/yyyy', '11/30/2015'))

        then: 'there should be no subspecimens that belong to a shipping cart outside the date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-30 00:00:00")

        def subspecimen = subspecimens.find { subspecimen ->
            subspecimen.shippingCart.shipDate < fromDate || subspecimen.shippingCart.shipDate > toDate
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of specimens for internal billing"() {
        when: 'internalBilling is called'
        def subspecimens = subspecimenService.internalBilling('CCC',
                Date.parse('MM/dd/yyyy', '11/01/2015'),
                Date.parse('MM/dd/yyyy', '11/30/2015'))

        then: 'there should be no subspecimens that belong to a shipping cart outside the date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-30 00:00:00")

        def subspecimen = subspecimens.find { subspecimen ->
            subspecimen.shippingCart.shipDate < fromDate || subspecimen.shippingCart.shipDate > toDate
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of specimens for search procurement"() {
        when: 'searchProcurement is called'
        def results = subspecimenService.searchProcurement(
                null,
                Date.parse('MM/dd/yyyy', '04/01/2016'),
                Date.parse('MM/dd/yyyy', '04/30/2016')
        )

        then: 'there should be no subspecimens that belong to specimens with procurement dates outside the selected date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-30 00:00:00")

        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < fromDate || subspecimen.specimen.procurementDate > toDate
        }

        subspecimen == null
    }

    void "the subspecimen service should return a list of specimens for search distribution"() {
        when: 'searchDistribution is called'
        def subspecimens = subspecimenService.searchDistribution(
                351,
                Date.parse('MM/dd/yyyy', '11/01/2015'),
                Date.parse('MM/dd/yyyy', '11/30/2015'))

        then: 'there should be no subspecimens that belong to a shipping cart outside the date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-11-30 00:00:00")

        def subspecimen = subspecimens.find { subspecimen ->
            subspecimen.shippingCart.shipDate < fromDate || subspecimen.shippingCart.shipDate > toDate
        }

        subspecimen == null
    }
}
