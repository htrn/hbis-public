package hbis

import grails.plugin.spock.IntegrationSpec
import spock.lang.Ignore

class ChartReviewIntegrationSpec extends IntegrationSpec {
    void "listSubspecimensNeedChartReview should only return subspecimens in the date range"() {
        given: 'a shipping cart controller'
        def chartReviewController = new ChartReviewController()

        and: 'request parameters that include a date range'
        chartReviewController.params.fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")
        chartReviewController.params.toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-30 23:59:59")

        when: 'listSubspecimensNeedChartReview is called'
        def results = chartReviewController.listSubspecimensNeedChartReview()

        then: 'there should be no subspecimens that belong to specimens outside the date range'
        results.subspecimens.size() == 9

        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-30 23:59:59")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.procurementDate < fromDate || subspecimen.specimen.procurementDate > toDate
        }

        subspecimen == null
    }

    // This test will be difficult to maintain in the future since it depends on finding subspecimens
    // from the previous 7 days. The procurement dates of some of the records in the specimen table will
    // need to be altered in order to make this test pass with any consistency
    @Ignore
    void "listSubspecimensNeedChartReview should return subspecimens from the previous 7 days if no date range is given"() {
        given: 'a shipping cart controller'
        def chartReviewController = new ChartReviewController()

        when: 'listSubspecimensNeedChartReview is called with no values for fromDate or toDate'
        def results = chartReviewController.listSubspecimensNeedChartReview()

        then: 'there should be no subspecimens that belong to specimens outside the date range'
        results.subspecimens.size() == 4

        def fromDate = new Date() - 7
        def toDate = new Date()

        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.procurementDate < fromDate || subspecimen.specimen.procurementDate > toDate
        }

        subspecimen == null
    }

    void "listSubspecimensNeedChartReview should return subspecimens if a value is given for fromDate but not toDate"() {
        given: 'a shipping cart controller'
        def chartReviewController = new ChartReviewController()

        and: 'request parameters that include a fromDate, but no toDate'
        chartReviewController.params.fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-01-01 00:00:00")

        when: 'listSubspecimensNeedChartReview is called'
        def results = chartReviewController.listSubspecimensNeedChartReview()

        then: 'there should be no subspecimens that belong to specimens outside the date range'
        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-01-01 00:00:00")
        def toDate = new Date()

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.procurementDate < fromDate || subspecimen.specimen.procurementDate > toDate
        }

        subspecimen == null
    }

    void "listSubspecimensNeedChartReview should return subspecimens if a value is given for toDate but not fromDate"() {
        given: 'a shipping cart controller'
        def chartReviewController = new ChartReviewController()

        and: 'request parameters that include a fromDate, but no toDate'
        chartReviewController.params.toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-01-01 00:00:00")

        when: 'listSubspecimensNeedChartReview is called'
        def results = chartReviewController.listSubspecimensNeedChartReview()

        then: 'there should be no subspecimens that belong to specimens outside the date range'
        results.subspecimens.size() == 6

        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "1970-01-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-01-01 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.subspecimens.find { subspecimen ->
            subspecimen.specimen.procurementDate < fromDate || subspecimen.specimen.procurementDate > toDate
        }

        subspecimen == null
    }
}
