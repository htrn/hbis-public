package hbis

import grails.plugin.spock.IntegrationSpec

class SpecimenIntegrationSpec extends IntegrationSpec {
    void "the pathology report should return a list of specimens with procedure dates between 03/01/2015 and 06/30/2015"(){
        given: 'Specimen Controller'
        def specimenController = new SpecimenController()

        and: 'search parameters that include values for fromDate and toDate'
        specimenController.params.fromDate = '03/01/2015'
        specimenController.params.toDate = '06/30/2015'

        when: 'pathologyReportList() is called'
        def results = specimenController.pathologyReportList()

        then: 'report results are returned'

        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-03-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-06-30 00:00:00")

        def specimen = results.specimenInstanceList.find { specimen ->
            specimen.procedure.procedureDate < fromDate || specimen.procedure.procedureDate > toDate
        }

        specimen == null
    }
}
