package hbis

import spock.lang.*

class ShippingCartServiceSpec extends Specification {
	def shippingCartService

	def setup() {
		shippingCartService = new ShippingCartService()
	}

	def cleanup() {
	}

	void "get a shipping cart with subspecimens sorted by specimen chtn id and then subspecimen chtn id"() {
		given: "a shipping cart in the database with multiple subspecimens and chart reviews"
		assert ShippingCart.get(24) != null

		when: "a user selects the shipping cart with an id of 24"
		def shippingCart = shippingCartService.getShippingCartForPackingSlip(24)

		then: "the shipping cart should have 4 subspecimens sorted by specimen chart number id then subspecimen chart number id"
		shippingCart[0].subspecimenList.size() == 8

		shippingCart[0].subspecimenList[0].subspecimenChtnId == '1'
		shippingCart[0].subspecimenList[0].specimen.specimenChtnId == 'M1141015C'

		shippingCart[0].subspecimenList[1].subspecimenChtnId == '1'
		shippingCart[0].subspecimenList[1].specimen.specimenChtnId == 'M1141015G'

		shippingCart[0].subspecimenList[2].subspecimenChtnId == '1'
		shippingCart[0].subspecimenList[2].specimen.specimenChtnId == 'M1141015H'

		shippingCart[0].subspecimenList[3].subspecimenChtnId == '2'
		shippingCart[0].subspecimenList[3].specimen.specimenChtnId == 'M1141015H'

		shippingCart[0].subspecimenList[4].subspecimenChtnId == '1'
		shippingCart[0].subspecimenList[4].specimen.specimenChtnId == 'M1141015I'

		shippingCart[0].subspecimenList[5].subspecimenChtnId == 'Q'
		shippingCart[0].subspecimenList[5].specimen.specimenChtnId == 'M1141015I'

		shippingCart[0].subspecimenList[6].subspecimenChtnId == '03'
		shippingCart[0].subspecimenList[6].specimen.specimenChtnId == 'M1141030A'

		shippingCart[0].subspecimenList[7].subspecimenChtnId == '2'
		shippingCart[0].subspecimenList[7].specimen.specimenChtnId == 'M1150009A'
	}
}