package hbis

import grails.plugin.spock.IntegrationSpec
import spock.lang.Unroll

class ShippingCartControllerIntegrationSpec extends IntegrationSpec{
    void "add multiple subspecimens to a shipping cart"() {
        given: 'a shipping cart controller'
        def shippingCartController = new ShippingCartController()

        and: 'a shipping cart with an id of 25'
        def shippingCart = ShippingCart.get(25)

        and: 'request parameters that include a shipping cart id, a list of subspecimen ids, and an "Add" parameter'
        shippingCartController.params.subspecimensToAdd = ['45', '54', '57']
        shippingCartController.params.shippingCartId = '25'
        shippingCartController.params.addRemove = 'Add'
        shippingCartController.params.shippingCartVersion = '11'

        expect: 'this shipping cart to have a certain number of subspecimens before we add more subspecimens'
        shippingCart.subspecimens.size() == 2

        when: 'addRemoveSubspecimen() is called'
        shippingCartController.addRemoveSubspecimen()
        shippingCart = ShippingCart.get(25)

        then: 'the shipping cart should now have three additional subspecimens'
        shippingCart.subspecimens.size() == 5
    }

    void "add a single subspecimen to a shipping cart"() {
        given: 'a shipping cart controller'
        def shippingCartController = new ShippingCartController()

        and: 'a shipping cart with an id of 25'
        def shippingCart = ShippingCart.get(25)

        and: 'request parameters that include a shipping cart id, a list of subspecimen ids, and an "Add" parameter'
        shippingCartController.params.subspecimensToAdd = ['45']
        shippingCartController.params.shippingCartId = '25'
        shippingCartController.params.addRemove = 'Add'
        shippingCartController.params.shippingCartVersion = '11'

        expect: 'this shipping cart to have a certain number of subspecimens before we add more subspecimens'
        shippingCart.subspecimens.size() == 2

        when: 'addRemoveSubspecimen() is called'
        shippingCartController.addRemoveSubspecimen()
        shippingCart = ShippingCart.get(25)

        then: 'the shipping cart should now have one additional subspecimen'
        shippingCart.subspecimens.size() == 3
    }

    void "remove a single subspecimen from a shipping cart"() {
        given: 'a shipping cart controller'
        def shippingCartController = new ShippingCartController()

        and: 'a shipping cart with an id of 25'
        def shippingCart = ShippingCart.get(25)

        and: 'request parameters that include a shipping cart id, a list of subspecimen ids, and an "Add" parameter'
        shippingCartController.params.subspecimensToRemove = ['66']
        shippingCartController.params.shippingCartId = '25'
        shippingCartController.params.addRemove = 'Remove'
        shippingCartController.params.shippingCartVersion = '11'

        expect: 'this shipping cart to have a certain number of subspecimens before we add more subspecimens'
        shippingCart.subspecimens.size() == 2

        when: 'addRemoveSubspecimen() is called'
        shippingCartController.addRemoveSubspecimen()
        shippingCart = ShippingCart.get(25)

        then: 'the shipping cart should now have one fewer subspecimen'
        shippingCart.subspecimens.size() == 1
    }

    void "remove multiple subspecimens from a ahipping cart"() {
        given: 'a shipping cart controller'
        def shippingCartController = new ShippingCartController()

        and: 'a shipping cart with an id of 25'
        def shippingCart = ShippingCart.get(25)

        and: 'request parameters that include a shipping cart id, a list of subspecimen ids, and an "Add" parameter'
        shippingCartController.params.subspecimensToRemove = ['66', '44']
        shippingCartController.params.shippingCartId = '25'
        shippingCartController.params.addRemove = 'Remove'
        shippingCartController.params.shippingCartVersion = '11'

        expect: 'this shipping cart to have a certain number of subspecimens before we add more subspecimens'
        shippingCart.subspecimens.size() == 2

        when: 'addRemoveSubspecimen() is called'
        shippingCartController.addRemoveSubspecimen()
        shippingCart = ShippingCart.get(25)

        then: 'the shipping cart should now have two fewer subspecimens'
        shippingCart.subspecimens.size() == 0
    }

    @Unroll
    void "Search for #status Shipping Carts"(){
        given: 'a shipping cart controller'
        def controller = new ShippingCartController()

        when: 'search a cart with shipping cart ID'
        controller.params.shippingCartId = shippingCartId
        controller.editErampNumber()

        then: 'get message'
        controller.flash.message == flashMessage

        where:
        status       |shippingCartId  | flashMessage
        "shipped"    |1               | null
        "deleted"    |17              | "The criteria selected did not produce any results.  Try again with different criteria."

    }
}
