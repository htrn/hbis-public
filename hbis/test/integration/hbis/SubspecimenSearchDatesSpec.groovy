package hbis

import grails.plugin.spock.IntegrationSpec

class SubspecimenSearchDatesSpec extends IntegrationSpec{
    def subspecimenService

    void "subspecimen search should be able to search by specimen procurement date for dates between 04/01/2015 to 04/30/2015"() {
        given: 'Search parameters that include values for procurementDateFromTextField and procurementDateToTextField'
        def params = [:]

        params.procurementDateFromTextField = '04/01/2015'
        params.procurementDateToTextField = '04/30/2015'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.size() == 10

        def procurementDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-04-01 00:00:00")
        def procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-04-30 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < procurementDateFrom || subspecimen.specimen.procurementDate > procurementDateTo
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen procurement date for dates between 03/01/2015 to 03/31/2015"() {
        given: 'Search parameters that include values for procurementDateFromTextField and procurementDateToTextField'
        def params = [:]

        params.procurementDateFromTextField = '03/01/2015'
        params.procurementDateToTextField = '03/31/2015'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.size() == 9

        def procurementDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-03-01 00:00:00")
        def procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-03-31 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < procurementDateFrom || subspecimen.specimen.procurementDate > procurementDateTo
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen procurement date for dates between 04/01/2014 to 04/30/2014"() {
        given: 'Search parameters that include values for procurementDateFromTextField and procurementDateToTextField'
        def params = [:]

        params.procurementDateFromTextField = '04/01/2014'
        params.procurementDateToTextField = '04/30/2014'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.size() == 9

        def procurementDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2014-04-01 00:00:00")
        def procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2014-04-30 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < procurementDateFrom || subspecimen.specimen.procurementDate > procurementDateTo
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen procurement date for dates between 05/01/2015 to 05/31/2015"() {
        given: 'Search parameters that include values for procurementDateFromTextField and procurementDateToTextField'
        def params = [:]

        params.procurementDateFromTextField = '05/01/2015'
        params.procurementDateToTextField = '05/31/2015'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.size() == 10

        def procurementDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-05-01 00:00:00")
        def procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2015-05-31 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < procurementDateFrom || subspecimen.specimen.procurementDate > procurementDateTo
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen procurement date for dates between 06/01/2014 to 06/30/2014"() {
        given: 'Search parameters that include values for procurementDateFromTextField and procurementDateToTextField'
        def params = [:]

        params.procurementDateFromTextField = '06/01/2014'
        params.procurementDateToTextField = '06/30/2014'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.size() == 10

        def procurementDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2014-06-01 00:00:00")
        def procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2014-06-30 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < procurementDateFrom || subspecimen.specimen.procurementDate > procurementDateTo
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen procurement date for dates between 04/01/2016 to 04/30/2016"() {
        given: 'Search parameters that include values for procurementDateFromTextField and procurementDateToTextField'
        def params = [:]

        params.procurementDateFromTextField = '04/01/2016'
        params.procurementDateToTextField = '04/30/2016'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.size() == 9

        def procurementDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")
        def procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-30 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < procurementDateFrom || subspecimen.specimen.procurementDate > procurementDateTo
        }

        subspecimen == null
    }

    void "subspecimen search should be able to search by specimen procurement date when values for procurementDateFromTextField and procurementDateToTextField are the same"() {
        given: 'Search parameters that include values for procurementDateFromTextField and procurementDateToTextField'
        def params = [:]

        params.procurementDateFromTextField = '04/01/2016'
        params.procurementDateToTextField = '04/01/2016'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.size() == 2

        def procurementDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")
        def procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < procurementDateFrom || subspecimen.specimen.procurementDate > procurementDateTo
        }

        subspecimen == null
    }

    void "subspecimen search should include all specimen objects within the procurement date range"() {
        given: 'Search parameters that include values for procurementDateFromTextField and procurementDateToTextField'
        def params = [:]

        params.procurementDateFromTextField = '04/01/2016'
        params.procurementDateToTextField = '04/30/2016'

        when: 'search is called'
        def results = subspecimenService.generalSearch(params)

        then: 'each subspecimen object returned from the query should contain the correct specimen object'
        results.size() == 9

        def procurementDateFrom = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-01 00:00:00")
        def procurementDateTo = Date.parse("yyyy-MM-dd HH:mm:ss", "2016-04-30 00:00:00")

        // try and find a specimen that has a procurementDate outside the expected range
        def subspecimen = results.find { subspecimen ->
            subspecimen.specimen.procurementDate < procurementDateFrom || subspecimen.specimen.procurementDate > procurementDateTo
        }

        subspecimen == null
    }
}
