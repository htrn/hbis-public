package hbis

import grails.plugin.spock.IntegrationSpec

class InvoiceControllerIntegrationSpec extends IntegrationSpec {

	def setup() {
	}

	def cleanup() {
	}

	void "test paymentsReceivedReport without parameters"() {
		given : "invoice controller without parameters"
		def controller = new InvoiceController()
		controller.params.invoiceDateFrom = null
		controller.params.invoiceDateTo = null
		controller.params.investigatorId  = null
		controller.params._format = "XLS"
		controller.params._file = "payments-received"

		when : "the paymentsReceivedReport method is called"
		controller.paymentsReceivedReport()

		then : "it create a xsl file"
		"application/vnd.ms-excel" == controller.response.contentType
		200 == controller.response.status

		and : "the investigatorName variable in params should be 'All Invastigators'"
		"All Investigators" == controller.params.investigatorName
	}

	void "test paymentsReceivedReport with value of investigatorId"() {
		given : "invoice controller with ivestigatorId=351"
		def controller = new InvoiceController()
		controller.params.invoiceDateFrom = null
		controller.params.invoiceDateTo = null
		controller.params.investigatorId  = 351
		controller.params._format = "XLS"
		controller.params._file = "payments-received"

		when : "the paymentsReceivedReport method is called"
		controller.paymentsReceivedReport()

		then : "it create a xsl file"
		"application/vnd.ms-excel" == controller.response.contentType
		200 == controller.response.status

		and : "the investigatorName variable in params should be 'AARONSON, STUART: A1864'"
		"AARONSON, STUART: A1864" == controller.params.investigatorName
	}

	void "test search all investigators"() {
		given : "invoice controller without parameters"
		def controller = new InvoiceController()
		controller.params.invoiceDateFrom = null
		controller.params.invoiceDateTo = null
		controller.params.investigatorId  = "null"
		
		when : "the investigatorInvoices method is called"
		def model = controller.investigatorInvoices()
		
		then : "it return records from all investigators"
		9 == model.invoiceList.size()
	}
}