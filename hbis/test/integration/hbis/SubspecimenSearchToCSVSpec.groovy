package hbis

import grails.plugin.spock.IntegrationSpec

class SubspecimenSearchToCSVSpec extends IntegrationSpec {
    def subspecimenService

    void "test general search csv file header generation"() {
        given: "a map of search params"
        def params = [
                showSpecimenChtnId: "on",
                showMrn: "on",
                showGender: "on",
                showRace: "on",
                showOriginInstitution: "on",
                showProcedureType: "on",
                showPatientAge: "on", // if we show the age, show the unit as well
                timePreparedFrom: '09/01/2015',
                timePreparedTo: '09/30/2015'
        ]

        and: "a list of fields the user is interested in exporting"
        def fields = subspecimenService.findSelectedFields(params)

        when: "generateCSVHeader() is called"
        def csvHeader = subspecimenService.generateCSVHeader(fields)

        then: "a comma separated string of headers is returned"
        csvHeader == 'Specimen ID,Subspecimen ID,MRN,Gender,Race,Institution,Patient Age,Unit,Procedure Type\n'
    }

    void "test general search csv file header generation (two)"() {
        given: "a map of search params"
        def params = [
                showTissueQcMatch: "on",
                showRace: "on",
                showGender: "on",
                showProcurementDate: "on",
                showInvestigatorFirstName: "on",
                showFinalDiagnosis: "on",
                showInvoiceDate: "on",
                timePreparedFrom: '09/01/2015',
                timePreparedTo: '09/30/2015'
        ]

        and: "a list of fields the user is interested in exporting"
        def fields = subspecimenService.findSelectedFields(params)

        when: "generateCSVHeader() is called"
        def csvHeader = subspecimenService.generateCSVHeader(fields)

        then: "a comma separated string of headers is returned"
        csvHeader == 'Subspecimen ID,Gender,Race,Invest. First Name,Procurement Date,Invoice Date,Final Diagnosis,QC Result\n'
    }

    void "test general search csv file content generation"() {
        given: "a map of search params"
        def params = [
                showTissueQcMatch: "on",
                showRace: "on",
                showGender: "on",
                showProcurementDate: "on",
                showInvestigatorFirstName: "on",
                showFinalDiagnosis: "on",
                showInvoiceDate: "on",
                timePreparedFrom: '0800',
                timePreparedTo: '1700'
        ]

        and: "a list of subspecimens as a search result"
        def subspecimens = subspecimenService.generalSearch(params)

        and: "a list of fields the user is interested in exporting"
        def fields = subspecimenService.findSelectedFields(params)

        when: "generateCSVContent() is called"
        def csvContent = subspecimenService.generateCSVContent(subspecimens[0], fields)

        then: "a comma separated string of subspecimen values is generated"
        csvContent == '1,Female,White,STUART,2015-04-03 00:00:00.0,2014-02-01 00:00:00.0,null,Pass\n'
    }

    void "test general search csv file header generation for fields that are always displayed together (Invest. ID Prefix)"() {
        given: "a map of search params"
        def params = [
                showInvestigatorChtnId: "on",
                timePreparedFrom: '09/01/2015',
                timePreparedTo: '09/30/2015'
        ]

        and: "a list of fields the user is interested in exporting"
        def fields = subspecimenService.findSelectedFields(params)

        when: "generateCSVHeader() is called"
        def csvHeader = subspecimenService.generateCSVHeader(fields)

        then: "a comma separated string of headers is returned"
        csvHeader == 'Subspecimen ID,Invest. ID Prefix,Invest. ID\n'
    }

    void "test general search csv file header generation for fields that are always displayed together"(){
        given: "a map of search params"
        def params = [
                showWeight: "on",
                showVolume: "on",
                showSizeLength: "on",
                showSizeWidth: "on",
                showSizeHeight: "on",
                timePreparedFrom: '09/01/2015',
                timePreparedTo: '09/30/2015'
        ]

        and: "a list of fields the user is interested in exporting"
        def fields = subspecimenService.findSelectedFields(params)

        when: "generateCSVHeader() is called"
        def csvHeader = subspecimenService.generateCSVHeader(fields)

        then: "a comma separated string of headers is returned"
        csvHeader == 'Subspecimen ID,Weight,Weight Unit,Volume,Volume Unit,Length,Units,Width,Units,Height,Units\n'
    }

    void "test general search csv file content generation for fields that are always displayed together"() {
        given: "a map of search params"
        def params = [
                showInvestigatorChtnId: "on",
                showWeight: "on",
                showVolume: "on",
                showSizeLength: "on",
                showSizeWidth: "on",
                showSizeHeight: "on",
                showSubspecimenLabelComment: "on",
                timePreparedFrom: '0800',
                timePreparedTo: '1700'
        ]

        and: "a list of subspecimens as a search result"
        def subspecimens = subspecimenService.generalSearch(params)

        and: "a list of fields the user is interested in exporting"
        def fields = subspecimenService.findSelectedFields(params)

        when: "generateCSVContent() is called"
        def csvContent = subspecimenService.generateCSVContent(subspecimens[0], fields)

        then: "a comma separated string of subspecimen values is generated"
        csvContent == '1,A,1864,As the Texas senator gains ground over the billionaire businessman,0.25,grams,null,null,null,null,null,null,null,null\n'
    }

    void "test general search csv file content generation for date fields"() {
        given: "a map of search params"
        def params = [
                showTimePreparedDate: "on",
                showProcurementDate: "on",
                timePreparedFrom: '0800',
                timePreparedTo: '1700'
        ]

        and: "a list of subspecimens as a search result"
        def subspecimens = subspecimenService.generalSearch(params)

        and: "a list of fields the user is interested in exporting"
        def fields = subspecimenService.findSelectedFields(params)

        when: "generateCSVContent() is called"
        def csvContent = subspecimenService.generateCSVContent(subspecimens[0], fields)

        then: "a comma separated string of subspecimen values is generated"
        csvContent == '1,2015-04-03 00:00:00.0,1970-01-01 10:00:00.0\n'
    }
	
	void "test general search csv file content generation for Investigator Institution Type field"() {
		given: "a map of search params"
		def params = [
            showInvestigatorInstitutionType: "on",
            procurementDateFromTextField: "04/01/2016",
            procurementDateToTextField: "04/30/2016"
		]

		and: "a list of subspecimens as a search result"
		def subspecimens = subspecimenService.generalSearch(params)

		and: "a list of fields the user is interested in exporting"
		def fields = subspecimenService.findSelectedFields(params)

		when: "generateCSVContent() is called"
		def csvContent = subspecimenService.generateCSVContent(subspecimens[0], fields)

		then: "a comma separated string of subspecimen values is generated"
		csvContent == '1,Commercial\n'
	}
	
	void "test general search csv file content generation for Preliminary Primary Anatomic Site field"() {
		given: "a map of search params"
		def params = [
				showPreliminaryPrimaryAnatomicSite: "on"
		]

		and: "a list of subspecimens as a search result"
		def subspecimens = subspecimenService.generalSearch(params)

		and: "a list of fields the user is interested in exporting"
		def fields = subspecimenService.findSelectedFields(params)

		when: "generateCSVContent() is called"
		def csvContent = subspecimenService.generateCSVContent(subspecimens[0], fields)

		then: "a comma separated string of subspecimen values is generated"
		csvContent == '1,BREAST\n'
	}
}
