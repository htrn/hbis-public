package hbis

import grails.plugin.spock.IntegrationSpec

class QcBatchControllerIntegrationSpec extends IntegrationSpec {
    void "add multiple specimens to the qc batch"() {
        given: 'the qc batch controller'
        def qcBatchController = new QcBatchController()

        and: 'a qc batch with an id of 16'
        def qcBatch = QcBatch.get(16)

        and: 'an appropriate set of request parameters'
        qcBatchController.params.qcBatchId = '16'
        qcBatchController.params.qcBatchVersion = '3'
        qcBatchController.params.specimensToAdd = ['69', '39', '57']
        qcBatchController.params.addRemove = 'Add'

        expect: 'this batch to have been assigned a certain number of specimens'
        qcBatch.specimens.size() == 8

        when: 'addRemoveSpecimen() is called'
        qcBatchController.addRemoveSpecimen()
        qcBatch = QcBatch.get(16)

        then: 'the qc batch should have three additional specimens'
        qcBatch.specimens.size() == 11
    }

    void "add a single specimen to the qc batch"() {
        given: 'the qc batch controller'
        def qcBatchController = new QcBatchController()

        and: 'a qc batch with an id of 16'
        def qcBatch = QcBatch.get(16)

        and: 'an appropriate set of request parameters'
        qcBatchController.params.qcBatchId = '16'
        qcBatchController.params.qcBatchVersion = '3'
        qcBatchController.params.specimensToAdd = ['69']
        qcBatchController.params.addRemove = 'Add'

        expect: 'this batch to have been assigned a certain number of specimens'
        qcBatch.specimens.size() == 8

        when: 'addRemoveSpecimen() is called'
        qcBatchController.addRemoveSpecimen()
        qcBatch = QcBatch.get(16)

        then: 'the qc batch should contain one additional specimen'
        qcBatch.specimens.size() == 9
    }

    void "remove multiple specimens from a qc batch"() {
        given: 'the qc batch controller'
        def qcBatchController = new QcBatchController()

        and: 'a qc batch with an id of 16'
        def qcBatch = QcBatch.get(16)

        and: 'an appropriate set of request parameters'
        qcBatchController.params.qcBatchId = '16'
        qcBatchController.params.qcBatchVersion = '3'
        qcBatchController.params.specimensToRemove = ['59', '55']
        qcBatchController.params.addRemove = 'Remove'

        expect: 'this batch to have been assigned a certain number of specimens'
        qcBatch.specimens.size() == 8

        when: 'addRemoveSpecimen() is called'
        qcBatchController.addRemoveSpecimen()
        qcBatch = QcBatch.get(16)

        then: 'the qc batch should contain two fewer specimens'
        qcBatch.specimens.size() == 6
    }

    void "remove a single specimen from a qc batch"() {
        given: 'the qc batch controller'
        def qcBatchController = new QcBatchController()

        and: 'a qc batch with an id of 16'
        def qcBatch = QcBatch.get(16)

        and: 'an appropriate set of request parameters'
        qcBatchController.params.qcBatchId = '16'
        qcBatchController.params.qcBatchVersion = '3'
        qcBatchController.params.specimensToRemove = ['55']
        qcBatchController.params.addRemove = 'Remove'

        expect: 'this batch to have been assigned a certain number of specimens'
        qcBatch.specimens.size() == 8

        when: 'addRemoveSpecimen() is called'
        qcBatchController.addRemoveSpecimen()
        qcBatch = QcBatch.get(16)

        then: 'the qc batch should contain one fewer specimen'
        qcBatch.specimens.size() == 7
    }

    void "the search function should return qc batch objects with date created values between 05/01/2014 and 05/30/2014"() {
        given: 'the qc batch controller'
        def qcBatchController = new QcBatchController()

        and: 'an appropriate set of request parameters'
        qcBatchController.params.fromDate = '05/01/2014'
        qcBatchController.params.toDate = '05/31/2014'

        when: 'search() is called'
        def results = qcBatchController.search()

        then: 'the qc batch should contain one fewer specimen'
        results.qcBatchInstanceList.size() == 5

        def fromDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2014-05-01 00:00:00")
        def toDate = Date.parse("yyyy-MM-dd HH:mm:ss", "2014-05-31 23:59:59")

        // try and find a specimen that has a procurementDate outside the expected range
        def qcBatch = results.qcBatchInstanceList.find { qcBatch ->
            qcBatch.dateCreated < fromDate || qcBatch.dateCreated > toDate
        }

        qcBatch == null
    }
}
