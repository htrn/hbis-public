package hbis



import org.junit.*
import grails.test.mixin.*

@TestFor(ProtocolController)
@Mock(Protocol)
class ProtocolControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/protocol/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.protocolInstanceList.size() == 0
        assert model.protocolInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.protocolInstance != null
    }
    @Ignore("Fix the populateValidParams method")
    void testSave() {
        controller.save()

        assert model.protocolInstance != null
        assert view == '/protocol/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/protocol/show/1'
        assert controller.flash.message != null
        assert Protocol.count() == 1
    }
    @Ignore("Fix the populateValidParams method")
    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/protocol/list'

        populateValidParams(params)
        def protocol = new Protocol(params)

        assert protocol.save() != null

        params.id = protocol.id

        def model = controller.show()

        assert model.protocolInstance == protocol
    }
    @Ignore("Fix the populateValidParams method")
    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/protocol/list'

        populateValidParams(params)
        def protocol = new Protocol(params)

        assert protocol.save() != null

        params.id = protocol.id

        def model = controller.edit()

        assert model.protocolInstance == protocol
    }
    @Ignore("Fix the populateValidParams method")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/protocol/list'

        response.reset()

        populateValidParams(params)
        def protocol = new Protocol(params)

        assert protocol.save() != null

        // test invalid parameters in update
        params.id = protocol.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/protocol/edit"
        assert model.protocolInstance != null

        protocol.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/protocol/show/$protocol.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        protocol.clearErrors()

        populateValidParams(params)
        params.id = protocol.id
        params.version = -1
        controller.update()

        assert view == "/protocol/edit"
        assert model.protocolInstance != null
        assert model.protocolInstance.errors.getFieldError('version')
        assert flash.message != null
    }
    @Ignore("Fix the populateValidParams method")
    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/protocol/list'

        response.reset()

        populateValidParams(params)
        def protocol = new Protocol(params)

        assert protocol.save() != null
        assert Protocol.count() == 1

        params.id = protocol.id

        controller.delete()

        assert Protocol.count() == 0
        assert Protocol.get(protocol.id) == null
        assert response.redirectedUrl == '/protocol/list'
    }
}
