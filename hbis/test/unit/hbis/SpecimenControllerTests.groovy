package hbis



import org.junit.*
import grails.test.mixin.*

@TestFor(SpecimenController)
@Mock(Specimen)
class SpecimenControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/specimen/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.specimenInstanceList.size() == 0
        assert model.specimenInstanceTotal == 0
    }
    @Ignore("Fix the populateValidParams method")
    void testCreate() {
        def model = controller.create()

        assert model.specimenInstance != null
    }
    @Ignore("Fix the populateValidParams method")
    void testSave() {
        controller.save()

        assert model.specimenInstance != null
        assert view == '/specimen/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/specimen/show/1'
        assert controller.flash.message != null
        assert Specimen.count() == 1
    }
    @Ignore("Fix the populateValidParams method")
    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/specimen/list'

        populateValidParams(params)
        def specimen = new Specimen(params)

        assert specimen.save() != null

        params.id = specimen.id

        def model = controller.show()

        assert model.specimenInstance == specimen
    }
    @Ignore("Fix the populateValidParams method")
    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/specimen/list'

        populateValidParams(params)
        def specimen = new Specimen(params)

        assert specimen.save() != null

        params.id = specimen.id

        def model = controller.edit()

        assert model.specimenInstance == specimen
    }
    @Ignore("Fix the populateValidParams method")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/specimen/list'

        response.reset()

        populateValidParams(params)
        def specimen = new Specimen(params)

        assert specimen.save() != null

        // test invalid parameters in update
        params.id = specimen.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/specimen/edit"
        assert model.specimenInstance != null

        specimen.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/specimen/show/$specimen.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        specimen.clearErrors()

        populateValidParams(params)
        params.id = specimen.id
        params.version = -1
        controller.update()

        assert view == "/specimen/edit"
        assert model.specimenInstance != null
        assert model.specimenInstance.errors.getFieldError('version')
        assert flash.message != null
    }
    @Ignore("Fix the populateValidParams method")
    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/specimen/list'

        response.reset()

        populateValidParams(params)
        def specimen = new Specimen(params)

        assert specimen.save() != null
        assert Specimen.count() == 1

        params.id = specimen.id

        controller.delete()

        assert Specimen.count() == 0
        assert Specimen.get(specimen.id) == null
        assert response.redirectedUrl == '/specimen/list'
    }
}
