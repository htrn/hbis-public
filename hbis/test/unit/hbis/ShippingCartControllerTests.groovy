package hbis



import org.junit.*
import grails.test.mixin.*

@TestFor(ShippingCartController)
@Mock(ShippingCart)
class ShippingCartControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/shippingCart/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.shippingCartInstanceList.size() == 0
        assert model.shippingCartInstanceTotal == 0
    }
    @Ignore("Fix the populateValidParams method")
    void testCreate() {
        def model = controller.create()

        assert model.shippingCartInstance != null
    }
    @Ignore("Fix the populateValidParams method")
    void testSave() {
        controller.save()

        assert model.shippingCartInstance != null
        assert view == '/shippingCart/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/shippingCart/show/1'
        assert controller.flash.message != null
        assert ShippingCart.count() == 1
    }
    @Ignore("Fix the populateValidParams method")
    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/shippingCart/list'

        populateValidParams(params)
        def shippingCart = new ShippingCart(params)

        assert shippingCart.save() != null

        params.id = shippingCart.id

        def model = controller.show()

        assert model.shippingCartInstance == shippingCart
    }
    @Ignore("Fix the populateValidParams method")
    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/shippingCart/list'

        populateValidParams(params)
        def shippingCart = new ShippingCart(params)

        assert shippingCart.save() != null

        params.id = shippingCart.id

        def model = controller.edit()

        assert model.shippingCartInstance == shippingCart
    }
    @Ignore("Fix the populateValidParams method")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/shippingCart/list'

        response.reset()

        populateValidParams(params)
        def shippingCart = new ShippingCart(params)

        assert shippingCart.save() != null

        // test invalid parameters in update
        params.id = shippingCart.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/shippingCart/edit"
        assert model.shippingCartInstance != null

        shippingCart.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/shippingCart/show/$shippingCart.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        shippingCart.clearErrors()

        populateValidParams(params)
        params.id = shippingCart.id
        params.version = -1
        controller.update()

        assert view == "/shippingCart/edit"
        assert model.shippingCartInstance != null
        assert model.shippingCartInstance.errors.getFieldError('version')
        assert flash.message != null
    }
    @Ignore("Fix the populateValidParams method")
    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/shippingCart/list'

        response.reset()

        populateValidParams(params)
        def shippingCart = new ShippingCart(params)

        assert shippingCart.save() != null
        assert ShippingCart.count() == 1

        params.id = shippingCart.id

        controller.delete()

        assert ShippingCart.count() == 0
        assert ShippingCart.get(shippingCart.id) == null
        assert response.redirectedUrl == '/shippingCart/list'
    }
}
