package hbis

import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.codehaus.groovy.grails.plugins.jasper.JasperService
import org.junit.*

import grails.test.mixin.*

@TestFor(InvoiceController)
@Mock(Invoice)
class InvoiceControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    def populateHiddenParams(params) {
        assert params != null
        params["_format"] = "XLS"
        params["_file"] = "payments-received"
    }

    void testIndex() {
        controller.index()
        assert "/invoice/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.invoiceInstanceList.size() == 0
        assert model.invoiceInstanceTotal == 0
    }

    @Ignore("Missing method exception")
    void testCreate() {
        def model = controller.create()

        assert model.invoiceInstance != null
    }

    @Ignore("Failing test will need to figure out why ")
    void testSave() {
        controller.save()

        assert model.invoiceInstance != null
        assert view == '/invoice/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/invoice/show/1'
        assert controller.flash.message != null
        assert Invoice.count() == 1
    }

    @Ignore("Failing test will need to figure out why ")
    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/invoice/list'

        populateValidParams(params)
        def invoice = new Invoice(params)

        assert invoice.save() != null

        params.id = invoice.id

        def model = controller.show()

        assert model.invoiceInstance == invoice
    }

    @Ignore("Failing test will need to figure out why ")
    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/invoice/list'

        populateValidParams(params)
        def invoice = new Invoice(params)

        assert invoice.save() != null

        params.id = invoice.id

        def model = controller.edit()

        assert model.invoiceInstance == invoice
    }

    @Ignore("Failing test will need to figure out why ")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/invoice/list'

        response.reset()

        populateValidParams(params)
        def invoice = new Invoice(params)

        assert invoice.save() != null

        // test invalid parameters in update
        params.id = invoice.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/invoice/edit"
        assert model.invoiceInstance != null

        invoice.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/invoice/show/$invoice.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        invoice.clearErrors()

        populateValidParams(params)
        params.id = invoice.id
        params.version = -1
        controller.update()

        assert view == "/invoice/edit"
        assert model.invoiceInstance != null
        assert model.invoiceInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    @Ignore("Failing test will need to figure out why ")
    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/invoice/list'

        response.reset()

        populateValidParams(params)
        def invoice = new Invoice(params)

        assert invoice.save() != null
        assert Invoice.count() == 1

        params.id = invoice.id

        controller.delete()

        assert Invoice.count() == 0
        assert Invoice.get(invoice.id) == null
        assert response.redirectedUrl == '/invoice/list'
    }

    void testPaymentsReceivedReportWithoutParams(){

        populateHiddenParams(params)

        def mockInvoiceService = mockFor(InvoiceService)
        mockInvoiceService.demand.investigatorInvoiceQueryHelper(1..1){
            fromDate, toDate, investigatorId ->  [[amountPaid:4,totalPrice:4.5, investigator:[lastName : "lastName"], invoiceDate: "12/30/2015"]]
        }
        controller.invoiceService = mockInvoiceService.createMock()

        def mockJasperService = mockFor(JasperService)
        mockJasperService.demand.buildReportDefinition(1..1){
            parameters, locale, testModel ->
                    JasperReportDef reportDef = new JasperReportDef(name: parameters._file, parameters: parameters, locale: locale)
                    reportDef.fileFormat = JasperExportFormat.determineFileFormat(parameters._format)
                    reportDef.contentStream = new ByteArrayOutputStream()
                    return reportDef
        }
        controller.jasperService = mockJasperService.createMock()

        controller.paymentsReceivedReport()

        assert response.status == 200
        assert response.contentType == "application/vnd.ms-excel"
    }

    void testAccountsReceivablePageDisplayZeroBalanceDue(){

        params["institution"] = "not null";

        Invoice.metaClass.static.createCriteria = {[
                list : {Closure  cls -> [[getTotalPrice : {0.00}],[getTotalPrice : {1.00}],[getTotalPrice : {0.00}]]}
        ]}

        def model = controller.accountsReceivable()

        assert model.invoiceInstanceList.size() == 1
    }
}
