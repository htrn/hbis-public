package hbis



import org.junit.*
import grails.test.mixin.*

@TestFor(TissueRequestController)
@Mock(TissueRequest)
class TissueRequestControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/tissueRequest/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.tissueRequestInstanceList.size() == 0
        assert model.tissueRequestInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.tissueRequestInstance != null
    }
    @Ignore("Fix the populateValidParams method")
    void testSave() {
        controller.save()

        assert model.tissueRequestInstance != null
        assert view == '/tissueRequest/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/tissueRequest/show/1'
        assert controller.flash.message != null
        assert TissueRequest.count() == 1
    }
    @Ignore("Fix the populateValidParams method")
    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/tissueRequest/list'

        populateValidParams(params)
        def tissueRequest = new TissueRequest(params)

        assert tissueRequest.save() != null

        params.id = tissueRequest.id

        def model = controller.show()

        assert model.tissueRequestInstance == tissueRequest
    }
    @Ignore("Fix the populateValidParams method")
    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/tissueRequest/list'

        populateValidParams(params)
        def tissueRequest = new TissueRequest(params)

        assert tissueRequest.save() != null

        params.id = tissueRequest.id

        def model = controller.edit()

        assert model.tissueRequestInstance == tissueRequest
    }
    @Ignore("Fix the populateValidParams method")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/tissueRequest/list'

        response.reset()

        populateValidParams(params)
        def tissueRequest = new TissueRequest(params)

        assert tissueRequest.save() != null

        // test invalid parameters in update
        params.id = tissueRequest.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/tissueRequest/edit"
        assert model.tissueRequestInstance != null

        tissueRequest.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/tissueRequest/show/$tissueRequest.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        tissueRequest.clearErrors()

        populateValidParams(params)
        params.id = tissueRequest.id
        params.version = -1
        controller.update()

        assert view == "/tissueRequest/edit"
        assert model.tissueRequestInstance != null
        assert model.tissueRequestInstance.errors.getFieldError('version')
        assert flash.message != null
    }
    @Ignore("Fix the populateValidParams method")
    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/tissueRequest/list'

        response.reset()

        populateValidParams(params)
        def tissueRequest = new TissueRequest(params)

        assert tissueRequest.save() != null
        assert TissueRequest.count() == 1

        params.id = tissueRequest.id

        controller.delete()

        assert TissueRequest.count() == 0
        assert TissueRequest.get(tissueRequest.id) == null
        assert response.redirectedUrl == '/tissueRequest/list'
    }
}
