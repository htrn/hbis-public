package hbis



import org.junit.*
import grails.test.mixin.*

@TestFor(MedicationController)
@Mock(Medication)
class MedicationControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/medication/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.medicationInstanceList.size() == 0
        assert model.medicationInstanceTotal == 0
    }
    @Ignore("Failing test will need to figure out why ")
    void testCreate() {
        def model = controller.create()

        assert model.medicationInstance != null
    }
    @Ignore("Failing test will need to figure out why ")
    void testSave() {
        controller.save()

        assert model.medicationInstance != null
        assert view == '/medication/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/medication/show/1'
        assert controller.flash.message != null
        assert Medication.count() == 1
    }
    @Ignore("Failing test will need to figure out why ")
    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/medication/list'

        populateValidParams(params)
        def medication = new Medication(params)

        assert medication.save() != null

        params.id = medication.id

        def model = controller.show()

        assert model.medicationInstance == medication
    }
    @Ignore("Failing test will need to figure out why ")
    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/medication/list'

        populateValidParams(params)
        def medication = new Medication(params)

        assert medication.save() != null

        params.id = medication.id

        def model = controller.edit()

        assert model.medicationInstance == medication
    }
    @Ignore("Failing test will need to figure out why ")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/medication/list'

        response.reset()

        populateValidParams(params)
        def medication = new Medication(params)

        assert medication.save() != null

        // test invalid parameters in update
        params.id = medication.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/medication/edit"
        assert model.medicationInstance != null

        medication.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/medication/show/$medication.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        medication.clearErrors()

        populateValidParams(params)
        params.id = medication.id
        params.version = -1
        controller.update()

        assert view == "/medication/edit"
        assert model.medicationInstance != null
        assert model.medicationInstance.errors.getFieldError('version')
        assert flash.message != null
    }
    @Ignore("Failing test will need to figure out why ")
    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/medication/list'

        response.reset()

        populateValidParams(params)
        def medication = new Medication(params)

        assert medication.save() != null
        assert Medication.count() == 1

        params.id = medication.id

        controller.delete()

        assert Medication.count() == 0
        assert Medication.get(medication.id) == null
        assert response.redirectedUrl == '/medication/list'
    }
}
