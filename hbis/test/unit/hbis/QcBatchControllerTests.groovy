package hbis



import org.junit.*
import grails.test.mixin.*

@TestFor(QcBatchController)
@Mock(QcBatch)
class QcBatchControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/qcBatch/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.qcBatchInstanceList.size() == 0
        assert model.qcBatchInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.qcBatchInstance != null
    }
    @Ignore("Fix the populateValidParams method")
    void testSave() {
        controller.save()

        assert model.qcBatchInstance != null
        assert view == '/qcBatch/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/qcBatch/show/1'
        assert controller.flash.message != null
        assert QcBatch.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/qcBatch/list'

        populateValidParams(params)
        def qcBatch = new QcBatch(params)

        assert qcBatch.save() != null

        params.id = qcBatch.id

        def model = controller.show()

        assert model.qcBatchInstance == qcBatch
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/qcBatch/list'

        populateValidParams(params)
        def qcBatch = new QcBatch(params)

        assert qcBatch.save() != null

        params.id = qcBatch.id

        def model = controller.edit()

        assert model.qcBatchInstance == qcBatch
    }
    @Ignore("Fix the populateValidParams method")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/qcBatch/list'

        response.reset()

        populateValidParams(params)
        def qcBatch = new QcBatch(params)

        assert qcBatch.save() != null

        // test invalid parameters in update
        params.id = qcBatch.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/qcBatch/edit"
        assert model.qcBatchInstance != null

        qcBatch.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/qcBatch/show/$qcBatch.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        qcBatch.clearErrors()

        populateValidParams(params)
        params.id = qcBatch.id
        params.version = -1
        controller.update()

        assert view == "/qcBatch/edit"
        assert model.qcBatchInstance != null
        assert model.qcBatchInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/qcBatch/list'

        response.reset()

        populateValidParams(params)
        def qcBatch = new QcBatch(params)

        assert qcBatch.save() != null
        assert QcBatch.count() == 1

        params.id = qcBatch.id

        controller.delete()

        assert QcBatch.count() == 0
        assert QcBatch.get(qcBatch.id) == null
        assert response.redirectedUrl == '/qcBatch/list'
    }
}
