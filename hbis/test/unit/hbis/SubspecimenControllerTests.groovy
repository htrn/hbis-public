package hbis



import org.junit.*
import grails.test.mixin.*

@TestFor(SubspecimenController)
@Mock(Subspecimen)
class SubspecimenControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/subspecimen/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.subspecimenInstanceList.size() == 0
        assert model.subspecimenInstanceTotal == 0
    }
    @Ignore("Fix the populateValidParams method")
    void testCreate() {
        def model = controller.create()

        assert model.subspecimenInstance != null
    }
    @Ignore("Fix the populateValidParams method")
    void testSave() {
        controller.save()

        assert model.subspecimenInstance != null
        assert view == '/subspecimen/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/subspecimen/show/1'
        assert controller.flash.message != null
        assert Subspecimen.count() == 1
    }
    @Ignore("Fix the populateValidParams method")
    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/subspecimen/list'

        populateValidParams(params)
        def subspecimen = new Subspecimen(params)

        assert subspecimen.save() != null

        params.id = subspecimen.id

        def model = controller.show()

        assert model.subspecimenInstance == subspecimen
    }
    @Ignore("Fix the populateValidParams method")
    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/subspecimen/list'

        populateValidParams(params)
        def subspecimen = new Subspecimen(params)

        assert subspecimen.save() != null

        params.id = subspecimen.id

        def model = controller.edit()

        assert model.subspecimenInstance == subspecimen
    }
    @Ignore("Fix the populateValidParams method")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/subspecimen/list'

        response.reset()

        populateValidParams(params)
        def subspecimen = new Subspecimen(params)

        assert subspecimen.save() != null

        // test invalid parameters in update
        params.id = subspecimen.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/subspecimen/edit"
        assert model.subspecimenInstance != null

        subspecimen.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/subspecimen/show/$subspecimen.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        subspecimen.clearErrors()

        populateValidParams(params)
        params.id = subspecimen.id
        params.version = -1
        controller.update()

        assert view == "/subspecimen/edit"
        assert model.subspecimenInstance != null
        assert model.subspecimenInstance.errors.getFieldError('version')
        assert flash.message != null
    }
    @Ignore("Fix the populateValidParams method")
    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/subspecimen/list'

        response.reset()

        populateValidParams(params)
        def subspecimen = new Subspecimen(params)

        assert subspecimen.save() != null
        assert Subspecimen.count() == 1

        params.id = subspecimen.id

        controller.delete()

        assert Subspecimen.count() == 0
        assert Subspecimen.get(subspecimen.id) == null
        assert response.redirectedUrl == '/subspecimen/list'
    }
}
