package hbis



import org.junit.*
import grails.test.mixin.*

@TestFor(SurgicalHistoryController)
@Mock(SurgicalHistory)
class SurgicalHistoryControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/surgicalHistory/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.surgicalHistoryInstanceList.size() == 0
        assert model.surgicalHistoryInstanceTotal == 0
    }
    @Ignore("Fix the populateValidParams method")
    void testCreate() {
        def model = controller.create()

        assert model.surgicalHistoryInstance != null
    }
    @Ignore("Fix the populateValidParams method")
    void testSave() {
        controller.save()

        assert model.surgicalHistoryInstance != null
        assert view == '/surgicalHistory/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/surgicalHistory/show/1'
        assert controller.flash.message != null
        assert SurgicalHistory.count() == 1
    }
    @Ignore("Fix the populateValidParams method")
    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/surgicalHistory/list'

        populateValidParams(params)
        def surgicalHistory = new SurgicalHistory(params)

        assert surgicalHistory.save() != null

        params.id = surgicalHistory.id

        def model = controller.show()

        assert model.surgicalHistoryInstance == surgicalHistory
    }
    @Ignore("Fix the populateValidParams method")
    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/surgicalHistory/list'

        populateValidParams(params)
        def surgicalHistory = new SurgicalHistory(params)

        assert surgicalHistory.save() != null

        params.id = surgicalHistory.id

        def model = controller.edit()

        assert model.surgicalHistoryInstance == surgicalHistory
    }
    @Ignore("Fix the populateValidParams method")
    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/surgicalHistory/list'

        response.reset()

        populateValidParams(params)
        def surgicalHistory = new SurgicalHistory(params)

        assert surgicalHistory.save() != null

        // test invalid parameters in update
        params.id = surgicalHistory.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/surgicalHistory/edit"
        assert model.surgicalHistoryInstance != null

        surgicalHistory.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/surgicalHistory/show/$surgicalHistory.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        surgicalHistory.clearErrors()

        populateValidParams(params)
        params.id = surgicalHistory.id
        params.version = -1
        controller.update()

        assert view == "/surgicalHistory/edit"
        assert model.surgicalHistoryInstance != null
        assert model.surgicalHistoryInstance.errors.getFieldError('version')
        assert flash.message != null
    }
    @Ignore("Fix the populateValidParams method")
    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/surgicalHistory/list'

        response.reset()

        populateValidParams(params)
        def surgicalHistory = new SurgicalHistory(params)

        assert surgicalHistory.save() != null
        assert SurgicalHistory.count() == 1

        params.id = surgicalHistory.id

        controller.delete()

        assert SurgicalHistory.count() == 0
        assert SurgicalHistory.get(surgicalHistory.id) == null
        assert response.redirectedUrl == '/surgicalHistory/list'
    }
}
